import os
from pymel.core import *

class Constants:
    animSceneDir = "m:/Trio/Animation/episodes/ep%02d/proc"
    animSceneNamePattern = "animation_ep%02d"
    #blackFrameSource = "m:/Zaryadka2/Source/ep00_render_scene_00000.png"
    
    #out
    logFolder = "m:/Trio/Out"
    logFile = "trio_ren_range_setter.log"

    renRangeDir = "m:/Trio/Rendering/episodes/ep%02d"
    persRenRangeFileName = "ep%02d_ren_range_pers.txt"
    locRenRangeFileName = "ep%02d_ren_range_loc.txt"

    #nodes
    cameraNodeName = "cam_scheme:RenderCam_conastr_grp"

class TrioRenRangeException(Exception):
    pass

def MyNormpath(path):

    if not path or path == "":
        return ""

    res = path.replace("\\", "/")

    while (res.find("//") > -1):
        res = res.replace("//", "/")

    if res[-1] == "/":
        res = res[:len(res) - 1]

    return res

def PrintLog(message):
    with open(Constants.logFolder + "/" + Constants.logFile, 'a') as logFile:
        logFile.write("\n" + message + "\n----------------------------------------------------\n")

def RunButtonCallback():
    global epStartTextField
    global epEndTextField

    try:
        epStartNum = -1
        epEndNum = -1
        try:
            epStartNum = int(epStartTextField.getText())
            epEndNum = int(epEndTextField.getText())
        except ValueError as ex2:
            raise TrioRenRangeException("Couldn't parse episode range field: " \
                                        + type(ex2).__name__ + ": " + str(ex2))

        if epStartNum > epEndNum:
            raise TrioRenRangeException("epStartNum > epEndNum")

        try:
            if not os.path.exists(Constants.logFolder):
                os.makedirs(Constants.logFolder)
            f = open(Constants.logFolder + "/" + Constants.logFile, 'w')
            f.close()
        except Exception as ex2:
            warning("Couldn't create log file at path: " + Constants.logFolder + "/" + Constants.logFile\
                + "\n" + type(ex2).__name__ + ": message: " + str(ex2))
            warning("failed to create log file")

        for epNum in range(epStartNum, epEndNum + 1):
            try:
                # check if dir exists
                if not os.path.isdir(Constants.animSceneDir % epNum):
                    raise TrioRenRangeException("Dir with animation scene file doesn't exists: " + (Constants.animSceneDir % epNum))

                try:
                    oldVersions = [x.split('.')[-2].split('v')[-1] for x in os.listdir(Constants.animSceneDir % epNum) if x.find('.') != -1 and x.find(Constants.animSceneNamePattern % epNum) != -1]
                except Exception as tempEx1:
                    raise TrioRenRangeException("Encountered an exception while listing dir: " + (Constants.animSceneDir % epNum) + "    Error message: " + tempEx1)
                oldVersionsInt = []
                try:
                    for x in oldVersions:
                        oldVersionsInt.append(int(x))
                except ValueError:
                    pass
                if len(oldVersionsInt) < 1:
                    TrioRenRangeException("Couldn't find any scenes to open at path: " + (Constants.animSceneDir % epNum) + "    Name pattern: " + (Constants.animSceneNamePattern % epNum))

                #open scene
                res = mel.eval('file -f -options "v=0;" -pmt 0 -ignoreVersion  -typ "mayaAscii" -o "%s";' % ((MyNormpath(Constants.animSceneDir % epNum) + "/" + (Constants.animSceneNamePattern % epNum) + ("_v%02d.ma" % max(oldVersionsInt)))))
                if MyNormpath(os.path.dirname(MyNormpath(str(res)))) != MyNormpath(Constants.animSceneDir % epNum):
                    PrintLog("ep: " + str(epNum) + ": dirname(res) != Constants.animSceneDir, dirname(res): " + MyNormpath(os.path.dirname(MyNormpath(str(res)))))

                try:
                    select(Constants.cameraNodeName, r=True)
                except Exception as tempEx1:
                    raise TrioRenRangeException("Couldn't select camera node: %s. Error message: %s" % (Constants.cameraNodeName, str(tempEx1)))

                playbackMin = playbackOptions(min=True, q=True)
                playbackMax = playbackOptions(max=True, q=True)

                #loc
                motionStart = playbackMin - 2
                motionEnd = motionStart
                locCamShouldBeProcessed = False
                isMotionSaved = True
                locRenRanges = []
                #pers
                persStart = playbackMin - 2
                persEnd = persStart
                isPersSaved = True

                prevCam = -1
                persRenRanges = []
                curTime = playbackMin
                maxTime = playbackMax + 1
                while curTime < maxTime:
                    curCam = getAttr(Constants.cameraNodeName + ".CamNum", t=curTime)
                    isMotionOn = bool(getAttr(Constants.cameraNodeName + ".motion", t=curTime))
                    if prevCam != curCam:
                        locCamShouldBeProcessed = True
                    tempFlag = curCam == 8 and isMotionOn
                    if curCam != 0 and (tempFlag or locCamShouldBeProcessed):
                        if not tempFlag:
                            locCamShouldBeProcessed = False
                        if motionEnd + 1 == curTime:
                            motionEnd = curTime
                        else:
                            isMotionSaved = False
                            motionStart = curTime
                            motionEnd = motionStart
                    elif not isMotionSaved:
                        locRenRanges.append([int(motionStart), int(motionEnd)])
                        isMotionSaved = True

                    if curCam != 0:
                        if persEnd + 1 == curTime:
                            persEnd = curTime
                        else:
                            isPersSaved = False
                            persStart = curTime
                            persEnd = persStart
                    elif not isPersSaved:
                        persRenRanges.append([int(persStart), int(persEnd)])
                        isPersSaved = True

                    if prevCam != curCam:
                        prevCam = curCam
                    curTime += 1

                if not isMotionSaved:
                    locRenRanges.append([int(motionStart), int(motionEnd)])
                if not isPersSaved:
                    persRenRanges.append([int(persStart), int(persEnd)])
                
                if len(persRenRanges) < 1:
                    PrintLog("ERROR: ep: %02d. Didn't write any 'Character' frames for render." % epNum)
                if len(locRenRanges) < 1:
                    PrintLog("ERROR: ep: %02d. Didn't write any 'Location' frames for render." % epNum)

                if not os.path.exists(Constants.renRangeDir % epNum):
                    os.makedirs(Constants.renRangeDir % epNum)

                with open((Constants.renRangeDir % epNum) + "/" + (Constants.locRenRangeFileName % epNum), 'w') as tempFile:
                    if len(persRenRanges) < 1:
                        persRenRanges.append([int(playbackMin), int(playbackMax)])
                    for x in locRenRanges:
                        tempFile.write(str(x[0]) + " " + str(x[1]) + "\n" if x[0] != x[1] else str(x[0]) + "\n")
                with open((Constants.renRangeDir % epNum) + "/" + (Constants.persRenRangeFileName % epNum), 'w') as tempFile:
                    if len(locRenRanges) < 1:
                        locRenRanges.append([int(playbackMin), int(playbackMax)])
                    for x in persRenRanges:
                        tempFile.write(str(x[0]) + " " + str(x[1]) + "\n" if x[0] != x[1] else str(x[0]) + "\n")

            except Exception as ex2:
                PrintLog("\nException: " + type(ex2).__name__ + ": Episode: " + str(epNum)\
                    + "\nmessage: " + str(ex2) + "\n")
                    
    except TrioRenRangeException as ex:
        PrintLog("\nException: " + type(ex).__name__\
                    + "\nmessage: " + str(ex) + "\n")

epStartTextField = None
epEndTextField = None

win = window(title="Trio render range setter", width=300, height=140)

with columnLayout(adjustableColumn=True, rowSpacing=50):
    with horizontalLayout():
        text('start', height=20)
        epStartTextField = textField(edit=False, tx=str('1'))

        text('end', height=20)
        epEndTextField = textField(edit=False, tx=str('2'))

    runButton = button(label='run')
    button(runButton, e=True, c=windows.Callback(RunButtonCallback))

win.show()