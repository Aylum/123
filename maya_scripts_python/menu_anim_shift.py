from pymel.core import *

class MyButton():

    def __init__(self, btn = None):
        self.btn = btn
        self.btnStatus = False

    def ToggleState(self):
        if not self.btn:
            return

        self.btnStatus = not self.btnStatus

        if self.btnStatus:
            button(self.btn, e=True, bgc=[0, 0.3, 0])
        else:
            button(self.btn, e=True, bgc=[0.361, 0.361, 0.361])

    def ToggleState2(self, defaultLabel='None', pressedLabel='None'):
        if not self.btn:
            return

        self.btnStatus = not self.btnStatus

        if self.btnStatus:
            button(self.btn, e=True, bgc=[0.156, 0.47, 0.3], label=defaultLabel)
        else:
            button(self.btn, e=True, bgc=[0.156, 0.26, 0.47], label=pressedLabel)

    def ToDefaultState(self):
        if not self.btn:
            return

        self.btnStatus = False
        button(self.btn, e=True, bgc=[0.361, 0.361, 0.361])

class MyLayout():

    def __init__(self, charGroup = None, bodyGroup = None):
        self.charGroup = charGroup
        self.bodyGroup = bodyGroup

        self.lShiftButton = None
        self.rShiftButton = None

        self.clSelectionBtn = None

        self.charBaranBtnStatus = False
        self.charStrausBtnStatus = False
        self.bodyFaceBtnStatus = False
        self.bodyBodyBtnStatus = False

        self.selectionModeBtn = None

        self.lShiftVal = 0
        self.rShiftVal = 0

        MyLayout.faceBaranSetName = 'BaranFaceSet'
        MyLayout.faceStrausSetName = 'StrausFaceSet'
        MyLayout.bodyBaranSetName = 'BaranBodySet'
        MyLayout.bodyStrausSetName = 'StrausBodySet'

    def SetSelectionModeBtn(self, btn):
        if btn:
            self.selectionModeBtn = MyButton(btn=btn)
    
    def GetSelectionModeBtn(self):
        return self.selectionModeBtn.btn

    def SelectionModeBtnCallback(self):
        
        self.selectionModeBtn.ToggleState2('select animCurves: True', 'select transforms: True')

        select(cl=True)

        if self.charBaranBtnStatus:
            if self.bodyFaceBtnStatus:
                self.__select_baran_face__()
            
            if self.bodyBodyBtnStatus:
                self.__select_baran_body__()

        if self.charStrausBtnStatus:
            if self.bodyFaceBtnStatus:
                self.__select_straus_face__()
            if self.bodyBodyBtnStatus:
                self.__select_straus_body__()

    def BaranCallback(self):
        if not self.charGroup:
            return

        self.charGroup.BaranCallback()

        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()
        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()
        self.bodyFaceBtnStatus = self.bodyGroup.FaceBtnStatus()
        self.bodyBodyBtnStatus = self.bodyGroup.BodyBtnStatus()

        self.__buttonController__('CHAR_BARAN')

    def StrausCallback(self):
        if not self.charGroup:
            return

        self.charGroup.StrausCallback()

        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()
        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()
        self.bodyFaceBtnStatus = self.bodyGroup.FaceBtnStatus()
        self.bodyBodyBtnStatus = self.bodyGroup.BodyBtnStatus()

        self.__buttonController__('CHAR_STRAUS')

    def CharAllCallback(self):
        if not self.charGroup:
            return
        
        self.charGroup.AllCallback()

        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()
        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()
        self.bodyFaceBtnStatus = self.bodyGroup.FaceBtnStatus()
        self.bodyBodyBtnStatus = self.bodyGroup.BodyBtnStatus()

        self.__buttonController__('CHAR_ALL')

    def FaceCallback(self):
        if not self.bodyGroup or not self.charGroup:
            return

        self.bodyGroup.FaceCallback()

        self.bodyFaceBtnStatus = self.bodyGroup.FaceBtnStatus()
        self.bodyBodyBtnStatus = self.bodyGroup.BodyBtnStatus()
        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()
        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()

        self.__buttonController__('BODY_FACE')

    def BodyCallback(self):
        if not self.bodyGroup or not self.charGroup:
            return

        self.bodyGroup.BodyCallback()

        self.bodyFaceBtnStatus = self.bodyGroup.FaceBtnStatus()
        self.bodyBodyBtnStatus = self.bodyGroup.BodyBtnStatus()
        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()
        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()

        self.__buttonController__('BODY_BODY')

    def BodyAllCallback(self):
        if not self.bodyGroup or not self.charGroup:
            return

        self.bodyGroup.AllCallback()

        self.bodyFaceBtnStatus = self.bodyGroup.FaceBtnStatus()
        self.bodyBodyBtnStatus = self.bodyGroup.BodyBtnStatus()
        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()
        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()

        self.__buttonController__('BODY_ALL')

    def ClearSelectionCallback(self):
        if not self.clSelectionBtn:
            return
 
        self.bodyFaceBtnStatus = False
        self.bodyBodyBtnStatus = False
        self.charBaranBtnStatus = False
        self.charStrausBtnStatus = False

        self.__buttonController__('CLEAR')

    def __buttonController__(self, buttonType):

        if buttonType == 'CHAR_BARAN':
            
            if not self.charGroup:
                return

            if self.charBaranBtnStatus:

                if self.bodyFaceBtnStatus:
                    self.__select_baran_face__()
                if self.bodyBodyBtnStatus:
                    self.__select_baran_body__()
            else:
                if self.bodyFaceBtnStatus:
                    self.__unselect_baran_face__()
                if self.bodyBodyBtnStatus:
                    self.__unselect_baran_body__()

                if not self.charStrausBtnStatus and self.bodyGroup:
                    self.bodyGroup.FaceButtonToDefaultState()
                    self.bodyGroup.BodyButtonToDefaultState()
                    self.bodyGroup.AllButtonToDefaultState()

                self.charGroup.AllBtnToDefaultState()
            
        elif buttonType == 'CHAR_STRAUS':
            
            if not self.charGroup:
                return

            if self.charStrausBtnStatus:

                if self.bodyFaceBtnStatus:
                    self.__select_straus_face__()
                if self.bodyBodyBtnStatus:
                    self.__select_straus_body__()

            else:
                if self.bodyFaceBtnStatus:
                    self.__unselect_straus_face__()
                if self.bodyBodyBtnStatus:
                    self.__unselect_straus_body__()

                if not self.charBaranBtnStatus and self.bodyGroup:
                    self.bodyGroup.FaceButtonToDefaultState()
                    self.bodyGroup.BodyButtonToDefaultState()
                    self.bodyGroup.AllButtonToDefaultState()

                self.charGroup.AllBtnToDefaultState()

        elif buttonType == 'CHAR_ALL':
            
            if not self.charGroup:
                return

            if self.charGroup.AllBtnStatus():

                if self.charBaranBtnStatus and self.charStrausBtnStatus:
                    return

                if self.bodyFaceBtnStatus:
                    self.__select_straus_face__()
                    self.__select_baran_face__()
                if self.bodyBodyBtnStatus:
                    self.__select_baran_body__()
                    self.__select_straus_body__()

            else:
                if self.bodyFaceBtnStatus:
                    self.__unselect_baran_face__()
                    self.__unselect_straus_face__()
                if self.bodyBodyBtnStatus:
                    self.__unselect_baran_body__()
                    self.__unselect_straus_body__()

                self.charGroup.BaranBtnToDefaultState()
                self.charGroup.StrausBtnToDefaultState()
                self.charGroup.AllBtnToDefaultState()

                if self.bodyGroup:
                    self.bodyGroup.FaceButtonToDefaultState()
                    self.bodyGroup.BodyButtonToDefaultState()
                    self.bodyGroup.AllButtonToDefaultState()

        elif buttonType == 'BODY_FACE':

            if not self.bodyGroup:
                return

            if self.bodyFaceBtnStatus:
                if self.charBaranBtnStatus:
                    self.__select_baran_face__()
                if self.charStrausBtnStatus:
                    self.__select_straus_face__()
            else:
                if self.charBaranBtnStatus:
                    self.__unselect_baran_face__()
                if self.charStrausBtnStatus:
                    self.__unselect_straus_face__()

                self.bodyGroup.AllButtonToDefaultState()

        elif buttonType == 'BODY_BODY':
            
            if not self.bodyGroup:
                return

            if self.bodyBodyBtnStatus:
                if self.charBaranBtnStatus:
                    self.__select_baran_body__()
                if self.charStrausBtnStatus:
                    self.__select_straus_body__()
            else:
                if self.charBaranBtnStatus:
                    self.__unselect_baran_body__()
                if self.charStrausBtnStatus:
                    self.__unselect_straus_body__()

                self.bodyGroup.AllButtonToDefaultState()

        elif buttonType == 'BODY_ALL':

            if not self.bodyGroup:
                return

            if self.bodyGroup.AllBtnStatus():

                if self.bodyBodyBtnStatus and self.bodyFaceBtnStatus:
                    return

                if self.charBaranBtnStatus():
                    self.__select_baran_face__()
                    self.__select_baran_body__()
                if self.charStrausBtnStatus():
                    self.__select_straus_face__()
                    self.__select_straus_body__()
            else:
                if self.charBaranBtnStatus:
                    self.__unselect_baran_face__()
                    self.__unselect_baran_body__()
                if self.charStrausBtnStatus:
                    self.__unselect_straus_face__()
                    self.__unselect_straus_body__()

                self.bodyGroup.FaceButtonToDefaultState()
                self.bodyGroup.BodyButtonToDefaultState()
                self.bodyGroup.AllButtonToDefaultState()

        elif buttonType == 'CLEAR':
            #mel.eval('selectKey -clear ;')
            cmds.select(cl=True)

            if self.charGroup:
                self.charGroup.BaranBtnToDefaultState()
                self.charGroup.StrausBtnToDefaultState()
                self.charGroup.AllBtnToDefaultState()

            if self.bodyGroup:
                self.bodyGroup.FaceButtonToDefaultState()
                self.bodyGroup.BodyButtonToDefaultState()
                self.bodyGroup.AllButtonToDefaultState()

    def LelftShiftCallback(self, txFieldObj):

        try:
            self.lShiftVal = int(txFieldObj.getText())
            cmds.keyframe(an='objects', o='over', r=True, tc = -self.lShiftVal)
            #mel.eval('keyframe -animation keys -option over -relative -timeChange (0 - %d) ;' % self.lShiftVal)
        except ValueError:
            self.lShiftVal = 0
            txFieldObj.setText('0')

    def RightShiftCallback(self, txFieldObj):

        try:
            self.rShiftVal = int(txFieldObj.getText())
            cmds.keyframe(an='objects', o='over', r=True, tc = self.rShiftVal)
            #mel.eval('keyframe -animation keys -option over -relative -timeChange (0 + %d) ;' % self.rShiftVal)
        except ValueError:
            self.rShiftVal = 0
            txFieldObj.setText('0')

    def __select_baran_face__(self):

        baranSet = ["Besha_face_anim:head_geo_0_1_2",\
            "Besha_face_anim:head_geo_3_4_5",\
            "Besha_face_anim:head_geo_6_7_8",\
            "Besha_face_anim:head_geo_9_10_11",\
            "Besha_face_anim:head_geo_12_13_14",\
            "Besha_face_anim:head_geo_15_last_last",\
            "Besha_face_anim:nose_geo_0_1_2",\
            "Besha_face_anim:nose_geo_3_4_5",\
            "Besha_face_anim:nose_geo_6_7_8",\
            "Besha_face_anim:nose_geo_9_10_11",\
            "Besha_face_anim:polySurface108_0_1_2",\
            "Besha_face_anim:polySurface108_3_4_last",\
            "Besha_face_anim:pTorus14_0_1_2"\
        ]

        #baranSet = sets(MyLayout.faceBaranSetName, q=True)

        if not baranSet:
            raise RuntimeError('Can\'t find set with name: \'' + self.faceBaranSetName + '\' or the set is empty')

        if self.selectionModeBtn and self.selectionModeBtn.btnStatus:
            for setNode in baranSet:
                curves = listConnections(str(setNode), type='animCurve')

                if not curves:
                    continue
            
                for curve in curves:
                    select(str(curve), add=True)

            return
        select(baranSet, add=True)

    def __unselect_baran_face__(self):
        
        baranSet = ["Besha_face_anim:head_geo_0_1_2",\
            "Besha_face_anim:head_geo_3_4_5",\
            "Besha_face_anim:head_geo_6_7_8",\
            "Besha_face_anim:head_geo_9_10_11",\
            "Besha_face_anim:head_geo_12_13_14",\
            "Besha_face_anim:head_geo_15_last_last",\
            "Besha_face_anim:nose_geo_0_1_2",\
            "Besha_face_anim:nose_geo_3_4_5",\
            "Besha_face_anim:nose_geo_6_7_8",\
            "Besha_face_anim:nose_geo_9_10_11",\
            "Besha_face_anim:polySurface108_0_1_2",\
            "Besha_face_anim:polySurface108_3_4_last",\
            "Besha_face_anim:pTorus14_0_1_2"\
        ]

        #baranSet = sets(MyLayout.faceBaranSetName, q=True)

        if not baranSet:
            raise RuntimeError('Can\'t find set with name: \'' + self.faceBaranSetName + '\' or the set is empty')

        if self.selectionModeBtn and self.selectionModeBtn.btnStatus:
            for setNode in baranSet:
                curves = listConnections(str(setNode), type='animCurve')

                if not curves:
                    continue
            
                for curve in curves:
                    select(str(curve), d=True)

            return
        select(baranSet, d=True)
    
    def __select_baran_body__(self):
        
        baranSet = ["Besha_trackers_anim:tracker_head",\
            "Besha_trackers_anim:tracker_hip",\
            "Besha_trackers_anim:tracker_r_leg",\
            "Besha_trackers_anim:tracker_l_leg",\
            "Besha_trackers_anim:tracker_l_hand",\
            "Besha_trackers_anim:tracker_r_hand",\
            "Besha_trackers_anim:tracker_l_foot",\
            "Besha_trackers_anim:tracker_r_foot" ,\
            "Besha_trackers_anim:tracker_l_arm",\
            "Besha_trackers_anim:tracker_r_arm",\
            "Besha_trackers_anim:tracker_spine"\
        ]

        #baranSet = sets(MyLayout.bodyBaranSetName, q=True)

        if not baranSet:
            raise RuntimeError('Can\'t find set with name: \'' + self.bodyBaranSetName + '\' or the set is empty')

        if self.selectionModeBtn and self.selectionModeBtn.btnStatus:
            for setNode in baranSet:
                curves = listConnections(str(setNode), type='animCurve')

                if not curves:
                    continue
                
                for curve in curves:
                    select(str(curve), add=True)

            return
        select(baranSet, add=True)

    def __unselect_baran_body__(self):
        
        baranSet = ["Besha_trackers_anim:tracker_head",\
            "Besha_trackers_anim:tracker_hip",\
            "Besha_trackers_anim:tracker_r_leg",\
            "Besha_trackers_anim:tracker_l_leg",\
            "Besha_trackers_anim:tracker_l_hand",\
            "Besha_trackers_anim:tracker_r_hand",\
            "Besha_trackers_anim:tracker_l_foot",\
            "Besha_trackers_anim:tracker_r_foot" ,\
            "Besha_trackers_anim:tracker_l_arm",\
            "Besha_trackers_anim:tracker_r_arm",\
            "Besha_trackers_anim:tracker_spine"\
        ]

        #baranSet = sets(MyLayout.bodyBaranSetName, q=True)

        if not baranSet:
            raise RuntimeError('Can\'t find set with name: \'' + self.bodyBaranSetName + '\' or the set is empty')

        if self.selectionModeBtn and self.selectionModeBtn.btnStatus:
            for setNode in baranSet:
                curves = listConnections(str(setNode), type='animCurve')

                if not curves:
                    continue
                
                for curve in curves:
                    select(str(curve), d=True)
            
            return
        select(baranSet, d=True)
    
    def __select_straus_face__(self):

        strausSet = ["Shtraus_face_anim:head_geo1_0_1_2",\
            "Shtraus_face_anim:head_geo1_3_last_last",\
            "Shtraus_face_anim:mesh_0_1_2",\
            "Shtraus_face_anim:mesh_3_4_5",\
            "Shtraus_face_anim:mesh_6_7_8",\
            "Shtraus_face_anim:mesh_9_10_11",\
            "Shtraus_face_anim:mesh_12_last_last",\
            "Shtraus_face_anim:pCube70_0_1_2",\
            "Shtraus_face_anim:pCube70_3_4_last"\
        ]

        #strausSet = sets(MyLayout.faceStrausSetName, q=True)

        if not strausSet:
            raise RuntimeError('Can\'t find set with name: \'' + self.faceStrausSetName + '\' or the set is empty')

        if self.selectionModeBtn and self.selectionModeBtn.btnStatus:
            for setNode in strausSet:
                curves = listConnections(str(setNode), type='animCurve')

                if not curves:
                    continue
                
                for curve in curves:
                    select(str(curve), add=True)

            return
        select(strausSet, add=True)

    def __unselect_straus_face__(self):

        strausSet = ["Shtraus_face_anim:head_geo1_0_1_2",\
            "Shtraus_face_anim:head_geo1_3_last_last",\
            "Shtraus_face_anim:mesh_0_1_2",\
            "Shtraus_face_anim:mesh_3_4_5",\
            "Shtraus_face_anim:mesh_6_7_8",\
            "Shtraus_face_anim:mesh_9_10_11",\
            "Shtraus_face_anim:mesh_12_last_last",\
            "Shtraus_face_anim:pCube70_0_1_2",\
            "Shtraus_face_anim:pCube70_3_4_last"\
        ]

        #strausSet = sets(MyLayout.faceStrausSetName, q=True)

        if not strausSet:
            raise RuntimeError('Can\'t find set with name: \'' + self.faceStrausSetName + '\' or the set is empty')

        if self.selectionModeBtn and self.selectionModeBtn.btnStatus:
            for setNode in strausSet:
                curves = listConnections(str(setNode), type='animCurve')

                if not curves:
                    continue
                
                for curve in curves:
                    select(str(curve), d=True)

            return
        select(strausSet, d=True)

    def __select_straus_body__(self):
        
        strausSet= ["Shtraus_trackers_anim:tracker_head",\
            "Shtraus_trackers_anim:tracker_hip",\
            "Shtraus_trackers_anim:tracker_r_leg",\
            "Shtraus_trackers_anim:tracker_l_leg",\
            "Shtraus_trackers_anim:tracker_l_hand",\
            "Shtraus_trackers_anim:tracker_r_hand",\
            "Shtraus_trackers_anim:tracker_l_foot" ,\
            "Shtraus_trackers_anim:tracker_r_foot",\
            "Shtraus_trackers_anim:tracker_l_arm",\
            "Shtraus_trackers_anim:tracker_r_arm",\
            "Shtraus_trackers_anim:tracker_spine"
        ]

        #strausSet = sets(MyLayout.bodyStrausSetName, q=True)

        if not strausSet:
            raise RuntimeError('Can\'t find set with name: \'' + self.bodyStrausSetName + '\' or the set is empty')

        if self.selectionModeBtn and self.selectionModeBtn.btnStatus:
            for setNode in strausSet:
                curves = listConnections(str(setNode), type='animCurve')

                if not curves:
                    continue
                
                for curve in curves:
                    select(str(curve), add=True)

            return
        select(strausSet, add=True)

    def __unselect_straus_body__(self):
        
        strausSet= ["Shtraus_trackers_anim:tracker_head",\
            "Shtraus_trackers_anim:tracker_hip",\
            "Shtraus_trackers_anim:tracker_r_leg",\
            "Shtraus_trackers_anim:tracker_l_leg",\
            "Shtraus_trackers_anim:tracker_l_hand",\
            "Shtraus_trackers_anim:tracker_r_hand",\
            "Shtraus_trackers_anim:tracker_l_foot" ,\
            "Shtraus_trackers_anim:tracker_r_foot",\
            "Shtraus_trackers_anim:tracker_l_arm",\
            "Shtraus_trackers_anim:tracker_r_arm",\
            "Shtraus_trackers_anim:tracker_spine"
        ]

        #strausSet = sets(MyLayout.bodyStrausSetName, q=True)

        if not strausSet:
            raise RuntimeError('Can\'t find set with name: \'' + self.bodyStrausSetName + '\' or the set is empty')

        if self.selectionModeBtn and self.selectionModeBtn.btnStatus:
            for setNode in strausSet:
                curves = listConnections(str(setNode), type='animCurve')

                if not curves:
                    continue
                
                for curve in curves:
                    select(str(curve), d=True)

                return
        select(strausSet, d=True)

class CharButtonGroup():

    def __init__(self, baran = None, straus = None, allBtn = None):
        self.baran = MyButton(baran)
        self.straus = MyButton(straus)
        self.allBtn = MyButton(allBtn)

    def BaranCallback(self):
        if not self.baran or not self.baran.btn \
            or not self.allBtn or not self.allBtn.btn:
            return
        
        if not self.baran.btnStatus:
            pass
        else:
            self.allBtn.ToDefaultState()

        self.baran.ToggleState()

    def StrausCallback(self):
        if not self.straus or not self.straus.btn:
            return

        if not self.straus.btnStatus:
            pass
        else:
            self.allBtn.ToDefaultState()

        self.straus.ToggleState()

    def AllCallback(self):
        if not self.allBtn or not self.baran or not self.straus \
            or not self.allBtn.btn or not self.baran.btn or not self.straus.btn:
            return

        if not self.allBtn.btnStatus:
            if self.baran.btnStatus and self.straus.btnStatus:
                self.allBtn.ToggleState()
                return

            if not self.baran.btnStatus:
                self.baran.ToggleState()
            if not self.straus.btnStatus:
                self.straus.ToggleState()

            self.allBtn.ToggleState()
        else:
            self.baran.ToDefaultState()
            self.straus.ToDefaultState()
            self.allBtn.ToDefaultState()

    def BaranBtnStatus(self):
        return self.baran and self.baran.btn and self.baran.btnStatus

    def StrausBtnStatus(self):
        return self.straus and self.straus.btn and self.straus.btnStatus

    def AllBtnStatus(self):
        return self.allBtn and self.allBtn.btn and self.allBtn.btnStatus

    def BaranBtnToDefaultState(self):
        if self.baran:
            self.baran.ToDefaultState()

    def StrausBtnToDefaultState(self):
        if self.straus:
            self.straus.ToDefaultState()

    def AllBtnToDefaultState(self):
        if self.allBtn:
            self.allBtn.ToDefaultState()

class BodyBtnGroup():

    def __init__(self, face = None, body = None, allBtn = None):
        self.face = MyButton(face)
        self.body = MyButton(body)
        self.allBtn = MyButton(allBtn)

    def FaceCallback(self):
        if not self.face or not self.face.btn:
            return

        if not self.face.btnStatus:
            pass

        else:
            self.allBtn.ToDefaultState()

        self.face.ToggleState()

    def BodyCallback(self):
        if not self.body or not self.body.btn:
            return

        if not self.body.btnStatus:
            pass

        else:
            self.allBtn.ToDefaultState()

        self.body.ToggleState()

    def AllCallback(self):
        if not self.allBtn or not self.face or not self.body \
            or not self.allBtn.btn or not self.face.btn or not self.body.btn:
            return

        if not self.allBtn.btnStatus:
            if self.face.btnStatus and self.body.btnStatus:
                self.allBtn.ToggleState()
                return

            if not self.face.btnStatus:
                self.face.ToggleState()

            if not self.body.btnStatus:
                self.body.ToggleState()

            self.allBtn.ToggleState()
        else:
            self.face.ToDefaultState()
            self.body.ToDefaultState()
            self.allBtn.ToDefaultState()

    def FaceBtnStatus(self):
        return self.face and self.face.btn and self.face.btnStatus

    def BodyBtnStatus(self):
        return self.body and self.body.btn and self.body.btnStatus

    def AllBtnStatus(self):
        return self.allBtn and self.allBtn.btn and self.allBtn.btnStatus

    def FaceButtonToDefaultState(self):
        if self.face:
            self.face.ToDefaultState()

    def BodyButtonToDefaultState(self):
        if self.body:
            self.body.ToDefaultState()

    def AllButtonToDefaultState(self):
        if self.allBtn:
            self.allBtn.ToDefaultState()

# myLayout = MyLayout(CharButtonGroup(), BodyBtnGroup())

# win = window(title="Anim shift", width=300, height=140)

# with horizontalLayout():
#     with columnLayout(adjustableColumn=True):
#         text("character", height=20, bgc=[.082, .164, .321])

#         myLayout.charGroup.baran.btn = button(label='baran')
#         button(myLayout.charGroup.baran.btn, e=True, c = windows.Callback(myLayout.BaranCallback))

#         myLayout.charGroup.straus.btn = button(label='straus')
#         button(myLayout.charGroup.straus.btn, e=True, c = windows.Callback(myLayout.StrausCallback))

#         myLayout.charGroup.allBtn.btn = button(label='both')
#         button(myLayout.charGroup.allBtn.btn, e=True, c = windows.Callback(myLayout.CharAllCallback))

#         framesTextField=textField(edit=0, tx='0')
    
#     with columnLayout(adjustableColumn=True):
#         text("body part", height=20, bgc=[.082, .164, .321])

#         myLayout.bodyGroup.face.btn = button(label='face')
#         button(myLayout.bodyGroup.face.btn, e=True, c = windows.Callback(myLayout.FaceCallback))

#         myLayout.bodyGroup.body.btn = button(label='body')
#         button(myLayout.bodyGroup.body.btn, e=True, c = windows.Callback(myLayout.BodyCallback))

#         myLayout.clSelectionBtn = button(label='clear selection')
#         button(myLayout.clSelectionBtn, e=True, c=windows.Callback(myLayout.ClearSelectionCallback))
#         # myLayout.bodyGroup.allBtn.btn = button(label='both')
#         # button(myLayout.bodyGroup.allBtn.btn, e=True, c = windows.Callback(myLayout.BodyAllCallback))

#         with horizontalLayout():
#             myLayout.lShiftButton = button(label='<<')
#             myLayout.rShiftButton = button(label='>>')
            
#             button(myLayout.lShiftButton, e=True, c=windows.Callback(myLayout.LelftShiftCallback, framesTextField))
#             button(myLayout.rShiftButton, e=True, c=windows.Callback(myLayout.RightShiftCallback, framesTextField))            

# win.show()