import os
import os.path
import shutil
import json

from pymel.core import *
from pymel.util import path as upath

class Constants:
    animScene = "m:/Zaryadka2/Animation/episodes/ep%02d/proc/ep%02d_animation_scene.ma"
    blackFrameSource = "m:/Zaryadka2/Source/ep00_render_scene_00000.png"
    
    #out
    logFolder = "m:/Zaryadka2/Out"
    logFile = "zaryadka_cam_geo_export.log"

    alembicOutDir = "m:/Zaryadka2/Out/animation/ep%02d"
    alembicOutFile = "ep%02d_alembic.abc"

    cameraOutDir = alembicOutDir
    cameraOutFile = "ep%02d_camera.ma"

    frameRangeDir = "m:/Zaryadka2/Deadline/frame_range/ep%02d"
    mainframeRangeFile = "ep%02d_frame_range.txt"

    blackFrameDir = "m:/Zaryadka2/Out/rendering/ep%02d/sequence"
    blackFrameFile = "ep%02d_render_scene_%05d.png"

    blackFrameJsonDir = "m:/Zaryadka2/Deadline/frame_range/ep%02d"
    blackFrameJsonFile = "ep%02d_render_frame.json"

    #constants
    refNames = ['cam_schemeRN', 'zuza_rigRN', 'calibration_sceneRN']
    cameraSetName = 'cam_scheme:camera_cache_set'
    cameraNodeName = 'cam_scheme:camera_move'
    alembicExportStr = "-root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_l_cross_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_l_leg_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_r_cross_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_r_leg_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo14 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo15 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo18 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo19 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo16 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo22 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo17 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo20 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo21 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo23 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo24 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo25 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo26 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_group12|zuza_rig_r_arm_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_l_eye_constr_grp|zuza_rig_l_eye_deform_grp|zuza_rig_l_eyelid_geo_grp|zuza_rig_l_up_eyelid_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_l_eye_constr_grp|zuza_rig_l_pupil_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_l_eye_constr_grp|zuza_rig_l_eye_deform_grp|zuza_rig_l_eye_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_l_eye_constr_grp|zuza_rig_l_eye_deform_grp|zuza_rig_l_eyelid_geo_grp|zuza_rig_l_down_eyelid_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_r_eye_mirrow_grp|zuza_rig_r_eye_constr_grp|zuza_rig_r_eye_deform_grp|zuza_rig_r_eye_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_r_eye_mirrow_grp|zuza_rig_r_eye_constr_grp|zuza_rig_r_eye_deform_grp|zuza_rig_r_eyelid_geo_grp|zuza_rig_r_down_eyelid_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_r_eye_mirrow_grp|zuza_rig_r_eye_constr_grp|zuza_rig_r_eye_deform_grp|zuza_rig_r_eyelid_geo_grp|zuza_rig_r_up_eyelid_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_r_eye_mirrow_grp|zuza_rig_r_eye_constr_grp|zuza_rig_r_pupil_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_shnurki3 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp|zuza_rig_shnurki4 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_l_arm_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_body_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_l_bant_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_l_pip_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_sweater_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_up_skirt_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_waistcoat_part_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_r_pip_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_waistcoat_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_button_01_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_skirt_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_button_05_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_button_03_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_r_bant_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_r_ear_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo2 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_l_ear_geo -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo1 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo3 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo4 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo7 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo5 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo6 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo8 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo9 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo10 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo11 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo12 -root |zuza_rig_zuza|zuza_rig_zuza_geo_grp_old|zuza_rig_eyelash_geo_grp|zuza_rig_eyelash_geo13"
    xmlNodeName = ""

class ZaryadkaCamGeoExportException(Exception):
    pass

def MyNormpath(path):

    if not path or path == "":
        return ""

    res = path.replace("\\", "/")

    while (res.find("//") > -1):
        res = res.replace("//", "/")

    if res[-1] == "/":
        res = res[:len(res) - 1]

    return res

def PrintLog(message):
    with open(Constants.logFolder + "/" + Constants.logFile, 'a') as logFile:
        logFile.write("\n" + message + "\n----------------------------------------------------\n")

def BlackFrame(epNum):

    if not os.path.exists(Constants.blackFrameDir % epNum):
        os.makedirs(Constants.blackFrameDir % epNum)

    prevIdx = playbackOptions(min=True, q=True)
    prevCam = getAttr(Constants.cameraNodeName + ".cam_pos", t=playbackOptions(min=True, q=True))
    frameRanges = []
    curTime = playbackOptions(min=True, q=True)
    endTime = playbackOptions(max=True, q=True)

    while curTime < endTime + 1:
        try:
            tempStr = (Constants.blackFrameDir % epNum) + "/" + (Constants.blackFrameFile % (epNum, int(curTime)))
            shutil.copy(Constants.blackFrameSource, tempStr)
        except Exception as ex2:
            pass
        curCam = getAttr(Constants.cameraNodeName + ".cam_pos", t=curTime)
        if prevCam != curCam:
            if len([x for x in [1, 4, 5] if prevCam == x]) > 0:
                frameRanges.append([int(prevIdx - 1), int(curTime - 1)])
            prevIdx = curTime
            prevCam = getAttr(Constants.cameraNodeName + ".cam_pos", t=curTime)
        curTime += 1

    if not os.path.exists(Constants.blackFrameJsonDir % epNum):
        os.makedirs(Constants.blackFrameJsonDir % epNum)

    tempStr = (Constants.blackFrameJsonDir % epNum) + "/" + (Constants.blackFrameJsonFile % epNum)
    with open(tempStr, 'w') as f:
        json.dump(frameRanges, f, indent=4)

def RunButtonCallback():
    global epStartTextField
    global epEndTextField

    try:

        epStartNum = -1
        epEndNum = -1
        try:
            epStartNum = int(epStartTextField.getText())
            epEndNum = int(epEndTextField.getText())
        except ValueError as ex2:
            raise ZaryadkaCamGeoExportException("Couldn't parse episode range field: " \
                                        + type(ex2).__name__ + ": " + str(ex2))

        if epStartNum > epEndNum:
            raise ZaryadkaCamGeoExportException("epStartNum > epEndNum")

        if len([x for x in pluginInfo(q=True, ls=True) if "xml" in x]) < 1:
            raise ZaryadkaCamGeoExportException("maya xml reader plugin isn't loaded")

        try:
            if not os.path.exists(Constants.logFolder):
                os.makedirs(Constants.logFolder)

            f = open(Constants.logFolder + "/" + Constants.logFile, 'w')
            f.close()

        except Exception as ex2:
            print "Couldn't create log file at path: " + Constants.logFolder + "/" + Constants.logFile\
                + "\n" + type(ex2).__name__ + ": message: " + str(ex2)
            raise ZaryadkaCamGeoExportException("failed to create log file")

        for epNum in range(epStartNum, epEndNum + 1):
            try:
                # check if scene exists
                if not os.path.isfile(Constants.animScene % (epNum, epNum)):
                    raise ZaryadkaCamGeoExportException("open scene file doesn't exists: " + Constants.animScene % (epNum, epNum))

                #open scene
                res = mel.eval('file -f -options "v=0;" -pmt 0 -ignoreVersion  -typ "mayaAscii" -o "%s";' % Constants.animScene % (epNum, epNum))
                if MyNormpath(res) != MyNormpath(Constants.animScene % (epNum, epNum)):
                    PrintLog("ep: " + str(epNum) + ": res != Constants.animScene, res: " + res)

                #check references
                refs = ls(type='reference')
                if len(refs) < 1:
                    raise ZaryadkaCamGeoExportException("Found 0 references in scene")
                refCounter = 0
                for ref in refs:
                    for refName in Constants.refNames:
                        if refName in str(ref):
                            if not referenceQuery(str(ref), il=True):
                                raise ZaryadkaCamGeoExportException("reference isn't loaded: " + str(ref))
                            refCounter += 1
                            break
                if refCounter < len(Constants.refNames):
                    raise ZaryadkaCamGeoExportException("One of" + len(Constants.refNames) + "references isn't loaded")

                #xml node
                try:
                    xmlNodes = ls(type='maya_xml_camera_reader')
                    if len(xmlNodes) < 1:
                        PrintLog("ep: " + str(epNum) + ": can't find any maya_xml_camera_reader nodes")
                    else:
                        tempNode = xmlNodes[0]
                        for x in xmlNodes:
                            if len(str(x)) < len(str(tempNode)):
                                tempNode = x
                        if len(listConnections(tempNode)) < 2:
                            PrintLog("ep: " + str(epNum) + ": connections count on maya_xml_camera_reader node < 2, make shure it connected properly")
                        else:
                            try:
                                setAttr(str(tempNode) + ".loadXml", True)
                                res = getAttr(tempNode + ".loadedXmlStatus")
                                try:
                                    res = int(res.split(',')[1].split(' ')[-1])
                                    if res < 1:
                                        PrintLog("ep: " + str(epNum) + ": while reloading xml file, clips found value < 1")
                                except Exception as resSplitEx:
                                    PrintLog("ep: " + str(epNum) + ": error while parsing res=getAttr(xmlNodes[0] + '.loadedXmlStatus') result string, res=" + res + ". Exception details: " + type(resSplitEx).__name__ + ': ' + str(resSplitEx))

                            except Exception as setAttrEx:
                                PrintLog("ep: " + str(epNum) + ": error setting .loadXml attribute on node: " + str(tempNode) + ". " + type(setAttrEx).__name__ + ': ' + str(setAttrEx))
                except Exception as ex2:
                    PrintLog("ep: " + str(epNum) + "exception wihle checking xml-camera node: " + type(ex2).__name__ + ": " + str(ex2))

                #select camera set and bake
                try:
                    select(Constants.cameraSetName, r=True)
                    selectedList = ls(sl=True)
                    bakeResults(selectedList, simulation=True, t=(playbackOptions(q=True, min=True), playbackOptions(q=True, max=True)), sampleBy=1, oversamplingRate=1, disableImplicitControl=True, preserveOutsideKeys=True, sparseAnimCurveBake=False, removeBakedAttributeFromLayer=False, removeBakedAnimFromLayer=False, bakeOnOverrideLayer=False, minimizeRotation=True, controlPoints=False, shape=True)
                except Exception as ex2:
                    raise ZaryadkaCamGeoExportException("Failed to select camera set: " + Constants.cameraSetName\
                                                    + "\n" + type(ex2).__name__ + ": message: " + str(ex2))
                #export camera
                if not os.path.exists(Constants.cameraOutDir % epNum):
                    os.makedirs(Constants.cameraOutDir % epNum)
                tempStr = (Constants.cameraOutDir % epNum) + "/" + (Constants.cameraOutFile % epNum)
                res = mel.eval('file -force -options "v=0;" -typ "mayaAscii" -es "%s";' % tempStr)

                #exprot alembic
                try:
                    if not os.path.exists(Constants.alembicOutDir % epNum):
                        os.makedirs(Constants.alembicOutDir % epNum)

                    tempStr = (Constants.alembicOutDir % epNum) + "/" + (Constants.alembicOutFile % epNum)
                    mel.eval('AbcExport -j "-frameRange %d %d -uvWrite -worldSpace -writeVisibility -dataFormat ogawa %s -file %s";' % (playbackOptions(q=True, min=True), playbackOptions(q=True, max=True), Constants.alembicExportStr, tempStr))
                except Exception as ex2:
                    raise ZaryadkaCamGeoExportException("alembic export: " + type(ex2).__name__ + ": message: " + str(ex2))
                
                #frame range main file
                if not os.path.exists(Constants.frameRangeDir % epNum):
                    os.makedirs(Constants.frameRangeDir % epNum)

                minTime = int(playbackOptions(q=True, min=True))
                maxTime = int(playbackOptions(q=True, max=True))
                tempStr = (Constants.frameRangeDir % epNum) + "/" + (Constants.mainframeRangeFile % epNum)
                with open(tempStr, 'a') as mainFrameRangeFile:
                    mainFrameRangeFile.write(("ep%02d" % epNum) + "|" + str(minTime) + "|" + str(maxTime) + "\n")

                try:
                    tempStr = (Constants.frameRangeDir % epNum) + "/" + (Constants.mainframeRangeFile % epNum)
                    BlackFrame(epNum)
                except Exception as ex2:
                    PrintLog("BlackFrame(): " + type(ex2).__name__+ ': ' + str(ex2))

            except Exception as ex2:
                PrintLog("\nException: " + type(ex2).__name__ + ": Episode: " + str(epNum)\
                    + "\nmessage: " + str(ex2) + "\n")
                    
    except ZaryadkaCamGeoExportException as ex:
        PrintLog("\nException: " + type(ex).__name__\
                    + "\nmessage: " + str(ex) + "\n")

epStartTextField = None
epEndTextField = None

win = window(title="Zaryadka assembly", width=300, height=140)

with columnLayout(adjustableColumn=True, rowSpacing=50):
    with horizontalLayout():
        text('start', height=20)
        epStartTextField = textField(edit=False, tx=str('1'))

        text('end', height=20)
        epEndTextField = textField(edit=False, tx=str('2'))

    runButton = button(label='run')
    button(runButton, e=True, c=windows.Callback(RunButtonCallback))

win.show()