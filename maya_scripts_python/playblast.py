import os

from pymel.core import *
from pymel.util import path as upath

#0 - alembic, 1 - camera
refNames = [
    "ep24_sc02RN",\
    "ep24_sc02RN1"\
]

cameraSearchName = 'cam_scheme_camera_move'

class MyLayout:

    defaultSceneStartIndex = 0
    defaultSceneEndIndex = 50

    def __init__(self):
        
        self.epNameTextField = None

        self.runButton = None

    def RunButtonCallback(self):
        
        epName = self.epNameTextField.getText()
    
        if not epName:
            raise RuntimeError("ep name field is empty")

        refNodes = ls(type='reference')

        tempCounter = 0
        for refName in refNames:
            for refNode in refNodes:
                if refName in str(refNode):
                    tempCounter += 1
                    break

        if tempCounter != 2:
            raise RuntimeError("couldn't find reference nodes with names: " + str(refNames))

        alembicRefNode = None
        cameraRefNode = None
        for refNode in refNodes:

            if refNames[1] in str(refNode):
                cameraRefNode = refNode
            elif refNames[0] in str(refNode):
                alembicRefNode = refNode

        try:

            rootDirPath = sceneName().dirname().parent

            if 'animation' not in str(rootDirPath.dirs()).lower()\
                or 'template' not in str(rootDirPath.dirs()).lower():

                raise RuntimeError("couln't find one of the following directories in scene's parent folder:"
                                    "Animation, Template. File render_scene should be located at path: "
                                    "m:/mocap/Template")

            alembicDirPath = upath(str(rootDirPath) + "/Animation/episodes/ep_" + epName + "/alembic")
            camerasDirPath = upath(str(rootDirPath) + "/Animation/episodes/ep_" + epName + "/cameras")

            alembicFiles = []
            camerasFiles = []

            alembicFiles = alembicDirPath.files('*.abc')
            camerasFiles = camerasDirPath.files('*.ma')

            if not alembicFiles:
                raise RuntimeError("couldn't find any alembic files at path: " + str(alembicDirPath))

            if not camerasFiles:
                raise RuntimeError("couldn't find any cameras files at path: " + str(camerasDirPath))

            alembicFiles.sort()
            camerasFiles.sort()

            logFilePath = str(str(rootDirPath) + "/temp/shadows_playblast_script_log_ep_" + epName + ".log")

            if not os.path.exists(os.path.dirname(logFilePath)):
                os.makedirs(os.path.dirname(logFilePath))

            f = open(logFilePath, 'w')
            f.close()

        except Exception as ex:
            print "EXCEPTION:\n" + type(ex).__name__ + '\n' + str(ex)
            raise ex

        renderScenePath = sceneName()

        for twoFiles in zip(alembicFiles, camerasFiles):

            try:
                
                alembicFile = twoFiles[0]
                cameraFile = twoFiles[1]

                alembicTempName = str(alembicFile.name).split('_')[-1].split('.')[0]
                cameraTempName = str(cameraFile.name).split('_')[-1].split('.')[0]

                if alembicTempName[0:2] != 'sc':
                    continue

                if cameraTempName[0:2] != 'sc' or alembicTempName[2:] != cameraTempName[2:]:
                    
                    isCameraFound = False

                    for camera in camerasFiles:
                        cameraTempName = str(camera.name).split('_')[-1].split('.')[0]
                        
                        if cameraTempName[0:2] == 'sc' and cameraTempName[2:] == alembicTempName[2:]:
                            cameraFile = camera
                            isCameraFound = True
                            break

                    if not isCameraFound:
                        raise RuntimeError("couldn't find camera file for scene: " + alembicTempName + ". Alembic file found at path: " + str(alembicFile))

                res = mel.eval('file -loadReference "%s" -type "Alembic" "%s";' % (refNames[0], str(alembicFile).replace(r"\\", '/').replace('\\', '/')))

                if os.path.normpath(res) != os.path.normpath(str(alembicFile)):
                    with open(logFilePath, 'a') as logFile:
                        logFile.write("\nERROR: reload alembic reference: res != alembicFilePath:\n"
                        "res: " + res + "\nalembicFilePath: " + str(alembicFile) + "\n")

                if not referenceQuery(alembicRefNode, il=True):
                    raise RuntimeError("failed to reload alembic reference")

                res = mel.eval('file -loadReference "%s" -type "mayaAscii" -options "v=0;p=17;f=0" "%s";' % (refNames[1], str(cameraFile).replace(r"\\", '/').replace('\\', '/')))

                if os.path.normpath(res) != os.path.normpath(str(cameraFile)):
                    with open(logFilePath, 'a') as logFile:
                        logFile.write("\nERROR: reload camera reference: res != cameraFilePath:\n"
                        "res: " + res + "\ncameraFilePath: " + str(cameraFile) + "\n")

                if not referenceQuery(cameraRefNode, il=True):
                    raise RuntimeError("failed to reload camera reference")

                cameras = ls(type='camera')
                cameraName = ''
                for camera in cameras:
                    if cameraSearchName in str(camera):
                        cameraName = str(camera)
                        break

                if not cameraName:
                    raise RuntimeError("couldn't find camera with name containing string: " + cameraSearchName)

                lookThru(cameraName)

                animScenePath = str(rootDirPath) + "/Animation/episodes/ep_%s/animation_scenes/ep%s_sc%s" % (epName, epName, alembicTempName[2:])
                animScenesList = upath(animScenePath).files('*.ma')

                if not animScenesList:
                    raise RuntimeError("couldn't find any scenes at path: " + animScenePath)

                sceneFileToOpen = ''
                latestSceneVersion = -1

                for sceneFile in animScenesList:
                    try:
                        sceneVersion = int(str(os.path.basename(sceneFile)).split('_')[-1].split('.')[0][1:])
                        if sceneVersion >= latestSceneVersion:
                            latestSceneVersion = sceneVersion
                            sceneFileToOpen = sceneFile
                    except Exception:
                        pass

                if not sceneFileToOpen:
                    raise RuntimeError("at path " + str(animScenePath) + " can't find any files with right name format")

                plMin = 0
                plMax = 0

                with open(str(sceneFileToOpen), 'r') as f:
                    for line in f:
                        po = line.find('playbackOptions')
                        if po != -1:
                            tempStrList = line[po:].split(' ')
                            minOptInd = tempStrList.index('-min')
                            maxOptInd = tempStrList.index('-max')

                            plMin = int(tempStrList[minOptInd + 1])
                            plMax = int(tempStrList[maxOptInd + 1])

                            break

                if plMin >= plMax:
                    raise RuntimeError("can't retrieve correct playbackOptions from scene file: " + str(sceneFileToOpen))

                playbackOptions(e=True, min=plMin)
                playbackOptions(e=True, max=plMax)

                pbPath = str(rootDirPath) + "/Source/shadow/shadow_ep%s/ep%s_sc%s.mov" % (epName, epName, alembicTempName[2:])

                if not os.path.exists(os.path.dirname(pbPath)):
                    os.makedirs(os.path.dirname(pbPath))

                playblast(format='qt', f=pbPath, sqt=0, cc=True, v=True, orn=True, os=False, fo=True, fp=4, p=100, c='H.264', qlt=100, wh=[1280, 720])

                saveAsPath = str(rootDirPath) + "/Animation/episodes/ep_%s/shadow_scenes/ep%s_sc%s_v01.ma" % (epName, epName, alembicTempName[2:])
                res = saveAs(saveAsPath)
                    
                if os.path.normpath(res) != os.path.normpath(saveAsPath):
                    with open(logFilePath, 'a') as logFile:
                        logFile.write("\nERROR: SaveAs scene: res != ..., res: %s\n" % res)

                mel.eval('file -f -options "v=0;"  -ignoreVersion  -typ "mayaAscii" -pmt 0 -o "%s";' % str(renderScenePath).replace(r"\\", '/').replace('\\', '/'))

                if os.path.normpath(str(sceneName())) != os.path.normpath(str(renderScenePath)):
                    with open(logFilePath, 'a') as logFile:
                        logFile.write("\nERROR: open render_scene: sceneName() != renderScenePath, sceneName(): %s\n" % str(sceneName()).replace(r"\\", '/').replace('\\', '/'))

            except Exception as ex:
                try:
                    with open(logFilePath, 'a') as logFile:
                        logFile.write(("\niteration:\nalembic file: %s\ncamera file: %s" % (str(alembicFile), str(cameraFile))) + '\n' + type(ex).__name__ + ': ' + str(ex) + '\n')
                except Exception as ex2:
                    print ("\niteration:\nalembic file: %s\ncamera file: %s" % (str(alembicFile), str(cameraFile))) + '\n' + type(ex2).__name__ + ': ' + str(ex2) + '\n'

buttons = MyLayout()

win = window(title="Shadows playblast", width=300, height=140)

with columnLayout(adjustableColumn=True):

    with horizontalLayout():
        text('ep name, for example: 05', height=20)
        buttons.epNameTextField = textField(edit=False, tx=str('05'))

    separator(horizontal=True, height=20)

    buttons.runButton = button(label='run')
    button(buttons.runButton, e=True, c = windows.Callback(buttons.RunButtonCallback))

win.show()