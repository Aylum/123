from pymel.core import *

class MyButton():

    def __init__(self, btn = None):
        self.btn = btn
        self.btnStatus = False

    def ToggleState(self):
        if not self.btn:
            return

        self.btnStatus = not self.btnStatus

        if self.btnStatus:
            button(self.btn, e=True, bgc=[0, 0.3, 0])
        else:
            button(self.btn, e=True, bgc=[0.361, 0.361, 0.361])

    def ToDefaultState(self):
        if not self.btn:
            return

        self.btnStatus = False
        button(self.btn, e=True, bgc=[0.361, 0.361, 0.361])

class MyLayout():

    def __init__(self, charGroup = None):
        self.charGroup = charGroup

        self.prevCamBtn = None
        self.nextCamBtn = None

        self.setKeyBtn = None
        self.setKeyBtnOut = None

        self.hideBtn = None
        self.showBtn = None

        self.charBaranBtnStatus = False
        self.charStrausBtnStatus = False

        self.strausSetName = 'Straus_rig_v02:SetStraus'
        self.baranSetName = 'besha_rig:SetBaran'

    def BaranCallback(self):
        if not self.charGroup:
            return

        self.charGroup.BaranCallback()
        
        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()
        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()

        self.__buttonController__('CHAR_BARAN')

    def StrausCallback(self):
        if not self.charGroup:
            return

        self.charGroup.StrausCallback()

        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()
        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()

        self.__buttonController__('CHAR_STRAUS')

    def CharClearCallback(self):
        if not self.charGroup:
            return
        
        self.charGroup.ClearCallback()

        self.charBaranBtnStatus = False
        self.charStrausBtnStatus = False

        self.__buttonController__('CHAR_CLEAR')

    def SetKeyCallback(self):
        if not self.charGroup:
            return

        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()
        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()

        curTime = currentTime()

        if self.charBaranBtnStatus and self.charStrausBtnStatus:

            baranSetLen = sets(self.baranSetName, q=True, s=True)
            strausSetLen = sets(self.strausSetName, q=True, s=True)

            if len(ls(sl=True)) != baranSetLen + strausSetLen:
                select(self.baranSetName, r=True, ne=False)
                select(self.strausSetName, add=True, ne=False)

            currentTime(curTime - 1)
            selectedList = ls(sl=True)

            setKeyframe(self.baranSetName, self.strausSetName, bd=False, hi='none', cp=False, s=False)
            currentTime(curTime)

            mel.eval('HideSelectedObjects ;')

            select(selectedList, r=True)

            setKeyframe(self.baranSetName, self.strausSetName, bd=False, hi='none', cp=False, s=False)

        elif self.charBaranBtnStatus:
            baranSetLen = sets(self.baranSetName, q=True, s=True)

            if len(ls(sl=True)) != baranSetLen:
                select(self.baranSetName, r=True, ne=False)

            currentTime(curTime - 1)
            selectedList = ls(sl=True)

            setKeyframe(self.baranSetName, bd=False, hi='none', cp=False, s=False)
            currentTime(curTime)

            mel.eval('HideSelectedObjects ;')

            select(selectedList, r=True)

            setKeyframe(self.baranSetName, bd=False, hi='none', cp=False, s=False)

        elif self.charStrausBtnStatus:
            strausSetLen = sets(self.strausSetName, q=True, s=True)

            if len(ls(sl=True)) != strausSetLen:
                select(self.strausSetName, r=True, ne=False)

            currentTime(curTime - 1)
            selectedList = ls(sl=True)

            setKeyframe(self.strausSetName, bd=False, hi='none', cp=False, s=False)
            currentTime(curTime)

            mel.eval('HideSelectedObjects ;')

            select(selectedList, r=True)

            setKeyframe(self.strausSetName, bd=False, hi='none', cp=False, s=False)

    def SetKeyOutCallback(self):

        if not self.charGroup:
            return

        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()
        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()

        curTime = currentTime()

        if self.charBaranBtnStatus and self.charStrausBtnStatus:

            baranSetLen = sets(self.baranSetName, q=True, s=True)
            strausSetLen = sets(self.strausSetName, q=True, s=True)

            if len(ls(sl=True)) != baranSetLen + strausSetLen:
                select(self.baranSetName, r=True, ne=False)
                select(self.strausSetName, add=True, ne=False)

            currentTime(curTime - 1)
            selectedList = ls(sl=True)

            mel.eval('HideSelectedObjects ;')
            select(selectedList, r=True)

            setKeyframe(self.baranSetName, self.strausSetName, bd=False, hi='none', cp=False, s=False)
            
            currentTime(curTime)

            select(selectedList, r=True)
            mel.eval('ShowSelectedObjects ;')

            setKeyframe(self.baranSetName, self.strausSetName, bd=False, hi='none', cp=False, s=False)

        elif self.charBaranBtnStatus:

            baranSetLen = sets(self.baranSetName, q=True, s=True)

            if len(ls(sl=True)) != baranSetLen:
                select(self.baranSetName, r=True, ne=False)

            currentTime(curTime - 1)
            selectedList = ls(sl=True)

            mel.eval('HideSelectedObjects ;')
            select(selectedList, r=True)

            setKeyframe(self.baranSetName, bd=False, hi='none', cp=False, s=False)
            
            currentTime(curTime)

            select(selectedList, r=True)
            mel.eval('ShowSelectedObjects ;')

            setKeyframe(self.baranSetName, bd=False, hi='none', cp=False, s=False)

        elif self.charStrausBtnStatus:

            strausSetLen = sets(self.strausSetName, q=True, s=True)

            if len(ls(sl=True)) != strausSetLen:
                select(self.strausSetName, r=True, ne=False)

            currentTime(curTime - 1)
            selectedList = ls(sl=True)

            mel.eval('HideSelectedObjects ;')
            select(selectedList, r=True)

            setKeyframe(self.strausSetName, bd=False, hi='none', cp=False, s=False)

            currentTime(curTime)

            select(selectedList, r=True)
            mel.eval('ShowSelectedObjects ;')

            setKeyframe(self.strausSetName, bd=False, hi='none', cp=False, s=False)

    def PrevCameraBtnCallback(self):
        
        xml_nodes = ls(type='maya_xml_camera_reader')
        
        if not xml_nodes:
            raise RuntimeError('can\'t find any nodes with type \'maya_xml_camera_reader\'')

        attrName = str(xml_nodes[0]) + '.camera'

        curTime = currentTime()
        curCamera = getAttr(attrName, t = curTime)

        minPlayback = playbackOptions(min=True, q=True)

        while curTime >= minPlayback:
            if getAttr(attrName, t = curTime) != curCamera:
                currentTime(curTime)
                return
            else:
                curTime -= 1

    def NextCameraBtnCallback(self):
        xml_nodes = ls(type='maya_xml_camera_reader')
        
        if not xml_nodes:
            raise RuntimeError('can\'t find any nodes with type \'maya_xml_camera_reader\'')

        attrName = str(xml_nodes[0]) + '.camera'

        curTime = currentTime()
        curCamera = getAttr(attrName, t = curTime)

        maxPlayback = playbackOptions(max=True, q=True)

        while curTime <= maxPlayback:
            if getAttr(attrName, t = curTime) != curCamera:
                currentTime(curTime)
                return
            else:
                curTime += 1

    def HideBtnCallback(self):
        if not self.charGroup:
            return

        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()
        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()

        selectedList = ls(sl=True)
        slLen = len(selectedList)

        if self.charBaranBtnStatus and self.charStrausBtnStatus:

            if slLen != sets(self.baranSetName, q=True, s=True) + sets(self.strausSetName, q=True, s=True):
                select(cl=True)

                if self.charBaranBtnStatus:
                    self.__select_baran_geo__(add=True)
                if self.charStrausBtnStatus:
                    self.__select_straus_geo__(add=True)

                selectedList = ls(sl=True)
                slLen = len(selectedList)

        elif self.charBaranBtnStatus:
            if slLen != sets(self.baranSetName, q=True, s=True):

                select(cl=True)

                if self.charBaranBtnStatus:
                    self.__select_baran_geo__(add=True)

                selectedList = ls(sl=True)
                slLen = len(selectedList)

        elif self.charStrausBtnStatus:
            if slLen != sets(self.strausSetName, q=True, s=True):
                select(cl=True)

                if self.charStrausBtnStatus:
                    self.__select_straus_geo__(add=True)

                selectedList = ls(sl=True)
                slLen = len(selectedList)

        mel.eval('HideSelectedObjects ;')

        select(selectedList, r=True)

    def ShowBtnCallback(self):
        if not self.charGroup:
            return

        self.charStrausBtnStatus = self.charGroup.StrausBtnStatus()
        self.charBaranBtnStatus = self.charGroup.BaranBtnStatus()

        selectedList = ls(sl=True)
        slLen = len(selectedList)

        if self.charBaranBtnStatus and self.charStrausBtnStatus:

            if slLen != sets(self.baranSetName, q=True, s=True) + sets(self.strausSetName, q=True, s=True):
                select(cl=True)

                if self.charBaranBtnStatus:
                    self.__select_baran_geo__(add=True)
                if self.charStrausBtnStatus:
                    self.__select_straus_geo__(add=True)

                selectedList = ls(sl=True)
                slLen = len(selectedList)

        elif self.charBaranBtnStatus:
            if slLen != sets(self.baranSetName, q=True, s=True):

                select(cl=True)

                if self.charBaranBtnStatus:
                    self.__select_baran_geo__(add=True)

                selectedList = ls(sl=True)
                slLen = len(selectedList)

        elif self.charStrausBtnStatus:
            if slLen != sets(self.strausSetName, q=True, s=True):
                select(cl=True)

                if self.charStrausBtnStatus:
                    self.__select_straus_geo__(add=True)

                selectedList = ls(sl=True)
                slLen = len(selectedList)

        mel.eval('ShowSelectedObjects ;')

        select(selectedList, r=True)

    def __buttonController__(self, buttonType):

        if buttonType == 'CHAR_BARAN':
            
            if not self.charGroup:
                return

            if self.charBaranBtnStatus:
                if self.charStrausBtnStatus:
                    self.__select_baran_geo__(add=True)
                else:
                    self.__select_baran_geo__(add=False)
            else:
                self.__unselect_baran_geo__()
            
        elif buttonType == 'CHAR_STRAUS':
            
            if not self.charGroup:
                return

            if self.charStrausBtnStatus:
                if self.charBaranBtnStatus:
                    self.__select_straus_geo__(add=True)
                else:
                    self.__select_straus_geo__(add=False)
            else:
                self.__unselect_straus_geo__()

        elif buttonType == 'CHAR_CLEAR':
            
            if not self.charGroup:
                return

            self.__clear_selection__()

            self.charGroup.BaranBtnToDefaultState()
            self.charGroup.StrausBtnToDefaultState()

            self.charBaranBtnStatus = False
            self.charStrausBtnStatus = False

    def __select_baran_geo__(self, add=True):
        if add:    
            select(self.baranSetName, add=True, ne=False)
        else:
            select(self.baranSetName, r=True, ne=False)

    def __unselect_baran_geo__(self):
        select(self.baranSetName, d=True, ne=False)
    
    def __select_straus_geo__(self, add=True):
        if add:    
            select(self.strausSetName, add=True, ne=False)
        else:
            select(self.strausSetName, r=True, ne=False)

    def __unselect_straus_geo__(self):
        select(self.strausSetName, d=True, ne=False)

    def __clear_selection__(self):
        select(cl=True)

class CharButtonGroup():

    def __init__(self, baran = None, straus = None, clBtn = None):
        self.baran = MyButton(baran)
        self.straus = MyButton(straus)
        self.clBtn = MyButton(clBtn)

    def BaranCallback(self):
        if not self.baran or not self.baran.btn:
            return

        self.baran.ToggleState()

    def StrausCallback(self):
        if not self.straus or not self.straus.btn:
            return

        self.straus.ToggleState()

    def ClearCallback(self):
        return

    def BaranBtnStatus(self):
        return self.baran and self.baran.btn and self.baran.btnStatus

    def StrausBtnStatus(self):
        return self.straus and self.straus.btn and self.straus.btnStatus

    def BaranBtnToDefaultState(self):
        if self.baran:
            self.baran.ToDefaultState()

    def StrausBtnToDefaultState(self):
        if self.straus:
            self.straus.ToDefaultState()

# myLayout = MyLayout(CharButtonGroup())

# win = window(title="Geo select", width=300, height=140)

# with columnLayout(adjustableColumn=True):
#     with horizontalLayout():

#         with columnLayout(adjustableColumn=True):

#             myLayout.charGroup.baran.btn = button(label='select baran')
#             button(myLayout.charGroup.baran.btn, e=True, c = windows.Callback(myLayout.BaranCallback))

#         with columnLayout(adjustableColumn=True):
#             myLayout.charGroup.straus.btn = button(label='select straus')
#             button(myLayout.charGroup.straus.btn, e=True, c = windows.Callback(myLayout.StrausCallback))

#         with columnLayout(adjustableColumn=True):
#             myLayout.charGroup.clBtn.btn = button(label='clear selection')
#             button(myLayout.charGroup.clBtn.btn, e=True, c = windows.Callback(myLayout.CharClearCallback))

#     with horizontalLayout():
#         with columnLayout(adjustableColumn=True):
#             myLayout.hideBtn = button(label='hide')
#             button(myLayout.hideBtn, e=True, c = windows.Callback(myLayout.HideBtnCallback))

#         with columnLayout(adjustableColumn=True):
#             myLayout.showBtn = button(label='show')
#             button(myLayout.showBtn, e=True, c = windows.Callback(myLayout.ShowBtnCallback))
            
#         with columnLayout(adjustableColumn=True):
#             myLayout.setKeyBtn = button(label='set keys')
#             button(myLayout.setKeyBtn, e=True, c = windows.Callback(myLayout.SetKeyCallback))

# win.show()