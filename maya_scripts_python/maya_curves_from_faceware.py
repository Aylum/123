import xml.etree.ElementTree as ET

from pymel.core import *

class Constants:
    xmlFileName = "inf1.xml"
    faceNodeName = "face11"

def MyNormpath(path):
    res = path.replace("\\", "/")
    while(res.find("//") > -1):
        res = res.replace("//", "/")
    if res[-1] == "/":
        res = res[:len(res) - 1]
    return res

def createCurves(groupName):
    global dict1
    for key in dict1[groupName]:
        createNode('animCurveTU', n=key + "_u", ss=True)
        createNode('animCurveTU', n=key + "_v", ss=True)
        tempList = listAttr(Constants.faceNodeName + '.' + groupName + '.' + groupName + '_' + key)
        for tempAttrName in tempList:
            if tempAttrName.find(key) != -1:
                continue
            if tempAttrName[0] == 'u':
                connectAttr(key + '_u.output', Constants.faceNodeName + '.' + groupName + '.' + groupName + '_' + key + '.' + tempAttrName)
            else:
                connectAttr(key + '_v.output', Constants.faceNodeName + '.' + groupName + '.' + groupName + '_' + key + '.' + tempAttrName)
        uvIndex = -1
        for uv in dict1[groupName][key]:
            uvIndex += 1
            setKeyframe(key + "_u", t=uvIndex, v=float(uv[0]))
            setKeyframe(key + "_v", t=uvIndex, v=float(uv[1]))

dict1 = {'mouth':{}, 'eyes':{}, 'brows':{}}

def Run():

    if sceneName() == '':
        warnings.warn('scene is untitled. save the scene', RuntimeWarning)
        return

    global dict1
    rootNode = ET.parse(MyNormpath(str(sceneName().dirname())) + "/" + Constants.xmlFileName).getroot()
    frameNodes = rootNode.findall("frames/frame")
    groups = frameNodes[0].findall('markup_groups/markup_group')
    for group in groups:
        landmarks = group.findall('landmarks/landmark')
        for landmark in landmarks:            
            dict1[group.get('name').lower()][landmark.get('name').replace(".", "_").replace("-", "_")] = []

    for frame in frameNodes:
        groups = frame.findall('markup_groups/markup_group')
        for group in groups:
            for landmark in group.findall('landmarks/landmark'):
                point = landmark.findall('texCoord')[0]
                dict1[group.get('name').lower()][landmark.get('name').replace(".", "_").replace("-", "_")].append([point.get('u'), point.get('v')])

    uniqueAttrNameCounter = 0
    createNode('network', n=Constants.faceNodeName, ss=True)
    for groupName in dict1:
        select(Constants.faceNodeName, r=True)
        addAttr(longName=groupName, numberOfChildren=len(dict1[groupName]), attributeType='compound')
        for key in dict1[groupName]:
            addAttr(longName=groupName + '_' + key, numberOfChildren=2, attributeType='compound', parent=groupName)
            addAttr(longName='u' + str(uniqueAttrNameCounter), attributeType='float', defaultValue=0, parent=groupName + '_' + key)
            addAttr(longName='v' + str(uniqueAttrNameCounter), attributeType='float', defaultValue=0, parent=groupName + '_' + key)
            uniqueAttrNameCounter += 1

    for dict2 in dict1:
        createCurves(dict2)

Run()