import os
import os.path
import math

from pymel.core import *
from pymel.util import path as upath

class Constants:
    openScenePath = "m:/_Preparation/template/sc1.ma"
    importSoundPath = "m:/_Preparation/episodes/ep%02d/in/ep%02d_actor_sound.wav"

    outSceneFolder = "m:/_Preparation/episodes/ep%02d/out"
    outSceneName = "ep%02dscene.ma"
    outFbxName = "ep%02dscene.fbx"

    mimikaPeakAnimName = "mimikaPeakAnim"
    networkNodeName = "mimikaPeakAnimGlobal"
    attrCounterName = "counter1"
    attrEpNumName = "epNum"
    attrEpEndName = "epEnd"
    attrOldCurvesNum = "oldCurvesNum"
    jobNum = -1

class Sound_export_exception(Exception):
    pass

def MyNormpath(path):

    if not path or path == "":
        return ""

    res = path.replace("\\", "/")

    while (res.find("//") > -1):
        res = res.replace("//", "/")

    if res[-1] == "/":
        res = res[:len(res) - 1]

    return res

def RunButtonCallback():
    global epStartTextField
    global epEndTextField

    epStartNum = -1
    epEndNum = -1
    try:
        epStartNum = int(epStartTextField.getText())
        epEndNum = int(epEndTextField.getText())
    except ValueError as ex:
        raise Sound_export_exception("Couldn't parse episode range field: " \
                                     + type(ex).__name__ + ": " + str(ex))

    if epStartNum > epEndNum:
        raise Sound_export_exception("epStartNum > epEndNum")

    # check if scene exists
    if not os.path.isfile(Constants.openScenePath):
        raise Sound_export_exception("open scene file doesn't exists: " + Constants.openScenePath)

    res = mel.eval('file -f -options "v=0;"  -ignoreVersion  -typ "mayaAscii" -o "%s";' % Constants.openScenePath)
    if MyNormpath(res) != MyNormpath(Constants.openScenePath):
        warnings.warn("res != Constants.openScenePath, res: " + res, RuntimeWarning)

    jobs = scriptJob(lj=True)
    for job in jobs:
        if Constants.networkNodeName + "." + Constants.attrCounterName in str(job)\
            and "attributeChange" in str(job):

            splitted = str(job).split(':')
            if splitted and len(splitted) > 0:
                tempNum = -1
                try:
                    tempNum = int(splitted[0])
                except ValueError:
                    pass
                if tempNum > -1:
                    scriptJob(kill=tempNum, force=True)
            break

    if Constants.networkNodeName not in str(ls(type='network')):
        createNode('network', n=Constants.networkNodeName, ss=True)
        select(Constants.networkNodeName, r=True)
        addAttr(longName = Constants.attrCounterName, attributeType='long', defaultValue=0)
        addAttr(longName = Constants.attrEpNumName, attributeType='long', defaultValue=epStartNum)
        addAttr(longName = Constants.attrEpEndName, attributeType='long', defaultValue=epEndNum)
        addAttr(longName = Constants.attrOldCurvesNum, attributeType='long', defaultValue=0)
        select(cl=True)
    else:
        setAttr(Constants.networkNodeName + "." + Constants.attrCounterName, 0)
        setAttr(Constants.networkNodeName + "." + Constants.attrEpNumName, epStartNum)
        setAttr(Constants.networkNodeName + "." + Constants.attrEpEndName, epEndNum)
        setAttr(Constants.networkNodeName + "." + Constants.attrOldCurvesNum, 0)

    #saveAs(Constants.openScenePath)

    epNum = epStartNum
    if not os.path.isfile(Constants.importSoundPath % (epNum, epNum)):
        raise Sound_export_exception("sound file doesn't exists: " + Constants.importSoundPath % (epNum, epNum))

    if not os.path.exists(Constants.outSceneFolder % epNum):
        os.makedirs(Constants.outSceneFolder % epNum)

    setAttr(Constants.networkNodeName + "." + Constants.attrOldCurvesNum, len(ls("old" + Constants.mimikaPeakAnimName + "*")))

    tempStr = Constants.importSoundPath % (epNum, epNum)
    res = mel.eval('doSoundImportArgList ("1", {"%s","0.0"});' % tempStr)

    try:
        select(res, r=True)
    except Exception as ex2:
        raise Sound_export_exception("Couldn't select sound after sound import: " + type(ex2).__name__ \
                                        + "\nmessage: " + str(ex2))

    playbackOptions(min=math.floor(getAttr(res + ".sourceStart")))
    playbackOptions(max=math.ceil(getAttr(res + ".sourceEnd")))

    playbackOptions(min=110)############################################
    playbackOptions(max=205)############################################

    Constants.jobNum = scriptJob(ac=[Constants.networkNodeName + "." + Constants.attrCounterName, AttrValChangedCallback], protected=True)

    tempStr = (Constants.outSceneFolder % epNum) + "/" + (Constants.outSceneName % epNum)
    mel.eval('mimikaPeakAnim -nn %s -an %s' % (Constants.networkNodeName, Constants.attrCounterName))

def AttrValChangedCallback():
    try:
        epNum = getAttr(Constants.networkNodeName + "." + Constants.attrEpNumName)
        epEnd = getAttr(Constants.networkNodeName + "." + Constants.attrEpEndName)

        if epNum > epEnd:
            scriptJob(kill=Constants.jobNum, force=True)
            return

        tempStr = (Constants.outSceneFolder % epNum) + "/" + Constants.outSceneName
        saveAs(tempStr)
        mel.eval('filterCurve -f simplify -timeTolerance 0.03  {"%s"};' % Constants.mimikaPeakAnimName)
        
        oldCurves = ls("old" + Constants.mimikaPeakAnimName + "*")
        if getAttr(Constants.networkNodeName + "." + Constants.attrOldCurvesNum) == len(oldCurves):
            warnings.warn("WARNING: ep: " + epNum + ":\n"\
                + "After executing mimikaPeakAnim command, curves count didn't change", RuntimeWarning)
        elif len(oldCurves) > 0:
            delete(oldCurves)

        tempStr = (Constants.outSceneFolder % epNum) + "/" + (Constants.outSceneName % epNum)
        res = mel.eval('file -force -options "v=0;" -type "FBX export" -pr -ea "%s";' % tempStr)

        epNum += 1
        setAttr(Constants.networkNodeName + "." + Constants.attrEpNumName, epNum)

        if epNum > epEnd:
            scriptJob(kill=Constants.jobNum, force=True)
            return

        #new iteration

        if not os.path.isfile(Constants.openScenePath):
            raise Sound_export_exception("open scene file doesn't exists: " + Constants.openScenePath)

        res = mel.eval('file -f -options "v=0;"  -ignoreVersion  -typ "mayaAscii" -o "%s";' % Constants.openScenePath)
        if MyNormpath(res) != MyNormpath(Constants.openScenePath):
            warnings.warn("res != Constants.openScenePath, res: " + res, RuntimeWarning)

        if not os.path.isfile(Constants.importSoundPath % (epNum, epNum)):
            raise Sound_export_exception("sound file doesn't exists: " + Constants.importSoundPath % (epNum, epNum))

        if not os.path.exists(Constants.outSceneFolder % epNum):
            os.makedirs(Constants.outSceneFolder % epNum)

        oldCurvesCount = len(ls("oldmimikaPeakAnim*"))
        setAttr(Constants.networkNodeName + "." + Constants.attrOldCurvesNum, oldCurvesCount)

        tempStr = Constants.importSoundPath % (epNum, epNum)
        res = mel.eval('doSoundImportArgList ("1", {"%s","0.0"});' % tempStr)

        try:
            select(res, r=True)
        except Exception as ex2:
            raise Sound_export_exception("Couldn't select sound after sound import: " + type(ex2).__name__ \
                                            + "\nmessage: " + str(ex2))

        playbackOptions(min=math.floor(getAttr(res + ".sourceStart")))
        playbackOptions(max=math.ceil(getAttr(res + ".sourceEnd")))

        playbackOptions(min=110)##################################################
        playbackOptions(max=205)##################################################

        tempStr = (Constants.outSceneFolder % epNum) + "/" + (Constants.outSceneName % epNum)
        mel.eval('mimikaPeakAnim -nn %s -an %s' % (Constants.networkNodeName, Constants.attrCounterName))

    except Sound_export_exception as ex:
        print "EXCEPTION: episode: " + str(epNum) + "\n" \
        + type(ex).__name__ + ": " + str(ex)    

    if epNum >= getAttr(Constants.networkNodeName + "." + Constants.attrEpEndName):
        scriptJob(kill=Constants.jobNum, force=True)

epStartTextField = None
epEndTextField = None

win = window(title="Sound export", width=300, height=140)

with columnLayout(adjustableColumn=True, rowSpacing=50):
    with horizontalLayout():
        text('start', height=20)
        epStartTextField = textField(edit=False, tx=str('1'))

        text('end', height=20)
        epEndTextField = textField(edit=False, tx=str('2'))

    runButton = button(label='run')
    button(runButton, e=True, c=windows.Callback(RunButtonCallback))

win.show()