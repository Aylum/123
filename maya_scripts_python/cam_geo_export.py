#run from m:/mocap/Animation/episodes/ep_48/animation_scenes/ep48_sc01

import os

from pymel.core import *
from pymel.util import path as upath

from magnetic_export import blackFrame_03 as bf03

class MyLayout:

    defaultSceneStartIndex = 0
    defaultSceneEndIndex = 50

    def __init__(self):
        
        self.applySceneRangeButton = None
        self.runButton = None

        self.epName = None

        self.sceneStartIndex = MyLayout.defaultSceneStartIndex
        self.sceneEndIndex = MyLayout.defaultSceneEndIndex

    def ApplySceneRangeButtonCallback(self, sceneStartTextField, sceneEndTextField):
        try:
            self.sceneStartIndex = int(sceneStartTextField.getText())
            self.sceneEndIndex = int(sceneEndTextField.getText())

            if self.sceneStartIndex > self.sceneEndIndex:
                self.sceneStartIndex = MyLayout.defaultSceneStartIndex
                self.sceneEndIndex = MyLayout.defaultSceneEndIndex
                sceneStartTextField.setText(self.sceneStartIndex)
                sceneEndTextField.setText(self.sceneEndIndex)

        except ValueError:
            self.sceneStartIndex = MyLayout.defaultSceneStartIndex
            self.sceneEndIndex = MyLayout.defaultSceneEndIndex
            sceneStartTextField.setText(self.sceneStartIndex)
            sceneEndTextField.setText(self.sceneEndIndex)

    def RunButtonCallback(self):

        xmlNodes = ls(type='maya_xml_camera_reader')
        if not xmlNodes:
            warnings.warn("can't find any maya_xml_camera_reader nodes", RuntimeWarning)
        else:

            if len(listConnections(xmlNodes[0])) < 2:
                warnings.warn("connections count on maya_xml_camera_reader node < 2, make shure it connected properly", RuntimeWarning)

            else:
                try:
                    setAttr(str(xmlNodes[0]) + ".loadXml", True)

                    res = getAttr(xmlNodes[0] + ".loadedXmlStatus")

                    try:
                        res = int(res.split(',')[1].split(' ')[-1])

                        if res < 1:
                            warnings.warn("While reloading xml file, clips found value < 1", RuntimeWarning)

                    except Exception as resSplitEx:
                        warnings.warn("Error while parsing res=getAttr(xmlNodes[0] + '.loadedXmlStatus') result string, res=" + res + ". Exception details: " + type(resSplitEx).__name__ + ': ' + str(resSplitEx), RuntimeWarning)

                except Exception as setAttrEx:
                    warnings.warn("Error setting .loadXml attribute on node: " + str(xmlNodes[0]) + ". " + type(setAttrEx).__name__ + ': ' + str(setAttrEx), RuntimeWarning)

        epName = ''
        sceneDirs = []
        rootDirPath = ""
        try:
            epName = str(sceneName().dirname().parent.parent)[str(sceneName().dirname().parent.parent).rfind('_') + 1:]
            sceneDirs = sceneName().dirname().parent.dirs()
            rootDirPath = sceneName().dirname().parent.parent.parent.parent.parent
        except Exception:
            print "wrong directory tree or directories names"
            raise

        try:
            frameRangeDirPath = str(rootDirPath) + "/Deadline/frame_range/ep" + epName

            if not os.path.exists(frameRangeDirPath):
                os.makedirs(frameRangeDirPath)

            mainFrameRangeFilePath = frameRangeDirPath + "/ep" + epName + "_frame_range.txt"

            f = open(mainFrameRangeFilePath, 'w')
            f.close()

        except Exception:
            print "couldn't create either derictories or main frame range file at path:"\
                + "directory path: " + frameRangeDirPath\
                + "main frame range file path: " + mainFrameRangeFilePath
            raise

        logFilePath = ''
        try:
            logFilePath = str(rootDirPath) + "/temp/cam_geo_export_script_log_ep_" + epName + ".log"

            if not os.path.exists(os.path.dirname(logFilePath)):
                os.makedirs(os.path.dirname(logFilePath))

            f = open(logFilePath, 'w')
            f.close()

        except Exception:
            print "couldn't create directories at path at path projectDrive:/temp"
            raise

        if not sceneDirs:
            raise RuntimeError("couldn't form directory list")

        for sceneDir in sceneDirs:
            try:

                curSceneIndex = int(os.path.split(sceneDir)[-1].split('_')[-1][2:])
                if curSceneIndex < self.sceneStartIndex or curSceneIndex > self.sceneEndIndex:
                    continue
                
                scenes = sceneDir.files('*.m*')

                if not scenes:
                    with open(logFilePath, 'a') as logFile:
                        logFile.write("\ncouldn't find any scene files in " + str(sceneDir) + '\n')

                    continue

                sceneFileToOpen = ''
                latestSceneVersion = -1

                for sceneFile in scenes:
                    try:
                        sceneVersion = int(str(os.path.basename(sceneFile)).split('_')[-1].split('.')[0][1:])
                        if sceneVersion >= latestSceneVersion:
                            latestSceneVersion = sceneVersion
                            sceneFileToOpen = sceneFile
                    except Exception:
                        pass

                if not sceneFileToOpen:
                    with open(logFilePath, 'a') as logFile:
                        logFile.write("\n at path " + str(sceneDir) + " can't find any files with right name format\n")

                    continue

                res = mel.eval('file -f -options "v=0;"  -ignoreVersion  -typ "mayaAscii" -o "%s";' % str(sceneFileToOpen).replace(r"\\", '/').replace('\\', '/'))
                
                if os.path.normpath(res) != os.path.normpath(str(sceneFileToOpen).replace(r"\\", '/').replace('\\', '/')):
                    with open(logFilePath, 'a') as logFile:
                        logFile.write("\nERROR: open scene: res != sceneFileToOpenPath\nres: %s\nsceneFileToOpenPath: %s\n" % (res, str(sceneFileToOpen)))

                tempDirName = os.path.split(sceneDir)[-1].split('_')
                _sceneName = tempDirName[-1][2:]
                tempDirName = tempDirName[0] + '_sc' + _sceneName
                if tempDirName not in str(sceneName()):
                    with open(logFilePath, 'a') as logFile:
                        logFile.write("\n%s not in sceneName()\nsceneName() = %s\n" % (tempDirName, str(sceneName())))

                minTime = int(playbackOptions(q=True, min=True))
                maxTime = int(playbackOptions(q=True, max=True))
                with open(mainFrameRangeFilePath, 'a') as mainFrameRangeFile:
                    mainFrameRangeFile.write("sc" + _sceneName + "|" + str(minTime) + "|" + str(maxTime) + "\n")

                selectCameraList = ['cam_scheme_camera_move', 'cam_scheme_camera_move_cam']

                select(selectCameraList, r=True)

                selectedList = ls(sl=True)

                if len(selectedList) != 2 and\
                    ('cam_scheme_camera_move' not in str(selectedList)\
                        or 'cam_scheme_camera_move_cam' not in str(selectedList)):

                    raise RuntimeError("can't select '%s' or '%s'" % ('cam_scheme_camera_move', 'cam_scheme_camera_move_cam'))

                bakeResults(selectCameraList, simulation=True, t=(playbackOptions(q=True, min=True), playbackOptions(q=True, max=True)), sampleBy=1, oversamplingRate=1, disableImplicitControl=True, preserveOutsideKeys=True, sparseAnimCurveBake=False, removeBakedAttributeFromLayer=False, removeBakedAnimFromLayer=False, bakeOnOverrideLayer=False, minimizeRotation=True, controlPoints=False, shape=True)

                camerasExportFilePath = str(sceneName().dirname().parent.parent) + '/cameras/' + os.path.split(sceneDir)[-1] + '.ma'
                res = mel.eval('file -force -options "v=0;" -typ "mayaAscii" -es "%s";' % camerasExportFilePath.replace(r"\\", '/').replace('\\', '/'))

                if os.path.normpath(res) != os.path.normpath(camerasExportFilePath):
                    with open(logFilePath, 'a') as logFile:
                        logFile.write("\nERROR: export selected cameras: res != camerasExportFilePath\nres: %s\ncamerasExportFilePath: %s\n" % (res, camerasExportFilePath))

                try:
                    bf03.BlackFrame(frameRangeDirPath + "/" + tempDirName + "_render_frame.json")
                except Exception as ex:
                    with open(logFilePath, 'a') as logFile:
                        logFile.write("\nERROR: BlackFrame(): " + type(ex).__name__+ ': ' + str(ex) + '\n')

                res = saveAs(str(sceneName().dirname()) + '/' + tempDirName + '_anim_v01_scr.ma')
                
                if os.path.normpath(res) != os.path.normpath((sceneName().dirname() + '/' + tempDirName + '_anim_v01_scr.ma').replace(r"\\", '/').replace('\\', '/')):
                    with open(logFilePath, 'a') as logFile:
                        logFile.write("\nERROR: SaveAs scene: res != ..., res: %s\n" % res)

                mel.eval('select -r besha_rig:pTorus13 besha_rig:r_leg_geo besha_rig:head_geo besha_rig:r_cross_geo3 besha_rig:pTorus22 besha_rig:besha_body_geo besha_rig:b_tail_geo besha_rig:polySurface108 besha_rig:geo_grp|besha_rig:face_geo|besha_rig:nose_geo besha_rig:l_eye_geo besha_rig:l_hand_geo2 besha_rig:r_hand_geo1 besha_rig:cap_geo1 besha_rig:r_eye_geo besha_rig:pTorus19 besha_rig:pTorus18 besha_rig:pTorus17 besha_rig:pTorus16 besha_rig:pTorus15 besha_rig:pTorus21 besha_rig:pTorus20 besha_rig:cap_geo3 besha_rig:cap_geo2 besha_rig:l_cross_geo2 besha_rig:l_cross_geo1 besha_rig:l_leg_geo besha_rig:hair_geo besha_rig:l_ear_geo besha_rig:r_ear_geo besha_rig:pTorus14 besha_rig:neck_geo besha_rig:r_cross_geo2 besha_rig:r_cross_geo1 besha_rig:l_cross_geo3 besha_rig:r_hand_geo Straus_rig_v02:pTorus3 Straus_rig_v02:pTorus2 Straus_rig_v02:pTorus6 Straus_rig_v02:pTorus5 Straus_rig_v02:pTorus4 Straus_rig_v02:l_eye_geo Straus_rig_v02:straus_body_geo Straus_rig_v02:l_cross_geo1 Straus_rig_v02:l_cross_geo4 Straus_rig_v02:r_leg_geo Straus_rig_v02:pTorus8 Straus_rig_v02:pTorus7 Straus_rig_v02:r_eye_geo Straus_rig_v02:polySurface156 Straus_rig_v02:polySurface155 Straus_rig_v02:l_leg_geo Straus_rig_v02:tail_geo1 Straus_rig_v02:hair_geo1 Straus_rig_v02:pTorus1 Straus_rig_v02:polySurface154 Straus_rig_v02:polySurface13 Straus_rig_v02:polySurface12 Straus_rig_v02:r_wings_geo Straus_rig_v02:l_wings_geo Straus_rig_v02:head_geo1 Straus_rig_v02:mesh Straus_rig_v02:pCube70 Straus_rig_v02:neck_feathers_base_geo Straus_rig_v02:shtraus_r_down_eyelid_geo Straus_rig_v02:shtraus_r_up_eyelid_geo1 Straus_rig_v02:shtraus_r_up_eyelid_geo Straus_rig_v02:shtraus_l_down_eyelid_geo besha_rig:besha_r_up_eyelid_geo besha_rig:besha_r_down_eyelid_geo besha_rig:besha_l_up_eyelid_geo besha_rig:besha_l_down_eyelid_geo')

                mel.eval('AbcExport -j "-frameRange %d %d -uvWrite -worldSpace -writeVisibility -dataFormat ogawa -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:face_geo|besha_rig:pTorus13 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:r_leg_geo_grp|besha_rig:r_leg_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:face_geo|besha_rig:head_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:r_leg_geo_grp|besha_rig:r_cross_geo_grp|besha_rig:group538|besha_rig:r_cross_geo3 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:r_leg_geo_grp|besha_rig:r_cross_geo_grp|besha_rig:group538|besha_rig:pTorus22 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:besha_body_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:b_tail_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:face_geo|besha_rig:polySurface108 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:face_geo|besha_rig:nose_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:head_geo_grp|besha_rig:head_geo_grp|besha_rig:l_eye_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:hand_geo_grp|besha_rig:l_hand_geo2 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:hand_geo_grp|besha_rig:r_hand_geo1 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:head_geo_grp|besha_rig:head_geo_grp|besha_rig:cap_geo_grp|besha_rig:cap_geo1 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:head_geo_grp|besha_rig:head_geo_grp|besha_rig:r_eye_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:r_leg_geo_grp|besha_rig:r_cross_geo_grp|besha_rig:group538|besha_rig:pTorus19 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:l_leg_geo_grp|besha_rig:l_cross_geo_grp|besha_rig:group539|besha_rig:pTorus18 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:l_leg_geo_grp|besha_rig:l_cross_geo_grp|besha_rig:group539|besha_rig:pTorus17 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:l_leg_geo_grp|besha_rig:l_cross_geo_grp|besha_rig:group539|besha_rig:pTorus16 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:l_leg_geo_grp|besha_rig:l_cross_geo_grp|besha_rig:group539|besha_rig:pTorus15 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:r_leg_geo_grp|besha_rig:r_cross_geo_grp|besha_rig:group538|besha_rig:pTorus21 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:r_leg_geo_grp|besha_rig:r_cross_geo_grp|besha_rig:group538|besha_rig:pTorus20 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:head_geo_grp|besha_rig:head_geo_grp|besha_rig:cap_geo_grp|besha_rig:cap_geo3 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:head_geo_grp|besha_rig:head_geo_grp|besha_rig:cap_geo_grp|besha_rig:cap_geo2 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:l_leg_geo_grp|besha_rig:l_cross_geo_grp|besha_rig:group539|besha_rig:l_cross_geo2 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:l_leg_geo_grp|besha_rig:l_cross_geo_grp|besha_rig:group539|besha_rig:l_cross_geo1 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:l_leg_geo_grp|besha_rig:l_leg_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:head_geo_grp|besha_rig:head_geo_grp|besha_rig:hair_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:head_geo_grp|besha_rig:head_geo_grp|besha_rig:l_ear_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:head_geo_grp|besha_rig:head_geo_grp|besha_rig:r_ear_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:face_geo|besha_rig:pTorus14 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:head_geo_grp|besha_rig:head_geo_grp|besha_rig:neck_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:r_leg_geo_grp|besha_rig:r_cross_geo_grp|besha_rig:group538|besha_rig:r_cross_geo2 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:r_leg_geo_grp|besha_rig:r_cross_geo_grp|besha_rig:group538|besha_rig:r_cross_geo1 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:legs_geo_grp|besha_rig:l_leg_geo_grp|besha_rig:l_cross_geo_grp|besha_rig:group539|besha_rig:l_cross_geo3 -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:hand_geo_grp|besha_rig:r_hand_geo -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_shnurki_geo_grp|Straus_rig_v02:pTorus3 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_shnurki_geo_grp|Straus_rig_v02:pTorus2 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_shnurki_geo_grp|Straus_rig_v02:group153|Straus_rig_v02:pTorus6 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_shnurki_geo_grp|Straus_rig_v02:group153|Straus_rig_v02:pTorus5 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_shnurki_geo_grp|Straus_rig_v02:pTorus4 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:head_grp|Straus_rig_v02:l_eye_geo -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:straus_body_geo -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:l_cross_grp|Straus_rig_v02:l_cross_geo1 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:l_cross_grp|Straus_rig_v02:l_cross_geo4 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:r_leg_geo -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_shnurki_geo_grp|Straus_rig_v02:group153|Straus_rig_v02:pTorus8 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_shnurki_geo_grp|Straus_rig_v02:group153|Straus_rig_v02:pTorus7 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:head_grp|Straus_rig_v02:r_eye_geo -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:tail_grp|Straus_rig_v02:tail_geo|Straus_rig_v02:polySurface156 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:tail_grp|Straus_rig_v02:tail_geo|Straus_rig_v02:polySurface155 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:l_leg_geo -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:tail_grp|Straus_rig_v02:tail_geo1 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:head_grp|Straus_rig_v02:hair_geo1 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_shnurki_geo_grp|Straus_rig_v02:pTorus1 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:tail_grp|Straus_rig_v02:tail_geo|Straus_rig_v02:polySurface154 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:r_cross_grp|Straus_rig_v02:polySurface13 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:r_cross_grp|Straus_rig_v02:polySurface12 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:wings_geo_grp|Straus_rig_v02:r_wings_geo -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:wings_geo_grp|Straus_rig_v02:l_wings_geo -root |Straus_rig_v02:Shtraus|Straus_rig_v02:shtraus_mimic|Straus_rig_v02:coCo_head_geo_grp|Straus_rig_v02:head_geo1 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:shtraus_mimic|Straus_rig_v02:coCo_head_geo_grp|Straus_rig_v02:mesh -root |Straus_rig_v02:Shtraus|Straus_rig_v02:shtraus_mimic|Straus_rig_v02:coCo_head_geo_grp|Straus_rig_v02:pCube70 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:head_grp|Straus_rig_v02:neck_feathers_base_geo -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_r_down_eyelid_geo -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_r_up_eyelid_geo1 -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_r_up_eyelid_geo -root |Straus_rig_v02:Shtraus|Straus_rig_v02:coco|Straus_rig_v02:geo_grp|Straus_rig_v02:shtraus_l_down_eyelid_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:besha_r_up_eyelid_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:besha_r_down_eyelid_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:besha_l_up_eyelid_geo -root |besha_rig:Besha|besha_rig:besha_base|besha_rig:geo_grp|besha_rig:besha_l_down_eyelid_geo -file %s";' % (playbackOptions(q=True, min=True), playbackOptions(q=True, max=True), str(str(sceneName().dirname().parent.parent) + '/alembic/' + os.path.split(sceneDir)[-1] + '.abc').replace(r"\\", '/').replace('\\', '/')))

            except Exception as e:
                try:
                    with open(logFilePath, 'a') as logFile:
                        logFile.write('\n' + str(sceneDir) + '\n' + type(e).__name__ + ': ' + str(e))
                except Exception as e2:
                    print "couldn't write to logFile, " + str(sceneDir) + '\n' + type(e2).__name__ + ': ' + str(e2)

buttons = MyLayout()

win = window(title="Magnetic cam geo export", width=300, height=140)

with columnLayout(adjustableColumn=True):

    with horizontalLayout():
        text('start, end scene number', height=20)
        sceneStartTextField = textField(edit=False, tx=str(MyLayout.defaultSceneStartIndex))
        sceneEndTextField = textField(edit=False, tx=str(MyLayout.defaultSceneEndIndex))

    buttons.applySceneRangeButton = button(label='set scene range')
    button(buttons.applySceneRangeButton, e=True, c = windows.Callback(buttons.ApplySceneRangeButtonCallback, sceneStartTextField, sceneEndTextField))

    separator(horizontal=True, height=20)

    buttons.runButton = button(label='run')
    button(buttons.runButton, e=True, c = windows.Callback(buttons.RunButtonCallback))

win.show()