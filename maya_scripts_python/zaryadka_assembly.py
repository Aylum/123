import os
import os.path
import math

from pymel.core import *
from pymel.util import path as upath

class Constants:
    animScene = "m:/Zaryadka2/Animation/episodes/ep%02d/in/animation_scene.ma"
    calibrationScene = "m:/Zaryadka2/Animation/episodes/ep%02d/in/calibration_scene.ma"
    camSchemeScene = "m:/Zaryadka2/Animation/episodes/ep%02d/in/cam_scheme.ma"

    videoFile = "m:/Zaryadka2/Animation/episodes/ep%02d/in/ep%02d_premaster.mp4"
    audioFile = "m:/Zaryadka2/Animation/episodes/ep%02d/in/ep%02d_actor_sound.wav"
    jsonFile = "m:/Zaryadka2/Animation/episodes/ep%02d/in/ep%02d_edit.json"
    fbxFolder = "m:/Zaryadka2/Animation/episodes/ep%02d/in/animation_fbx"
    audioFbxFile = "m:/Zaryadka2/Animation/episodes/ep%02d/in/animation_fbx/ep%02d_actor_sound_anim.fbx"

    #out
    logFolder = "m:/Zaryadka2/Out"
    logFile = "zaryadka_assembly.log"
    
    saveSceneFolder = "m:/Zaryadka2/Animation/episodes/ep%02d/proc"
    saveSceneName = "ep%02d_animation_scene.ma"

    #constants
    refNames = [['cam_schemeRN', camSchemeScene], ['zuza_rigRN', ''], ['calibration_sceneRN', calibrationScene]]
    fbxImportNames = ["trackers_exported.fbx", "left_exported.fbx", "right_exported.fbx"]#left_glove.fbx, right_glove.fbx
    compName = "Composition1"
    imagePlaneNodeName = "cam_scheme:imagePlaneShape3"
    audioNodeSearchName = "*_actor_sound"

class ZaryadkaAssemblyException(Exception):
    pass

def MyNormpath(path):

    if not path or path == "":
        return ""

    res = path.replace("\\", "/")

    while (res.find("//") > -1):
        res = res.replace("//", "/")

    if res[-1] == "/":
        res = res[:len(res) - 1]

    return res

def PrintLog(message):
    with open(Constants.logFolder + "/" + Constants.logFile, 'a') as logFile:
        logFile.write("\n" + message + "\n----------------------------------------------------\n")

def RunButtonCallback():
    global epStartTextField
    global epEndTextField

    try:
        epStartNum = -1
        epEndNum = -1
        try:
            epStartNum = int(epStartTextField.getText())
            epEndNum = int(epEndTextField.getText())
        except ValueError as ex2:
            raise ZaryadkaAssemblyException("Couldn't parse episode range field: " \
                                        + type(ex2).__name__ + ": " + str(ex2))

        if epStartNum > epEndNum:
            raise ZaryadkaAssemblyException("epStartNum > epEndNum")

        if len([x for x in pluginInfo(q=True, ls=True) if "xml" in x]) < 1:
            raise ZaryadkaAssemblyException("maya xml reader plugin isn't loaded")

        try:
            if not os.path.exists(Constants.logFolder):
                os.makedirs(Constants.logFolder)
            f = open(Constants.logFolder + "/" + Constants.logFile, 'w')
            f.close()
        except Exception as ex2:
            print "Couldn't create log file at path: " + Constants.logFolder + "/" + Constants.logFile\
                + "\n" + type(ex2).__name__ + ": message: " + str(ex2)
            raise ZaryadkaAssemblyException("failed to create log file")

        for epNum in range(epStartNum, epEndNum + 1):
            try:
                # check if scene exists
                if not os.path.isfile(Constants.animScene % epNum):
                    raise ZaryadkaAssemblyException("open scene file doesn't exists: " + Constants.animScene % epNum)
                #open scene
                res = mel.eval('file -f -options "v=0;" -pmt 0 -ignoreVersion  -typ "mayaAscii" -o "%s";' % (Constants.animScene % epNum))
                if MyNormpath(res) != MyNormpath(Constants.animScene % epNum):
                    PrintLog("ERROR: ep: " + str(epNum) + ": res != Constants.animScene, res: " + res)
                #calibration scene
                if not os.path.isfile(Constants.calibrationScene % epNum):
                    raise ZaryadkaAssemblyException("open scene file doesn't exist: " + Constants.calibrationScene % epNum)
                #cam scheme
                if not os.path.isfile(Constants.camSchemeScene % epNum):
                    raise ZaryadkaAssemblyException("open scene file doesn't exist: " + Constants.camSchemeScene % epNum)
                #audio
                if not os.path.isfile(Constants.audioFile % (epNum, epNum)):
                    raise ZaryadkaAssemblyException("audio file doesn't exist: " + Constants.audioFile % (epNum, epNum))
                #fbx audio
                if not os.path.isfile(Constants.audioFbxFile % (epNum, epNum)):
                    raise ZaryadkaAssemblyException("audio file doesn't exist: " + Constants.audioFbxFile % (epNum, epNum))
                #video
                if not os.path.isfile(Constants.videoFile % (epNum, epNum)):
                    raise ZaryadkaAssemblyException("video file doesn't exist: " + Constants.videoFile % (epNum, epNum))
                #json
                if not os.path.isfile(Constants.jsonFile % (epNum, epNum)):
                    raise ZaryadkaAssemblyException("json file doesn't exist: " + Constants.jsonFile % (epNum, epNum))
                #fbx folder
                if not os.path.exists(os.path.dirname(Constants.fbxFolder % epNum)):
                    raise ZaryadkaAssemblyException("fbx folder doesn't exist: " + Constants.fbxFolder % epNum)

                #check references
                refs = ls(type='reference')
                if len(refs) < 1:
                    raise ZaryadkaAssemblyException("Found 0 references in scene")
                refCounter = 0
                for ref in refs:
                    for refName in Constants.refNames:
                        if refName[0] in str(ref):
                            isNeedRefReload = False
                            if referenceQuery(str(ref), il=True):
                                try:
                                    refFile = MyNormpath(referenceQuery(str(ref), un=True, f=True))
                                    tempIdx = refFile.rfind('ep%02d' % epNum)
                                    if refName[1] != "" and tempIdx < 0:
                                        isNeedRefReload = True
                                except Exception:
                                    isNeedRefReload = True
                            else:
                                isNeedRefReload = True

                            if isNeedRefReload:
                                if refName[1] == "":
                                    raise ZaryadkaAssemblyException("reference isn't loaded: " + str(ref))
                                try:
                                    mel.eval('file -loadReference "%s" -type "mayaAscii" -pmt 0 -options "v=0;" "%s";' % (str(ref), (refName[1] % epNum)))
                                except Exception:
                                    pass
                                if not referenceQuery(str(ref), il=True):
                                    raise ZaryadkaAssemblyException("reference isn't loaded: " + str(ref))
                            refCounter += 1
                            break
                if refCounter < len(Constants.refNames):
                    raise ZaryadkaAssemblyException("One of" + len(Constants.refNames) + "references isn't loaded")

                #xml node
                try:
                    xmlNodes = ls(type='maya_xml_camera_reader')
                    if len(xmlNodes) < 1:
                        PrintLog("Error: xml plugin: ep: " + str(epNum) + ": can't find any maya_xml_camera_reader nodes")
                    else:
                        tempNode = xmlNodes[0]
                        for x in xmlNodes:
                            if len(str(x)) < len(str(tempNode)):
                                tempNode = x
                        if len(listConnections(tempNode)) < 2:
                            PrintLog("ERROR: xml plugin: ep: " + str(epNum) + ": connections count on maya_xml_camera_reader node < 2, make shure it connected properly")
                        else:
                            try:
                                setAttr(str(tempNode) + '.path', MyNormpath(Constants.jsonFile % (epNum, epNum)))
                                setAttr(str(tempNode) + ".loadXml", True)
                                currentTime(playbackOptions(max=True, q=True))
                                currentTime(playbackOptions(min=True, q=True))
                                res = getAttr(str(tempNode) + ".loadedXmlStatus")
                                try:
                                    res = int(res.split(',')[1].split(' ')[-1])
                                    if res < 1:
                                        PrintLog("ERROR: xml plugin: ep: " + str(epNum) + ": while reloading xml file, clips found value < 1")
                                except Exception as resSplitEx:
                                    PrintLog("ERROR: xml plugin: ep: " + str(epNum) + ": error while parsing res=getAttr(xmlNodes[0] + '.loadedXmlStatus') result string, res=" + res + ". Exception details: " + type(resSplitEx).__name__ + ': ' + str(resSplitEx))

                            except Exception as setAttrEx:
                                PrintLog("ERROR: xml plugin: ep: " + str(epNum) + ": error setting .loadXml attribute on node: " + str(tempNode) + ". " + type(setAttrEx).__name__ + ': ' + str(setAttrEx))
                except Exception as ex2:
                    PrintLog("ERROR: xml plugin: ep: " + str(epNum) + "exception wihle checking xml-camera node: " + type(ex2).__name__ + ": " + str(ex2))

                #import fbx
                #delete clips
                comps = timeEditorComposition(acp=True, q=True)
                if len(comps) < 1:
                    raise ZaryadkaAssemblyException("found 0 compositions in time editor")
                isCompFound = False
                for comp in comps:
                    if Constants.compName in comp:        
                        timeEditorComposition(comp, e=True, active=True)
                        isCompFound = True
                        break
                if not isCompFound:
                    raise ZaryadkaAssemblyException("Couldn't find composition with name: " + Constants.compName)
                clips = timeEditor(alc='')
                if len(clips) < 1:
                    raise ZaryadkaAssemblyException("found 0 clips in composition: " + str(comp))
                for clip  in clips:
                    source = timeEditorClip(clip, query=True, animSource=True)
                    if source:
                        delete(str(source))
                #import clips
                fbxSceneDirs = os.listdir(Constants.fbxFolder % epNum)
                if len(fbxSceneDirs) < 1:
                    raise ZaryadkaAssemblyException("fbx folder is empty")
                timeEditorClip("ep" + str(epNum) + "_soundFbx", fbx=(Constants.audioFbxFile % (epNum, epNum)), sar=False, ipo='curves', io='generate', s=1, aft=True, track=Constants.compName + ":0")
                timeOffset = 0
                for sceneDir in fbxSceneDirs:
                    sceneDirPath = (Constants.fbxFolder % epNum) + "/" + str(sceneDir)
                    if not os.path.isdir(sceneDirPath):
                        continue
                    tempList = os.listdir(sceneDirPath)
                    tempList.sort()
                    folders = [(sceneDirPath + "/" + str(x)) for x in tempList if os.path.isdir(sceneDirPath + "/" + str(x))]
                    if len(folders) > 0:
                        files = [(folders[-1] + "/" + str(x)) for x in os.listdir(folders[-1]) if os.path.isfile(folders[-1] + "/" + x)]
                        files.sort()
                        fileCounter = 0
                        for file in files:
                            for name in Constants.fbxImportNames:
                                if name in os.path.basename(file):
                                    timeEditorClip(name, fbx=MyNormpath(str(file)), sar=False, ipo='curves', io='generate', s=timeOffset, aft=True, track=Constants.compName + ":" + str(fileCounter + 1))
                                    fileCounter += 1
                                    break
                        timeOffset += 1500
                        if fileCounter < 3:
                            PrintLog("Warning: ep: " + str(epNum) + ": couldn't import one of " + str(len(Constants.fbxImportNames)) + " fbx files")
                    else:
                        raise ZaryadkaAssemblyException("folder fbxFolder/scene0x contains 0 folders")

                try:
                    select(Constants.imagePlaneNodeName)
                    setAttr(Constants.imagePlaneNodeName + ".imageName", Constants.videoFile % (epNum, epNum))
                except Exception:
                    PrintLog("Warning: ep: " + str(epNum) + ": couldn't either select imagePlaneShape node or replace it's source: " + Constants.imagePlaneNodeName)

                audioNodes = ls(Constants.audioNodeSearchName, type='audio')
                if len(audioNodes) > 0:
                    delete(str(audioNodes[0]))

                mel.eval('file -import -type "audio"  -ignoreVersion -ra true -mergeNamespacesOnClash false -options "o=0"  -pr -pmt 0  -importTimeRange "combine" "%s";' % (Constants.audioFile % (epNum, epNum)))

                if not os.path.exists(Constants.saveSceneFolder % epNum):
                    os.makedirs(Constants.saveSceneFolder % epNum)

                tempStr = (Constants.saveSceneFolder % epNum) + "/" + (Constants.saveSceneName % epNum)
                saveAs(tempStr)

            except Exception as ex2:
                PrintLog("Exception: " + type(ex2).__name__ + ": Episode: " + str(epNum)\
                    + "\nmessage: " + str(ex2))

    except ZaryadkaAssemblyException as ex:
        print "Exception: " + type(ex).__name__\
                    + "\nmessage: " + str(ex)

epStartTextField = None
epEndTextField = None

win = window(title="Zaryadka assembly1", width=300, height=140)

with columnLayout(adjustableColumn=True, rowSpacing=50):
    with horizontalLayout():
        text('start', height=20)
        epStartTextField = textField(edit=False, tx=str('1'))

        text('end', height=20)
        epEndTextField = textField(edit=False, tx=str('2'))

    runButton = button(label='run')
    button(runButton, e=True, c=windows.Callback(RunButtonCallback))

win.show()