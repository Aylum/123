from pymel.core import *

from magnetic_tool import menu_anim_shift as mas
from magnetic_tool import select_geo_hide as sgh

myLayoutAnim = mas.MyLayout(mas.CharButtonGroup(), mas.BodyBtnGroup())
myLayoutGeo = sgh.MyLayout(sgh.CharButtonGroup())

win = window(title="Magnetic tool", width=300, height=140)

with columnLayout(adjustableColumn=True):
    with horizontalLayout():

        with columnLayout(adjustableColumn=True):

            myLayoutGeo.charGroup.baran.btn = button(label='select baran')
            button(myLayoutGeo.charGroup.baran.btn, e=True, c = windows.Callback(myLayoutGeo.BaranCallback))

        with columnLayout(adjustableColumn=True):
            myLayoutGeo.charGroup.straus.btn = button(label='select straus')
            button(myLayoutGeo.charGroup.straus.btn, e=True, c = windows.Callback(myLayoutGeo.StrausCallback))

        with columnLayout(adjustableColumn=True):
            myLayoutGeo.charGroup.clBtn.btn = button(label='clear selection')
            button(myLayoutGeo.charGroup.clBtn.btn, e=True, c = windows.Callback(myLayoutGeo.CharClearCallback))

    with horizontalLayout():
        with columnLayout(adjustableColumn=True):
            myLayoutGeo.hideBtn = button(label='hide')
            button(myLayoutGeo.hideBtn, e=True, c = windows.Callback(myLayoutGeo.HideBtnCallback))

        with columnLayout(adjustableColumn=True):
            myLayoutGeo.showBtn = button(label='show')
            button(myLayoutGeo.showBtn, e=True, c = windows.Callback(myLayoutGeo.ShowBtnCallback))
            
        with columnLayout(adjustableColumn=True):
            myLayoutGeo.setKeyBtn = button(label='set in keys')
            button(myLayoutGeo.setKeyBtn, e=True, c = windows.Callback(myLayoutGeo.SetKeyCallback))   

    with horizontalLayout():
        with columnLayout(adjustableColumn=True):
            myLayoutGeo.prevCamBtn = button(label='prev camera')
            button(myLayoutGeo.prevCamBtn, e=True, c = windows.Callback(myLayoutGeo.PrevCameraBtnCallback))

        with columnLayout(adjustableColumn=True):
            myLayoutGeo.nextCamBtn = button(label='next camera')
            button(myLayoutGeo.nextCamBtn, e=True, c = windows.Callback(myLayoutGeo.NextCameraBtnCallback))
        
        with columnLayout(adjustableColumn=True):
            myLayoutGeo.setKeyBtnOut = button(label='set out keys')
            button(myLayoutGeo.setKeyBtnOut, e=True, c = windows.Callback(myLayoutGeo.SetKeyOutCallback))

    separator(horizontal=True, height=40)

    with horizontalLayout():

        with columnLayout(adjustableColumn=True):
            myLayoutAnim.SetSelectionModeBtn(button(label='select transforms: True', bgc=[0.156, 0.26, 0.47]))
            button(myLayoutAnim.GetSelectionModeBtn(), e=True, c = windows.Callback(myLayoutAnim.SelectionModeBtnCallback))

            text(label="", height=15, bgc=[.27, .27, .27])
            text("character", height=20, bgc=[.082, .164, .321])

            myLayoutAnim.charGroup.baran.btn = button(label='baran')
            button(myLayoutAnim.charGroup.baran.btn, e=True, c = windows.Callback(myLayoutAnim.BaranCallback))

            myLayoutAnim.charGroup.straus.btn = button(label='straus')
            button(myLayoutAnim.charGroup.straus.btn, e=True, c = windows.Callback(myLayoutAnim.StrausCallback))

            myLayoutAnim.charGroup.allBtn.btn = button(label='both')
            button(myLayoutAnim.charGroup.allBtn.btn, e=True, c = windows.Callback(myLayoutAnim.CharAllCallback))

            framesTextField=textField(edit=0, tx='0')
        
        with columnLayout(adjustableColumn=True):
            text(label="", height=23, bgc=[.27, .27, .27])
            text(label="", height=15, bgc=[.27, .27, .27])
            text("body part", height=20, bgc=[.082, .164, .321])

            myLayoutAnim.bodyGroup.face.btn = button(label='face')
            button(myLayoutAnim.bodyGroup.face.btn, e=True, c = windows.Callback(myLayoutAnim.FaceCallback))

            myLayoutAnim.bodyGroup.body.btn = button(label='body')
            button(myLayoutAnim.bodyGroup.body.btn, e=True, c = windows.Callback(myLayoutAnim.BodyCallback))

            myLayoutAnim.clSelectionBtn = button(label='clear selection')
            button(myLayoutAnim.clSelectionBtn, e=True, c=windows.Callback(myLayoutAnim.ClearSelectionCallback))
            # myLayout.bodyGroup.allBtn.btn = button(label='both')
            # button(myLayout.bodyGroup.allBtn.btn, e=True, c = windows.Callback(myLayout.BodyAllCallback))

            with horizontalLayout():
                myLayoutAnim.lShiftButton = button(label='<<')
                myLayoutAnim.rShiftButton = button(label='>>')
                
                button(myLayoutAnim.lShiftButton, e=True, c=windows.Callback(myLayoutAnim.LelftShiftCallback, framesTextField))
                button(myLayoutAnim.rShiftButton, e=True, c=windows.Callback(myLayoutAnim.RightShiftCallback, framesTextField)) 

win.show()