//#ifndef _maya_xml_camera_readerNode
//#define _maya_xml_camera_readerNode

#pragma once

#include <maya/MPxNode.h>
#include <maya/MTypeId.h> 
 
class maya_xml_camera_reader : public MPxNode
{
public:

	static MTypeId  id;

	static MObject  time;
	static MObject  test;

	static MObject loadButtonAttr;
	static MObject loadedStatusFieldAttr;
	static MObject pathAttr;

public:

	maya_xml_camera_reader() {};
	~maya_xml_camera_reader() override {};

	static  void*   creator()
	{
		return new maya_xml_camera_reader;
	}

	static  MStatus initialize();

	MStatus compute(const MPlug& plug, MDataBlock& data) override;	
	
private:

	static std::pair<std::string, std::string> retriveSceneFileNameAndPath(const std::string path);
};

//#endif