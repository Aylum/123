#include "camera_node.h"
//#include "xml_reader.h"

#include <maya/MDataHandle.h>
#include <maya/MGlobal.h>
#include <maya/MTime.h>
#include <maya/MFnMesh.h>
#include <maya/MPoint.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
//#include <maya/MFnPlugin.h>
#include <maya/MPxNode.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MFnMeshData.h>
#include <maya/MIOStream.h>

#define McheckErr(stat,msg)         \
    if ( MS::kSuccess != stat )		\
	{								\
        cerr << msg;                \
        return MS::kFailure;        \
    }

MTypeId camera_node::id(0x80510);
MObject camera_node::time;
MObject camera_node::outputMesh;
MObject camera_node::test_attr;

void* camera_node::creator()
{
	return new camera_node;
}

MStatus camera_node::initialize()
{
	//xml_reader& reader = xml_reader::getInstance();
	//reader.read_xml("bla.json");

	MFnUnitAttribute unitAttr;
	MFnTypedAttribute typedAttr;
	MFnNumericAttribute num_attr_test;
	MStatus returnStatus;

	//create
	camera_node::time = unitAttr.create("time", "tm", MFnUnitAttribute::kTime, 0.0, &returnStatus);
	McheckErr(returnStatus, "ERROR creating animCube time attribute\n");

#pragma warning(suppress : 4996)
	camera_node::outputMesh = typedAttr.create("outputMesh", "out", MFnData::Type::kMesh, &returnStatus);
	McheckErr(returnStatus, "ERROR creating animCube output attribute\n");
	typedAttr.setStorable(false);

	camera_node::test_attr = num_attr_test.create("testAttr", "tsat", MFnNumericData::kInt, 0.0, &returnStatus);
	McheckErr(returnStatus, "ERROR blabla\n");
	//num_attr_test.setStorable(false);

	//add
	returnStatus = addAttribute(camera_node::time);
	McheckErr(returnStatus, "ERROR adding time attribute\n");

	returnStatus = addAttribute(camera_node::test_attr);
	McheckErr(returnStatus, "ERROR adding test attribute\n");

	returnStatus = addAttribute(camera_node::outputMesh);
	McheckErr(returnStatus, "ERROR adding outputMesh attribute\n");

	//affects
	returnStatus = attributeAffects(camera_node::time, camera_node::outputMesh);
	McheckErr(returnStatus, "ERROR in attributeAffects\n");

	returnStatus = attributeAffects(camera_node::time, camera_node::test_attr);
	McheckErr(returnStatus, "ERROR in attributeAffects\n");

	return MS::kSuccess;
}

MStatus camera_node::compute(const MPlug& plug, MDataBlock& data)
{
	MStatus returnStatus;

	if (plug == outputMesh)
	{
		/* Get time */
		MDataHandle timeData = data.inputValue(time, &returnStatus);
		McheckErr(returnStatus, "Error getting time data handle\n");

		MTime time = timeData.asTime();

		/* Get output object */
		MDataHandle outputHandle = data.outputValue(outputMesh, &returnStatus);
		McheckErr(returnStatus, "ERROR getting polygon data handle\n");

		MDataHandle out_test_attr_handle = data.outputValue(test_attr, &returnStatus);
		McheckErr(returnStatus, "ERROR getting test_attr data\n");

		//create new data
		MFnMeshData dataCreator;
		MObject newOutputData = dataCreator.create(&returnStatus);
		McheckErr(returnStatus, "ERROR creating outputData");

		MFnNumericData data_test_attr_creator;
		MObject new_test_out_data = data_test_attr_creator.create(MFnNumericData::kInt, &returnStatus);
		McheckErr(returnStatus, "ERROR creating test_out_data");

		//fill data
		createMesh(time, newOutputData, returnStatus);
		McheckErr(returnStatus, "ERROR creating new Cube");

		//xml_reader& reader = xml_reader::getInstance();
		int frame = (int)time.as(MTime::kFilm);
		//int camera = reader.getCamera(frame);
		int camera = 6;
		
		returnStatus = ((MFnNumericData)new_test_out_data).setData(camera, camera);
		McheckErr(returnStatus, "ERROR setData(int, int)\n");

		//set
		outputHandle.set(newOutputData);
		out_test_attr_handle.set(new_test_out_data);

		data.setClean(plug);
	}
	else
		return MS::kUnknownParameter;

	return MS::kSuccess;
}

MObject camera_node::createMesh(const MTime& time, MObject& outData, MStatus& stat)
{
	// Scale the cube on the frame number, wrap every 10 frames.
	//
	const int frame = (int)time.as(MTime::kFilm);
	const float cubeSize = 0.5f * (float)(frame % 10 + 1);

	MFloatPointArray points;
	points.append(-cubeSize, -cubeSize, -cubeSize);
	points.append(cubeSize, -cubeSize, -cubeSize);
	points.append(cubeSize, -cubeSize, cubeSize);
	points.append(-cubeSize, -cubeSize, cubeSize);
	points.append(-cubeSize, cubeSize, -cubeSize);
	points.append(-cubeSize, cubeSize, cubeSize);
	points.append(cubeSize, cubeSize, cubeSize);
	points.append(cubeSize, cubeSize, -cubeSize);

	MObject newMesh;
	static const bool sTestVertexIdAndFaceId = (getenv("MAYA_TEST_VERTEXID_AND_FACEID") != NULL);

	if (sTestVertexIdAndFaceId)
	{
		// If the env var is set, the topology of the cube will be changed over
		// frame number (looping in every 4 frames). When the shape is assigned
		// with a hwPhongShader, the shader receives vertex ids and face ids,
		// which are generated from polygonConnects passed to MFnMesh::create
		// method in this plugin.
		//
		//	switch (frame % 4)
		//	{
		//	case 1:
		//		newMesh = createQuads(points, outData, stat);
		//		break;
		//	case 2:
		//		newMesh = createReverseQuads(points, outData, stat);
		//		break;
		//	case 3:
		//		newMesh = createTris(points, outData, stat);
		//		break;
		//	case 0:
		//		newMesh = createReverseTris(points, outData, stat);
		//		break;
		//	default:
		//		newMesh = createQuads(points, outData, stat);
		//		break;
		//	}
		//}
		//else
		//{
		//	newMesh = createQuads(points, outData, stat);
	}

	return newMesh;
}

#ifdef McheckErr
#undef McheckErr

#endif // McheckErr
