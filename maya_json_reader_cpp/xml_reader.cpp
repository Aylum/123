#include <fstream>
#include <iostream>

#include "xml_reader.h"

void to_json(nlohmann::json& j, const camera_info& p)
{
	using namespace nlohmann;
	j = json{ { "camera", p.camera },{ "start", p.start },{ "end", p.end } };
}

void from_json(const nlohmann::json& j, camera_info& p)
{
	j.at("camera").get_to(p.camera);
	j.at("start").get_to(p.start);
	j.at("end").get_to(p.end);
}

std::string xml_reader::read_xml(const std::string& path)
{
	using namespace nlohmann;
	using namespace std;

	ifstream file(path, ios::in);

	if (!file)
	{
		cerr << "xml_reader::read_xml() : couldn't open file at path " + path + '\n';
		return "couldn't load file at this path";
	}

	try
	{
		cameras.clear();

		json jsonObj;
		file >> jsonObj;

		cameras = jsonObj["list"].get<list<camera_info>>();//check what happens if file doesn't contain "list"
		cameras_iter = cameras.begin();

		jsonObj.clear();
		file.close();

		filePath = path;

		//std::string res = "success";
	}
	catch (std::exception& e)
	{
		cerr << e.what() << '\n';
		cameras.clear();
		cameras_iter = cameras.end();

		if(file)
			file.close();	

		return e.what();
	}
	
	return "success";
}

int xml_reader::getCamera(int curFrame)
{
	int res = 0;
		
	//for (auto& el : cameras)
	//{
	//	if (curFrame >= el.start && curFrame < el.end)
	//	{
	//		return el.camera;
	//		//break;
	//	}
	//}

	if (cameras.size() == 0)
		return 0;

	if (cameras_iter == cameras.end())
		cameras_iter = cameras.begin();

	try
	{
		bool is_iter_at_begin = cameras.begin() == cameras_iter;
		bool is_found = false;

		for (; cameras_iter != cameras.end(); cameras_iter++)
		{
			if (curFrame >= cameras_iter->start && curFrame < cameras_iter->end)
			{
				res = cameras_iter->camera;
				is_found = true;
				break;
			}
		}

		if (cameras_iter == cameras.end())
		{
			if (!is_iter_at_begin && !is_found)
			{
				for (cameras_iter = cameras.begin(); cameras_iter != cameras.end(); cameras_iter++)
				{
					if (curFrame >= cameras_iter->start && curFrame < cameras_iter->end)
					{
						res = cameras_iter->camera;
						break;
					}
				}
			}

			cameras_iter = cameras.begin();
		}
	}
	catch (std::exception&)
	{

	}

	

	return res;
}