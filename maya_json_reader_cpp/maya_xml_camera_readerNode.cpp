#include <maya/MTime.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericData.h>
#include <maya/MObject.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MIOStream.h>
#include <maya/MGlobal.h>
#include <maya/MStreamUtils.h>
#include <maya/MFileIO.h>
#include <maya/MString.h>
#include <maya/MFnStringData.h>

//#include <maya/MFnDependencyNode.h>
//#include <maya/MDagModifier.h>

#include "maya_xml_camera_readerNode.h"
#include "xml_reader.h"

MStatus returnStatus;

#define McheckErr(stat,msg)         \
    if ( MS::kSuccess != stat )		\
	{								\
        cerr << msg;                \
        return MS::kFailure;        \
    }

MTypeId maya_xml_camera_reader::id(0x81703);

MObject maya_xml_camera_reader::time;
MObject maya_xml_camera_reader::test;

MObject maya_xml_camera_reader::loadButtonAttr;
MObject maya_xml_camera_reader::loadedStatusFieldAttr;
MObject maya_xml_camera_reader::pathAttr;

MStatus maya_xml_camera_reader::initialize()
{
	MFnUnitAttribute unitAttr;//time
	MFnNumericAttribute numerAttr;//test (camera)
	MFnNumericAttribute loadBtnAttrAttr;
	MFnTypedAttribute loadedStatAttrAttr;
	MFnTypedAttribute pathAttrAttr;

	MStatus returnStatus;

	maya_xml_camera_reader::time = unitAttr.create("time", "tm", MFnUnitAttribute::kTime, 0.0, &returnStatus);
	McheckErr(returnStatus, "ERROR creating animCube time attribute\n");
	
	maya_xml_camera_reader::test = numerAttr.create("camera", "tstt", MFnNumericData::kInt, 0.0, &returnStatus);
	McheckErr(returnStatus, "ERROR creating animCube test attribute\n");
	//numerAttr.setStorable(false);

	maya_xml_camera_reader::loadButtonAttr = loadBtnAttrAttr.create("loadXml", "lxml", MFnNumericData::kBoolean, 0.0, &returnStatus);
	McheckErr(returnStatus, "ERROR creating loadButton attribute\n");
	
	MString strStatus("");
	MString pathMStr("");
	MString currentScene(MFileIO::currentFile());
	std::string currentSceneStr(currentScene.asChar());
	if (currentSceneStr != "" && currentSceneStr.length() > 3)
	{
		std::pair<std::string, std::string> retreivedFileNameAndPath = maya_xml_camera_reader::retriveSceneFileNameAndPath(currentSceneStr);

		size_t slash_pos = retreivedFileNameAndPath.second.find_last_of('/');
		if (slash_pos != std::string::npos)
		{
			std::string parentFolderPath = retreivedFileNameAndPath.second.substr(0, slash_pos);

			std::string pathToJson = parentFolderPath + '/' + "ep" + retreivedFileNameAndPath.first[2] + retreivedFileNameAndPath.first[3] + ".json";

			xml_reader::getInstance().read_xml(pathToJson);

			pathMStr.set(pathToJson.c_str());
			strStatus.set(std::string("xml loaded, clips found: " + std::to_string(xml_reader::getInstance().getListCount()) + ", path: " + pathToJson).c_str());
		}
	}

	//create status attribute
	MFnStringData statusFieldDefVal;
	maya_xml_camera_reader::loadedStatusFieldAttr = loadedStatAttrAttr.create("loadedXmlStatus", "ldxmls", MFnStringData::kString, statusFieldDefVal.create(strStatus), &returnStatus);
	McheckErr(returnStatus, "ERROR creating loaded status attribute\n");

	//create path attribute
	MFnStringData pathDefVal;
	maya_xml_camera_reader::pathAttr = pathAttrAttr.create("path", "pth", MFnStringData::kString, pathDefVal.create(pathMStr), &returnStatus);
	McheckErr(returnStatus, "ERROR creatind path attribute\n");

	//add atributes
	returnStatus = addAttribute(maya_xml_camera_reader::time);
	McheckErr(returnStatus, "ERROR adding time attribute\n");

	returnStatus = addAttribute(maya_xml_camera_reader::test);
	McheckErr(returnStatus, "ERROR adding test attribute\n");

	returnStatus = addAttribute(maya_xml_camera_reader::loadButtonAttr);
	McheckErr(returnStatus, "ERROR adding load button attribute\n");

	returnStatus = addAttribute(maya_xml_camera_reader::loadedStatusFieldAttr);
	McheckErr(returnStatus, "ERROR addin loaded status field attribute\n");

	returnStatus = addAttribute(maya_xml_camera_reader::pathAttr);
	McheckErr(returnStatus, "ERROR adding path attribute\n");

	//attribute affects
	returnStatus = attributeAffects(maya_xml_camera_reader::time, maya_xml_camera_reader::test);
	McheckErr(returnStatus, "ERROR in attributeAffects\n");

	returnStatus = attributeAffects(maya_xml_camera_reader::loadButtonAttr, maya_xml_camera_reader::loadedStatusFieldAttr);

	//xml_reader& xml = xml_reader::getInstance();
	//xml.read_xml(R"(C:\Users\Crusteritto\Documents\Visual Studio 2015\Projects\maya_xml_camera_reader\maya_xml_camera_reader\Release\bla.json)");//path C:\Users\Crusteritto\Documents\Visual Studio 2015\Projects\maya_xml_camera_reader\maya_xml_camera_reader\Release

	//MSelectionList previous_list;
	//returnStatus = MGlobal::getActiveSelectionList(previous_list);

	//MFnDependencyNode fn;
	//fn.create(maya_xml_camera_reader::id);
	//MString nodeName = fn.name();

	//returnStatus = MGlobal::selectByName("time1", MGlobal::kReplaceList);
	//McheckErr(returnStatus, "ERROR in selecting node time1\n");

	//MSelectionList sl;
	//returnStatus = MGlobal::getActiveSelectionList(sl);
	//McheckErr(returnStatus, "ERROR getting active selection list\n");

	//MObject oTime;
	//returnStatus = sl.getDependNode(0, oTime);
	//McheckErr(returnStatus, "ERROR getting depend node from selection list\n");

	//MFnDependencyNode fnTime(oTime);
	//MPlug outTime = fnTime.findPlug("outTime", &returnStatus);
	//McheckErr(returnStatus, "ERROR Could not get time1.outTime attr\n");

	//MPlug inTime = fn.findPlug("time");
	//
	//MDagModifier m_Mod;
	//m_Mod.connect(outTime, inTime);

	//returnStatus = MGlobal::setActiveSelectionList(previous_list);

	return MS::kSuccess;
}

MStatus maya_xml_camera_reader::compute(const MPlug& plug, MDataBlock& data)
{
	MStatus returnStatus;

	if (plug == maya_xml_camera_reader::test)
	{
		//MStreamUtils::stdOutStream() << MFileIO::currentFile() << "\n";

		

		MDataHandle timeData = data.inputValue(time, &returnStatus);
		McheckErr(returnStatus, "Error getting time data handle\n");

		MTime time = timeData.asTime();
		int frame = (int)time.as(MTime::k25FPS);
		const int camera = frame;

		MDataHandle outHandle = data.outputValue(test, &returnStatus);
		McheckErr(returnStatus, "ERROR getting test attr");

		xml_reader& instance = xml_reader::getInstance();

		int a = instance.getCamera(frame);

		outHandle.setInt(a);

		data.setClean(plug);
	}
	else if (plug == maya_xml_camera_reader::loadedStatusFieldAttr)
	{
		MDataHandle inBtnData = data.inputValue(loadButtonAttr, &returnStatus);
		McheckErr(returnStatus, "ERROR getting input load btn attr\n");

		if (inBtnData.asBool())
		{
			MDataHandle buttonData = data.outputValue(loadButtonAttr, &returnStatus);
			McheckErr(returnStatus, "ERROR getting load btn attr\n");
			buttonData.setBool(false);

			MDataHandle pathData = data.inputValue(pathAttr, &returnStatus);
			McheckErr(returnStatus, "ERROR getting pathAttr\n");

			MString pathMStr = pathData.asString();

			MString strStatus("");

			if (std::string(pathMStr.asChar()) == "")
				strStatus.set("path is empty, xml will not be loaded. Specify xml path and press 'load'\n");
			else
			{
				std::pair<std::string, std::string> retreivedFileNameAndPath = maya_xml_camera_reader::retriveSceneFileNameAndPath(pathMStr.asChar());

				std::string jsonFileName("");

				if (retreivedFileNameAndPath.first.length() > 3
					&& std::string(&retreivedFileNameAndPath.first[0]) == "e"
					&& std::string(&retreivedFileNameAndPath.first[1]) == "p"
					)
					jsonFileName = "ep" + retreivedFileNameAndPath.first[2] + retreivedFileNameAndPath.first[3] + std::string(".json");
				else
					jsonFileName = retreivedFileNameAndPath.first + ".json";

				//xml_reader::getInstance().read_xml(retreivedFileNameAndPath.second + '/' + jsonFileName);
				xml_reader::getInstance().read_xml(pathMStr.asChar());
				//strStatus.set(std::string("xml loaded, clips found: " + std::to_string(xml_reader::getInstance().getListCount()) + ", path: " + retreivedFileNameAndPath.second + '/' + jsonFileName).c_str());
				//strStatus.set(std::string("xml loaded, clips found: " + std::to_string(xml_reader::getInstance().getListCount()) + ", path: " + pathMStr.asChar()).c_str());
				strStatus.set(std::string("xml loaded, clips found: " + std::to_string(xml_reader::getInstance().getListCount()) + ", path: " + xml_reader::getInstance().getFilePath()).c_str());
			}

			MDataHandle outStatusData = data.outputValue(loadedStatusFieldAttr, &returnStatus);
			McheckErr(returnStatus, "ERROR getting load status attr\n");

			outStatusData.setString(strStatus);

			data.setClean(plug);
		}

	}
	else
		return MS::kUnknownParameter;

	return MS::kSuccess;
}

inline std::pair<std::string, std::string> maya_xml_camera_reader::retriveSceneFileNameAndPath(const std::string path)
{
	using namespace std;

	if (path == "")
		return pair<string, string>("", "");

	size_t slashPos = path.find_last_of("/\\");

	if (slashPos == string::npos)
		return pair<string, string>("", "");

	string str1;
	string str2;

	std::string fileNameWOutExt("");
	std::string pathRes("");

	try
	{
		str1 = path.substr(0, slashPos);//path
		string str1Replaced(str1.length(), 0);
		for (int i = 0; i < str1.length(); i++)
		{
			if (str1[i] == '\\')
				str1Replaced[i] = '/';
			else
				str1Replaced[i] = str1[i];
		}
		pathRes = str1Replaced;

		str2 = path.substr(slashPos + 1);//name

		size_t dotPos = str2.find_last_of('.');
		if (dotPos != std::string::npos)
			fileNameWOutExt = str2.substr(0, dotPos);
		else
			fileNameWOutExt = str2;

		//return pair<string, string>(str1, s);
	}
	catch (exception&)
	{
		return pair<string, string>("", "");
	}

	return pair<string, string>(fileNameWOutExt, pathRes);
}