#pragma once

#include <string>
#include <list>

#include "nlohmann/json.hpp"

namespace json_defs
{
	//using namespace nlohmann;

	

	//void to_json(json& j, const camera_info& p)
	//{
	//	j = json{ { "camera", p.camera },{ "start", p.start },{ "end", p.end } };
	//}

	//void from_json(const json& j, camera_info& p)
	//{
	//	j.at("camera").get_to(p.camera);
	//	j.at("start").get_to(p.start);
	//	j.at("end").get_to(p.end);
	//}
}

struct camera_info
{
	int camera;
	int start;
	int end;
};

class xml_reader
{
private:

	std::list<camera_info> cameras;
	std::list<camera_info>::iterator cameras_iter;
	std::string filePath;

private:

	xml_reader() {}

public:

	xml_reader(xml_reader const&) = delete;
	void operator=(xml_reader const&) = delete;

	static xml_reader& getInstance()
	{
		static xml_reader instance;
		
		return instance;
	}

	std::string read_xml(const std::string& path);

	int getCamera(int curFrame);
	int getListCount() { return cameras.size(); }
	std::string getFilePath() { return filePath; }
};