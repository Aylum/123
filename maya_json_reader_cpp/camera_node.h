#pragma once

#include <maya/MPxNode.h>
#include <maya/MTypeId.h> 

class camera_node : public MPxNode
{
public:
	static MTypeId id;

	static MObject time;
	static MObject outputMesh;
	static MObject test_attr;

public:

	static void* creator();
	static MStatus initialize();

	camera_node() {};
	virtual ~camera_node() {};

	virtual MStatus compute(const MPlug& plug, MDataBlock& data);

protected:

	MObject createMesh(const MTime& time, MObject& outData, MStatus& stat);
};