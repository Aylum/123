#include <maya/MFnPlugin.h>

#include "maya_xml_camera_readerNode.h"

MStatus initializePlugin( MObject obj )
{ 
	MStatus   status;
	MFnPlugin plugin( obj, "ILYA", "2019", "Any");

	status = plugin.registerNode("maya_xml_camera_reader", maya_xml_camera_reader::id, maya_xml_camera_reader::creator, maya_xml_camera_reader::initialize);
	if (!status) {
		status.perror("registerNode");
		return status;
	}

	return status;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   status;
	MFnPlugin plugin( obj );

	status = plugin.deregisterNode( maya_xml_camera_reader::id );
	if (!status) {
		status.perror("deregisterNode");
		return status;
	}

	return status;
}
