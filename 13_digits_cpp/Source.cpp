#include <iostream>
#include <string>
#include <set>

using namespace std;

struct SavedComparator
{
	bool operator()(const string& left, const string& right) const
	{
		if (left.length() != right.length()) {
			return false;
		}
		for (uint8_t i = 0; i < left.length(); i++) {
			if (left[i] == right[i]) {
				continue;
			}
			return left[i] < right[i];
		}
		return false;
	}
};

class GetSum
{
private:
	set<string, SavedComparator> saved;
	SavedComparator cmp;
	int depth;

public:
	GetSum() {}

	void init(const string& startingSum = {12, 12, 12, 12, 12, 12}, int depth = 0)
	{
		this->saved = set<string, SavedComparator>(cmp);
		this->saved.emplace(startingSum);
		this->depth = depth;
	}

	const set<string, SavedComparator>& getCurrent()
	{
		return this->saved;
	}

	const set<string, SavedComparator>& operator()()
	{
		set<string, SavedComparator> newSaved(cmp);
		size_t depth = this->depth + 1;
		for (auto& savedIt : this->saved) {
			for (int i = 0; i < 6; i++) {
				char val = savedIt[i] - char(1);
				if (val < 0) {
					continue;
				}
				string temp1 = savedIt;
				temp1[i] = val;
				newSaved.emplace(move(temp1));
			}
		}

		this->saved = newSaved;
		return this->saved;
	}


};

int main() {

	GetSum sum;
	sum.init();

	auto saved = sum.getCurrent();
	size_t res = 0;
	while (saved.size()) {
		res += saved.size() * saved.size() * 13;
		saved = sum();
	}

	cout << res << '\n';

	return 0;
}