﻿using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace xmlCamerasToJsonZaryadka
{
    [System.Serializable]
    public class CameraInfo
    {

        [SerializeField] public int camera = 0;
        [SerializeField] public int start = 0;
        [SerializeField] public int end = 0;

        public CameraInfo() { }

        public CameraInfo(int start, int end, int camera)
        {
            this.start = start;
            this.end = end;
            this.camera = camera;
        }
    }

    [System.Serializable]
    public class ListToJson
    {
        [SerializeField] public List<CameraInfo> list = new List<CameraInfo>();
    }

    public class XmlTreats
    {
        public class ClipInfo
        {
            public string masterclipid = "";
            public string filename = "";
            public string start = "";
            public string end = "";
        }

        public static List<xmlCamerasToJsonZaryadka.CameraInfo> ProcessXml(string filePath, int capacity)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;

            try
            {
                xmlDoc.Load(filePath);
            }
            catch (System.IO.FileNotFoundException /*e*/)
            {
                Debug.Log("can't find xml file with path " + filePath);
                return null;
            }

            List<xmlCamerasToJsonZaryadka.CameraInfo> res = null;

            var firstList = xmlDoc.ChildNodes;
            for (int i = 0; i < firstList.Count; i++)
            {
                XmlNode sequenceNode = RecursiveSearch(firstList.Item(i), "sequence");
                if (sequenceNode != null)
                {
                    res = ProcessSequence(sequenceNode, capacity);

                    break;
                }

                //XmlNode parentSequenceNode = RecursiveSearchParent(firstList.Item(i), "sequence");
                //if (parentSequenceNode != null)
                //{
                //    var childrenNodes = parentSequenceNode.ChildNodes;
                //    for (int k = 0; k < childrenNodes.Count; k++)
                //    {
                //        if (childrenNodes.Item(k).Name.Contains("sequence"))
                //        {
                //            ProcessSequence(childrenNodes.Item(k), ref res);
                //        }
                //    }
                //    break;//maybe remove that
                //}
            }

            return res;
        }

        private static List<xmlCamerasToJsonZaryadka.CameraInfo> ProcessSequence(XmlNode node, int capacity)
        {
            node = RecursiveSearch(node, "media");
            if (node == null)
            {
                Debug.Log("can't fine node with name 'media'");
                return null;
            }

            node = RecursiveSearch(node, "video");
            if (node == null)
            {
                Debug.Log("can't find node  with name 'video'");
                return null;
            }

            SortedList<int, ClipInfo> clips = new SortedList<int, ClipInfo>(capacity);

            var videoChildren = node.ChildNodes;
            for (int i = 0; i < videoChildren.Count; i++)
            {
                if (videoChildren[i].Name.Contains("track") && videoChildren[i]["clipitem"] != null)
                {
                    var trackChildren = videoChildren[i].ChildNodes;

                    for (int k = 0; k < trackChildren.Count; k++)
                    {
                        if (trackChildren[k].Name.Contains("clipitem"))
                        {
                            var fileNode = trackChildren[k]["file"];
                            if (fileNode == null)
                            {
                                Debug.Log("can't find node with name 'file'");
                                continue;
                            }

                            var nameNode = trackChildren[k]["name"];
                            if (nameNode == null)
                            {
                                //Debug.Log("can't find node with name 'name'");
                                continue;
                            }

                            ClipInfo clip = new ClipInfo();
                            clip.filename = nameNode.InnerText;

                            clip.masterclipid = "";

                            foreach (XmlAttribute attr in trackChildren[k].Attributes)
                            {
                                if (attr.Value.ToLower().Contains("clipitem"))
                                {
                                    clip.masterclipid = attr.Value;
                                    if (!attr.Name.ToLower().Contains("id"))
                                    {
                                        Debug.Log("clipitem id attribute doesn't contains 'clipitem'");
                                    }
                                    break;
                                }
                            }

                            var masterclipNode = trackChildren[k]["masterclipid"];
                            if (masterclipNode == null)
                                continue;

                            //clip.masterclipid = masterclipNode.InnerText;

                            var startNode = trackChildren[k]["start"];
                            if (startNode == null)
                                continue;

                            clip.start = startNode.InnerText;

                            var endNode = trackChildren[k]["end"];
                            if (endNode == null)
                                continue;

                            clip.end = endNode.InnerText;

                            try
                            {
                                int key;
                                if (Int32.TryParse(clip.start, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out key))
                                    clips.Add(key, clip);
                                else
                                    Debug.Log("couldn't find id=clipitem-...");
                            }
                            catch (System.ArgumentException) { }

                        }
                    }

                    break;//stop processing videoChildren
                }
            }

            return ProcessClipInfo(clips, capacity);
        }

        private static List<xmlCamerasToJsonZaryadka.CameraInfo> ProcessClipInfo(SortedList<int, ClipInfo> clips, int capacity)
        {
            List<xmlCamerasToJsonZaryadka.CameraInfo> res = new List<xmlCamerasToJsonZaryadka.CameraInfo>(capacity);

            foreach (var clip in clips)
            {
                xmlCamerasToJsonZaryadka.CameraInfo cameraInfo = new xmlCamerasToJsonZaryadka.CameraInfo();
                int counter = 0;

                //retrieve camera number
                if (clip.Value.filename.Contains("CA"))
                {
                    int ind1 = clip.Value.filename.IndexOf("CA") + 2;
                    int ind2 = clip.Value.filename.IndexOf("_");
                    if(ind1 != -1 && ind2 != -1 && ind1 < ind2 && ind2 - ind1 > 0)
                    {
                        string temp = clip.Value.filename.Substring(ind1, ind2 - ind1);

                        int number = 0;
                        if (Int32.TryParse(temp, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out number))
                        {
                            cameraInfo.camera = number;
                            counter++;
                        }
                    }
                }
                else if (clip.Value.filename.ToLower().Contains("graphic"))
                {
                    cameraInfo.camera = 0;
                    counter++;
                }

                int start = 0;
                if (Int32.TryParse(clip.Value.start, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out start))
                {
                    cameraInfo.start = start;
                    counter++;
                }


                int end = 0;
                if (Int32.TryParse(clip.Value.end, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out end))
                {
                    cameraInfo.end = end;
                    counter++;
                }

                if (counter >= 3)
                    res.Add(cameraInfo);
            }

            return res;
        }

        private static XmlElement RecursiveSearch(XmlNode node, string name) //test this method
        {
            var temp = node[name];
            if (temp != null)
                return temp;

            var nodeChildren = node.ChildNodes;
            for (int i = 0; i < nodeChildren.Count; i++)
            {
                var res = RecursiveSearch(nodeChildren.Item(i), name);
                if (res != null)
                {
                    return res;
                }
            }

            return null;
        }

        private static XmlNode RecursiveSearchParent(XmlNode node, string name) //test this method
        {
            if (node[name] != null)
                return node;

            var nodeChildren = node.ChildNodes;
            for (int i = 0; i < nodeChildren.Count; i++)
            {
                var res = RecursiveSearchParent(nodeChildren.Item(i), name);
                if (res != null)
                {
                    return res;
                }
            }

            return null;
        }
    }

}

public class xmlCamerasToJsonZaryadkaEditor : EditorWindow
{
    [MenuItem("MocapExport/xmlToJsonZaryadka")]
    private static void Init()
    {
        xmlCamerasToJsonZaryadkaEditor window = EditorWindow.GetWindow<xmlCamerasToJsonZaryadkaEditor>();
        window.Show();
    }

    xmlCamerasToJsonZaryadka.ListToJson list = new xmlCamerasToJsonZaryadka.ListToJson();

    string path;
    string fileName;

    private void OnGUI()
    {
        path = EditorGUILayout.TextField("path to xml", path);
        fileName = EditorGUILayout.TextField("xml file name", fileName);

        if (GUILayout.Button("convert"))
        {
            if (path == "" || fileName == "")
                return;

            path = path.Replace(@"\", @"/").TrimEnd('/');

            if (!fileName.Contains("."))
                fileName += ".xml";

            list.list = xmlCamerasToJsonZaryadka.XmlTreats.ProcessXml(path + "/" + fileName, 500);

            //cut of last '.'
            string fileNameWithoutPoint = "";
            int i = fileName.Length - 1;
            for (; i > -1; i--)
                if (fileName[i] == '.')
                    break;

            if (i > -1)
                fileNameWithoutPoint = fileName.Substring(0, i);
            else
                fileNameWithoutPoint = fileName;

            File.WriteAllText(path + "/" + fileNameWithoutPoint + ".json", JsonUtility.ToJson(list, true));
        }
    }
}
