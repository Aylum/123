﻿using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class CameraInfo
{
    public int camera;
    public int start;
    public int end;
}

public class XmlTreats
{
    public class ClipInfo
    {
        public string masterclipid = "";
        public string filename = "";
        public string start = "";
        public string end = "";
    }

    public static List<CameraInfo> ProcessXml(string filePath, int capacity)
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.PreserveWhitespace = true;

        try
        {
            xmlDoc.Load(filePath);
        }
        catch (System.IO.FileNotFoundException /*e*/)
        {
            Debug.Log("can't find xml file with path " + filePath);
            return null;
        }
       
        List<CameraInfo> res = null;

        var firstList = xmlDoc.ChildNodes;
        for (int i = 0; i < firstList.Count; i++)
        {
            XmlNode sequenceNode = RecursiveSearch(firstList.Item(i), "sequence");
            if(sequenceNode != null)
            {
                res = ProcessSequence(sequenceNode, capacity);

                break;
            }

            //XmlNode parentSequenceNode = RecursiveSearchParent(firstList.Item(i), "sequence");
            //if (parentSequenceNode != null)
            //{
            //    var childrenNodes = parentSequenceNode.ChildNodes;
            //    for (int k = 0; k < childrenNodes.Count; k++)
            //    {
            //        if (childrenNodes.Item(k).Name.Contains("sequence"))
            //        {
            //            ProcessSequence(childrenNodes.Item(k), ref res);
            //        }
            //    }
            //    break;//maybe remove that
            //}
        }

        return res;
    }    

    private static List<CameraInfo> ProcessSequence(XmlNode node, int capacity)
    {
        node = RecursiveSearch(node, "media");
        if (node == null)
        {
            Debug.Log("can't fine node with name 'media'");
            return null;
        }

        node = RecursiveSearch(node, "video");
        if (node == null)
        {
            Debug.Log("can't find node  with name 'video'");
            return null;
        }

        SortedList<string, ClipInfo> clips = new SortedList<string, ClipInfo>(capacity);

        var videoChildren = node.ChildNodes;
        for(int i = 0; i < videoChildren.Count; i++)
        {
            if(videoChildren[i].Name.Contains("track") && videoChildren[i]["clipitem"] != null)
            {
                var trackChildren = videoChildren[i].ChildNodes;

                for(int k = 0; k < trackChildren.Count; k++)
                {
                    if (trackChildren[k].Name.Contains("clipitem"))
                    {
                        var fileNode = trackChildren[k]["file"];
                        if (fileNode == null)
                        {
                            Debug.Log("can't find node with name 'file'");
                            continue;
                        }

                        var nameNode = trackChildren[k]["name"];
                        if(nameNode == null)
                        {
                            //Debug.Log("can't find node with name 'name'");
                            continue;
                        }

                        ClipInfo clip = new ClipInfo();
                        clip.filename = nameNode.InnerText;

                        clip.masterclipid = "";

                        foreach (XmlAttribute attr in trackChildren[k].Attributes)
                        {
                            if (attr.Value.ToLower().Contains("clipitem"))
                            {
                                clip.masterclipid = attr.Value;
                                if (!attr.Name.ToLower().Contains("id"))
                                {
                                    Debug.Log("clipitem id attribute doesn't contains 'clipitem'");
                                }
                                break;
                            }    
                        }

                        var masterclipNode = trackChildren[k]["masterclipid"];
                        if (masterclipNode == null)
                            continue;

                        //clip.masterclipid = masterclipNode.InnerText;

                        var startNode = trackChildren[k]["start"];
                        if (startNode == null)
                            continue;

                        clip.start = startNode.InnerText;

                        var endNode = trackChildren[k]["end"];
                        if (endNode == null)
                            continue;

                        clip.end = endNode.InnerText;

                        try
                        {
                            if (clip.masterclipid != "")
                                clips.Add(clip.masterclipid, clip);
                            else
                                Debug.Log("couldn't find id=clipitem-...");
                        }
                        catch (System.ArgumentException) { }

                    }
                }

                break;//stop processing videoChildren
            }
        }

        return ProcessClipInfo(clips, capacity);
    }

    private static List<CameraInfo> ProcessClipInfo(SortedList<string, ClipInfo> clips, int capacity)
    {
        List<CameraInfo> res = new List<CameraInfo>(capacity);

        foreach (var clip in clips)
        {
            CameraInfo cameraInfo = new CameraInfo();
            int counter = 0;

            //retrieve camera number
            if (clip.Value.filename.ToLower().Contains("cam"))
            {
                int i = 0;
                while (i < clip.Value.filename.Length - 3)
                {
                    if (clip.Value.filename.Substring(i, 1).ToLower().Contains("c") && clip.Value.filename.Substring(i + 1, 1).ToLower().Contains("a") && clip.Value.filename.Substring(i + 2, 1).ToLower().Contains("m"))
                        break;
                    else
                        i++;
                }

                if (i < clip.Value.filename.Length - 3)
                {
                    i += 3;

                    string temp = "";
                    while (i < clip.Value.filename.Length && "0123456789".Contains(clip.Value.filename.Substring(i, 1)))
                    {
                        temp += clip.Value.filename.Substring(i, 1);
                        i++;
                    }
                    if (temp != "")
                    {
                        int number = 0;
                        if (Int32.TryParse(temp, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out number))
                        {
                            cameraInfo.camera = number;
                            counter++;
                        }

                        temp = "";
                    }
                }
            }

            int start = 0;
            if (Int32.TryParse(clip.Value.start, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out start))
            {
                cameraInfo.start = start;
                counter++;
            }


            int end = 0;
            if (Int32.TryParse(clip.Value.end, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out end))
            {
                cameraInfo.end = end;
                counter++;
            }

            if (counter >= 3)
                res.Add(cameraInfo);
        }

        return res;
    }

    private static XmlElement RecursiveSearch(XmlNode node, string name) //test this method
    {
        var temp = node[name];
        if (temp != null)
            return temp;

        var nodeChildren = node.ChildNodes;
        for (int i = 0; i < nodeChildren.Count; i++)
        {
            var res = RecursiveSearch(nodeChildren.Item(i), name);
            if (res != null)
            {
                return res;
            }
        }

        return null;
    }

    private static XmlNode RecursiveSearchParent(XmlNode node, string name) //test this method
    {
        if (node[name] != null)
            return node;

        var nodeChildren = node.ChildNodes;
        for (int i = 0; i < nodeChildren.Count; i++)
        {
            var res = RecursiveSearchParent(nodeChildren.Item(i), name);
            if (res != null)
            {
                return res;
            }
        }

        return null;
    }
}

public class xmlReader : MonoBehaviour
{
    private string filePath = @"Assets/programma01.xml";
    public List<CameraInfo> cameras;

    void Start()
    {
        GameObject.Find("Button").GetComponent<Button>().onClick.AddListener(ProcessXml);
    }

    void Update()
    {
        
    }

    private void ProcessXml()
    {
        cameras = XmlTreats.ProcessXml(filePath, 10);
    }
}
