function AssetsPath(){}
var AssetsPathObj = new AssetsPath();

//in
AssetsPath.prototype.videoFilePath = [[""], ["m:/Zaryadka2/Source/episodes/ep", 0, "_premaster.mp4"]];

//AssetsPath.prototype.backFilesFolderPath = [[""], ["m:/Zaryadka2/Preparation/episodes/ep", 0, "/out"]];
AssetsPath.prototype.backFilesFolderPath = "m:/Zaryadka2/Source";
AssetsPath.prototype.camFolderSearchName = "cam" // for example folder named "cam1"

//out
AssetsPath.prototype.compFilesOutFolderPath = [[""], ["m:/Zaryadka2/Preparation/episodes/ep", 0, "/out"]];
AssetsPath.prototype.logFileFolderPath = [[""], ["m:/Zaryadka2/Preparation/episodes/ep", 0, "/out"]];
AssetsPath.prototype.logFileName = "zaryadka_cleanup.log";

AssetsPath.prototype.outCompFilePath = [[""], ["m:/Zaryadka2/Preparation/episodes/ep", 0, "/out/zaryadka_cleanup.aep"]];
AssetsPath.prototype.outCompName = "zaryadka_cleanup";

AssetsPath.prototype.printLog = function(message)
{
    try{
        var logFile = new File(AssetsPathObj.logFileFolderPath + "/" + AssetsPathObj.logFileName);

        if(!logFile.open("a")){
            throw logFile.error;
        }

        logFile.writeln(message);
        logFile.writeln();
        logFile.writeln("-----------------------------------------------------");

        logFile.close();
    }
    catch(err){
        writeLn("can't write to log file: " + logFileFolderPath + "/" + logFileName
        + "\nlog message: " + message + "\nexception: " + err.toString());
    }
}
function printLog(message) { AssetsPathObj.printLog(message); }

function Constants(){}
var ConstantsObj = new Constants();

Constants.prototype.fps = 25;
Constants.prototype.importPremiereProjCommandId = 5022;
Constants.prototype.premasterVideoName = [[""], ["ep", 0, "_premaster.mp4"]];
Constants.prototype.cleanupCompName = "cleanup";
Constants.prototype.compFromSecondImportPrProjName = "clipname";//choose better name
Constants.prototype.guide_premasterName = "guide_premaster";
Constants.prototype.cameraNames = [ "CA1", "CA4", "CA5" ];

function Utils(){}
var UtilsObj = new Utils();

Utils.prototype.CHECK_IF_CREATED_MODE_ENUM =
{
    PRINTLOGFUNCTION : 0,
    THROW : 1,
    ALERT : 2
};

Utils.prototype.getObjKeys = function(obj, m)
{
    var keys = [];
    for(var key in obj){
        if(!m){
            keys.push(key);
        } else if(typeof obj[key] == "function"){
            keys.push(key);
        }
    }
    return keys;
}

Utils.prototype.printLogFunction = AssetsPathObj.printLog;
Utils.prototype.checkIfFileCreated = function(item, tell_message_mode/* = 0*/, message/* = ""*/)
{
    if(item.exists){
        return true;
    }

    var resMessage = "failed to create: " + item.name + "\nerror: " + item.error;
    
    if(message != null){
        resMessage += "\nmessage2: " + message;
    }

    if(tell_message_mode == null){
        Utils.printLogFunction(resMessage);
    } else {
        if(tell_message_mode == 0){
            Utils.printLogFunction(resMessage);
        } else if(tell_message_mode == 1) {
            throw resMessage;
        } else if(tell_message_mode == 2) {
            alert(resMessage);
        }
    }
    
    return false;
}

Utils.prototype.getCompFromIported = function(imported)
{
    for(var i = 1; i < imported.items.length + 1; i++){
        if(imported.items[i] instanceof CompItem){
            return imported.items[i];
        }
    }

    return null;
}

Utils.prototype.fpsToSec = function(frame, fps)
{
    return frame * 1/fps
}

Utils.prototype.fStrJoin = function(fstr)
{
    if(fstr[1].length < 1){
        return "";
    }

    var res = "";
    for(var i = 0; i < fstr[1].length; i++)
    {
        if(typeof fstr[1][i] == 'number' && typeof fstr[1][i] != 'string')
        {
            res += fstr[0].toString();
        } else{
            res += fstr[1][i];
        }
    }

    return res;
}

function Zaryadka(){}
var ZaryadkaObj = new Zaryadka();

Zaryadka.prototype.run = function(thisObj)
{
    var myPanel = (thisObj instanceof Panel) ? thisObj : new Window("palette", "Zaryadka_cleanup", [0, 0, 50, 300]);
    myPanel.margins = 50;
    
    res="group{orientation:'column', alignment:['fill', 'fill'], alignChildren:['fill', 'fill'],\
        cutBtn: Button{text:'run'},\
    }"

    //Add resource string to panel
    myPanel.grp = myPanel.add(res);

    myPanel.grp.cutBtn.onClick = function() {

        try
        {
            //import premiere project
            if(app.version.length > 1 && app.version.slice(0, 2) == "16"){
                app.executeCommand(ConstantsObj.importPremiereProjCommandId);
            } else {
                app.executeCommand(app.findMenuCommandId("Adobe Premiere Pro Project..."));
            }

            var importedPremiereProjFolder = null;
            var importedPremiereProjFolderName = "";
            var premiereCompSearchName = "";
            var epName = "";
            for(var i = 1; i < app.project.items.length + 1; i++)
            {
                if(app.project.items[i] instanceof FolderItem
                    && app.project.items[i].parentFolder == app.project.rootFolder)
                {
                    importedPremiereProjFolder = app.project.items[i];

                    var dotIdx = importedPremiereProjFolder.name.indexOf('.');
                    if(dotIdx != -1)
                    {
                        importedPremiereProjFolderName = importedPremiereProjFolder.name.slice(0, dotIdx);
                    }

                    var tempIdx1 = importedPremiereProjFolderName.indexOf("_v_");
                    if(tempIdx1 != -1)
                    {
                        premiereCompSearchName = importedPremiereProjFolderName.slice(0, tempIdx1);

                        if(tempIdx1 + 3 < importedPremiereProjFolderName.length)
                        {
                            var vidName = importedPremiereProjFolderName.slice(tempIdx1 + 3);
                            tempIdx1 = vidName.indexOf("_");
                            if(tempIdx1 != -1 && tempIdx1 > 1)
                            {
                                epName = vidName.slice(2, tempIdx1);
                            }
                        }
                    }

                    break;
                }
                
            }

            if(importedPremiereProjFolder == null || importedPremiereProjFolderName == "")
            {
                printLog("Couldn't find premiere project folder or get it's name");
                alert("Couldn't find premiere project folder or get it's name");
                throw "Couldn't find premiere project folder or get it's name";
            }

            if(premiereCompSearchName == "")
            {
                printLog("Couldn't get name of the composition, imported from premiere project");
                alert("Couldn't get name of the composition, imported from premiere project");
                throw "Couldn't get name of the composition, imported from premiere project";
            }

            if(epName == "")
            {
                printLog("Couldn't get ep name from imported premiere project");
                alert("Couldn't get ep name from imported premiere project");
                throw "Couldn't get ep name from imported premiere project";
            }

            AssetsPathObj.videoFilePath[0][0] = epName;
            AssetsPathObj.videoFilePath = UtilsObj.fStrJoin(AssetsPathObj.videoFilePath);

            //AssetsPathObj.backFilesFolderPath[0][0] = epName;
            //AssetsPathObj.backFilesFolderPath = UtilsObj.fStrJoin(AssetsPathObj.backFilesFolderPath);

            AssetsPathObj.compFilesOutFolderPath[0][0] = epName;
            AssetsPathObj.compFilesOutFolderPath = UtilsObj.fStrJoin(AssetsPathObj.compFilesOutFolderPath);

            AssetsPathObj.logFileFolderPath[0][0] = epName;
            AssetsPathObj.logFileFolderPath = UtilsObj.fStrJoin(AssetsPathObj.logFileFolderPath);

            AssetsPathObj.outCompFilePath[0][0] = epName;
            AssetsPathObj.outCompFilePath = UtilsObj.fStrJoin(AssetsPathObj.outCompFilePath);

            ConstantsObj.premasterVideoName[0][0] = epName;
            ConstantsObj.premasterVideoName = UtilsObj.fStrJoin(ConstantsObj.premasterVideoName);

            //check if background footage folder exists
            var backFolder = new Folder(AssetsPathObj.backFilesFolderPath);

            if(!backFolder.exists){
                printLog("Can't find folder which contains background files, path: "
                + AssetsPathObj.backFilesFolderPath);
                alert("Can't find folder which contains background files, path: "
                + AssetsPathObj.backFilesFolderPath);
                throw "Can't find folder which contains background files, path: "
                    + AssetsPathObj.backFilesFolderPath;
            }

            var camFolders = backFolder.getFiles();

            if(camFolders.length < 1){
                printLog("Folder contains 0 folders, path: "
                    + AssetsPathObj.backFilesFolderPath);
                alert("Folder contains 0 folders, path: "
                + AssetsPathObj.backFilesFolderPath);
                throw "Folder contains 0 folders, path: "
                    + AssetsPathObj.backFilesFolderPath;
                    
            }

            //load back files into dictionary
            var backFiles = {};
            var isBackFilesImportedOk = true;

            for(var i = 0; i < camFolders.length; i++){

                if(!(camFolders[i] instanceof Folder)){
                    continue;
                }

                var camStrInd = camFolders[i].name.indexOf(AssetsPathObj.camFolderSearchName);
                if(camStrInd == -1){
                    continue;
                }

                if(camStrInd + 3 >= camFolders[i].name.length){
                    isBackFilesImportedOk = false;
                    printLog("wrong name for Backfiles folder, this folder is ignored, path: "
                            + camFolders[i].path);
                    continue;
                }

                var backFile = camFolders[i].getFiles();
                if(backFile.length < 1){
                    isBackFilesImportedOk = false;
                    printLog("Backfiles folder contains 0 files, path: "
                            + camFolders[i].path);
                    continue;
                }

                backFiles[camFolders[i].name.slice(camStrInd + 3)] = backFile[0];
            }

            if(UtilsObj.getObjKeys(backFiles, false).length < 1){
                printLog("Couldn't find any backFiles in folder at path: "
                        + backFolder.path);
                alert("Couldn't find any backFiles in folder at path: "
                    + backFolder.path);
                throw "Couldn't find any backFiles in folder at path: "
                    + backFolder.path;
            }

            if(!isBackFilesImportedOk){
                printLog("Probably couldn't find some backfiles in folder at path: "
                        + backFolder.path);
                alert("Probably couldn't find some backfiles in folder at path: "
                    + backFolder.path);
            }

            //check if out folder exists
            var tempFolder = new Folder(AssetsPathObj.compFilesOutFolderPath);
            
            if(!tempFolder.exists){
                alert("Can't find folder with path: " + AssetsPathObj.compFilesOutFolderPath);
                throw "Can't find folder with path: " + AssetsPathObj.compFilesOutFolderPath;
            }

            var logFile = null;

            //check .log folder
            var tempFolder = new Folder(AssetsPathObj.logFileFolderPath);
            if(!tempFolder.exists){
                tempFolder.create();

                alert("Can't find folder with path: " + AssetsPathObj.compFilesOutFolderPath);
            } else{
                //create .log file
                var tempFile = new File(AssetsPathObj.logFileFolderPath + "/" + AssetsPathObj.logFileName);
                if(!tempFile.open("w")){
                    alert("Couldn't create log file at path: M:/ZARYADKA O!/1_LION"
                        + "\nerror: " + tempFile.error);

                    UtilsObj.checkIfFileCreated(tempFile, new UtilsObj.CHECK_IF_CREATED_MODE_ENUM.ALERT, "couldn't create logFile");
                }
                else{
                    logFile = tempFile;
                }

                if(logFile){
                    logFile.close();
                }
            }

            //and remove all items outside that folder
            var tempRemoveList = [];
            for(var i = 1; i < app.project.items.length + 1; i++){
                if(!(app.project.items[i] instanceof FolderItem)
                    && app.project.items[i].parentFolder == app.project.rootFolder)
                {
                    tempRemoveList.push(importedPremiereProjFolder.items[i]);   
                }
            }

            for(var i = tempRemoveList.length - 1; i > -1; i--){
                tempRemoveList[i].remove();
            }

            var listToCleanup = [];
            for(var i = 1; i < importedPremiereProjFolder.items.length + 1; i++){
                listToCleanup.push(importedPremiereProjFolder.items[i]);
            }

            //search for composition to work with
            var premiereProjComp = null;

            for(var i = 0; i < listToCleanup.length; i++){
                if(listToCleanup[i] instanceof CompItem
                    && listToCleanup[i].name.indexOf(premiereCompSearchName) != -1){
                        premiereProjComp = listToCleanup[i];
                        break;
                }
            }

            if(premiereProjComp == null){
                printLog("Couldn't get composition with name: " + premiereCompSearchName
                + " from premiere project");
                alert("Couldn't get composition with name: " + premiereCompSearchName
                    + " from premiere project");
                throw "Couldn't get composition with name: " + premiereCompSearchName
                    + " from premiere project";
            }

            //move composition outside of premiereProj folder
            premiereProjComp.parentFolder = app.project.rootFolder;

            //form list with items to remove
            //and form dictionary with camera names
            var itemsToRemoveList = [];
            var cameraNamesDictionary = {};

            //and get video name from premiere project
            var prProjVideo = null;
            var camCompsList = [];
            var camFoldersDict = {};

            for(var i = 0; i < listToCleanup.length; i++){

                if(listToCleanup[i] instanceof CompItem
                    && listToCleanup[i].name.indexOf(premiereCompSearchName) != -1){
                    continue;
                } else if(listToCleanup[i] instanceof FootageItem){

                    //source.name is CA*_
                    var tempInd = listToCleanup[i].name.indexOf("CA");
                    var tempInd_ = listToCleanup[i].name.indexOf("_");

                    if(tempInd != -1
                        && tempInd_ != -1
                        && tempInd < tempInd_){

                        var tempKey = listToCleanup[i].name.slice(tempInd, tempInd_);

                        if(ConstantsObj.cameraNames.toString().indexOf(tempKey) != -1){
                            //if camera name appeared first time
                            if(!cameraNamesDictionary.hasOwnProperty(tempKey)){
                                var newFolder = app.project.items.addFolder(tempKey);
                                listToCleanup[i].parentFolder = newFolder;//move item to different folder

                                //replaceSourceFootage();

                                cameraNamesDictionary[tempKey] = newFolder;//add folder reference to dictionary

                                var duplicatedComp = premiereProjComp.duplicate();
                                duplicatedComp.name = tempKey;
                                duplicatedComp.parentFolder = newFolder;
                                camCompsList.push(duplicatedComp);//this list later will be used for adding layers to main composition

                                var layersToRemoveList = [];//remove all excess layers from current CA*_ comp
                                for(var layerIndex = 1; layerIndex < duplicatedComp.layers.length + 1; layerIndex++){
                                    if(duplicatedComp.layers[layerIndex].name.indexOf(tempKey) == -1){
                                        layersToRemoveList.push(duplicatedComp.layers[layerIndex]);
                                    }
                                }

                                for(var layerIndex = layersToRemoveList.length - 1; -1 < layerIndex; layerIndex--){
                                    layersToRemoveList[layerIndex].remove();
                                }
                            } else {//else pool existing name from dictionary
                                listToCleanup[i].parentFolder = cameraNamesDictionary[tempKey];

                                //replaceSourceFootage();
                            }

                            if(!camFoldersDict.hasOwnProperty(tempKey)){
                                camFoldersDict[tempKey] = [listToCleanup[i]];
                            } else {
                                camFoldersDict[tempKey].push(listToCleanup[i]);
                            }

                            continue;
                        }
                    } else if(listToCleanup[i].name.indexOf(ConstantsObj.premasterVideoName) != -1){ //premaster video

                        listToCleanup[i].parentFolder = app.project.rootFolder;
                        prProjVideo = listToCleanup[i];

                        continue;
                    }

                }

                //add to remove list
                itemsToRemoveList.push(listToCleanup[i]);
                
            }

            if(prProjVideo == null){
                printLog("Couldn't find find video from premiere project"
                        + "\nold name: " + ConstantsObj.premasterVideoName
                );
                alert("Couldn't find find video from premiere project"
                        + "\nold name: " + ConstantsObj.premasterVideoName
                );
            }

            //remove items
            for(var i = itemsToRemoveList.length - 1; -1 < i; i--){
                itemsToRemoveList[i].remove();
            }

            //remove foler
            importedPremiereProjFolder.remove();

            //replace footage
            for(var key in camFoldersDict) if(camFoldersDict.hasOwnProperty(key)){
                
                var camName = key.slice(2);
                var backFile = null;
                if(camName != "" && backFiles.hasOwnProperty(camName)){
                    backFile = new File(backFiles[camName]);
                }

                for(var i = 0; i < camFoldersDict[key].length; i++){

                    if(camName != "" && backFiles != null){
                        camFoldersDict[key][i].replace(backFile);
                    } else {
                        printLog("Couldn't find background file for replacing missing footage"
                                + "\nmissing footage name: " + camFoldersDict[key][i].name
                                + "\nsearch folder path: " + AssetsPathObj.backFilesFolderPath);
                    }
                }
            }

            //create cleanup comp
            var cleanupComp = app.project.items.addComp(
                ConstantsObj.cleanupCompName,
                premiereProjComp.width,
                premiereProjComp.height,
                premiereProjComp.pixelAspect,
                premiereProjComp.duration,
                premiereProjComp.frameRate
            );

            premiereProjComp.remove();

            //add layers to cleanupComp
            var tempSort = function (a, b){ return a.name < b.name; };
            camCompsList.sort(tempSort);

            //add the premaster video to layers
            var premasterVideo = cleanupComp.layers.add(prProjVideo);
            if(premasterVideo != null){
                var transformProp = premasterVideo.property("ADBE Transform Group");
                var scProp = transformProp.property("Scale");
                scProp.setValue([150, 150, scProp.value[2]]);
            }

            //add camera comps to layers
            var cam1Layer = null;
            for(var i = 0; i < camCompsList.length; i++){
                cam1Layer = cleanupComp.layers.add(camCompsList[i]);
            }

            //create mask
            if(cam1Layer != null){
                var cam1MaskProp = cam1Layer.property("Masks").addProperty("ADBE Mask Atom");
                
                if(cam1MaskProp == null){
                    printLog("Couldn't add mask to camera1");
                    alert("Couldn't add mask to camera1");
                }
                
                cam1MaskProp.property("ADBE Mask Feather").setValue([70, 70]);
                var cam1MaskShape = cam1MaskProp.property("maskShape");
                var tempShape = new Shape();
                //0,0,0,1080,1920,1080,1920,0
                tempShape.closed = cam1MaskShape.value.closed;
                tempShape.featherInterps = cam1MaskShape.value.featherInterps;
                tempShape.featherRadii = cam1MaskShape.value.featherRadii;
                tempShape.featherRelCornerAngles = cam1MaskShape.value.featherRelCornerAngles;
                tempShape.featherRelSegLocs = cam1MaskShape.value.featherRelSegLocs;
                tempShape.featherSegLocs = cam1MaskShape.value.featherSegLocs;
                tempShape.featherTensions = cam1MaskShape.value.featherTensions;
                tempShape.featherTypes = cam1MaskShape.value.featherTypes;
                tempShape.inTangents = cam1MaskShape.value.inTangents;
                tempShape.outTangents = cam1MaskShape.value.outTangents;
                tempShape.vertices = [[1254, -119], [1254, 1201], [2106, 1201], [2106, -119]]
                cam1MaskShape.setValue(tempShape);
            }

            //import prproj again
            if(app.version.length > 1 && app.version.slice(0, 2) == "16"){
                app.executeCommand(ConstantsObj.importPremiereProjCommandId);
            } else {
                app.executeCommand(app.findMenuCommandId("Adobe Premiere Pro Project..."));
            }

            importedPremiereProjFolder = null;
            tempRemoveList = [];
            for(var i = 1; i < app.project.items.length + 1; i++){

                if(app.project.items[i].parentFolder != app.project.rootFolder){
                    continue;
                }

                if(app.project.items[i] instanceof FolderItem){

                    if(app.project.items[i].name.indexOf(".prproj") != -1){
                        importedPremiereProjFolder = app.project.items[i];
                    } else if(ConstantsObj.cameraNames.toString().indexOf(app.project.items[i].name) == -1){
                        tempRemoveList.push(app.project.items[i]);
                    }
                } else if(app.project.items[i].name == prProjVideo.name){
                    continue;
                } else if(app.project.items[i].name.indexOf(ConstantsObj.cleanupCompName) == -1){
                    tempRemoveList.push(app.project.items[i]);
                }
            }

            if(importedPremiereProjFolder == null){
                printLog("After importing premiere project second time, coudn't find .prproj folder");
                alert("After importing premiere project second time, coudn't find .prproj folder");
                throw "After importing premiere project second time, coudn't find .prproj folder";
            }

            for(var i = tempRemoveList.length - 1; i > -1; i--){
                tempRemoveList[i].remove();
            }

            premiereProjComp = null;
            prProjVideo = null;
			for(var i = 1; i < importedPremiereProjFolder.items.length + 1; i++){
				if(importedPremiereProjFolder.items[i] instanceof CompItem
					&& importedPremiereProjFolder.items[i].name.indexOf(premiereCompSearchName) != -1){

					premiereProjComp = importedPremiereProjFolder.items[i];
					premiereProjComp.parentFolder = app.project.rootFolder;
                    premiereProjComp.name = ConstantsObj.compFromSecondImportPrProjName;
					break;
				}
			}
			
            for(var i = 1; i < importedPremiereProjFolder.items.length + 1; i++){
				//alert(importedPremiereProjFolder.items[i].name);
                if(importedPremiereProjFolder.items[i] instanceof FootageItem
					&& importedPremiereProjFolder.items[i].name.indexOf(ConstantsObj.premasterVideoName) != -1){

                    prProjVideo = importedPremiereProjFolder.items[i];
                    prProjVideo.parentFolder = app.project.rootFolder;

                    var splitted = prProjVideo.name.split(".");
                    prProjVideo.name = ConstantsObj.guide_premasterName + "." + splitted[splitted.length - 1];
					break;
                    //continue;
                } //else if(importedPremiereProjFolder.items[i].name == AssetsPathObj.importedFromPremiereCompName){
					//alert("1111");
                   // premiereProjComp = importedPremiereProjFolder.items[i];
                    //premiereProjComp.parentFolder = app.project.rootFolder;
                    //premiereProjComp.name = ConstantsObj.compFromSecondImportPrProjName;
               // }
            }

            if(premiereProjComp != null){

                //copy markers inside []
                var markers = [];
                for(var i = 1; i < premiereProjComp.markerProperty.numKeys + 1; i++){
                    markers.push(premiereProjComp.markerProperty.keyTime(i));
                }

                //add markers to main comp
                for(var i = 0; i < markers.length; i++){
                    cleanupComp.markerProperty.addKey(markers[i]);
                }

                for(var i = 1; i < premiereProjComp.layers.length + 1; i++){
                    if(premiereProjComp.layers[i].name.indexOf(prProjVideo.name) != -1){
                        premiereProjComp.layers[i].remove();
                        break;
                    }
                }
            } else {
				alert("after second import prProj, mainPrProjComp == null");
			}

            //add guide video, set effect on video
            var guidePremasterVideoLayer = cleanupComp.layers.add(prProjVideo);
            if(guidePremasterVideoLayer != null){
                var transformProp = guidePremasterVideoLayer.property("ADBE Transform Group");
                var scProp = transformProp.property("Scale");
                scProp.setValue([50, 50, scProp.value[2]]);

                var posProp = transformProp.property("Position");
                posProp.setValue([1630, 163, posProp.value[2]]);
            }
            guidePremasterVideoLayer.guideLayer = true;
            
            //add comp to layers, create and set mask
            var clipnameCompLayer = cleanupComp.layers.add(premiereProjComp);
            if(clipnameCompLayer != null){
                var compMaskShape = clipnameCompLayer.property("Masks").addProperty("ADBE Mask Atom").property("maskShape");
                var tempShape = new Shape();
                //0,0,0,1080,1920,1080,1920,0
                tempShape.closed = compMaskShape.value.closed;
                tempShape.featherInterps = compMaskShape.value.featherInterps;
                tempShape.featherRadii = compMaskShape.value.featherRadii;
                tempShape.featherRelCornerAngles = compMaskShape.value.featherRelCornerAngles;
                tempShape.featherRelSegLocs = compMaskShape.value.featherRelSegLocs;
                tempShape.featherSegLocs = compMaskShape.value.featherSegLocs;
                tempShape.featherTensions = compMaskShape.value.featherTensions;
                tempShape.featherTypes = compMaskShape.value.featherTypes;
                tempShape.inTangents = compMaskShape.value.inTangents;
                tempShape.outTangents = compMaskShape.value.outTangents;
                tempShape.vertices = [[-17, -11], [-17, 74], [300, 74], [300, -11]];
                compMaskShape.setValue(tempShape);
            }
            clipnameCompLayer.guideLayer = true;

            //save project

            var outFile = new File(AssetsPathObj.outCompFilePath);

            app.project.save(outFile);

            UtilsObj.checkIfFileCreated(
                outFile,
                UtilsObj.CHECK_IF_CREATED_MODE_ENUM.THROW,
                "Counldn't create out copmposition file: " + AssetsPathObj.outCompFilePath
            );
            
        }
        catch(err)
        {
            if(logFile){
                printLog("EXCEPTION: " + err.toString());
            }
            
            alert("EXCEPTION: " + err.toString());

            throw "EXCEPTION: " + err.toString();
        }
        
    };
    
    //Setup panel sizing and make panel resizable
    myPanel.layout.layout(true);
    myPanel.grp.minimumSize = myPanel.grp.size;
    myPanel.layout.resize();
    myPanel.onResizing = myPanel.onResize = function () {this.layout.resize();}

    return myPanel;
}

var myScriptPal = ZaryadkaObj.run(this);

if ((myScriptPal != null) && (myScriptPal instanceof Window)) {
        myScriptPal.center();
        myScriptPal.show();
}