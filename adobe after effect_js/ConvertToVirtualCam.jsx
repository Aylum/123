function AssetsPath(){}
var AssetsPathObj = new AssetsPath();

//in
//AssetsPath.prototype.epFolder = "e:/zaryadka_cam4";
AssetsPath.prototype.epFolder = "e:/zaryadka_cam4";
AssetsPath.prototype.effectFilePath = "e:/cam4_to_cam5_crop.ffx";
AssetsPath.prototype.effectCompPath = "e:/ec1.aep";

//out
AssetsPath.prototype.logFileFolderPath = "e:/Temp";
AssetsPath.prototype.logFileName = "ConvertToVirtualCam.log";
AssetsPath.prototype.camVideoFilesOutFolder = "out/cam5_footage";

function Constants(){}
var ConstantsObj = new Constants();

Constants.prototype.FPS = 25;
Constants.prototype.MAX_COMP_LENGTH = 1200;
Constants.prototype.SOURCE_VIDEO_SEARCH_MASK = "CA*_*.mov";
Constants.prototype.NEW_FILE_NAME_PART = "CA5";

function Utils(){}
var UtilsObj = new Utils();

Utils.prototype.printLogFunction = function(message)
{
    try{
        var logFile = new File(AssetsPathObj.logFileFolderPath + "/" + AssetsPathObj.logFileName);

        if(!logFile.open("a")){
            throw logFile.error;
        }

        logFile.writeln(message);
        logFile.writeln();
        logFile.writeln("-----------------------------------------------------");

        logFile.close();
    }
    catch(err){
        writeln("can't write to log file: " + logFileFolderPath + "/" + logFileName
        + "\nlog message: " + message + "\nexception: " + err.toString());
    }
}
function printLog(message) { UtilsObj.printLogFunction(message); }

function ConvertToVirtualCam(){}
var ConvertToVirtualCamObj = new ConvertToVirtualCam();

ConvertToVirtualCam.prototype.run = function(thisObj)
{
    var myPanel = (thisObj instanceof Panel) ? thisObj : new Window("palette", "ConvertToVirtualCam", [0, 0, 50, 300]);
    myPanel.margins = 50;
    
    res="group{orientation:'column', alignment:['fill', 'fill'], alignChildren:['fill', 'fill'],\
        n_tl: StaticText{text:'select Output Module:'},\
        outModulesList: DropDownList { alignment:['fill','top'], alignment:['fill','top'] },\
        runBtn: Button{text:'run'},\
    }"

    //Add resource string to panel
    myPanel.grp = myPanel.add(res);

    //fill outModulesList
    var tempComp = app.project.items.addComp("temp", 1280, 720, 1, 61, 25);
    
    app.project.renderQueue.items.add(tempComp);

    var rqi = app.project.renderQueue.items.add(tempComp);
    var om = rqi.outputModule(1);

    for (var i = 0; i < om.templates.length; i++){
        if (om.templates[i].indexOf("_HIDDEN") != 0){
            myPanel.grp.outModulesList.add("item", om.templates[i]);
        }
    }
    
    rqi.remove();
    tempComp.remove();

    myPanel.grp.runBtn.onClick = function()
    {

        try{

            if (myPanel.grp.outModulesList.selection == null) {
                alert("select Output Module");
                return;
            }

            app.beginSuppressDialogs();

            //create log folder
            var tempFolder = new Folder(AssetsPathObj.logFileFolderPath);
            if(!tempFolder.exists){
                tempFolder.create();

                if(!tempFolder.exists){
                    printLog("couldn't create logFolder: " + AssetsPathObj.logFileFolderPath);
                }
            }

            //create log file
            var tempFile = new File(AssetsPathObj.logFileFolderPath + "/" + AssetsPathObj.logFileName);
            if(!tempFile.open("w")){
                printLog("can't open logFile for writing: " + tempFile.error);
            }
            tempFile.close();

            //check if input folder exists
            tempFolder = new Folder(AssetsPathObj.epFolder);
            if(!tempFolder.exists){
                throw "Episodes folder doesn't exists, path: " + AssetsPathObj.epFolder;
            }

            var epFolderList = tempFolder.getFiles();

            if(epFolderList.length < 1){
                throw "Episodes folder is empty, path: " + AssetsPathObj.epFolder;
            }

            //load effect file
            var effectFile = new File(AssetsPathObj.effectFilePath);
            if(!effectFile.exists){
                throw "Effect file doesn't exists, path: " + AssetsPathObj.effectFilePath;
            }

            app.project.close(CloseOptions.DO_NOT_SAVE_CHANGES);
            app.newProject();

            var compFile = new File(AssetsPathObj.effectCompPath);
            
            var comp = null;

            var importCompOptions = new ImportOptions(compFile);
            if (importCompOptions.canImportAs(ImportAsType.PROJECT)){
                importCompOptions.importAs = ImportAsType.PROJECT;
                comp = app.project.importFile(importCompOptions).items[1];

            }

            if(comp == null){
                alert("comp == null");
            }

			var epFolderInd = 1;

            //for(var epFolderInd1 = 0; epFolderInd1 < epFolderList.length; epFolderInd1++){
			for(var epFolderInd1 = 0; epFolderInd1 < 1; epFolderInd1++){

                if(!(epFolderList[epFolderInd] instanceof Folder)){
                    continue;
                }

                try{

                    tempFolder = new Folder(epFolderList[epFolderInd].toString() + "/cam_4");
                    if(!tempFolder.exists){
                        throw "folder doesn't exists";
                    }
    
                    var endPointFolderList = tempFolder.getFiles();
    
                    if(endPointFolderList.length < 1){
                        throw "folder is empty";
                    }

                    for(var dropFolderInd = 0; dropFolderInd < endPointFolderList.length; dropFolderInd++){

                        if(!(endPointFolderList[dropFolderInd] instanceof Folder)){
                            continue;
                        }

                        var videoFilesList = endPointFolderList[dropFolderInd].getFiles(ConstantsObj.SOURCE_VIDEO_SEARCH_MASK);

                        for(var videoFileInd = 0; videoFileInd < videoFilesList.length; videoFileInd++){

                            if(!(videoFilesList[videoFileInd] instanceof File)){
                                continue;
                            }
    
                            try{
    
                                var importedVideo = null;
                                var importOpt = new ImportOptions(videoFilesList[videoFileInd]);
                                if (importOpt.canImportAs(ImportAsType.FOOTAGE)){
                                    importOpt.importAs = ImportAsType.FOOTAGE;
                                    var importedVideo = app.project.importFile(importOpt);
    
                                } else{
                                    throw "can't import file as FOOTAGE";
                                }
    
                                if(importedVideo == null){
                                    throw "importedVideo == null";
                                }
    
                                var newFileName = videoFilesList[videoFileInd].name.split(".")[0];//CA4_0000
    
                                newFileName = ConstantsObj.NEW_FILE_NAME_PART + newFileName.slice(newFileName.indexOf("_"));
    
                                // var comp = app.project.items.addComp(
                                //     epFolderList[epFolderInd].name + "_" + newFileName,
                                //     importedVideo.width,
                                //     importedVideo.height,
                                //     importedVideo.pixelAspect,
                                //     importedVideo.duration,
                                //     importedVideo.frameRate
                                // );
    
                                var dupComp = comp.duplicate();

                                dupComp.name = epFolderList[epFolderInd].name + "_" + newFileName;
                                dupComp.width = importedVideo.width;
                                dupComp.height = importedVideo.height;
                                dupComp.pixelAspect = importedVideo.pixelAspect;
                                dupComp.frameRate = importedVideo.frameRate;
                                dupComp.duration = importedVideo.duration;

                                var layer = dupComp.layers.add(importedVideo);
                                layer.moveToEnd();
                    
                                //layer.selected = true;
                                //layer.applyPreset(effectFile);
    
                                var renderItem = app.project.renderQueue.items.add(dupComp);
                                renderItem.applyTemplate("Best Settings");
    
                                renderItem.outputModules[1].file =
                                    new File(epFolderList[epFolderInd].toString() + "/cam5/" + newFileName);
                                        
                                renderItem.outputModules[1].applyTemplate(myPanel.grp.outModulesList.selection);
    
                                tempFolder = new Folder(epFolderList[epFolderInd].toString() + "/cam5");
                                if(!tempFolder.exists){
                                    tempFolder.create();
    
                                    if(!tempFolder.exists){
                                        throw "video has been imported, added to render queue. Failed to create out folder";
                                    }
                                }
    
                            } catch(err){
                                printLog("failed to import file: " + videoFilesList[videoFileInd].toString()
                                        + "\nmessage: " + err.toString());
                            }

                        }

                    }

                } catch(err){
                    printLog("EXCEPTION: path: " + epFolderList[epFolderInd].toString()
                            + "n\nmessage: " + err.toString());    
                }

            }

            app.endSuppressDialogs(true);

        } catch(err){
            printLog("EXCEPTION: " + err.toString());
        }
    }

    //Setup panel sizing and make panel resizable
    myPanel.layout.layout(true);
    myPanel.grp.minimumSize = myPanel.grp.size;
    myPanel.layout.resize();
    myPanel.onResizing = myPanel.onResize = function () {this.layout.resize();}

    return myPanel;
}

var myScriptPal = ConvertToVirtualCamObj.run(this);

if ((myScriptPal != null) && (myScriptPal instanceof Window)) {
        myScriptPal.center();
        myScriptPal.show();
}