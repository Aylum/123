{
function Queue_Tool(thisObj) {
          function myScript_buildUI(thisObj) {
                    var myPanel = (thisObj instanceof Panel) ? thisObj : new Window("palette", "Queue Tool 1.2", [0, 0, 50, 300]);
					
                    res="group{orientation:'column', alignment:['fill', 'fill'], alignChildren:['fill', 'fill'],\
						n_tl: StaticText{text:'Output Module:'},\
						tempList: DropDownList { alignment:['fill','top'], alignment:['fill','top'] },\
                           n_start: StaticText{text:'Skip layers from the start:'},\  inputPrefix: EditText{text:'0'},\
                           n_end: StaticText{text:'Skip layers from the end:'},\  inputSuffix: EditText{text:'0'},\
						n_ip: StaticText{text:'Enter the name of the episode:'},\
						inputPrefixEp: EditText{text:'ep'},\
						renderLayers: Button{text:'Add all layers to Render Queue'},\
						clearQueue: Button{text:'Clear Render Queue'},\
                    }"
                    //Add resource string to panel
					myPanel.grp = myPanel.add(res);

//					myPanel.grp.tempList.onClick = function() {
//						if (myPanel.grp.tempList.selection == null) {
//							fillTempList();
//						}
//					}
                    
					fillTempList()

					function fillTempList() {
						var activeComp = app.project.activeItem;
						var proj = app.project;
						var selection = proj.selection;
						
						if ((activeComp == null) || !(activeComp instanceof CompItem) || (app.project.activeItem instanceof CompItem)) {
//							alert("Cannot perform operation. Please select or open a single composition in the Project panel, and try again.");
							var varComp = app.project.items.addComp("queue_tool_var_comp_JUST_REMOVE", 1280, 720, 1, 61, 30)
						}
						
						var rqi = app.project.renderQueue.items.add(varComp);
						var om = rqi.outputModule(1);

						myPanel.grp.tempList.selection = null;
						myPanel.grp.tempList.removeAll();
			
						for (var i=0; i<om.templates.length; i++){
							if (om.templates[i].indexOf("_HIDDEN") != 0){
								myPanel.grp.tempList.add("item", om.templates[i]);
							}
						}
						
						if (om.templates.length > 0) {
							myPanel.grp.tempList.selection = 0;
						}
						
						rqi.remove();
						varComp.remove();
						if (app.project.activeItem instanceof CompItem) { activeComp.openInViewer(); }
					}

 
					
					myPanel.grp.renderLayers.onClick = function() {

						var myOutputPath = null;
						var myComp = app.project.activeItem;

						if (myComp != null && myComp instanceof CompItem)
						{
							myOutputPath = Folder.selectDialog("Choose destination folder:");
							if (myOutputPath == null)
								{alert ("First select the destination folder!");return;}

							// var mylayers = app.project.activeItem.layers;
							// if(mylayers)
							// {
							// 	for(k=1; k <= mylayers.length; k++){
							// 		mylayers[k].selected = false;
							// 	}
							// 	// for(k=mylayers.length; k >= 1; k--){
							// 	// 		mylayers[k].selected = true;
							// 	// }

							// 	// for(k = 1; k <= mylayers.length; k++){
							// 	// 	mylayers[k].selected = true;
							// 	// }
							// }
						}
						else{
							alert("Select the comp you want to render!");
						}

						myComp.displayStartTime = 0;
						var saveWAS = myComp.workAreaStart;
						var saveWAD = myComp.workAreaDuration;

						for (i = (myComp.numLayers - parseInt(myPanel.grp.inputSuffix.text)); i >= 1 + parseInt(myPanel.grp.inputPrefix.text); i--)
						{							
							var curLayer = myComp.layers[i];
							if (curLayer != undefined)
							{
								myComp.workAreaStart = curLayer.inPoint;

 									if(myComp.duration<curLayer.outPoint)
									{myComp.workAreaDuration = myComp.duration - curLayer.inPoint;}
								else
									{myComp.workAreaDuration = curLayer.outPoint - curLayer.inPoint;}

								if (myComp != null && myComp instanceof CompItem)
								{
									for(var layerindex = 1; layerindex < myComp.layers.length + 1; layerindex++)
									{
										myComp.layers[layerindex].enabled = false;
									}

									myComp.layers[i].enabled = true;

									var theRender = app.project.renderQueue.items.add(myComp);
									theRender.applyTemplate("Best Settings");

									theRender.setSetting("Use this frame rate", "25.0");
									theRender.setSetting("Time Span Start", curLayer.inPoint);
									theRender.setSetting("Time Span End", curLayer.inPoint + myComp.workAreaDuration);

									// theRender.timeSpanStart = myComp.workAreaStart;
									// theRender.timeSpanDuration = myComp.workAreaDuration;

									var myFile = new File(myOutputPath.fsName+"\\"
									+ myPanel.grp.inputPrefixEp.text
									+ "_scene" + ("0" + curLayer.index).slice(-2)
									+ "_" + Math.floor(curLayer.inPoint * 25)
									+ "_" + curLayer.label);

									theRender.outputModules[1].file = myFile;
									
									if (myPanel.grp.tempList.selection != null) {
										theRender.outputModules[1].applyTemplate(myPanel.grp.tempList.selection);
									}
								}
							}
							myComp.workAreaStart = saveWAS;
							myComp.workAreaDuration = saveWAD;
						}
					};
 
					myPanel.grp.clearQueue.onClick = function() {
						while (app.project.renderQueue.numItems > 0){
							app.project.renderQueue.item(app.project.renderQueue.numItems).remove();
						}
					}
					
                    //Setup panel sizing and make panel resizable
                    myPanel.layout.layout(true);
                    myPanel.grp.minimumSize = myPanel.grp.size;
                    myPanel.layout.resize();
                    myPanel.onResizing = myPanel.onResize = function () {this.layout.resize();}
 
                    return myPanel;
          }
 
 
          var myScriptPal = myScript_buildUI(thisObj);
 
 
          if ((myScriptPal != null) && (myScriptPal instanceof Window)) {
                    myScriptPal.center();
                    myScriptPal.show();
                    }
          }
 
 
          Queue_Tool(this);
}