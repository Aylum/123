function AssetsPath(){}
var AssetsPathObj = new AssetsPath();

//in
AssetsPath.prototype.backFolderPath = "d:/temp/project/in/back";
AssetsPath.prototype.camerasFilePath = "d:/temp/project/in/cameras/ep13_cameras_pos.json";
AssetsPath.prototype.masksFolderPath = "d:/temp/project/in/masks";
AssetsPath.prototype.videoFolderPath = "d:/temp/project/in/video";
AssetsPath.prototype.videoFilePath = "d:/temp/project/in/video/33.avi";
AssetsPath.prototype.sequenceFolderPath = "d:/temp/project/in/sequence/ep13";
AssetsPath.prototype.effectFilePath = "d:/temp/project/in/effects/pr2.ffx";

//out
AssetsPath.prototype.logFileFolderPath = "d:/temp/project/out";
AssetsPath.prototype.logFileName = "zaryadka1.log";
AssetsPath.prototype.compFilesOutFolderPath = "d:/temp/project/out/compose/back";

AssetsPath.prototype.printLog = function(message)
{
    try{
        var logFile = new File(AssetsPathObj.logFileFolderPath + "/" + AssetsPathObj.logFileName);

        if(!logFile.open("a")){
            throw logFile.error;
        }

        logFile.writeln(message);
        logFile.writeln();
        logFile.writeln("-----------------------------------------------------");

        logFile.close();
    }
    catch(err){
        writeln("can't write to log file: " + logFileFolderPath + "/" + logFileName
        + "\nlog message: " + message + "\nexception: " + err.toString());
    }
}
function printLog(message) { AssetsPathObj.printLog(message); }

function Constants(){}
var ConstantsObj = new Constants();

Constants.prototype.fps = 25;

function Utils(){}
var UtilsObj = new Utils();

Utils.prototype.getObjKeys = function(obj, m)
{
    var keys = [];
    for(var key in obj){
        if(!m){
            keys.push(key);
        } else if(typeof obj[key] == "function"){
            keys.push(key);
        }
    }
    return keys;
}

Utils.prototype.printLogFunction = AssetsPathObj.printLog;
Utils.prototype.checkIfFileCreated = function(item, doThrow/*=false*/)
{
    if(item.exists){
        return true;
    }

    if(doThrow){
        throw "failed to create: " + item.name + "\nerror: " + item.error;
    } else{
        Utils.printLogFunction("failed to create: " + item.name + "\nerror: " + item.error);
    }
    
    return false;
}

Utils.prototype.getCompFromIported = function(imported)
{
    for(var i = 1; i < imported.items.length + 1; i++){
        if(imported.items[i] instanceof CompItem){
            return imported.items[i];
        }
    }

    return null;
}

Utils.prototype.fpsToSec = function(frame, fps)
{
    return frame * 1/fps
}

function Zaryadka(){}
var ZaryadkaObj = new Zaryadka();

Zaryadka.prototype.run = function(thisObj)
{
    var myPanel = (thisObj instanceof Panel) ? thisObj : new Window("palette", "Zaryadka", [0, 0, 50, 300]);
    myPanel.margins = 50;
    
    res="group{orientation:'column', alignment:['fill', 'fill'], alignChildren:['fill', 'fill'],\
        cutBtn: Button{text:'run'},\
    }"

    //Add resource string to panel
    myPanel.grp = myPanel.add(res);

    myPanel.grp.cutBtn.onClick = function() {

        try
        {
            app.beginSuppressDialogs();

            //create out folder
            var tempFolder = new Folder(AssetsPathObj.compFilesOutFolderPath);
            if(!tempFolder.exists){
                tempFolder.create();

                UtilsObj.checkIfFileCreated(tempFolder, false);
            }

            //check .log folder
            tempFolder = new Folder(AssetsPathObj.logFileFolderPath);
            if(!tempFolder.exists){
                tempFolder.create();

                UtilsObj.checkIfFileCreated(tempFolder, false);
            }

            //create .log file
            var tempFile = new File(AssetsPathObj.logFileFolderPath + "/" + AssetsPathObj.logFileName);
            if(!tempFile.open("w")){
                writeln("can't open logFile for writing: " + tempFile.error);
            }
            tempFile.close();

            //load data from camerasFile
            var cameras = {};

            var tempCamerasFile = new File(AssetsPathObj.camerasFilePath);
            if(!tempCamerasFile.open("r")){
                throw "Can't open file at path: " + AssetsPathObj.camerasFilePath + "\nerror message:\n" + tempCamerasFile.error;
            }

            var camerasJson = JSON.parse(tempCamerasFile.read());
            
            tempCamerasFile.close();

            if(!('list' in camerasJson)){
                throw "cameras Json file doesn't contains property 'list'";
            }

            if(camerasJson.list.length < 1){
                throw "cameras Json file: list length < 1";
            }

            if(camerasJson.list[0].camera == null
                || camerasJson.list[0].start == null
                || camerasJson.list[0].end == null){
                    
                throw "Json file: in list[0] can't find one of the following properties 'camera', 'start', 'end'";
            }

            for(var i = 0; i < camerasJson.list.length; i++){
                
                var camera = camerasJson.list[i];

                if(!cameras.hasOwnProperty(camera.camera)){
                    cameras[camera.camera] = [];
                }
                
                cameras[camera.camera].push([camera.start, camera.end]);
            }

            var tempSort = function (a, b){ return a[0] - b[0]; };

            for(var camera in cameras){
                if(cameras.hasOwnProperty(camera)){
                    cameras[camera] = cameras[camera].sort(tempSort);
                }
            }
            //cameras data ready

            //load data from backFolder
            var backs = [];

            tempFolder = new Folder(AssetsPathObj.backFolderPath);
            if(!tempFolder.exists){
                throw AssetsPathObj.backFolderPath + " doesn't exists";
            }

            var tempList = tempFolder.getFiles();

            for(var i = 0; i < tempList.length; i++){
                if(!(tempList[i] instanceof Folder)){
                    continue;
                }

                var tempTempList = tempList[i].getFiles();
                var tempOutList = [];
                for(var k = 0; k < tempTempList.length; k++){
                    if(tempTempList[k] instanceof File){
                        tempOutList.push(tempTempList[k]);
                    }
                }

                if(tempOutList.length < 1){
                    printLog("at path: " + tempList[i].path + "\nfound 0 files(folders)");
                    continue;
                }

                backs.push([tempOutList, tempList[i].name]);
            }

            if(backs.length < 1){
                throw "at path(including subfolders): " + AssetsPathObj.backFolderPath + "\nfound 0 files(folders)";
            }
            //backFolder data ready

            for(var backInd = 0; backInd < backs.length; backInd++){
                try{
                    
                    var backCameraCompFolderName = "back_" + backs[backInd][1];

                    for(var camera in cameras) if (cameras.hasOwnProperty(camera)){
                        try{
                            
                            var backToImport = null;
                            for(var i = 0; i < backs[backInd][0].length; i++){
                                if(backs[backInd][0][i].name.indexOf("cam" + camera.toString()) != -1){
                                    backToImport = backs[backInd][0][i];
                                    break;
                                }
                            }

                            if(backToImport == null){
                                throw "can't find back file for camera " + camera.toString();
                            }

                            app.project.close(CloseOptions.DO_NOT_SAVE_CHANGES);
                            app.newProject();

                            var importedBack = null;

                            try{
                                var importOpt = new ImportOptions(backToImport);
                                if (importOpt.canImportAs(ImportAsType.FOOTAGE)){
                                    importOpt.importAs = ImportAsType.FOOTAGE;
                                    importedBack = app.project.importFile(importOpt);
                                }
                                else{
                                    throw "can't import this file as footage";
                                }

                                if(importedBack == null){
                                    throw "failed to import";
                                }

                            }
                            catch(err){
                                throw "importing backFile:\nfailed to import: " + backToImport.path
                                + "\nmessage: " + err.toString();
                            }

                            var comp = app.project.items.addComp(
                                "back_" + backs[backInd][1] + "_cam" + camera.toString() + "_comp",
                                1920,
                                1080,
                                1,
                                UtilsObj.fpsToSec(camerasJson.list.slice(-1)[0].end + 1, ConstantsObj.fps),
                                ConstantsObj.fps
                            );

                            var solid = comp.layers.add(importedBack);
                            
                            if(cameras[camera][0][0] > cameras[camera][0][1]){
                                throw "camera " + camera.toString() + " error: start > end; start = "
                                    + cameras[camera][0][0].toString() + ", end = " + cameras[camera][0][1].toString();
                            }

                            for(var cameraStartEndInd = cameras[camera].length - 1; cameraStartEndInd > 0; cameraStartEndInd--){

                                if(cameras[camera][cameraStartEndInd][0] > cameras[camera][cameraStartEndInd][1]){
                                    throw "camera " + camera.toString() + " error: start > end; start = "
                                        + cameras[camera][cameraStartEndInd][0] + ", end = " + cameras[camera][cameraStartEndInd][1];
                                }

                                var duplicated = solid.duplicate();
                                duplicated.startTime = UtilsObj.fpsToSec(cameras[camera][cameraStartEndInd][0], ConstantsObj.fps);
                                duplicated.outPoint = UtilsObj.fpsToSec(cameras[camera][cameraStartEndInd][1], ConstantsObj.fps);
                            }

                            solid.startTime = UtilsObj.fpsToSec(cameras[camera][0][0], ConstantsObj.fps);
                            solid.outPoint = UtilsObj.fpsToSec(cameras[camera][0][1], ConstantsObj.fps);

                            tempFolder = new Folder(AssetsPathObj.compFilesOutFolderPath + "/" + backCameraCompFolderName);
                            if(!tempFolder.exists){
                                tempFolder.create();

                                UtilsObj.checkIfFileCreated(tempFolder, true);
                            }

                            var mnCompFileName = AssetsPathObj.compFilesOutFolderPath + "/" + backCameraCompFolderName + "/" + comp.name + ".aep";

                            app.project.save(File(mnCompFileName));

                            var mnCompFile = new File(mnCompFileName);
                            UtilsObj.checkIfFileCreated(mnCompFile, true);
                        }
                        catch(err){
                            printLog("EXCEPTION: back: " + backs[backInd][1] + ", camera: " + camera + "\nmessage: " + err.toString());
                        }
                    }

                    app.project.close(CloseOptions.DO_NOT_SAVE_CHANGES);

                    app.newProject();

                    //load created camera compositions for current back
                    var backCameraCompFiles = [];

                    var backCameraCompFilesFolder = new Folder(AssetsPathObj.compFilesOutFolderPath + "/" + backCameraCompFolderName);

                    if(!backCameraCompFilesFolder.exists){
                        continue;
                    }

                    backCameraCompFiles = backCameraCompFilesFolder.getFiles();

                    if(backCameraCompFiles.length < 1){
                        continue;
                    }
                    //data ready

                    //load masks
                    var maskFiles = [];
                    tempFolder = new Folder(AssetsPathObj.masksFolderPath);
                    if(tempFolder.exists){
                        maskFiles = tempFolder.getFiles();
                    } else{
                        printLog("masks folder doesn't exists");
                    }

                    if(maskFiles.length < 1){
                        printLog("can't find any mask files at path: " + AssetsPathObj.masksFolderPath);
                    }
                    //masks ready

                    var backComp = app.project.items.addComp(
                        "back_" + backs[backInd][1] + "_comp",
                        1920,
                        1080,
                        1,
                        UtilsObj.fpsToSec(camerasJson.list.slice(-1)[0].end + 1, ConstantsObj.fps),
                        ConstantsObj.fps
                    );

                    var importedProjects = [];
                    for(var backCameraCompInd = 0; backCameraCompInd < backCameraCompFiles.length; backCameraCompInd++){
                        try{
                            if(!(backCameraCompFiles[backCameraCompInd] instanceof File)){
                                continue;
                            }

                            var importOpt = new ImportOptions(backCameraCompFiles[backCameraCompInd]);
                            if (importOpt.canImportAs(ImportAsType.PROJECT)){
                                importOpt.importAs = ImportAsType.PROJECT;
                                var imported = app.project.importFile(importOpt);

                                if(imported != null){
                                    importedProjects.push(imported);
                                }
                                else{
                                    throw "failed to import: " + backCameraCompFiles[backCameraCompInd].name;
                                }

                                var importedComp = UtilsObj.getCompFromIported(imported);
                                if(importedComp == null){
                                    throw "can't find composition inside " + backCameraCompFiles[backCameraCompInd].name;
                                }

                                backComp.layers.add(importedComp);

                                var camName = null;
                                try{
                                    var tempArr = backCameraCompFiles[backCameraCompInd].name.split("_");
                                    for(var i = 0; i < tempArr.length; i++){
                                        if(tempArr[i].indexOf("cam") != -1){
                                            camName = tempArr[i];
                                            break;
                                        }
                                    }

                                    if(camName == null){
                                        throw "can't find word 'camera' in filename";
                                    }
                                } catch(err){
                                    printLog("failed to retrieve camera name from file: " + backCameraCompFiles[backCameraCompInd].path
                                        + "\nerror: " + err.toString());
                                }

                                if(camName != null){

                                    var importedMask = null;
                                    try{
                                        for(var i = 0; i < maskFiles.length; i++){
                                            if(maskFiles[i] instanceof File && maskFiles[i].name.indexOf(camName) != -1){

                                                var importOpt = new ImportOptions(maskFiles[i]);
                                                if (importOpt.canImportAs(ImportAsType.PROJECT)){
                                                    importOpt.importAs = ImportAsType.PROJECT;
                                                    importedMask = app.project.importFile(importOpt);
                                                    break;
                                                }
                                            }
                                        }

                                        if(importedMask == null){
                                            throw "importedMask == null";
                                        }
                                    } catch(err){
                                        printLog("failed to import mask file for composition: " + backCameraCompFiles[backCameraCompInd].path
                                                + " camera name: " + camName + "\nerror: " + err.toString());
                                    }

                                    if(importedMask != null){
                                        var maskComp = UtilsObj.getCompFromIported(importedMask);

                                        if(maskComp == null){
                                            printLog("can't find composition inside: " + importedMask.name);
                                        } else{
                                            var maskLayer = importedComp.layers.add(maskComp);
                                            maskLayer.blendingMode = BlendingMode.STENCIL_ALPHA;
                                            //maskLayer.enabled = false;

                                            //backComp.layers.add(maskComp);
                                        }
                                    }
                                    
                                }

                            }
                            else{
                                throw "can't import .aep file as composition, file path: " + backCameraCompFiles[backCameraCompInd].path
                                    + "\nerror: " + backCameraCompFiles[backCameraCompInd].error;
                            }
                        }
                        catch(err){
                            printLog("import .aep file failed:\n" + err.toString());
                        }
                    }
                    
                    var backCompFile = new File(AssetsPathObj.compFilesOutFolderPath + "/" + backComp.name + ".aep");
                    app.project.save(backCompFile);
                    UtilsObj.checkIfFileCreated(backCompFile, true)
                }
                catch(err){
                    printLog("EXCEPTION: back: " + backs[backInd][1] + "\nmessage: " + err.toString());
                }
                
            }

            app.project.close(CloseOptions.DO_NOT_SAVE_CHANGES);
            app.newProject();

            var allComp = app.project.items.addComp(
                "shared1_comp",
                1920,
                1080,
                1,
                UtilsObj.fpsToSec(camerasJson.list.slice(-1)[0].end + 1, ConstantsObj.fps),
                ConstantsObj.fps
            );

            //import sequences, add them as layers
            try{
                tempFolder = new Folder(AssetsPathObj.sequenceFolderPath);
                
                if(!tempFolder.exists){
                    throw "folder doesnt exists";
                }

                var seqFolders = tempFolder.getFiles();

                if(seqFolders.length < 1){
                    throw "sequence folder contains 0 files";
                }

                seqFolders.sort();
    
                var importedSeqList = [];
                for(var i = 0; i < seqFolders.length; i++){
                    
                    try{
                        if(!(seqFolders[i] instanceof Folder)){
                            continue;
                        }
    
                        var seqPictures = seqFolders[i].getFiles();
    
                        if(seqPictures.length < 1){
                            throw "sequence folder contains 0 files";
                        }
    
                        seqPictures.sort();
    
                        var beautyPic = null;
    
                        for(var k = 0; k < seqPictures.length; k++){
                            if(!(seqPictures[k] instanceof File)){
                                continue;
                            }
    
                            if(seqPictures[k].name.toLowerCase().indexOf("beauty") != -1 && beautyPic == null){
                                beautyPic = seqPictures[k];
                                break;
                            }
                        }
    
                        if(beautyPic == null){
                            throw "can't find any \"Beauty\" picture in folder";
                        }
    
                        var importedSequence = null
                        var importOpt = new ImportOptions(beautyPic);
                        if (importOpt.canImportAs(ImportAsType.FOOTAGE)){
                            importOpt.sequence = true;
                            importOpt.importAs = ImportAsType.FOOTAGE;
                            importedSequence = app.project.importFile(importOpt);
                        }
    
                        if(importedSequence == null){
                            throw "couldn't import file as sequence, file path: " + beautyPic.path;
                        }
    
                        importedSeqList.push(importedSequence);
                    }
                    catch(err){
                        printLog("error while importing sequences, sequence folder: " + seqFolders[i].path
                                + ", message:\n" + err.toString());
                    }

                }

                var prevCompOutPoint = 0.0;
                if(importedSeqList.length > 0){
                    prevCompOutPoint = allComp.layers.add(importedSeqList[0]).outPoint;
                }

                for(var i = 1; i < importedSeqList.length; i++){
                    
                    var layer = allComp.layers.add(importedSeqList[i]);

                    layer.startTime = prevCompOutPoint
                    prevCompOutPoint = layer.outPoint;
                }

            }
            catch(err){
                printLog("failed to import/add(as layers) sequences: " + AssetsPathObj.sequenceFolderPath
                        + "\nmessage: " + err.toString());//fix error
            }

            //import video and add as layer
            try{
                var importedVideo = null;

                var videoFile = new File(AssetsPathObj.videoFilePath);

                if(!videoFile.exists){
                    throw "video file doesn't exists";
                } 

                var importOpt = new ImportOptions(videoFile);
                if (importOpt.canImportAs(ImportAsType.FOOTAGE)){
                    importOpt.importAs = ImportAsType.FOOTAGE;
                    importedVideo = app.project.importFile(importOpt);
                } else {
                    throw "can't import as FOOTAGE";
                }
                
                if(importedVideo == null){
                    throw "importedVideo == null";
                }

                allComp.layers.add(importedVideo);

            }
            catch(err){
                printLog("failed to import video file: " + AssetsPathObj.videoFilePath
                        + "\nmessage: " + err.toString());
            }

            //import all back compositions and add them as layers
            var allBackCompFiles = new Folder(AssetsPathObj.compFilesOutFolderPath).getFiles();

            if(allBackCompFiles == null || allBackCompFiles.length < 1){
                printLog("failed to save back compositions into: " + AssetsPathObj.compFilesOutFolderPath);
            } else {

                for(var i = 0; i < allBackCompFiles.length; i++){
                    try{

                        if(!(allBackCompFiles[i] instanceof File)){
                            continue;
                        }
        
                        var importOpt = new ImportOptions(allBackCompFiles[i]);
                        if (importOpt.canImportAs(ImportAsType.PROJECT)){
                            importOpt.importAs = ImportAsType.PROJECT;
                            var imported = app.project.importFile(importOpt);
    
                            if(imported == null){
                                throw "imported == null";
                            }
    
                            var importedComp = UtilsObj.getCompFromIported(imported);
                            if(importedComp == null){
                                throw "can't find composition inside project";
                            }
        
                            allComp.layers.add(importedComp);
                            
                        } else {
                            throw "can't import file as project";
                        }
                    }
                    catch(err){
                        printLog("failed to import file: " + allBackCompFiles[i].path + "\nmessage: " + err.toString());
                    }

                }
            }

            //load, apply effect
            try{
                var effectFile = new File(AssetsPathObj.effectFilePath);

                if(!effectFile.exists){
                    throw "file doesn't exists";
                }
    
                var firstLayer = allComp.layers[1];
    
                firstLayer.adjustmentLayer = true;
    
                var currSelection = app.project.selection;
                for(var i = 0; i < currSelection.length; i++){
                    currSelection[i].selected = false;
                }
    
                firstLayer.selected = true;
                firstLayer.applyPreset(effectFile);

            }
            catch(err){
                printLog("failed to load/apply effect: " + AssetsPathObj.effectFilePath + "\nmessage: " + err.toString());
            }




            app.project.save(File(AssetsPathObj.compFilesOutFolderPath + "/shared1_comp.aep"));
            
        }
        catch(err)
        {
            printLog("EXCEPTION: " + err.toString());
        }
        
        app.endSuppressDialogs(true);
    };
    
    //Setup panel sizing and make panel resizable
    myPanel.layout.layout(true);
    myPanel.grp.minimumSize = myPanel.grp.size;
    myPanel.layout.resize();
    myPanel.onResizing = myPanel.onResize = function () {this.layout.resize();}

    return myPanel;
}

var myScriptPal = ZaryadkaObj.run(this);

if ((myScriptPal != null) && (myScriptPal instanceof Window)) {
        myScriptPal.center();
        myScriptPal.show();
}