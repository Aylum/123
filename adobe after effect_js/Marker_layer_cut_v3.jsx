{
function Queue_Tool(thisObj) {
          function myScript_buildUI(thisObj) {
                    var myPanel = (thisObj instanceof Panel) ? thisObj : new Window("palette", "Cut layer by markers", [0, 0, 50, 300]);
					myPanel.margins = 50;
                    // res="group{orientation:'column', alignment:['fill', 'fill'], alignChildren:['fill', 'fill'],\
					// 	n_tl: StaticText{text:'Output Module:'},\
					// 	tempList: DropDownList { alignment:['fill','top'], alignment:['fill','top'] },\
                    //        n_start: StaticText{text:'Skip layers from the start:'},\  inputPrefix: EditText{text:'0'},\
                    //        n_end: StaticText{text:'Skip layers from the end:'},\  inputSuffix: EditText{text:'0'},\
					// 	n_ip: StaticText{text:'Enter the name of the episode:'},\
					// 	inputPrefixEp: EditText{text:'ep'},\
					// 	renderLayers: Button{text:'Add all layers to Render Queue'},\
					// 	clearQueue: Button{text:'Clear Render Queue'},\
					// }"
					
					res="group{orientation:'column', alignment:['fill', 'fill'], alignChildren:['fill', 'fill'],\
						delKeys: Checkbox{text:'delete markers', value:false},\
						cutBtn: Button{text:'Cut'},\
                    }"
                    //Add resource string to panel
					myPanel.grp = myPanel.add(res);

					myPanel.grp.cutBtn.onClick = function() {

						try
						{
							delKeys2 = myPanel.grp.delKeys.value;

							var oldLayers = [];

							var myComp = app.project.activeItem;
	
							if (myComp != null && myComp instanceof CompItem)
							{
								var mylayers = app.project.activeItem.layers;
								if(mylayers)
								{
									for(k=1; k <= mylayers.length; k++){
										mylayers[k].selected = false;
									}
									// for(k=mylayers.length; k >= 1; k--){
									// 		mylayers[k].selected = true;
									// }
	
									for(k = 1; k <= mylayers.length; k++)
									{
										mylayers[k].selected = true;
										oldLayers.unshift(mylayers[k]);
									}
								}
							}
							else{
								alert("Select the comp in which you want to cut the layers!");
							}
	
							// var duplicated = app.project.activeItem.layers[4].duplicate();
							// duplicated.comment = "hi";
	
							//TODO: check all indexes, decrements/increments in all for loops
							for(layerIndex = 0; layerIndex < oldLayers.length; layerIndex++)
							{
								var curLayer = oldLayers[layerIndex];
	
								var prevTime = curLayer.outPoint;
								var curTime = prevTime;
								//var curMarker;
	
								if(prevTime < curTime){
									alert("ERROR: prevTime < curTime");
									continue;
								}
	
								if(curLayer.property("Marker").numKeys == 0) continue;
	
								for(markerIndex = curLayer.property("Marker").numKeys; markerIndex >= 1 ; markerIndex--)
								{
									curTime = curLayer.property("Marker").keyTime(markerIndex);
	
									if(curTime > (myComp.workAreaStart + myComp.workAreaDuration) || curTime < (myComp.workAreaStart)) continue;
	
									curLayer.property("Marker").removeKey(markerIndex);
	
									duplicatedLayer = curLayer.duplicate();
									if(delKeys2){
										for(dlIndex = duplicatedLayer.property("Marker").numKeys; dlIndex >= 1; dlIndex--){	//remove all markers from duplicated layer
											duplicatedLayer.property("Marker").removeKey(dlIndex);
										}
									}
									
									duplicatedLayer.inPoint = curTime;
									duplicatedLayer.outPoint = prevTime;
										
									prevTime = curTime;
								}
	
								if(prevTime > 0 && prevTime <= (myComp.workAreaStart + myComp.workAreaDuration) && prevTime >= myComp.workAreaStart)
								{
									if(curLayer.index - 1 >= 1)
									{
										dl = curLayer.duplicate();
		
										if(delKeys2){
											for(dlIndex = dl.property("Marker").numKeys; dlIndex >= 1; dlIndex--){	//remove all markers from duplicated layer
												dl.property("Marker").removeKey(dlIndex);
											}
										}
			
										//dldl = dl.duplicate();
			
										dl.inPoint = 0;
										dl.outPoint = prevTime;
	
										//tempLayer.remove();
									}
								}
	
								// if(curTime > 0 && curTime <= (myComp.workAreaStart + myComp.workAreaDuration) && curTime >= myComp.workAreaStart){
								// 	if(curLayer.index - 1 >= 1)
								// 	{
								// 		tempLayer = app.project.activeItem.layers[curLayer.index - 1];
	
								// 		dl = tempLayer.duplicate();
		
								// 		for(dlIndex = dl.property("Marker").numKeys; dlIndex >= 1; dlIndex--){	//remove all markers from duplicated layer
								// 			duplicatedLayer.property("Marker").removeKey(dlIndex);
								// 		}
			
								// 		dldl = dl.duplicate();
			
								// 		dl.inPoint = 0;
								// 		dl.outPoint = curTime;
	
								// 		tempLayer.remove();
								// 	}
								// }
	
									//curLayer.remove();
							}
						}
						catch(err)
						{
							alert("EXCEPTION: " + err.message);
						}

						

						// for (i = myComp.numLayers; i >= 1 ; i--)
						// {							
						// 	var curLayer = app.project.activeItem.layers[i];

						// 	for(markerIndex = curLayer.property("Marker").numKeys; markerIndex >= 1 ; markerIndex--)
						// 	{
						// 		curTime = curLayer.property("Marker").keyTime(markerIndex);

								

						// 		prevTime = curTime;
						// 	}

						// 	for(jk = 1; jk <= curLayer.property("Marker").numKeys; jk++)
						// 	{
						// 		var marker3 = curLayer.property("Marker").keyTime(jk);
						// 	}

						// 	var marker2 = curLayer.property("Marker").keyTime(1);
						// 	if (curLayer != undefined)
						// 	{
						// 		myComp.workAreaStart = curLayer.inPoint;

 						// 			if(myComp.duration<curLayer.outPoint)
						// 			{myComp.workAreaDuration = myComp.duration - curLayer.inPoint;}
						// 		else
						// 			{myComp.workAreaDuration = curLayer.outPoint - curLayer.inPoint;}
								
						// 	}
						// 	myComp.workAreaStart = saveWAS;
						// 	myComp.workAreaDuration = saveWAD;
						// }
						
					};
					
                    //Setup panel sizing and make panel resizable
                    myPanel.layout.layout(true);
					myPanel.grp.minimumSize = myPanel.grp.size;
                    myPanel.layout.resize();
                    myPanel.onResizing = myPanel.onResize = function () {this.layout.resize();}
 
                    return myPanel;
          }
 
 
          var myScriptPal = myScript_buildUI(thisObj);
 
 
          if ((myScriptPal != null) && (myScriptPal instanceof Window)) {
                    myScriptPal.center();
                    myScriptPal.show();
                    }
          }
 
 
          Queue_Tool(this);
}