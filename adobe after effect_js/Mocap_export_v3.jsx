function AssetsPath(){}
var AssetsPathObj = new AssetsPath();

//in
AssetsPath.prototype.premiereProjectPath = "m:/ZARYADKA O!/1_LION/ep01_cut_edit.prproj";

//out
AssetsPath.prototype.compFilesOutFolderPath = "m:/ZARYADKA O!/1_LION";

AssetsPath.prototype.printLog = function(message)
{
    try{
        var logFile = new File(AssetsPathObj.logFileFolderPath + "/" + AssetsPathObj.logFileName);

        if(!logFile.open("a")){
            throw logFile.error;
        }

        logFile.writeLn(message);
        logFile.writeLn();
        logFile.writeLn("-----------------------------------------------------");

        logFile.close();
    }
    catch(err){
        writeLn("can't write to log file: " + logFileFolderPath + "/" + logFileName
        + "\nlog message: " + message + "\nexception: " + err.toString());
    }
}
function printLog(message) { AssetsPathObj.printLog(message); }

function Constants(){}
var ConstantsObj = new Constants();

Constants.prototype.fps = 25;

function Utils(){}
var UtilsObj = new Utils();

Utils.prototype.CHECK_IF_CREATED_MODE_ENUM =
{
    PRINTLOGFUNCTION : 0,
    THROW : 1,
    ALERT : 2
};

Utils.prototype.getObjKeys = function(obj, m)
{
    var keys = [];
    for(var key in obj){
        if(!m){
            keys.push(key);
        } else if(typeof obj[key] == "function"){
            keys.push(key);
        }
    }
    return keys;
}

Utils.prototype.printLogFunction = AssetsPathObj.printLog;
Utils.prototype.checkIfFileCreated = function(item, tell_message_mode/* = 0*/, message/* = ""*/)
{
    if(item.exists){
        return true;
    }

    var resMessage = "failed to create: " + item.name + "\nerror: " + item.error;
    
    if(message != null){
        resMessage += "\nmessage2: " + message;
    }

    if(tell_message_mode == null){
        Utils.printLogFunction(resMessage);
    } else {
        if(tell_message_mode == 0){
            Utils.printLogFunction(resMessage);
        } else if(tell_message_mode == 1) {
            throw resMessage;
        } else if(tell_message_mode == 2) {
            alert(resMessage);
        }
    }
    
    return false;
}

Utils.prototype.fpsToSec = function(frame, fps)
{
    return frame * 1/fps
}

function Tancory(){}
var TancoryObj = new Tancory();

Tancory.prototype.run = function(thisObj)
{
    var myPanel = (thisObj instanceof Panel) ? thisObj : new Window("palette", "Mocap export", [0, 0, 50, 300]);
    myPanel.margins = 50;
    
    res="group{orientation:'column', alignment:['fill', 'fill'], alignChildren:['fill', 'fill'],\
        n_tl: StaticText{text:'Output Module:'},\
        tempList: DropDownList { alignment:['fill','top'], alignment:['fill','top'] },\
        n_start: StaticText{text:'Skip layers from the start:'},\  inputPrefix: EditText{text:'0'},\
        n_end: StaticText{text:'Skip layers from the end:'},\  inputSuffix: EditText{text:'0'},\
        n_ip: StaticText{text:'Enter the name of the episode:'},\
        inputPrefixEp: EditText{text:'ep'},\
        renderLayers: Button{text:'Add all layers to Render Queue'},\
    }"

    //Add resource string to panel
    myPanel.grp = myPanel.add(res);

    fillTempList()

    function fillTempList() {
        var varComp = app.project.items.addComp("queue_tool_var_comp_JUST_REMOVE", 1280, 720, 1, 61, 30)
        
        var rqi = app.project.renderQueue.items.add(varComp);
        var om = rqi.outputModule(1);

        myPanel.grp.tempList.selection = null;
        myPanel.grp.tempList.removeAll();

        for (var i=0; i<om.templates.length; i++){
            if (om.templates[i].indexOf("_HIDDEN") != 0){
                myPanel.grp.tempList.add("item", om.templates[i]);
            }
        }
        
        if (om.templates.length > 0) {
            myPanel.grp.tempList.selection = 0;
        }
        
        rqi.remove();
        varComp.remove();
        //if (app.project.activeItem instanceof CompItem) { activeComp.openInViewer(); }
    }

    myPanel.grp.renderLayers.onClick = function() {

        var myOutputPath = null;
        var myComp = app.project.activeItem;

        if (myComp != null && myComp instanceof CompItem)
        {
            myOutputPath = Folder.selectDialog("Choose destination folder:");
            if (myOutputPath == null)
                {alert ("First select the destination folder!");return;}
        }
        else{
            alert("Select the comp you want to render!");
        }

        if(myComp.frameRate != ConstantsObj.fps){
            alert("Warning: Selected composition fps != " + ConstantsObj.fps);
        }

        myComp.displayStartTime = 0;

        for (var i = (myComp.numLayers - parseInt(myPanel.grp.inputSuffix.text)); i >= 1 + parseInt(myPanel.grp.inputPrefix.text); i--)
        {							
            var curLayer = myComp.layers[i];
            if (curLayer != null || curLayer != undefined)
            {
                //     if(myComp.duration<curLayer.outPoint)
                //     {myComp.workAreaDuration = myComp.duration - curLayer.inPoint;}
                // else
                //     {myComp.workAreaDuration = curLayer.outPoint - curLayer.inPoint;}

                if (myComp != null && myComp instanceof CompItem)
                {
                    for(var layerindex = 1; layerindex < myComp.layers.length + 1; layerindex++)
                    {
                        myComp.layers[layerindex].enabled = false;
                    }

                    myComp.layers[i].enabled = true;

                    var theRender = app.project.renderQueue.items.add(myComp);
                    theRender.applyTemplate("Best Settings");

                    theRender.setSetting("Use this frame rate", "25.0");
                    theRender.setSetting("Time Span Start", curLayer.inPoint);
                    theRender.setSetting("Time Span End", curLayer.outPoint);

                    var myFile = new File(myOutputPath.fsName+"\\"
                                    + myPanel.grp.inputPrefixEp.text
                                    + "_scene" + ("0" + curLayer.index).slice(-2)
                                    + "_" + Math.floor(curLayer.inPoint * 25)
                                    + "_" + curLayer.label
                                );

                    theRender.outputModules[1].file = myFile;
                    
                    if (myPanel.grp.tempList.selection != null) {
                        theRender.outputModules[1].applyTemplate(myPanel.grp.tempList.selection);
                    }
                } else {
                    alert("comp == null || comp not instanceof CompItem");
                    throw "comp == null || comp not instanceof CompItem";
                }
            } else {
                alert("layers[i] == null");
                throw "layers[i] == null";
            }
        }
        
    };
    
    //Setup panel sizing and make panel resizable
    myPanel.layout.layout(true);
    myPanel.grp.minimumSize = myPanel.grp.size;
    myPanel.layout.resize();
    myPanel.onResizing = myPanel.onResize = function () {this.layout.resize();}

    return myPanel;
}

var myScriptPal = TancoryObj.run(this);

if ((myScriptPal != null) && (myScriptPal instanceof Window)) {
        myScriptPal.center();
        myScriptPal.show();
}