#include "MimikaCommand.h"

#include <maya/MFnPlugin.h>
#include <maya/MObject.h>

MStatus initializePlugin(MObject obj)
{
	MStatus status;
	MFnPlugin plugin(obj, "ILYA", "2019", "Any");

	status = plugin.registerCommand("mimikaPeakAnim", MimikaCommand::creator, MimikaCommand::syntaxCreator);

	if (!status)
	{
		status.perror("registerCommand");
		return status;
	}

	return status;
}

MStatus uninitializePlugin(MObject obj)
{
	MimikaCommand::Unload();

	MStatus status;
	MFnPlugin plugin(obj);

	status = plugin.deregisterCommand("mimikaPeakAnim");
	if (!status) {
		status.perror("deregisterCommand");
		return status;
	}

	return status;
}