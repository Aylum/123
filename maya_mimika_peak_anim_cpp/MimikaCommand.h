#pragma once

#include <list>
#include <string>

#include <maya/MGlobal.h>
#include <maya/MStatus.h>
#include <maya/MPxCommand.h>
#include <maya/MEventMessage.h>
#include <maya/MTime.h>
#include <maya/MSyntax.h>

class MimikaCommand : public MPxCommand
{
private:

	static MCallbackId callbackId;
	static bool isCallbackSet;

	static MTime prevTime;

	static bool isRecording;

	static std::list<std::pair<MTime, float>> peaks;

	static std::string curveNodeName;

	static std::string nnArg;
	static std::string atrArg;

public:

	static void* creator()
	{
		return new MimikaCommand;
	}

	static MSyntax syntaxCreator();

	static void Unload();

	MStatus	doIt(const MArgList& args) override;

private:

	static void _cdecl TimerCallback(float, float, void*);
	static MStatus AddTimerCallback();
	static MStatus RemoveTimerCallback();
	
	static void IfNotStat(const MStatus& stat, const char* message, bool doThrow = false);

};

class NotStatException : public std::runtime_error
{
public:
	NotStatException(const char* message) : runtime_error(message) {}
};