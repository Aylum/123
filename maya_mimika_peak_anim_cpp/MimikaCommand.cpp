#include "MimikaCommand.h"

#include <windows.h>
#include <atlbase.h>
#include <mmdeviceapi.h>
#include <audiopolicy.h>
#include <endpointvolume.h>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>

#include <maya/MString.h>
#include <maya/MAnimControl.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MTimeArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MTimerMessage.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MFnAttribute.h>
#include <maya/MObject.h>
#include <maya/MDGModifier.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MArgList.h>
#include <maya/MArgParser.h>

MCallbackId MimikaCommand::callbackId = 1;
bool MimikaCommand::isCallbackSet = false;
MTime MimikaCommand::prevTime;
bool MimikaCommand::isRecording = false;
std::list<std::pair<MTime, float>> MimikaCommand::peaks;
std::string MimikaCommand::curveNodeName("mimikaPeakAnim");
std::string MimikaCommand::nnArg("");
std::string MimikaCommand::atrArg("");

using namespace std;

class CoUninitializeOnExit
{
public:
	CoUninitializeOnExit() {}
	~CoUninitializeOnExit() {
		CoUninitialize();
	}
};

void PrintHRESULT(HRESULT hr, char* message)
{
	ostringstream oss;
	oss << "0x" << std::uppercase << std::setfill('0') << std::setw(8) << std::hex << hr;
	
	string errCode;
	oss.str(errCode);

	MGlobal::displayError(string(string("") + "TimerCallback error: HRESULT = " + errCode + ", message:" + message).c_str());
}

MSyntax MimikaCommand::syntaxCreator()
{
	MSyntax syntax;
	syntax.addFlag("-nn", "-nodeName", MSyntax::MArgType::kString);
	syntax.addFlag("-an", "-atrName", MSyntax::MArgType::kString);
	return syntax;
}

void MimikaCommand::Unload()
{
	if (isCallbackSet || isRecording)
	{
		MStatus stat = MimikaCommand::RemoveTimerCallback();
		MimikaCommand::IfNotStat(stat, "failed to remove timer callback");
	}
}

MStatus	MimikaCommand::doIt(const MArgList& args)
{
	MStatus stat;
	
	try
	{
		MArgParser argParser(syntax(), args);

		if (argParser.isFlagSet("-nn"))
		{
			MString mStr;
			argParser.getFlagArgument("-nn", 0, mStr);
			MimikaCommand::nnArg = mStr.asChar();
		}

		if (argParser.isFlagSet("-an"))
		{
			MString mStr;
			argParser.getFlagArgument("-an", 0, mStr);
			MimikaCommand::atrArg = mStr.asChar();
		}

		if (isCallbackSet || isRecording)
		{
			stat = MimikaCommand::RemoveTimerCallback();
			MimikaCommand::IfNotStat(stat, "failed to remove timer callback");
			
			MimikaCommand::isRecording = false;
		}

		peaks.clear();

		stat = MAnimControl::setCurrentTime(MAnimControl::minTime());
		MimikaCommand::IfNotStat(stat, "couldn't set current time");

		MimikaCommand::prevTime = --MAnimControl::currentTime();

		stat = MAnimControl::setPlaybackMode(MAnimControl::PlaybackMode::kPlaybackOnce);
		MimikaCommand::IfNotStat(stat, "couldn't set playback mode");

		stat = MimikaCommand::AddTimerCallback();
		MimikaCommand::IfNotStat(stat, "couldn't add timer callback", true);

		MimikaCommand::isRecording = true;

		stat = MGlobal::executeCommand("play -playSound true");
		MimikaCommand::IfNotStat(stat, "couldn't execute command play");

	}
	catch (const std::exception& e)
	{
		MGlobal::displayError(e.what());
	}

	return stat;
}

void _cdecl MimikaCommand::TimerCallback(float, float, void*)
{
	try
	{
		if (!MimikaCommand::isRecording)
			return;

		if (!MAnimControl::isPlaying())
		{
			MimikaCommand::RemoveTimerCallback();
			return;
		}

		MTime currentTime = MAnimControl::currentTime();

		if (MimikaCommand::prevTime == currentTime)
			return;

		MimikaCommand::prevTime = currentTime;

		HRESULT hr = S_OK;

		hr = CoInitialize(NULL);
		
		if (FAILED(hr))
		{
			PrintHRESULT(hr, "CoInitialize(NULL) failed");
			return;
		}

		CoUninitializeOnExit cuoe;

		// get default device
		CComPtr<IMMDeviceEnumerator> pMMDeviceEnumerator;
		hr = pMMDeviceEnumerator.CoCreateInstance(__uuidof(MMDeviceEnumerator));
		
		if (FAILED(hr))
		{
			PrintHRESULT(hr, "CoCreateInstance(IMMDeviceEnumerator) failed");
			return;
		}

		CComPtr<IMMDevice> pMMDevice;
		hr = pMMDeviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &pMMDevice);
		
		if (FAILED(hr))
		{
			PrintHRESULT(hr, "IMMDeviceEnumerator::GetDefaultAudioEndpoint() failed");
			return;
		}

		//// get the current audio peak meter level for this endpoint
		//CComPtr<IAudioMeterInformation> pAudioMeterInformation_Endpoint;
		//hr = pMMDevice->Activate(
		//	__uuidof(IAudioMeterInformation),
		//	CLSCTX_ALL,
		//	NULL,
		//	reinterpret_cast<void**>(&pAudioMeterInformation_Endpoint)
		//);

		//if (FAILED(hr))
		//{
		//	PrintHRESULT(hr, "IMMDevice::Activate(IAudioMeterInformation) failed");
		//	return;
		//}

		//float peak_endpoint = 0.0f;
		//hr = pAudioMeterInformation_Endpoint->GetPeakValue(&peak_endpoint);
		//
		//if (FAILED(hr))
		//{
		//	PrintHRESULT(hr, "IAudioMeterInformation::GetPeakValue() failed");
		//	return;
		//}

		CComPtr<IAudioSessionManager2> pAudioSessionManager2;
		hr = pMMDevice->Activate(
			__uuidof(IAudioSessionManager2),
			CLSCTX_ALL,
			nullptr,
			reinterpret_cast<void **>(&pAudioSessionManager2)
		);

		if (FAILED(hr))
		{
			PrintHRESULT(hr, "IMMDevice::Activate(IAudioSessionManager2) failed");
			return;
		}

		CComPtr<IAudioSessionEnumerator> pAudioSessionEnumerator;
		hr = pAudioSessionManager2->GetSessionEnumerator(&pAudioSessionEnumerator);
		
		if (FAILED(hr))
		{
			PrintHRESULT(hr, "IAudioSessionManager2::GetSessionEnumerator() failed");
			return;
		}

		// iterate over all the sessions
		int count = 0;
		hr = pAudioSessionEnumerator->GetCount(&count);

		if (FAILED(hr))
		{
			PrintHRESULT(hr, "IAudioSessionEnumerator::GetCount() failed");
			return;
		}

		float peak_session = 0.0f;

		for (int session = 0; session < count; session++)
		{
			// get the session identifier
			CComPtr<IAudioSessionControl> pAudioSessionControl;
			hr = pAudioSessionEnumerator->GetSession(session, &pAudioSessionControl);

			if (FAILED(hr))
			{
				PrintHRESULT(hr, "IAudioSessionEnumerator::GetSession() failed");
				return;
			}

			AudioSessionState state1;
			hr = pAudioSessionControl->GetState(&state1);

			if (FAILED(hr))
			{
				PrintHRESULT(hr, "IAudioSessionControl::GetState() failed");
				return;
			}

			if (AudioSessionStateActive != state1)
			{
				continue;
			}

			CComPtr<IAudioSessionControl2> pAudioSessionControl2;
			hr = pAudioSessionControl->QueryInterface(IID_PPV_ARGS(&pAudioSessionControl2));

			if (FAILED(hr))
			{
				PrintHRESULT(hr, "IAudioSessionControl::QueryInterface(IAudioSessionControl2) failed");
				return;
			}

			CComHeapPtr<WCHAR> szSessionInstanceIdentifier;
			hr = pAudioSessionControl2->GetSessionInstanceIdentifier(&szSessionInstanceIdentifier);

			if (FAILED(hr))
			{
				PrintHRESULT(hr, "IAudioSessionControl2::GetSessionInstanceIdentifier() failed");
				return;
			}

			LPCWSTR tempVar1 = static_cast<LPCWSTR>(szSessionInstanceIdentifier);
			std::wstring sessIdWStr(tempVar1);

			if (sessIdWStr.rfind(L"maya.exe") != std::wstring::npos)
			{
				CComPtr<IAudioMeterInformation> pAudioMeterInformation_Session;
				hr = pAudioSessionControl->QueryInterface(IID_PPV_ARGS(&pAudioMeterInformation_Session));

				if (FAILED(hr))
				{
					PrintHRESULT(hr, "IAudioSessionControl::QueryInterface(IAudioMeterInformation) failed");
					return;
				}

				hr = pAudioMeterInformation_Session->GetPeakValue(&peak_session);

				if (FAILED(hr))
				{
					PrintHRESULT(hr, "IAudioMeterInformation::GetPeakValue() failed");
					return;
				}

				continue;
			}
		}

		peaks.push_back(std::pair<MTime, float>(currentTime, peak_session));

		if (currentTime == MAnimControl::maxTime())
		{
			MStatus stat;

			MFnAnimCurve peaksCurve = MFnAnimCurve().create(MFnAnimCurve::kAnimCurveTU, nullptr, &stat);
			MimikaCommand::IfNotStat(stat, "failed to create animCurve", true);

			MFnDependencyNode peaksCurveNode;
			peaksCurveNode.setObject(peaksCurve.object());

			MDGModifier mdgModifier2;
			mdgModifier2.renameNode(peaksCurveNode.object(), MString(MimikaCommand::curveNodeName.c_str()));
			mdgModifier2.doIt();

			MTimeArray timeArr = MTimeArray((int)peaks.size(), MTime());
			MDoubleArray peaksArr = MDoubleArray(timeArr.length());

			int peaksInd = 0;
			for (auto& el : peaks)
			{
				timeArr[peaksInd] = el.first;
				peaksArr[peaksInd] = el.second;

				peaksInd++;
			}

			stat = peaksCurve.addKeys(&timeArr, &peaksArr);
			MimikaCommand::IfNotStat(stat, "failed to add keys to animCurve", true);

			stat = MimikaCommand::RemoveTimerCallback();
			MimikaCommand::isRecording = stat ? false : true;

			MimikaCommand::IfNotStat(stat, "failed to remove TimerCallback");

			MFnDependencyNode oldPeaksCurve;
			bool isPeaksCurveFound = false;
			for (MItDependencyNodes iter; !iter.isDone(); iter.next())
			{
				oldPeaksCurve.setObject(iter.thisNode());

				string nodeName = oldPeaksCurve.name().asChar();
				if (nodeName.find(MimikaCommand::curveNodeName) != string::npos && nodeName.find("old") == string::npos)
				{
					isPeaksCurveFound = true;
					break;
				}
			}

			if (isPeaksCurveFound)
			{
				MObject oldOutAttr = oldPeaksCurve.attribute("output", &stat);

				if (!stat)
				{
					MimikaCommand::IfNotStat(stat, (string("coudn't get output attribute from node: ") + MimikaCommand::curveNodeName).c_str(), true);
				}

				MPlug oldOutPlug(oldPeaksCurve.object(), oldOutAttr);

				MPlugArray oldConnections;
				oldOutPlug.connectedTo(oldConnections, false, true);

				if (oldConnections.length() > 0)
				{
					MDGModifier mdgModifier;

					for (auto& el : oldConnections)
					{
						mdgModifier.disconnect(oldOutPlug, el);
					}

					mdgModifier.doIt();

					MFnDependencyNode newPeaksCurveNode(peaksCurve.object());
					MPlug newOutPlug(newPeaksCurveNode.object(), newPeaksCurveNode.attribute("output", &stat));

					if (!stat)
					{
						MimikaCommand::IfNotStat(stat, "coudn't get output attribute from newly created animCurveTU node", true);
					}

					for (auto& el : oldConnections)
					{
						mdgModifier.connect(newOutPlug, el);
					}

					mdgModifier.doIt();

					mdgModifier.renameNode(oldPeaksCurve.object(), string(string("old") + MimikaCommand::curveNodeName + "#").c_str());
					mdgModifier.renameNode(newPeaksCurveNode.object(), MimikaCommand::curveNodeName.c_str());
					mdgModifier.doIt();
				}
			}
			
			if (MimikaCommand::nnArg != "" && MimikaCommand::atrArg != "")
			{
				MFnDependencyNode theNode;
				for (MItDependencyNodes iter; !iter.isDone(); iter.next())
				{
					theNode.setObject(iter.thisNode());

					string nodeName = theNode.name().asChar();
					if (nodeName.find(MimikaCommand::nnArg) != string::npos)
					{
						MObject atr = theNode.attribute(MimikaCommand::atrArg.c_str(), &stat);
						if (stat)
						{
							MPlug atrPlug(theNode.object(), atr);
							int val = atrPlug.asInt(&stat);
							if (stat)
							{
								if (val > 1000)
									val = 1;
								else
									val++;
								atrPlug.setInt(val);
							}
						}

						break;
					}
				}
			}
		}

	}
	catch (const std::exception& e)
	{
		MGlobal::displayError(e.what());
		MimikaCommand::RemoveTimerCallback();
	}
}

MStatus MimikaCommand::AddTimerCallback()
{
	MStatus stat;

	MimikaCommand::callbackId = MTimerMessage::addTimerCallback(0.02f, MimikaCommand::TimerCallback, nullptr, &stat);

	isCallbackSet = stat ? true : false;

	return stat;
}

MStatus MimikaCommand::RemoveTimerCallback()
{
	if (!isCallbackSet)
		return MStatus::kSuccess;

	MStatus stat = MEventMessage::removeCallback(MimikaCommand::callbackId);

	isCallbackSet = stat ? false : true;

	return stat;
}

void MimikaCommand::IfNotStat(const MStatus& stat, const char* message, bool doThrow)
{
	if (!stat)
	{
		stat.perror(message);
		
		if (doThrow)
			throw NotStatException(message);
	}
}