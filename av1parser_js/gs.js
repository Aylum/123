var pickColor = (mult) =>
  isNaN(mult) ? null : mult >= 1 && mult < 2 ? [34, 168, 192] : mult >= 2 && mult < 10 ? [107, 7, 210] : 
  mult >=10 ? [144, 0, 135] : [100, 146, 170];

function doPost(e) {
  var date1 = new Date(Date.now());
  var sheetName = 'aviator_' + String(date1.getFullYear()) + '_' + String(date1.getMonth() + 1) + '_'
    + String(date1.getDate()) + ' ' + String(date1.getHours()) + ':'
    + String(date1.getMinutes()) + ':' + String(date1.getSeconds());
  
  var ss = SpreadsheetApp.create(sheetName);

  try{
  var dicts = JSON.parse(e.postData.contents);
  var jj = dicts[0];

  var maxEpisode = -1;
  var minEpisode = 999999;
  for (var i in jj) {
    var keys = Object.getOwnPropertyNames(jj[i]);
    for (var k = 0; k < keys.length; k++) {
      var key = parseInt(keys[k]);
      if (key > maxEpisode) {
        maxEpisode = key;
      } else if (key < minEpisode) {
        minEpisode = key;
      }
    }
  }

  var header1 = ['Player', 'Total bet', 'Total win', 'Win - Bet'];
  for (var i = minEpisode; i <= maxEpisode; i++) {
    header1.push('Player');
    header1.push('Bet');
    header1.push('Multiplier');
    header1.push('Win')
    header1.push('Episode');
  }

  var toWrite = [header1];
  var allBet = 0;
  var allWin = 0;

  for (var i in jj) {
    var resPlayer = [i];
    var tempPlayer = [];
    for (var k = minEpisode; k <= maxEpisode; k++) {
      tempPlayer.push(i);
      var playerTotalBet = 0;
      var playerWin = 0;
      
      if (jj[i].hasOwnProperty(k)) {
        var bet = parseFloat(jj[i][k][0]);
        if (!isNaN(bet)) {
          tempPlayer.push(bet);
          playerTotalBet += bet;
        } else {
          tempPlayer.push('');
        }
        tempPlayer.push(isNaN(parseFloat(jj[i][k][1])) ? '' : String(jj[i][k][1]) + 'x');
        
        var win = parseFloat(jj[i][k][2]);
        if (!isNaN(win)) {
          tempPlayer.push(win);
          playerWin += win;
        } else {
          tempPlayer.push('');
        }
      } else {
        tempPlayer.push('');
        tempPlayer.push('');
        tempPlayer.push('');
      }
      tempPlayer.push('episode ' + String(k));
    }

    resPlayer.push(playerTotalBet);
    resPlayer.push(playerWin);
    resPlayer.push(playerWin-playerTotalBet);
    toWrite.push(resPlayer.concat(tempPlayer));
    
    allBet += playerTotalBet;
    allWin += playerWin;
  }

  var maxRows = Object.getOwnPropertyNames(jj).length;
  ss.getSheets()[0].getRange(1, 1, 1, 6).setValues([['All bets', allBet, 'Players win',
    allWin, 'Players win (win - bets)', allWin - allBet]]
  );
  ss.getSheets()[0].getRange(3, 1, toWrite.length, toWrite[0].length).setValues(toWrite);

  var multsKeys = Object.getOwnPropertyNames(dicts[1]).sort((a, b)=>parseInt(a) < parseInt(b) ? -1 : 1);
  for (var i = 0; i < multsKeys.length; i++){
    var epNum = parseInt(multsKeys[i]);
    if (epNum > maxEpisode || epNum < minEpisode) {
      continue;
    }
    var range1 = ss.getSheets()[0].getRange(2, i+1);
    range1.setValue(['ep' + String(multsKeys[i]) + '  ' + String(dicts[1][multsKeys[i]]) + 'x']);
    var color1 = pickColor(parseFloat(dicts[1][multsKeys[i]]));
    range1.setBackgroundRGB(color1[0], color1[1], color1[2]);
  }

  var vals = ss.getDataRange().getValues();
  for (var i = 0; i < vals[2].length; i++) {
    if (vals[2][i] == 'Player') {
      ss.getSheets()[0].getRange(3, i+1, maxRows+1, 1).setBackgroundRGB(153, 153, 153);
    }
  }
  for (var i = 0; i < vals.length; i++) {
    for (var k = 0; k < vals[i].length; k++) {
      var parsed = parseFloat(vals[i][k]);
      if (vals[2][k] == 'Bet' && !isNaN(parsed)) {
        ss.getSheets()[0].getRange(i+1, k+1).setBackgroundRGB(255, 242, 204);
      } else if (vals[2][k] == 'Multiplier' && !isNaN(parsed)) {
        var color1 = pickColor(parsed)
        ss.getSheets()[0].getRange(i+1, k+1).setBackgroundRGB(color1[0], color1[1], color1[2]);
      } else if (vals[2][k] == 'Win' && !isNaN(parsed)) {
        ss.getSheets()[0].getRange(i+1, k+1).setBackgroundRGB(217, 234, 211);
      } else if (vals[2][k] == 'Total win' && parsed > 0) {
        ss.getSheets()[0].getRange(i+1, k+1).setBackgroundRGB(217, 234, 211);
      } else if (vals[2][k] == 'Win - Bet' && parsed > 0) {
        ss.getSheets()[0].getRange(i+1, k+1).setBackgroundRGB(147, 196, 125);
      }
    }
  }
} catch (e) {
  ss.appendRow([e]);
}
}