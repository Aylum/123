//document.location.href = document.getElementsByClassName('CasinoGameAviator__game-inner')[0].children[0].src


var toSheetUrl = 'https://script.google.com/macros/s/AKfycbwGtwTIsBltTVUipEkeq_ADCUvFf0p3coqrCNjbj6R7EJQmteXsJBCEwUajHFvhvmEYZw/exec';
var allScrolled = {};
var allMults = {};
var roundNumber = 0;
var avStop = false;
var avSend = false;

var i = document.createElement('iframe');
i.style.display = 'none';
document.body.appendChild(i);
window.console = i.contentWindow.console;

class Player1 {
    bet;
    win;
    adj;
    constructor(bet, win, adj) {
        this.bet = bet;
        this.win = win;
        this.adj = adj;
    }

    isAdjHasEmptyStr() {
        return this.adj[0] == '' || this.adj[1] == '';
    }

    isAdjTheSame(newAdj) {
        return (this.adj[0] == '' || newAdj.adj[0] == '' ? true : this.adj[0] == newAdj.adj[0])
        && (this.adj[2] == '' || newAdj.adj[2] == '' ? true : this.adj[2] == newAdj.adj[2])
        && (this.adj[1] == newAdj.adj[1]);
    }

    static getAdjacentPlayers(innerText1Arr, ind) {
        return [
            (ind - 4 > -1 ? innerText1Arr[ind-4] : ''),
            (innerText1Arr[ind]),
            (ind + 4 < innerText1Arr.length ? innerText1Arr[ind+4] : '')
        ];
    }
}

Array.prototype.tryPushPlayer1 = function (pl1) {
    let i = this.length;
    let res = [];
    while (i--) {
        if (this[i].bet == pl1.bet
            && this[i].isAdjTheSame(pl1)) {
            
            if (this[i].isAdjHasEmptyStr()
                && !pl1.isAdjHasEmptyStr()) {
                
                this[i].adj = pl1.adj;
            }
            if (isNaN(parseFloat(this[i].win))
                && !isNaN(parseFloat(pl1.win))) {
                
                this[i].win = pl1.win;
                res.push('w_' + String(pl1.win));
            }
            i = -2;
            break;
        }
    }
    if (i != -2) {
        this.push(pl1);
        res.push('b_' + String(pl1.bet));
        if (!isNaN(parseFloat(pl1.win))) {
            res.push('w_' + String(pl1.win));
        }
    }
    return res;
}

var resolvePlayers = function (innerTextArr)
{
    let epPlayers = {}
    let epStats = {
        'playersCount' : 0,
        'totalBets' : 0,
        'totalWin' : 0,
        'betsMinusWin' : 0,
    };

    for (let i = 0; i < innerTextArr.length; i++) {
        let splitted = innerTextArr[i].split('|');
        for (let k = 0; k < splitted.length; k+=4) {
            if (isNaN(parseFloat(splitted[k+1]))) {
                continue;
            }
            
            let plName = splitted[k];
            let pl1 = new Player1(
                splitted[k+1],
                splitted[k+3],
                Player1.getAdjacentPlayers(splitted, k)
            );
            if (!epPlayers.hasOwnProperty(plName)) {
                epPlayers[plName] = [pl1];
                epStats.playersCount += 1;
                epStats.totalBets += parseFloat(pl1.bet);
                let tempWin = parseFloat(pl1.win);
                if (!isNaN(tempWin)) {
                    epStats.totalWin += tempWin;
                }
            } else {
                let stats = epPlayers[plName].tryPushPlayer1(pl1);
                //assert stats.length <= 3
                for (let s = 0; s < stats.length; s++) {
                    if (stats[s][0] == 'w' && stats[s][1] == '_') {
                        epStats.totalWin += parseFloat(stats[s].slice(2));
                    } else if (stats[s][0] == 'b' && stats[s][1] == '_') {
                        epStats.playersCount += 1;
                        epStats.totalBets += parseFloat(stats[s].slice(2));
                    }
                }
            }
        }
    }
    
    epStats.betsMinusWin = epStats.totalBets - epStats.totalWin;

    return epStats;
}

var isAVsended = null;
var sendToTable = function (toSheetUrl, whatSend, maxSendEp) {
    fetch(toSheetUrl, {
        method : 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(whatSend),
        mode : 'no-cors',
    }).then(function (response) {
        console.log('Send to table: ok');
        isAVsended = maxSendEp;
    }).catch(function (err) {
        console.log('Couldn\'t send data for episode ' + String(roundNumber) + '. Error: ', err);
    });
}

var avEventLoopQueue1 = () => {
    return new Promise(resolve =>
        setTimeout(() => {
            resolve();
        })
    );
}

var prepareToSend = function(inptDict) {
    let epStats = {};
    for (let i in inptDict) {
        epStats[i] = resolvePlayers(inptDict[i]);
    }
    
    return epStats;
}

var aviatorSender1 = async () => {
    while(!avStop) {
        if (avSend) {
            avSend = false;
            
            let whatSend = {};
            Object.assign(whatSend, allScrolled);
            let maxSendEp = Object.getOwnPropertyNames(whatSend).sort((a, b)=>parseInt(a) < parseInt(b) ? -1 : 1).slice(-1)[0];
            whatSend = prepareToSend(whatSend);

            let whatSend2 = {};
            Object.assign(whatSend2, allMults);

            sendToTable(toSheetUrl, [whatSend, whatSend2], maxSendEp);
        }
        if (isAVsended != null) {
            let lastSended = isAVsended;
            isAVsended = null;
            for (let i = 0; i <= lastSended; i++) {
                if (allScrolled.hasOwnProperty(i)) {
                    delete allScrolled[i];
                }
                if (allMults.hasOwnProperty(i)) {
                    delete allMults[i];
                }
            }
        }
        await avEventLoopQueue1();
    }
}

var getGameState1 = () => [].slice.call(document.getElementsByClassName('dom-container')[1].children)[0].innerText;
const sleepNow = (delay) => new Promise((resolve) => setTimeout(resolve, delay));

var isRoundEnded = true;
var SCROLL_BY = 300;
var prevStr = '';
var prevScrollPos = -1;
var scrolledRound = [];

var aviatorParser1 = async () => {
    while (!avStop) {
        let gameState = getGameState1();
        let splittedGM = gameState.split('\n');

        if (splittedGM.length == 1 && splittedGM[0] != '' && isNaN(parseFloat(splittedGM[0]))) {
            isRoundEnded = true;
        } else {
            let sc1 = document.getElementsByTagName('cdk-virtual-scroll-viewport')[0];
            if (isRoundEnded && splittedGM.length == 1) {
                isRoundEnded = false;
                if (scrolledRound.length > 0) {
                    let multsRound = Object.getOwnPropertyNames(allMults).sort((a, b)=>parseInt(a) < parseInt(b) ? -1 : 1).slice(-1)[0];
                    allScrolled[multsRound] = scrolledRound;
                } else if (allMults.hasOwnProperty(roundNumber)) {
                    delete allMults[roundNumber];
                }
                roundNumber++;
                sc1.scrollTo(0, 0);
                prevStr = '';
                scrolledRound = [];
                prevScrollPos = -1;
            }
            if (splittedGM.length > 1 && scrolledRound.length > 0 && !allMults.hasOwnProperty(roundNumber)) {
                let mult = parseFloat(splittedGM[1]);
                if (!isNaN(mult)) {
                    allMults[roundNumber] = mult;
                }
            }

            if (sc1.scrollTop == 0) {
                SCROLL_BY = sc1.clientHeight * 1 - 2;
            } else if (prevScrollPos == sc1.scrollTop) {
                SCROLL_BY = -1 * (sc1.clientHeight) * 1 - 2;
            }
            prevScrollPos = sc1.scrollTop;

            let currStr = document.getElementsByTagName('cdk-virtual-scroll-viewport')[0].innerText;
            sc1.scrollBy(0, SCROLL_BY);
            //await sleepNow(1);

            if (currStr != prevStr) {
                scrolledRound.push(currStr.replaceAll('\n', '|'));
            }
            prevStr = currStr;
        }
        await avEventLoopQueue1();
    }
}

aviatorParser1().then();
aviatorSender1().then();