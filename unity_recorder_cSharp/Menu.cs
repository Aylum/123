﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Menu : MonoBehaviour
{
    private const string BACKUP_VIDEO_OUT_DIR = "Assets/BackupAnimation";//rename video dir
    private const string MAIN_VIDEO_OUT_DIR = "Assets/RawAnimation";//rename video dir
    private string DEFAULT_VIDEO_DIR = "M:/premaster/cut";
    private const string VIDEO_SEARCH_PATTERN = "ep{0:00}_sc*{1:00}*.mov";
    private const string BACKUP_ANIM_CLIP_NAME = "{0}/ep{1:00}_sc{2:00}_take{3:00}_{4}_{5}.anim";
    private const string MAIN_ANIM_CLIP_NAME1 = "{0}/ep{1:00}/sc{2:00}";
    private const string MAIN_ANIM_CLIP_NAME2 = "/{3}_{4}.anim";

    private MenuObjectsStorage main = null;
    bool isVideoOk = false;

    public void OnStartBtnClick()
    {
        try
        {
            if (isVideoOk)
            {
                VideoPlayer video = main.videoPlayer.GetComponent<VideoPlayer>();
                if (!video.url.Contains("//") || !File.Exists(video.url.Replace("//", "\\").Split('\\')[1]))
                {
                    Debug.LogError("OnRecordBtnClick: video.url == \"\" or file doesn't exists");
                    isVideoOk = false;
                }
                video.Prepare();
                if (video.isPrepared)
                {
                    Debug.LogError("OnRecordBtnClick: video.isPrepared == false");
                }
                video.Play();
            }
        }
        catch (System.Exception ex) { }

        main.recorder.GetComponent<AnimRecorder>().LoadRecord();
        main.recorder.GetComponent<AnimRecorder>().StartRecord();

        main.stopButton.SetActive(true);
        main.startButton.SetActive(false);
    }

    public void OnRecordBtnClick()
    {
        bool mainToggle = main.mainToggle.GetComponent<Toggle>().isOn;
        SetRecPath("zuza",
            int.Parse(main.episodeNumLabel.GetComponent<Text>().text),
            int.Parse(main.sceneNumLabel.GetComponent<Text>().text),
            main.zuzaToggle.GetComponent<Toggle>().isOn,
            mainToggle
        );
        SetRecPath("gek",
            int.Parse(main.episodeNumLabel.GetComponent<Text>().text),
            int.Parse(main.sceneNumLabel.GetComponent<Text>().text),
            main.gekToggle.GetComponent<Toggle>().isOn,
            mainToggle
        );
        SetRecPath("platon",
            int.Parse(main.episodeNumLabel.GetComponent<Text>().text),
            int.Parse(main.sceneNumLabel.GetComponent<Text>().text),
            main.platonToggle.GetComponent<Toggle>().isOn,
            mainToggle
        );
        SetRecPath("camera",
            int.Parse(main.episodeNumLabel.GetComponent<Text>().text),
            int.Parse(main.sceneNumLabel.GetComponent<Text>().text),
            main.cameraToggle.GetComponent<Toggle>().isOn,
            mainToggle
        );      

        main.firstLayout.SetActive(false);
        main.secondLayout.SetActive(true);
        main.startButton.SetActive(true);
        main.stopButton.SetActive(false);
    }

    public void onStopRecordBtnClick()
    {
        main.videoPlayer.GetComponent<VideoPlayer>().Stop();
        main.recorder.GetComponent<AnimRecorder>().StopRecord();
        main.startButton.SetActive(true);
        main.stopButton.SetActive(false);
    }

    public void OnBackDataMenuBtnClick()
    {
        main.videoPlayer.GetComponent<VideoPlayer>().Stop();
        main.recorder.GetComponent<AnimRecorder>().StopRecord();
        main.firstLayout.SetActive(true);
        main.secondLayout.SetActive(false);
    }

    public void onEpUpBtnClick()
    {
        Text temp = main.episodeNumLabel.GetComponent<Text>();
        ChangeNumLabel(ref temp, true);
        if (int.Parse(temp.text) < 1 || int.Parse(main.sceneNumLabel.GetComponent<Text>().text) < 1)
        {
            VideoPlayer video = main.videoPlayer.GetComponent<VideoPlayer>();
            video.Stop();
            isVideoOk = false;
            main.videoExistLabel.GetComponent<Text>().text = "";
            return;
        }
        ChangeVideo();
    }

    public void onEpDownBtnClick()
    {
        Text temp = main.episodeNumLabel.GetComponent<Text>();
        ChangeNumLabel(ref temp, false);
        if (int.Parse(temp.text) < 1 || int.Parse(main.sceneNumLabel.GetComponent<Text>().text) < 1)
        {
            VideoPlayer video = main.videoPlayer.GetComponent<VideoPlayer>();
            video.Stop();
            isVideoOk = false;
            main.videoExistLabel.GetComponent<Text>().text = "";
            return;
        }
        ChangeVideo();
    }

    public void onScUpBtnClick()
    {
        Text temp = main.sceneNumLabel.GetComponent<Text>();
        ChangeNumLabel(ref temp, true);
        if (int.Parse(temp.text) < 1 || int.Parse(main.episodeNumLabel.GetComponent<Text>().text) < 1)
        {
            VideoPlayer video = main.videoPlayer.GetComponent<VideoPlayer>();
            video.Stop();
            isVideoOk = false;
            main.videoExistLabel.GetComponent<Text>().text = "";
            return;
        }
        ChangeVideo();
    }

    public void onScDownBtnClick()
    {
        Text temp = main.sceneNumLabel.GetComponent<Text>();
        ChangeNumLabel(ref temp, false);
        if (int.Parse(temp.text) < 1 || int.Parse(main.episodeNumLabel.GetComponent<Text>().text) < 1)
        {
            VideoPlayer video = main.videoPlayer.GetComponent<VideoPlayer>();
            video.Stop();
            isVideoOk = false;
            main.videoExistLabel.GetComponent<Text>().text = "";
            return;
        }
        ChangeVideo();
    }

    private void ChangeNumLabel(ref Text text, bool increase)
    {
        MenuObjectsStorage main = GetMainLayoutObj();
        int curNum;
        curNum = int.TryParse(text.text, out curNum) ? curNum : 0;
        curNum = increase ? curNum + 1 : curNum - 1;
        text.text = curNum.ToString("00");
    }

    private void ChangeVideo()
    {
        VideoPlayer video = main.videoPlayer.GetComponent<VideoPlayer>();
        Text text = main.videoExistLabel.GetComponent<Text>();
        video.Stop();

        string path = MyNormpath(main.videoPathLabel.GetComponent<Text>().text);
        string[] foundVids = Directory.GetFiles(path,
            string.Format(VIDEO_SEARCH_PATTERN,
            int.Parse(main.episodeNumLabel.GetComponent<Text>().text),
            int.Parse(main.sceneNumLabel.GetComponent<Text>().text))
        );
        //path += "/" + string.Format(
        //    VIDEO_NAME,
        //    int.Parse(main.episodeNumLabel.GetComponent<Text>().text),
        //    int.Parse(main.sceneNumLabel.GetComponent<Text>().text)
        //);
        if (foundVids.Length < 1 || !File.Exists(foundVids[0]))
        {
            isVideoOk = false;
            text.text = "NO EXIST";
            text.color = new Color((float)210 / 255, (float)25 / 255, (float)25 / 255);
            return;
        }

        video.url = "file://" + foundVids[0].Replace('\\', '/');
        text.text = "EXIST";
        text.color = new Color((float)77 / 255, (float)255 / 255, (float)45 / 255);
        isVideoOk = true;
    }

    private void SetRecPath(string titleName, int ep, int sc, bool toggle, bool mainToggle)
    {
        AnimRecorder recorder = main.recorder.GetComponent<AnimRecorder>();
        for (int i = 0; i < recorder.recObjects.Length; i++)
        {
            if (recorder.recObjects[i].titleName == titleName)
            {
                if (mainToggle)
                {
                    Directory.CreateDirectory(Directory.GetParent(Application.dataPath) + "/" + BACKUP_VIDEO_OUT_DIR);
                    recorder.recObjects[i].savePaths = new List<string>();
                    recorder.recObjects[i].savePaths.Add(string.Format(
                        BACKUP_ANIM_CLIP_NAME,
                        BACKUP_VIDEO_OUT_DIR,
                        ep,
                        sc,
                        GetNewTakeNum(ep, sc),
                        recorder.recObjects[i].gObject.name,
                        recorder.recObjects[i].titleName
                    ));
                }
                if (toggle)
                {
                    Directory.CreateDirectory(string.Format(MAIN_ANIM_CLIP_NAME1, Directory.GetParent(Application.dataPath) + "/" + MAIN_VIDEO_OUT_DIR, ep, sc));
                    recorder.recObjects[i].savePaths.Add(string.Format(
                        MAIN_ANIM_CLIP_NAME1 + MAIN_ANIM_CLIP_NAME2,
                        MAIN_VIDEO_OUT_DIR,
                        ep,
                        sc,
                        recorder.recObjects[i].gObject.name,
                        recorder.recObjects[i].titleName
                    ));
                }
            }
        }
    }

    private int GetNewTakeNum(int ep, int sc)
    {
        //"m:/backup\\ep01_sc01_take01_Cube_zuza.anim"
        string[] files = Directory.GetFiles(BACKUP_VIDEO_OUT_DIR, string.Format("ep{0:00}_sc{1:00}*.anim", ep, sc));
        if (files.Length < 1)
            return 0;
        int maxTake = -1;
        foreach (var file in files)
        {
            string[] splitted = MyNormpath(file).Split('/');
            if (splitted.Length < 1)
                continue;
            splitted = splitted[splitted.Length - 1].Split('.');
            if (splitted.Length < 1)
                continue;
            splitted = splitted[0].Split('_');
            if (splitted.Length < 3)
                continue;
            string tempStr = SelectDigits(splitted[2]);
            int tempTake;
            if (int.TryParse(tempStr, out tempTake) && tempTake > maxTake)
            {
                maxTake = tempTake;
            }
        }
        return maxTake == -1 ? 999 : maxTake + 1;
    }

    private MenuObjectsStorage GetMainLayoutObj()
    {
        GameObject main = GameObject.Find("mainLayout");
        if (main == null)
        {
            Debug.Log("Couldn't find 'mainLayout' gameobject");
        }
        MenuObjectsStorage res = main.GetComponent<MenuObjectsStorage>();
        if (res == null)
        {
            Debug.Log("Couldn't get 'MenuObjectsStorage' component from gameobject mainLayout");
        }
        return res;
    }

    private static string MyNormpath(string path, bool trimEndSlash = true)
    {
        path = path.Replace("\\", "/");
        do
        {
            path = path.Replace("//", "/");
        } while (path.Contains("//"));
        return trimEndSlash && path[path.Length - 1] == '/' ? path.Substring(0, path.Length - 1) : path;
    }

    private static string SelectDigits(string str)
    {
        string res = "";
        foreach (char c in str)
        {
            if ("0123456789".Contains(c))
            {
                res += c;
            }
        }
        return res;
    }

    void Start()
    {
        main = GetMainLayoutObj();
        main.firstLayout.SetActive(true);
        main.secondLayout.SetActive(false);

        DEFAULT_VIDEO_DIR = main.videoPathLabel.GetComponent<Text>().text;

        main.sceneNumLabel.GetComponent<Text>().text = "00";
        main.episodeNumLabel.GetComponent<Text>().text = "00";
        main.videoExistLabel.GetComponent<Text>().text = "";
    }

    void Update()
    {

    }
}
