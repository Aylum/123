﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class ExporterEditorWindow : EditorWindow
{
    private class _Pair<T>
    {
        public T a;
        public T b;

        public _Pair(T a, T b)
        {
            this.a = a;
            this.b = b;
        }
    }

    [MenuItem("MocapExport/Export")]
    private static void Init()
    {
        ExporterEditorWindow window = EditorWindow.GetWindow<ExporterEditorWindow>();
        window.Show();
    }

    private string exportFolderName = "FbxExport";
    private string serializeFilePath = @"Assets/FbxExport/ExporterEditorWindow.json";

    //[SerializeField] private string[] persPopup = new string[2] { "straus", "baran" };
    //[SerializeField] private int persChoise = 0;

    [SerializeField] private bool isShowClipNameListButton = true;
    [SerializeField] private List<string> clipNamesToExportList = new List<string>();
    [SerializeField] private string addNameToListTextField = "";

    private string gobjSharedTag = "MocapExport";
    private string gobjChildToExportTag1 = "tracker";
    private string gobjChildToExportTag2 = "glove";
    //[SerializeField] private string clipName = "";
    [SerializeField] private string epName = "";
    [SerializeField] private string epPath = "";
    [SerializeField] private string sceneTandecDir = "Tanec";

    //private int directoryCapacity1 = 50;
    private int directoryCapacity2 = 30;

    private string exportScenesRangeTextField = "";
    private int sceneStartInd = 0;
    private int sceneEndInd = 50;

    private string sceneDirName = "sc";
    private string takeDirName = "take_";

    private List<string> controllersPath = new List<string>();

    private void OnEnable()
    {
        string exportFolderPath = "";
        if (!AssetDatabase.IsValidFolder(@"Assets/" + exportFolderName))
        {
            string guid = AssetDatabase.CreateFolder("Assets", exportFolderName);
            exportFolderPath = AssetDatabase.GUIDToAssetPath(guid);
        }
        if (exportFolderPath == "") exportFolderPath = "Assets";

        if (!System.IO.File.Exists(serializeFilePath))
            System.IO.File.WriteAllText(serializeFilePath, JsonUtility.ToJson(this, true));
        else
            JsonUtility.FromJsonOverwrite(System.IO.File.ReadAllText(serializeFilePath), this);
    }

    private void OnGUI()
    {
        //persChoise = EditorGUILayout.Popup(persChoise, persPopup);

        if (GUILayout.Button(isShowClipNameListButton ? "Hide clip names list" : "Show clip names list"))
        {
            isShowClipNameListButton = !isShowClipNameListButton;
        }
        if (isShowClipNameListButton)
        {
            addNameToListTextField = EditorGUILayout.TextField("anim clip name", addNameToListTextField);

            if (GUILayout.Button("Add name to list"))
            {
                if (addNameToListTextField != "")
                {
                    string tempStr = addNameToListTextField;

                    if (!tempStr.Contains(".anim"))
                        tempStr += ".anim";

                    if (!clipNamesToExportList.Contains(tempStr))
                        clipNamesToExportList.Add(tempStr);
                }
            }

            ScriptableObject target = this;
            SerializedObject so = new SerializedObject(target);
            SerializedProperty stringsProperty = so.FindProperty("clipNamesToExportList");

            EditorGUILayout.PropertyField(stringsProperty, true);
            so.ApplyModifiedProperties();
        }

        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

        //gobj = (GameObject)EditorGUILayout.ObjectField("GameObject", (Object)gobj, typeof(GameObject), true);
        //clipName = EditorGUILayout.TextField("clip name", clipName);
        epPath = EditorGUILayout.TextField("animation dir name", epPath);
        epName = EditorGUILayout.TextField("episode dir name", epName);
        exportScenesRangeTextField = EditorGUILayout.TextField("scenes to export 1-3, 5, 7", exportScenesRangeTextField);
        //sceneStartInd = EditorGUILayout.IntField("scene start number", sceneStartInd);
        //sceneEndInd = EditorGUILayout.IntField("scene end number", sceneEndInd);

        if (GUILayout.Button("export") && epPath != "" && epName != "")
        {
            epPath = epPath.Replace(@"\", "").Replace(@"/", "");
            epName = epName.Replace(@"\", "").Replace(@"/", "");
            //string[] splittedPath = tempEpPathStr.Split('/');
            //epName = splittedPath.Length > 0 ? splittedPath[splittedPath.Length - 1] : tempEpPathStr;

            GameObject[] gobjectsToExport = GameObject.FindGameObjectsWithTag(gobjSharedTag);

            if (gobjectsToExport.Length < 1)
            {
                Debug.Log("ExporterEditorWindow: can't find any GameObject with tag " + gobjSharedTag + " nothing will be exported");
                return;
            }

            exportScenesRangeTextField = exportScenesRangeTextField.Replace(" ", "");
            string[] splittedString = exportScenesRangeTextField.Split(',');
            List<_Pair<int>> parsedSceneRanges = new List<_Pair<int>>(splittedString.Length);
            bool isExportRangeWrongStr = false;

            foreach (var str in splittedString)
            {
                var splitted = str.Split('-');

                if (splitted.Length > 2)
                {
                    isExportRangeWrongStr = true;
                    continue;
                }

                if (splitted.Length == 1)
                {
                    int res;
                    if (int.TryParse(splitted[0], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out res))
                    {
                        parsedSceneRanges.Add(new _Pair<int>(res, res));
                    }
                    else
                    {
                        isExportRangeWrongStr = true;
                    }
                }
                else
                {
                    int res1;
                    int res2;

                    if (int.TryParse(splitted[0], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out res1)
                        && int.TryParse(splitted[1], System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out res2))
                    {
                        parsedSceneRanges.Add(new _Pair<int>(res1, res2));

                    }
                    else
                    {
                        isExportRangeWrongStr = true;
                    }
                }
            }

            if (isExportRangeWrongStr || parsedSceneRanges.Count < 1)
            {
                Debug.Log("Couldn't read properly scene ranges TextField");
            }

            foreach (var clipName in clipNamesToExportList)
            {
                if (clipName.Length < 7)
                {
                    Debug.Log("ExporterEditorWindow: wrong .anim clip name, name: " + clipName);
                    continue;
                }
                string gobjName = "";
                if (clipName.Contains(".anim")) gobjName = clipName.Split('.')[0];
                var splitted = gobjName.Split('_');
                if (splitted.Length < 2)
                {
                    Debug.Log("ExporterEditorWindow: wrong .anim clip name. Name should contain at least one '_' symbol, name: " + clipName);
                    continue;
                }

                gobjName = splitted[splitted.Length - 2];
                GameObject gobj = null;

                foreach (var tempGobj in gobjectsToExport)
                {
                    if (tempGobj.name == gobjName)
                    {
                        gobj = tempGobj;
                        break;
                    }
                }
                if (gobj == null)
                {
                    Debug.Log("ExporterEditorWindow: can't find GameObject with name " + gobjName);
                    continue;
                }

                if (!gobj.tag.Contains(gobjSharedTag))
                {
                    Debug.Log("ExporterEditorWindow: found GameObject with name: " + gobjName
                            + ". But it's missing required for export tag: " + gobjSharedTag
                            + ". This GameObject won't be exported.");

                    continue;
                }

                GameObject proxyGobj = new GameObject("Proxy_" + gobj.name);
                Transform[] allComps = gobj.GetComponentsInChildren<Transform>().Skip(1).ToArray();
                List<Transform> filteredByTagComps = new List<Transform>();

                foreach (var comp in allComps)
                {
                    if (comp.gameObject.tag.Contains(gobjChildToExportTag1) || comp.gameObject.tag.Contains(gobjChildToExportTag2))
                    {
                        filteredByTagComps.Add(comp);
                    }
                }

                if (filteredByTagComps.Count < 1)
                {
                    Debug.Log("GameObject with name: " + gobjName
                            + ". Won't be exported, make shure GameObject's children have tag: "
                            + gobjChildToExportTag1 + "or" + gobjChildToExportTag2);
                    continue;
                }

                Transform[] comps = filteredByTagComps.ToArray();
                List<Transform> proxyGobjects = new List<Transform>(comps.Length + 1);
                proxyGobjects.Add(proxyGobj.transform);

                RecreateHierarchy(gobj, comps, proxyGobjects[0].gameObject, ref proxyGobjects);

                Animator addedAnim = ObjectFactory.AddComponent<Animator>(proxyGobj);

                if (addedAnim == null)
                {
                    Debug.Log("ExporterEditorWindow: can't add Animator to gameobject with name"
                        + proxyGobj.name
                    );
                    continue;
                }

                //int takeInd = 0;
                //while (takeInd < directoryCapacity2)
                //{
                //    IterateTakes(sceneTandecDir, takeInd, addedAnim, clipName.Contains(".anim") ? clipName : clipName + ".anim", proxyGobj, gobj.name);
                //    takeInd++;
                //}
                //takeInd = 0;

                foreach (var range in parsedSceneRanges)
                {
                    int sceneInd1 = range.a;
                    int sceneEndInd2 = range.b;
                    while (sceneInd1 <= sceneEndInd2)
                    {
                        IterateTakes(sceneInd1.ToString("00"), addedAnim, clipName.Contains(".anim") ? clipName : clipName + ".anim", proxyGobj, gobj.name);
                        sceneInd1++;
                    }
                }

                foreach (var el in proxyGobjects)
                    if (el != null)
                        DestroyImmediate(el.gameObject);

                foreach (var controller in controllersPath)
                    AssetDatabase.DeleteAsset(controller);
                controllersPath.Clear();
            }

        }

    }

    private void OnDisable()
    {
        string exportFolderPath = "";
        if (!AssetDatabase.IsValidFolder(@"Assets/" + exportFolderName))
        {
            string guid = AssetDatabase.CreateFolder("Assets", exportFolderName);
            exportFolderPath = AssetDatabase.GUIDToAssetPath(guid);
        }
        if (exportFolderPath == "") exportFolderPath = "Assets";

        System.IO.File.WriteAllText(serializeFilePath, JsonUtility.ToJson(this, true));
    }

    private void IterateTakes(string addSceneFolderName, Animator animator, string clipName, GameObject proxyGobj, string gobjName)
    {
        string clipPath = "Assets/" + epPath + "/" + epName + "/"
                + sceneDirName + addSceneFolderName + "/" + clipName;
        AnimationClip clip = AssetDatabase.LoadAssetAtPath<AnimationClip>(clipPath);

        if (clip == null)
            return;

        //if (clip == null)
        //{
        //    Debug.Log("ExporterEditorWindow: can't load animation clip at path: "
        //        + epPath + "/" + epName + "/" + sceneDirName + addSceneFolderName + "/" + takeDirName + takeInd.ToString("00"));
        //    return;
        //}

        string tempControllerPath = CheckOrCreateFolder(CheckOrCreateFolder("Assets", exportFolderName), "temp_controllers");
        if (tempControllerPath == "") tempControllerPath = "Assets";
        tempControllerPath += "/" + epName + "_" + sceneDirName + addSceneFolderName + "_" + gobjName + ".controller";

        var controller = UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPathWithClip(tempControllerPath, clip);
        if (controller == null)
        {
            Debug.Log("ExporterEditorWindow: can't create controller at path: " + tempControllerPath);
            return;
        }

        animator.runtimeAnimatorController = controller;
        controllersPath.Add(tempControllerPath);

        string savePath =
            CheckOrCreateFolder(
                CheckOrCreateFolder(
                    CheckOrCreateFolder("Assets", exportFolderName),
                    epName),
                sceneDirName + addSceneFolderName);

        if (savePath != "")
        {
            var strings = savePath.Split('/').Skip(1);
            string tempStr = "";
            foreach (var str in strings) tempStr += str + '/';
            savePath = tempStr;
        }

        string fbxPath = System.IO.Path.Combine(Application.dataPath, savePath + gobjName + "_exported.fbx");

        UnityEditor.Formats.Fbx.Exporter.ModelExporter.ExportObject(fbxPath, proxyGobj);
    }

    private static void RecreateHierarchy(GameObject sourceParent, Transform[] sourceGobjects, GameObject newParent, ref List<Transform> newGobjects)
    {
        foreach (var gobj in sourceGobjects)
        {
            if (gobj.parent == sourceParent.transform)
            {
                GameObject temp = new GameObject(gobj.name);
                temp.transform.parent = newParent.transform;
                newGobjects.Add(temp.transform);
                RecreateHierarchy(gobj.gameObject, sourceGobjects, temp, ref newGobjects);
            }

        }
    }

    private static string CheckOrCreateFolder(string path, string folderName)
    {
        string newPath = "";
        if (!AssetDatabase.IsValidFolder(path + "/" + folderName))
        {
            string guid = AssetDatabase.CreateFolder(path, folderName);
            newPath = AssetDatabase.GUIDToAssetPath(guid);
        }
        else
            newPath = path + "/" + folderName;

        return newPath;
    }

}