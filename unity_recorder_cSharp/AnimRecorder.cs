﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEditor.Animations;

namespace AnimRecorderUtils
{
    public class Pair<T1, T2>
    {
        public T1 First { get; set; }
        public T2 Second { get; set; }

        public Pair(T1 first, T2 second)
        {
            First = first;
            Second = second;
        }
    }
}

[System.Serializable]
public struct Recobj
{
    public string titleName;
    public GameObject gObject;
    public bool recordThisGameObject;

    public string tag;
    public bool recordChildren;
    public List<string> savePaths;
}

public class AnimRecorder : MonoBehaviour
{
    public string dir;

    public Recobj[] recObjects;

    private List<Tuple<GameObjectRecorder, List<string>>> recordersToSave = null;

    private bool record = false;

    private bool isRecordBeenPressed = false;

    public bool IsRecording()
    {
        return record;
    }

    public void LoadRecord()
    {
        dir = "Assets/RawAnimation";

        //GameObject.Find("Button1").GetComponent<Button>().onClick.AddListener(ToggleRecord);

        if (recordersToSave != null)
        {
            recordersToSave.Clear();
        }

        recordersToSave = new List<Tuple<GameObjectRecorder, List<string>>>(recObjects.Length);

        foreach(var el in recObjects)
        {
            if (el.gObject == null)
                continue;

            GameObjectRecorder gobjRecorder = new GameObjectRecorder(el.gObject);
            bool atLeastOne = false;
            if (el.recordThisGameObject)
            {
                gobjRecorder.BindComponent(el.gObject.GetComponent<Transform>());
                atLeastOne = true;
            }

            if (el.recordChildren)
            {
                GameObject[] children = null;

                Transform[] childrensTransforms = el.gObject.GetComponentsInChildren<Transform>().Skip(1).ToArray();
                if(childrensTransforms.Length > 0)
                {
                    List<GameObject> tempChildren = new List<GameObject>();
					foreach (var childTransform in childrensTransforms)
					{
						if (childTransform.gameObject.tag == el.tag)
							tempChildren.Add(childTransform.gameObject);
					}
                    /* if (el.tag != null && el.tag != "" && el.tag != "Untagged")
                    {
                        foreach (var childTransform in childrensTransforms)
                        {
                            if (childTransform.gameObject.tag == el.tag)
                                tempChildren.Add(childTransform.gameObject);
                        }
                    }
                    else
                    {
                        foreach (var childTransform in childrensTransforms)
                            tempChildren.Add(childTransform.gameObject);
                    } */
                    if (tempChildren.Count > 0)
                        children = tempChildren.ToArray();
                }

                if(children != null)
                {
                    foreach(var child in children)
                    {
                        gobjRecorder.BindComponent(child.GetComponent<Transform>());
                        atLeastOne = true;
                    }
                }  
            }

            if(atLeastOne)
            {
                //if (el.savePaths == null || el.savePaths.Count < 1)
                //    el.savePaths.Add(dir + "/11.anim");
                recordersToSave.Add(new Tuple<GameObjectRecorder, List<string>>(gobjRecorder, el.savePaths));
            }
        }
    }

    private void Start()
    {
        
    }

    private void LateUpdate()
    {
        if (record && recordersToSave != null)
        {
            foreach (var rec in recordersToSave)
            {
                rec.Item1.TakeSnapshot(Time.deltaTime);
            }
        }
        //else if (isRecordBeenPressed)
        //{
        //}
    }
    
    private void OnDisable()
    {
        StopRecord();
    }

    public void StartRecord()
    {
        record = true;
        isRecordBeenPressed = true;
    }

    public void StopRecord()
    {
        if (!record)
            return;

        record = false;
        isRecordBeenPressed = false;

        if (recordersToSave == null)
            return;

        foreach (var recTuple in recordersToSave)
        {
            if(recTuple.Item2.Count < 1)
            {
                continue;
            }
            AnimationClip[] tempClips = new AnimationClip[recTuple.Item2.Count];
            for (int i = 0; i < tempClips.Length; i++)
            {
                tempClips[i] = new AnimationClip();
                tempClips[i].frameRate = 25;
            }
            int clipInd = 0;
            if (recTuple.Item1.isRecording)
            {
                foreach (var clip in tempClips) recTuple.Item1.SaveToClip(clip, 25);
                recTuple.Item1.ResetRecording();
            }
            foreach (var path in recTuple.Item2)
            {
                AssetDatabase.CreateAsset(tempClips[clipInd], path);
                AssetDatabase.SaveAssets();
                clipInd++;
            }
        }
        recordersToSave.Clear();
        recordersToSave = null;
    }

}