var doc = fl.getDocumentDOM();
if (!doc) {
	alert("Please open or create a flashfile.");
} else {
	CreateLabels();
}

function CreateLabels() {
    //get script name and copy it to config directory
    var thisScriptPath = fl.scriptURI;
    var splitted2 = thisScriptPath.split("/");
    var thisScriptName = splitted2[splitted2.length-1];
    var thiScriptDir = thisScriptPath.slice(0, splitted2[splitted2.length-1].length * -1);
    if (!FLfile.exists(fl.configURI + thisScriptName)) {
        FLfile.copy(fl.scriptURI, fl.configURI + thisScriptName);
    }

    //get timeline and layers
    var layers = doc.getTimeline().layers;
	var dialogXML = '';
	dialogXML += '<label value="select layer" width="280" />'

	dialogXML += '<menulist id="names" editable="true" width="380"><menupopup>'
	for (var i=0;i<layers.length;i++) dialogXML += '<menuitem selected="'+(i==0)+'" label="' + layers[i].name + '"/>'
	dialogXML += '</menupopup></menulist>'
	
    var data = createDialogXML(dialogXML);
    if (data.dismiss == "cancel") return;

    var slLayerInd = -1;
    for (var i = 0; i < layers.length; i++) {
        if (data.names == layers[i].name) {
            slLayerInd = i;
            break;
        }
    }
    if (slLayerInd == -1) {
        alert("Error");
        return;
    }
    if (layers[slLayerInd].frameCount < 1) {
        alert("Error: layer " + layers[slLayerInd].name + " contains 0 frames");
        return;
    }
    
    var docPath = document.pathURI;
    if (!docPath) {
        alert("You need to save project in order to export keyframes.");
        return;
    }
    var splitted1 = docPath.split("/");
    var outDir = "";
    if (splitted1.length < 2) {
        outDir = fl.configURI;
        alert("Error: wrong project path, file will be saved to: " + outDir);
    } else {
        outDir = docPath.slice(0, splitted1[splitted1.length-1].length * -1);
    }

    strToWrite = "";
    var outKeyframes = [];
    for (var i = 0; i < layers[slLayerInd].frames.length; i++) {
        if (i != layers[slLayerInd].frames[i].startFrame)/* ||
                    layers[slLayerInd].frames[i].isEmpty ||
                    layers[slLayerInd].frames[i].name =="")*/
        {
            continue;
        }
        strToWrite += layers[slLayerInd].frames[i].name + "|" + (i + 1).toString() + ":" + layers[slLayerInd].frames[i].duration + " ";
        // var parsed = Number(layers[slLayerInd].frames[i].name);
        // if (isNaN(parsed)) continue;
        // if (parsed < 0) continue;
        // outKeyframes.push([parsed, i+1]);
    }

    //unique(outKeyframes)
    // var strToWrite = "";
    // var lastI = 0;
    // for (var i = 0; i < outKeyframes.length; i++) {
    //     var isFirst = false;
    //     for(var k = 0; k < outKeyframes.length; k++) {
    //         if (outKeyframes[k][0] == outKeyframes[i][0]) {
    //             if (i == k) {
    //                 isFirst = true;
    //             }
    //             break;
    //         }
    //     }
    //     if (!isFirst) continue;
    //     strToWrite += outKeyframes[i][0].toString() + "|" + outKeyframes[i][1].toString() + " ";
    //     lastI = i;
    // }

    // var lastFrameDuration = layers[slLayerInd].frames[outKeyframes[lastI][1]-1].duration;
    // strToWrite += (outKeyframes[lastI][0] + lastFrameDuration - 1).toString() + "|" + (outKeyframes[lastI][1] + lastFrameDuration - 1).toString();

    alert("saving file: " + FLfile.write(outDir + "exportedKeyframes.txt", strToWrite) +
            "\npath: " + outDir + "exportedKeyframes.txt"
    );
}

function createDialogXML(xmlString)
{
	var dialogXML = '<dialog title="Label keyframes" buttons="accept, cancel" >';
    dialogXML += '<vbox>' + xmlString + '</vbox>';
	dialogXML +='</dialog>';
	
	return fl.xmlPanelFromString(dialogXML);
}