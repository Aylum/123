from pymel.core import *

import os

EXPORTED_FILE_PATH = "e:/temp/exportedKeyframes.txt"

class test1Ex(Exception):
    pass

def leftKeyIsUniq(source, key):
    if key in source:
        return False
    source.append(key)
    return True

def getLeftoverKeysInRange(keys, a, b):
    res = []
    for key in keys:
        if a < key < b:
            res.append(key)
    return res

def RunButtonCallback():
    try:
        #what if there are spaces in nodeName
        global nodeNameTextField
        if nodeNameTextField is None or nodeNameTextField.getText() == "":
            raise test1Ex("Was entered wrong node name")
        select(nodeNameTextField.getText(), r=True)
        selected = ls(sl=True)
        if len(selected) < 1:
            raise test1Ex("Couldn't select node '{}'".format(nodeNameTextField.getText()))
        connections = listConnections(selected[0], type='animCurve')
        if len(connections) < 1:
            raise test1Ex("Node with name '{}' has 0 connections".format(nodeNameTextField.getText()))
        
        #file
        if not os.path.exists(EXPORTED_FILE_PATH):
            raise test1Ex("File not found, path: " + EXPORTED_FILE_PATH)

        rangesStr = ""
        try:
            with open("e:/temp/exportedKeyframes.txt") as f:
                rangesStr = f.readline()
        except Exception as e:
            raise test1Ex("Couldn't open file: " + str(e))

        if rangesStr == "" or rangesStr.find("|") == -1:
            raise test1Ex("Wrong file format: file is empty or doesn't contains at least one '|' character")

        splittedRangesStr = rangesStr.split(' ')
        if len(splittedRangesStr) < 1:
            raise test1Ex("Wrong file format. Condition len(splittedRangesStr) < 1 is true")

        createdAnimCurves = []
        for inputCurve in connections:
            connectionType = str(nodeType(str(inputCurve)))
            if connectionType.lower().find('anim') == -1:
                continue

            INPUT_CURVE_NAME = str(inputCurve)
            INPUT_CURVE_TYPE = nodeType(INPUT_CURVE_NAME)
            OUT_CURVE_NODE_NAME = INPUT_CURVE_NAME + '_script'

            select(cl=True)
            createNode(INPUT_CURVE_TYPE, n=OUT_CURVE_NODE_NAME, ss=False)
            OUT_CURVE_NODE_NAME = str(ls(sl=True)[0])

            #buffer curve
            tempAnimCurveName = "temp_" + OUT_CURVE_NODE_NAME + "_temp"
            select(cl=True)
            createNode(INPUT_CURVE_TYPE, n=tempAnimCurveName, ss=False)
            tempAnimCurveName = str(ls(sl=True)[0])

            animStartTime = playbackOptions(q=True, animationStartTime=True)
            parsedLeftKeys = []
            leftoverAnimKeys = []
            for i in range(0, len(splittedRangesStr)):
                tempSplitted = splittedRangesStr[i].split('|')
                if len(tempSplitted) < 2:
                    continue
                left = tempSplitted[0].split(':')
                right = tempSplitted[1].split(':')
                if len(left) < 2 or len(right) < 2:
                    continue
                leftKeyIsOk = False
                try:
                    leftKey = int(left[0])
                    leftDuration = int(left[1])
                    if leftKeyIsUniq(parsedLeftKeys, leftKey):
                        leftKeyIsOk = True
                except ValueError:
                    pass
                try:
                    rightKey = int(right[0])
                    rightDuration = int(right[1])
                    if not leftKeyIsOk:
                        leftoverAnimKeys.append(rightKey - animStartTime + 1)
                        continue
                except ValueError:
                    pass
                setKeyframe(tempAnimCurveName,
                    t=rightKey - animStartTime + 1,
                    v=keyframe(INPUT_CURVE_NAME, query=True, eval=True, time=leftKey - animStartTime + 1)[0],
                )
                setKeyframe(OUT_CURVE_NODE_NAME,
                    t=rightKey - animStartTime + 1,
                    v=keyframe(INPUT_CURVE_NAME, query=True, eval=True, time=leftKey - animStartTime + 1)[0],
                    ott='step'
                )

            tempTimes = keyframe(OUT_CURVE_NODE_NAME, query=True, timeChange=True)
            tempValues = keyframe(OUT_CURVE_NODE_NAME, query=True, valueChange=True)
            animCurveKeyframes = []
            #return merge
            for i in range(0, len(tempTimes) - 1):
                tempKeys = getLeftoverKeysInRange(leftoverAnimKeys, tempTimes[i], tempTimes[i+1])
                for k in range(0, tempKeys):
                    
                    setKeyframe(OUT_CURVE_NODE_NAME, t=tempTimes[i] + )
                    #(tempTimes[i+1] - tempTimes[i]) / (len(tempKeys) + 1)(i+1) #int.Parse?


            createdAnimCurves.append(OUT_CURVE_NODE_NAME)
        
        select(cl=True)
        select(createdAnimCurves, r=True)
        # sets(name=nodeNameTextField.getText() + "_new_set")
    except Exception as e:
        raise test1Ex(str(e))

nodeNameTextField = ""
 
win = window(title="Win1", width=300, height=140)

with columnLayout(adjustableColumn=True, rowSpacing=50):
    with horizontalLayout():
        text('name', height=20)
        nodeNameTextField = textField(edit=False, tx=str('name1'))

    runButton = button(label='run')
    button(runButton, e=True, c=windows.Callback(RunButtonCallback))

win.show()