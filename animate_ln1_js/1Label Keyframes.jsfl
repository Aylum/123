var doc = fl.getDocumentDOM();
if (!doc) {
	alert("Please open or create a flashfile.");
} else {
	CreateLabels();
}

function CreateLabels() {
    //get script name and copy it to config directory
    var thisScriptPath = fl.scriptURI;
    var splitted2 = thisScriptPath.split("/");
    var thisScriptName = splitted2[splitted2.length-1];
    var thiScriptDir = thisScriptPath.slice(0, splitted2[splitted2.length-1].length * -1);
    if (!FLfile.exists(fl.configURI + thisScriptName)) {
        FLfile.copy(fl.scriptURI, fl.configURI + thisScriptName);
    }

    //get timeline and layers
    var layers = doc.getTimeline().layers;
	var dialogXML = '';
	dialogXML += '<label value="select layer" width="280" />'

	dialogXML += '<menulist id="names" editable="true" width="380"><menupopup>'
	for (var i=0;i<layers.length;i++) dialogXML += '<menuitem selected="'+(i==0)+'" label="' + layers[i].name + '"/>'
	dialogXML += '</menupopup></menulist>'
	
    var data = createDialogXML(dialogXML);
    if (data.dismiss == "cancel") return;

    var slLayerInd = -1;
    for (var i = 0; i < layers.length; i++) {
        if (data.names == layers[i].name) {
            slLayerInd = i;
            break;
        }
    }
    if (slLayerInd == -1) {
        alert("Error");
        return;
    }
    if (layers[slLayerInd].frameCount < 1) {
        alert("Error: layer " + layers[slLayerInd].name + " contains 0 frames");
        return;
    }
    
    for (var i = 0; i < layers[slLayerInd].frames.length; i++) {
        if (i != layers[slLayerInd].frames[i].startFrame) continue;
        layers[slLayerInd].frames[i].name = (i + 1).toString() + ":" + (layers[slLayerInd].frames[i].duration).toString();
        layers[slLayerInd].frames[i].labelType = "comment";
    }
}

function createDialogXML(xmlString)
{
	var dialogXML = '<dialog title="Label keyframes" buttons="accept, cancel" >';
    dialogXML += '<vbox>' + xmlString + '</vbox>';
	dialogXML +='</dialog>';
	
	return fl.xmlPanelFromString(dialogXML);
}