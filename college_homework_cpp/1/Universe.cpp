﻿#include "Ecosystem.h"

using namespace std;
using namespace Ecosystem;
using namespace Ecosystem::EcosystemMath;

////////////////////////////////////////////struct ThingInfo/////////////////////////////////////

Ecosystem::ThingInfo::ThingInfo
(
	ThingType type1,
	int sensitivity,
	const EcosystemMath::Position& pos,
	const EcosystemMath::Velocity& vel
)
{
	m_type1 = type1;
	m_sensetivity = sensitivity;
	m_pos = pos;
	m_vel = vel;
}

Ecosystem::ThingInfo::ThingInfo
(
	ThingType type1,
	int sensitivity
)
{
	m_type1 = type1;
	m_sensetivity = sensitivity;
}

//////////////////////////////////////////////////class Universe//////////////////////////////

Ecosystem::Universe::Universe
(
	std::list<ThingInfo> things,
	int iterationsNumber,
	InitParams initParams
)
{
	for (auto& i : things)
	{
		switch (i.m_type1)
		{
		case ThingType::DRAGONFLY:
		{
			//Dragonfly dr(i.type1, i.sensetivity, initParams.drEatRadius, i.pos, i.vel);
			//this->things.push_back(unique_ptr<Thing>(new Dragonfly(dr)));
			m_things.push_back(make_unique<Dragonfly>(i.m_type1, i.m_sensetivity, initParams.drEatRadius, i.m_pos, i.m_vel));

			break;
		}
		case ThingType::STORK:
		{
			/*Stork st(i.type1, i.sensetivity, initParams.stEatRadius, i.pos, i.vel);

			this->things.push_back(unique_ptr<Thing>(new Stork(st)));*/

			m_things.push_back(make_unique<Stork>(i.m_type1, i.m_sensetivity, initParams.stEatRadius, i.m_pos, i.m_vel));

			break;
		}
		case ThingType::FROG:
		{
			/*Frog fr(i.type1, i.sensetivity, initParams.frEatRadius, i.pos, i.vel);

			this->things.push_back(unique_ptr<Thing>(new Frog(fr)));
			*/
			m_things.push_back(make_unique<Frog>(i.m_type1, i.m_sensetivity, initParams.frEatRadius, i.m_pos, i.m_vel));

			break;
		}
		case ThingType::HEDGEHOG:
		{
			/*Hedgehog hd(i.type1, i.sensetivity, initParams.hdEatRadius, i.pos, i.vel);

			this->things.push_back(unique_ptr<Thing>(new Hedgehog(hd)));
			*/
			m_things.push_back(make_unique<Hedgehog>(i.m_type1, i.m_sensetivity, initParams.hdEatRadius, i.m_pos, i.m_vel));

			break;
		}
		default:
			break;
		}
	}

	m_numberOfIterations = iterationsNumber;
	m_currentIteration = 0;
	m_isRunning = false;

	m_initParams = initParams;

	//this->generalIter = this->things.begin();

	m_logFile = nullptr;
	m_isLogEnabled = false;
}

void Ecosystem::Universe::Move()
{
	PrintToLogFile("---------------------------------Stage1(Move)--------------------------------\n");

	for (auto& currentThing : this->m_things)
	{
		Position pos = currentThing->GetPosition();
		Velocity vel = currentThing->GetVelocity();
		Velocity vel0 = currentThing->GetInitialVelocity();

		bool thatsEnough = false;

		if (pos.m_z + vel.m_z < 0)
		{
			if (pos.m_z != 0)
			{
				pos.m_x = pos.m_x - (vel.m_x*pos.m_z) / vel.m_z;
				pos.m_y = pos.m_y - (vel.m_y*pos.m_z) / vel.m_z;
				pos.m_z = 0;

				thatsEnough = true;
			}

			Velocity velProj(vel);
			velProj.m_z = 0;

			long double lambda = Velocity::Lambda(vel0, velProj);

			vel = velProj * lambda;

			vel.m_z = 0;

			currentThing->SetVelocity(vel);

			if (m_isLogEnabled)
			{
				PrintToLogFile
				(
					"stage1: " + to_string(currentThing->GetName()) + "hit floor and his velocity: [ " +
					to_string(vel.m_x) + ", " + to_string(vel.m_y) + ", " + to_string(vel.m_z) + " ]\n"
				);
			}
		}

		if (!thatsEnough)
		{
			pos.m_x += vel.m_x;
			pos.m_y += vel.m_y;
			pos.m_z += vel.m_z;
		}

		currentThing->SetPosition(pos);

		if (m_isLogEnabled)
		{
			PrintToLogFile
			(
				"stage1:        name: " + to_string(currentThing->GetName()) + ",    type: " + to_string((int)currentThing->GetThingType())
				+ ",    position: [ " + to_string(pos.m_x) + ", " + to_string(pos.m_y) + ", " + to_string(pos.m_z) + " ]\n"
			);
		}
	}

	if (m_isLogEnabled)
	{
		PrintToLogFile("--------------------------------- End of Stage1(Move)--------------------------------\n");
	}
}

void Ecosystem::Universe::Analyze()
{
	if (m_things.empty())
	{
		return;
	}

	if (m_isLogEnabled)
	{
		PrintToLogFile("---------------------------------Stage2(Analyze)--------------------------------\n");
	}

	//auto thingsBegin = this->things.begin();
	auto thingsEnd = m_things.end();

	for (auto&& currentThing = m_things.begin(); currentThing != thingsEnd; ++currentThing)
	{
		auto& refCurrentThing = **currentThing;

		bool isTargetApproved = false;
		Thing* approvedTarget = nullptr;

		if (refCurrentThing.IsAliveTarget() &&
			refCurrentThing.GetTargetTarget() != nullptr &&
			refCurrentThing.DistanceToTargetFast() <= refCurrentThing.GetSensetivitySqr())
		{
			isTargetApproved = true;
			approvedTarget = refCurrentThing.GetTargetTarget();
		}

		bool isFoundPredator = false;
		bool isFoundVictim = false;

		Thing* target = nullptr;
		long minDistFastToTarget = 0;

		for (auto&& restThing = m_things.begin(); restThing != thingsEnd; ++restThing)
		{
			auto& refRestThing = **restThing;

			long distFastFromCurToRest = refCurrentThing.DistanceFast(refRestThing);

			int currentCanKillRest = refCurrentThing.CanKill(refRestThing);

			if (currentCanKillRest == -1)
			{
				if (isFoundPredator)
				{
					if (refRestThing.IsAlive() &&
						distFastFromCurToRest < minDistFastToTarget
						)
					{
						target = restThing->get();
						minDistFastToTarget = distFastFromCurToRest;
					}
				}
				else
				{
					if (refRestThing.IsAlive() && distFastFromCurToRest <= refCurrentThing.GetSensetivitySqr())
					{
						target = restThing->get();
						minDistFastToTarget = refCurrentThing.DistanceFast(*target);
						isFoundPredator = true;
					}
				}
			}
			else if (!isFoundPredator && !isTargetApproved && currentCanKillRest == 1)
			{
				if (isFoundVictim)
				{
					if (/*refRestThing.IsAlive() &&*/
						distFastFromCurToRest < minDistFastToTarget
						)
					{
						target = restThing->get();
						minDistFastToTarget = distFastFromCurToRest;
					}
				}
				else
				{
					if (distFastFromCurToRest <= refCurrentThing.GetSensetivitySqr())
					{
						target = restThing->get();
						minDistFastToTarget = refCurrentThing.DistanceFast(*target);
						isFoundVictim = true;
					}
				}
			}

			if (
				refRestThing.IsAlive() &&
				currentCanKillRest == 1 &&
				distFastFromCurToRest <= refCurrentThing.GetEatRadiusSqr()
				)
			{
				if (m_isLogEnabled)
				{
					PrintToLogFile
					(
						string
						(
							to_string(refCurrentThing.GetName()) + " ate " + to_string(refRestThing.GetName()) + "\n"
						)
					);
				}

				refRestThing.Kill();

				m_events.emplace_back(KillEvent(restThing));

				//m_killsForHandling.emplace_back(EventType::KILL, restThing);
			}
		}

		if ((isFoundPredator || isFoundVictim) && target != nullptr && !isTargetApproved)
		{
			refCurrentThing.SetTargetTarget(target);
		}

		if (isFoundPredator)
		{
			if (m_isLogEnabled)
			{
				PrintToLogFile
				(
					string
					(
						to_string(refCurrentThing.GetName()) + " chose " + to_string(refCurrentThing.GetNameTarget()) +
						" for escaping"
					)
				);

			}

			refCurrentThing.SetMovingFreely(false);

			m_events.emplace_back(ChangeDirectionEvent(currentThing, refCurrentThing.Escape()));
			//m_changeDirectionsForHandling.emplace_back(&m_things, currentThing, refCurrentThing.Escape());
		}
		else
		{
			if (isFoundVictim)
			{
				if (m_isLogEnabled)
				{
					PrintToLogFile
					(
						string
						(
							to_string(refCurrentThing.GetName()) + " chose " + to_string(refCurrentThing.GetNameTarget()) +
							" for attacking"
						)
					);
				}

				refCurrentThing.SetMovingFreely(false);

				m_events.emplace_back(ChangeDirectionEvent(currentThing, refCurrentThing.Attack()));
				//m_changeDirectionsForHandling.emplace_back(&m_things, currentThing, refCurrentThing.Attack());
			}
			else
			{
				if (isTargetApproved)
				{
					//Thing* temp = refCurrentThing.GetTargetTarget();

					refCurrentThing.SetTargetTarget(approvedTarget);

					if (
						approvedTarget != nullptr &&
						approvedTarget->GetName() != -1 &&
						refCurrentThing.CanKill(*approvedTarget) == 1 &&
						refCurrentThing.DistanceToTargetFast() <= refCurrentThing.GetSensetivitySqr()
						)
					{
						if (m_isLogEnabled)
						{
							PrintToLogFile
							(
								string
								(
									to_string(refCurrentThing.GetName()) + " chose " + to_string(refCurrentThing.GetNameTarget()) +
									" for attacking"
								)
							);
						}

						refCurrentThing.SetMovingFreely(false);

						m_events.emplace_back(ChangeDirectionEvent(currentThing, refCurrentThing.Attack()));
						//m_changeDirectionsForHandling.emplace_back(&m_things, currentThing, refCurrentThing.Attack());
					}
				}

				if (!isTargetApproved)
				{

					refCurrentThing.SetMovingFreely(true);
					refCurrentThing.SetTargetTarget(nullptr);

					if (m_isLogEnabled)
					{
						PrintToLogFile
						(
							string
							(
								to_string(refCurrentThing.GetName()) + " moving freely"
							)
						);
					}
				}
			}
		}

		if (refCurrentThing.IsAlive() && !refCurrentThing.IsInRange(0, COORDINATE_LIMIT))
		{
			if (refCurrentThing.IsMovingFreely())
			{
				if (m_isLogEnabled)
				{
					PrintToLogFile
					(
						string
						(
							to_string(refCurrentThing.GetName()) + " went out of range and was killed\n"
						)
					);
				}

				refCurrentThing.Kill();

				m_events.emplace_back(KillEvent(currentThing));
				//m_killsForHandling.emplace_back(&m_things, currentThing);
			}
			else
			{
				if (m_isLogEnabled)
				{
					PrintToLogFile
					(
						string
						(
							to_string(refCurrentThing.GetName()) + " caused stop universe\n"
						)
					);
				}

				m_events.emplace_back(StopUniverseEvent());

				//m_stopUniverseForHandling.emplace_back();
			}
		}
	}

	if (m_isLogEnabled)
	{
		PrintToLogFile("---------------------------------End of Stage2(Analyze)--------------------------------\n");
	}
}

void Ecosystem::Universe::HandleAllEvents()
{
	HandleAllEvents();
	/*HandleAllOnChangeDirection();
	HandleAllOnKill();*/

	m_numberOfIterations--;
	m_currentIteration++;

	if (m_numberOfIterations == 0)
	{ //генерация события остановки
		m_events.emplace_back(StopUniverseEvent());
		//m_stopUniverseForHandling.emplace_back();
	}

	m_events.clear();

	//HandleAllOnStopUniverse();

	/*m_killsForHandling.clear();
	m_stopUniverseForHandling.clear();
	m_changeDirectionsForHandling.clear();*/
}

void Ecosystem::Universe::Run()
{
	if (m_numberOfIterations <= 0)
	{
		m_isRunning = false;
		return;
	}

	m_isRunning = true;

	if (m_isLogEnabled)
	{
		m_logFile = new ofstream("log.txt", ios::trunc);

		if (!*m_logFile)
		{
			cout << "can't open log.txt" << endl;
			m_isLogEnabled = false;
		}
	}

	while (m_isRunning)
	{
		if (m_isLogEnabled)
		{
			*m_logFile << endl << "iteration: " << m_currentIteration << endl;
		}

		this->Move();
		this->Analyze();
		this->HandleAllEvents();
		//cout << endl << "iteration: " << iteration;
		//currentIteration++;
	}


	if (m_logFile != nullptr && m_isLogEnabled)
	{
		m_logFile->close();
		delete m_logFile;
	}
}

void Ecosystem::Universe::Stop()
{
	m_isRunning = false;
}

std::list<ThingInfo> Ecosystem::Universe::GetState()
{
	list<ThingInfo> res;

	for (auto& current : m_things)
	{
		res.push_back
		(
			ThingInfo
			(
				current->GetThingType(),
				current->GetSensetivity(),
				current->GetPosition(),
				current->GetVelocity()
			)
		);
	}

	return res;
}

int Ecosystem::Universe::GetIterationsNumber() const
{
	return m_numberOfIterations;
}

int Ecosystem::Universe::GetCurrentIteration() const
{
	return m_currentIteration;
}

InitParams Ecosystem::Universe::GetInitParams() const
{
	return m_initParams;
}

void Ecosystem::Universe::HandleAllEvents()
{
	for (auto& curEvent : m_events)
	{
		if (curEvent->GetEventType() == EventType::CHANGE_DIRECTION)
		{
			ChangeDirectionEvent* changeDirection = dynamic_cast<ChangeDirectionEvent*>(curEvent.get());

			if (changeDirection != nullptr)
			{
				(*changeDirection->GetIter())->SetVelocity(changeDirection->GetVelocity());
			}
		}
		else if (curEvent->GetEventType() == EventType::KILL)
		{
			KillEvent* kill = dynamic_cast<KillEvent*>(curEvent.get());

			if (kill != nullptr)
			{
				m_things.erase(kill->GetIter());
			}
		}
		else
		{
			StopUniverseEvent* stopUn = dynamic_cast<StopUniverseEvent*>(curEvent.get());

			if (stopUn != nullptr)
			{
				Stop();
			}
		}
	}
}

//void Ecosystem::Universe::HandleAllOnKill()
//{
//	if (m_killsForHandling.empty())
//	{
//		return;
//	}
//
//	for (auto& curKillArgs : m_killsForHandling)
//	{
//		if (curKillArgs.GetCollecton() != nullptr)
//		{
//			curKillArgs.GetCollecton()->erase(curKillArgs.GetIter());
//		}
//	}
//}
//
//void Ecosystem::Universe::HandleAllOnChangeDirection()
//{
//	if (m_changeDirectionsForHandling.empty())
//	{
//		return;
//	}
//
//	for (auto& current : m_changeDirectionsForHandling)
//	{
//		if (current.GetCollecton() != nullptr)
//		{
//			(*current.GetIter())->SetVelocity(current.GetVelocity());
//
//			if (m_isLogEnabled)
//			{
//				PrintToLogFile
//				(
//					string
//					(
//						"and velocity: [ " + to_string(current.GetVelocity().m_x) + ", " +
//						to_string(current.GetVelocity().m_y) + ", " + to_string(current.GetVelocity().m_z) + " ]\n"
//					)
//				);
//			}
//		}
//	}
//}
//
//void Ecosystem::Universe::HandleAllOnStopUniverse()
//{
//	if (!m_stopUniverseForHandling.empty())
//	{
//		this->Stop();
//	}
//}

//void Ecosystem::Universe::Update(EventArgs* e)
//{
//	if (!e) {
//		throw logic_error("Universe::Update(e). e is null");
//	}

//	/*if (e->GetName() != (*this->generalIter)->GetName()) 
//	{
//		throw logic_error("Universe::Update(e). e->GetName != (*this->generalIter)->GetName");
//	}*/

//	/*string logMessage(
//		"iteration: " + to_string(this->currentIteration) + "\n" +
//		"stage2(Universe::Update()): \nname: " + to_string(e->GetName()) + "\nvelocity: [ " +
//		to_string(e->GetVelocity().x) + ", " + to_string(e->GetVelocity().y) + ", " + to_string(e->GetVelocity().z) + " ]\n"
//	);

//	PrinIntotLogFile(logMessage);*/

//	if (e->GetEventType() == EventType::ON_STOP_UNIVERSE) 
//	{
//		StopUniverseArgs*  temp = dynamic_cast<StopUniverseArgs*>(e);

//		//assert(temp != nullptr);
//		if (temp == nullptr)
//		{
//			throw runtime_error("Universe::Update(): dynamic_cast<StopUniverseArgs*>(e) returnded nullptr");
//		}

//		//положить указатель на объект в список обработки
//		this->OnStopUniverseForHandling.push_back(move(unique_ptr<StopUniverseArgs>(temp)));
//		return;
//	}

//	e->ContinueInit(&this->things);
//	//доинициализировать указатель на объект
//	e->ContinueInit(this->generalIter);

//	if (e->GetEventType() == EventType::ON_KILL)
//	{ 
//		KillArgs*  temp = dynamic_cast<KillArgs*>(e);

//		//assert(temp != nullptr);
//		if (temp == nullptr)
//		{
//			throw runtime_error("Universe::Update(): temp = dynamic_cast<KillArgs*>(e) returnded nullptr");
//		}

//		//и положить его в список обработки
//		this->onKillsForHandling.push_back(move(unique_ptr<KillArgs>(temp)));
//		return;
//	}

//	ChangeDirectionArgs*  temp = dynamic_cast<ChangeDirectionArgs*>(e);

//	//assert(temp != nullptr);
//	if (temp == nullptr)
//	{
//		throw runtime_error("Universe::Update(): temp = dynamic_cast<ChangeDirectionArgs*>(e) returnded nullptr");
//	}

//	//и положить его в список обработки
//	this->onChangeDirectionsForHandling.push_back(move(unique_ptr<ChangeDirectionArgs>(temp)));

//	if (this->isLogEnabled)
//	{
//		PrintToLogFile
//		(
//			string
//			(
//				"and velocity: [ " + to_string(temp->GetVelocity().x) + ", " +
//				to_string(temp->GetVelocity().y) + ", " + to_string(temp->GetVelocity().z) + " ]\n"
//			)
//		);
//	}
//}

/*void Ecosystem::Universe::OnStopUniverseSubscribe(Subscriber* sub)
{
this->OnStopUniverse.Subscribe(this);
}

void Ecosystem::Universe::OnStopUniverseUnsubscribe(Subscriber* sub)
{
this->OnStopUniverse.Unsubscribe(this);
}*/

void Ecosystem::Universe::EnableLogging(bool yes)
{
	m_isLogEnabled = yes;
}

void Ecosystem::Universe::PrintToLogFile(const std::string& what)
{
	if (m_isLogEnabled)
	{
		if (m_logFile != nullptr && *m_logFile)
		{
			*m_logFile << what + "\n";
		}
		else
		{
			cout << "ERROR: can't open log file" << endl
				<< "can't print this: " << endl
				<< what << endl;
		}
	}
}