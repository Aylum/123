#pragma once

#include "Ecosystem.h"


namespace Ecosystem
{
	struct ThingInfo;
	struct InitParams;

	namespace EcosystemIO
	{
		class EcosystemReader
		{
		public:
			EcosystemReader(const std::string& fileName);

			//throw ios_base::failure
			void Read();

			void SetEatRadius(int drRad, int stRad, int frRad, int hdRad);

			bool IsSuccsessReading() const;

			std::list<ThingInfo> GetThings() const;

			int GetIterationsNumber() const;

			InitParams GetInitParams() const;

		private:
			void AddToThings(ThingType type1, int number, int sensitivity);

		private:
			std::string m_fileName;
			std::list<ThingInfo> m_things;
			int m_iterationsNumber;
			bool m_isSuccsessReading;
			InitParams m_initParams;
		};

		class EcosystemWriter
		{
		public:
			EcosystemWriter
			(
				const std::list<ThingInfo>& things,
				int iterationsNumber,
				InitParams initParams,
				const std::string& fileName
			);

			//throw ios_base::failure
			void Print();

		private:
			std::list<ThingInfo> m_things;
			InitParams m_initParams;
			std::string m_fileName;
			int m_iterationsNumber;
			bool m_isSuccsessWriting;
		};
	}
}