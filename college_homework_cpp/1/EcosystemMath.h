﻿#pragma once

#include "Ecosystem.h"

namespace Ecosystem
{
	namespace EcosystemMath
	{
		const static double _PRECISION = 1E-9;

		struct Point
		{
			Point()
			{
				m_x = m_y = m_z = 0;
			}

			Point(int x, int y, int z)
			{
				m_x = x;
				m_y = y;
				m_z = z;
			}

			Point(const Point& pos2)
			{
				m_x = pos2.m_x;
				m_y = pos2.m_y;
				m_z = pos2.m_z;
			}

			bool operator==(const Point& pos2) const
			{
				return
					m_x == pos2.m_x &&
					m_y == pos2.m_y &&
					m_z == pos2.m_z;
			}

			Point operator-(const Point& pos2) const
			{
				return Point(m_x - pos2.m_x, m_y - pos2.m_y, m_z - pos2.m_z);
			}

		public:
			int m_x;
			int m_y;
			int m_z;
		};

		struct Velocity : public Point
		{
			Velocity() : Point() {}

			Velocity(int x, int y, int z) : Point(x, y, z) {}

			Velocity(const Velocity& vel2)
			{
				m_x = vel2.m_x;
				m_y = vel2.m_y;
				m_z = vel2.m_z;
			}

			/*explicit operator Position() const
			{
			return Position(m_x, m_y, m_z);
			}*/

			long double Length() const
			{
				return sqrt((double)m_x * (double)m_x + (double)m_y * (double)m_y + (double)m_z * (double)m_z);
			}

			static long double Lambda(const Velocity& vel1, const Velocity& vel2)
			{
				if (vel2.m_z == 0 && vel2.m_x == 0 && vel2.m_y == 0)
				{
					return 0;
				}

				return
					sqrt
					(
					((double)vel1.m_x*(double)vel1.m_x + (double)vel1.m_y*(double)vel1.m_y + (double)vel1.m_z*(double)vel1.m_z) /
						((double)vel2.m_x*(double)vel2.m_x + (double)vel2.m_y*(double)vel2.m_y + (double)vel2.m_z*(double)vel2.m_z)
					);
			}

			Velocity operator-(const Velocity& vel2) const
			{
				return Velocity(m_x - vel2.m_x, m_y - vel2.m_y, m_z - vel2.m_z);
			}

			Velocity operator*(float val)
			{
				return Velocity(trunc(val * (float)m_x), trunc(val * (float)m_y), trunc(val * (float)m_z));
			}
		};

		struct Position : public Point
		{
			Position() : Point() {}

			Position(int x, int y, int z) : Point(x, y, z) {}

			Position(const Position& pos2)
			{
				m_x = pos2.m_x;
				m_y = pos2.m_y;
				m_z = pos2.m_z;
			}

			explicit operator Velocity() const
			{
				return Velocity(m_x, m_y, m_z);
			}

			bool operator==(const Position& pos2) const
			{
				return
					m_x == pos2.m_x &&
					m_y == pos2.m_y &&
					m_z == pos2.m_z;
			}

			Position operator-(const Position& pos2) const
			{
				return Position(m_x - pos2.m_x, m_y - pos2.m_y, m_z - pos2.m_z);
			}

			long DistanceFast(const Position& pos2) const
			{
				return
					(long)(pos2.m_x - m_x)*(long)(pos2.m_x - m_x) +
					(long)(pos2.m_y - m_y)*(long)(pos2.m_y - m_y) +
					(long)(pos2.m_z - m_z)*(long)(pos2.m_z - m_z);
			}

			double Distance(const Position& pos2) const
			{
				return
					sqrt
					(
					(double)(pos2.m_x - m_x)*(double)(pos2.m_x - m_x) +
						(double)(pos2.m_y - m_y)*(double)(pos2.m_y - m_y) +
						(double)(pos2.m_z - m_z)*(double)(pos2.m_z - m_z)
					);
			}
		};
	}
}