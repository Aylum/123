﻿#pragma once

#include "Ecosystem.h"

namespace EM = Ecosystem::EcosystemMath;

namespace Ecosystem
{
	enum class ThingType;
	enum class EventType;
	struct TargetThing;
	class EventArgs;
	class KillArgs;/////
	class ChangeDirectionArgs;//////
	class StopUniverseArgs;//////
	class Subscriber;
	class Event;
	class KillEvent;/////
	class ChangeDirectionEvent;/////
	//class StopUniverseEvent;
	class Thing;

	struct InitParams
	{
		int dr;
		int st;
		int fr;
		int hd;
		int iterationsNumber;
		int drEatRadius;
		int stEatRadius;
		int frEatRadius;
		int hdEatRadius;
	};

	struct ThingInfo
	{
		ThingInfo(
			ThingType type1,
			int senstivity,
			const EcosystemMath::Position& pos,
			const EcosystemMath::Velocity& vel
		);
		ThingInfo(
			ThingType type1,
			int senstivity
		);

	public:
		ThingType m_type1;
		int m_sensetivity;
		EcosystemMath::Position m_pos;
		EcosystemMath::Velocity m_vel;
	};

	class StopUniverseEvent : public Event {};

	class Universe
	{
	public:
		Universe() = delete;
		Universe(
			std::list<ThingInfo> things,
			int iterationsNumber,
			InitParams initParams
		);

		void Move();
		void Analyze();
		void HandleAllEvents();

		void Run();
		void Stop();

		std::list<ThingInfo> GetState();

		int GetIterationsNumber() const;

		int GetCurrentIteration() const;

		InitParams GetInitParams() const;

		void EnableLogging(bool yes = true);

	private:

		void HandleAllEvents();

		/*void HandleAllOnKill();
		void HandleAllOnChangeDirection();
		void HandleAllOnStopUniverse();*/

		void PrintToLogFile(const std::string& what);

	private:
		int m_numberOfIterations;
		int m_currentIteration;
		bool m_isRunning;

		InitParams m_initParams;

		StopUniverseEvent m_stopUniverseEvent;

		std::list<std::unique_ptr<Thing>> m_things;

		std::list<unique_ptr<Event>> m_events;

		//std::list<KillArgs> m_killsForHandling;
		//std::list<ChangeDirectionArgs> m_changeDirectionsForHandling;
		//std::list<StopUniverseArgs> m_stopUniverseForHandling;

		std::ofstream* m_logFile;
		bool m_isLogEnabled;
	};
}