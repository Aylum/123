#include <iostream>
#include <string>
#include <exception>
#include <stdexcept>

#include "Ecosystem.h"


using namespace std;
using namespace Ecosystem;
using namespace Ecosystem::EcosystemIO;
using namespace Ecosystem::EcosystemMath;

int main(int argc, char** argv)
{
	try
	{
		EcosystemIO::EcosystemReader reader(string("input.txt"));
		reader.Read();

		reader.SetEatRadius(0, 1, 3, 1);

		Universe universe1(reader.GetThings(), reader.GetIterationsNumber(), reader.GetInitParams());

		/*universe1.SetCtorParam1(reader.GetThings());
		universe1.SetCtorParam2(reader.GetIterationsNumber());*/

		//universe1.EnableLogging();

		universe1.Run();




		EcosystemWriter writer(universe1.GetState(), universe1.GetCurrentIteration(), universe1.GetInitParams(), string("output.txt"));

		writer.Print();
	}
	catch (ios_base::failure ex)
	{
		cout << ex.what() << endl;
	}
	catch (logic_error ex)
	{
		cout << ex.what() << endl;
	}
	catch (runtime_error ex)
	{
		cout << ex.what() << endl;
	}

	return 0;
}