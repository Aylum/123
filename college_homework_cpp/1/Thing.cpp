﻿#include "Ecosystem.h"

using namespace std;
using namespace Ecosystem;
using namespace Ecosystem::EcosystemMath;

////////////////////////////////////////////////struct TargetThing/////////////////////////////////

Ecosystem::TargetThing::TargetThing()
{
	m_target = nullptr;
	m_isAlive = false;
}

Ecosystem::TargetThing::TargetThing(Thing* sender)
{
	m_isAlive = m_target = sender;
}

Thing* Ecosystem::TargetThing::GetTarget() const
{
	return m_target;
}

bool Ecosystem::TargetThing::IsAlive() const
{
	return m_isAlive;
}

Position Ecosystem::TargetThing::GetPosition() const
{
	if (m_target == nullptr)
	{
		return Position(-1, -1, -1);
	}

	return m_target->GetPosition();
}

Velocity Ecosystem::TargetThing::GetVelocity() const
{
	if (m_target == nullptr)
	{
		return Velocity(-1, -1, -1);
	}

	return m_target->GetVelocity();
}

int Ecosystem::TargetThing::GetName() const
{
	if (m_target == nullptr)
	{
		return -1;
	}

	return m_target->GetName();
}

void Ecosystem::TargetThing::SetThing(Thing* thing)
{
	this->m_target = thing;
}

void Ecosystem::TargetThing::SetIsAlive(bool isAlive)
{
	m_isAlive = isAlive;
}

/////////////////////////////////////////class Event///////////////////////////////////////

Ecosystem::Event::Event(EventType type)
{
	m_type = type;
	//m_collection = nullptr;
}

Ecosystem::Event::Event(EventType type,
	std::list<std::unique_ptr<Thing>>::iterator& iter)
{
	m_type = type;
	//m_collection = collection;
	m_iter = iter;
}

EventType Ecosystem::Event::GetEventType() const
{
	return m_type;
}

//void Ecosystem::EventArgs::ContinueInit(std::list<std::unique_ptr<Thing>>* collection)
//{
//	m_collection = collection;
//}

void Ecosystem::Event::ContinueInit(list<std::unique_ptr<Thing>>::iterator& iter)
{
	m_iter = iter;
}

//std::list<std::unique_ptr<Thing>>* Ecosystem::EventArgs::GetCollecton() const
//{
//	return m_collection;
//}
std::list<std::unique_ptr<Thing>>::iterator Ecosystem::Event::GetIter() const
{
	return m_iter;
}

////////////////////////////////////////////class ChangeDirectionEvent////////////////////////////////////////

Ecosystem::ChangeDirectionEvent::ChangeDirectionEvent(std::list<std::unique_ptr<Thing>>::iterator who, const EM::Velocity& vel)
	: Event(EventType::CHANGE_DIRECTION, who)
{
	m_vel = vel;
}

EM::Velocity Ecosystem::ChangeDirectionEvent::GetVelocity() const
{
	return m_vel;
}

///////////////////////////////////////////class SpeialTargetEventArgs/////////////////////////////////////////

Ecosystem::SpeialTargetEventArgs::SpeialTargetEventArgs(const TargetThing& sender)
{
	m_sender = sender;
}

////////////////////////////////////////////class SpeialTargetEvent////////////////////////////////

void Ecosystem::SpeialTargetEvent::Subscribe(Subscriber* sub)
{
	m_subscribers.insert(sub);
}

void Ecosystem::SpeialTargetEvent::Unsubscribe(Subscriber* sub)
{
	m_subscribers.erase(sub);
}

void Ecosystem::SpeialTargetEvent::Notify(SpeialTargetEventArgs* e)
{
	for (auto& i : m_subscribers)
	{
		i->OnHandle(e);
	}
}

EventType Ecosystem::SpeialTargetEvent::GetType() const
{
	return m_type;
}

///////////////////////////////////////////////class Thing////////////////////////////////////////

int Ecosystem::Thing::s_objCounter = 0;

Ecosystem::Thing::Thing()
{
	m_isAlive = false;
	m_isMovingFreely = true;
	m_name = ++s_objCounter;
	m_sensitivity = 0;

	m_target.SetThing(nullptr);
	m_target.SetIsAlive(false);
}

Ecosystem::Thing::Thing(ThingType type, int sensetivity, int eatRadius, const Position& pos, const Velocity& vel)
{
	m_type = type;
	m_sensitivity = sensetivity;
	m_eatRadius = eatRadius;
	m_position = pos;
	m_velocity0 = vel;
	m_velocity = vel;
	m_isAlive = true;
	m_isMovingFreely = true;
	m_name = ++s_objCounter;
}

Ecosystem::Thing::Thing(const Thing& thing2)
{
	m_isAlive = thing2.m_isAlive;
	m_isMovingFreely = thing2.m_isMovingFreely;
	m_name = thing2.m_name;
	m_killEvent = thing2.m_killEvent;
	m_position = thing2.m_position;
	m_sensitivity = thing2.m_sensitivity;
	m_eatRadius = thing2.m_eatRadius;
	m_type = thing2.m_type;
	m_velocity = thing2.m_velocity;
	m_velocity0 = thing2.m_velocity0;
	m_target = thing2.m_target;
}

double Ecosystem::Thing::Distance(const Thing& thing2) const
{
	return m_position.Distance(thing2.m_position);
}

long Ecosystem::Thing::DistanceFast(const Thing& thing2) const
{
	return m_position.DistanceFast(thing2.m_position);
}

double Ecosystem::Thing::DistanceToTarget() const
{
	Position temp = m_target.GetPosition();

	if (temp.m_x == -1 && temp.m_y == -1 && temp.m_z == -1)
	{
		return -1;
	}

	return m_position.Distance(temp);
}

long Ecosystem::Thing::DistanceToTargetFast() const
{
	Position temp = m_target.GetPosition();

	if (temp.m_x == -1 && temp.m_y == -1 && temp.m_z == -1)
	{
		return -1;
	}

	return m_position.DistanceFast(temp);
}

int Ecosystem::Thing::CanKill(const Thing& thing2) const
{
	/*if (!thing2)
	{
	throw invalid_argument("Thing::CanKill(thing2). thing2 is null");
	}*/
	return 0;
}

bool Ecosystem::Thing::IsInRange(int begin, int end) const
{
	return
		m_position.m_x >= begin && m_position.m_x <= end &&
		m_position.m_y >= begin && m_position.m_y <= end &&
		m_position.m_z >= begin && m_position.m_z <= end;
}

bool Ecosystem::Thing::IsAlive() const
{
	return m_isAlive;
}

bool Ecosystem::Thing::IsMovingFreely() const
{
	return m_isMovingFreely;
}

int Ecosystem::Thing::GetSensetivity() const
{
	return m_sensitivity;
}

int Ecosystem::Thing::GetSensetivitySqr() const
{
	return m_sensitivity*m_sensitivity;
}

int Ecosystem::Thing::GetEatRadius() const
{
	return m_eatRadius;
}

int Ecosystem::Thing::GetEatRadiusSqr() const
{
	return m_eatRadius*m_eatRadius;
}

int Ecosystem::Thing::GetName() const
{
	return m_name;
}

ThingType Ecosystem::Thing::GetThingType() const
{
	return m_type;
}

Position Ecosystem::Thing::GetPosition() const
{
	return m_position;
}

Velocity Ecosystem::Thing::GetVelocity() const
{
	return m_velocity;
}

Velocity Ecosystem::Thing::GetInitialVelocity() const
{
	return m_velocity0;
}

void Ecosystem::Thing::SetPosition(const Position& position)
{
	m_position = position;
}

void Ecosystem::Thing::SetVelocity(const Velocity& velocity)
{
	m_velocity = velocity;
}

void Ecosystem::Thing::SetMovingFreely(bool isMovingFreely)
{
	m_isMovingFreely = isMovingFreely;
}

Velocity Ecosystem::Thing::Escape()
{
	if (m_target.GetTarget() == nullptr)
	{
		m_isMovingFreely = true;
		return m_velocity;
	}

	//assert(this->m_target.GetName() != -1);
	if (m_target.GetName() == -1)
	{
		throw logic_error("Thing::Escape(): trying to Escape from target named ""-1""");
	}

	m_isMovingFreely = false;

	Position targetPos = m_target.GetPosition();

	if (m_position == targetPos)
	{
		return m_velocity;
	}

	Velocity d = (Velocity)(m_position - targetPos);

	long double lambda = Velocity::Lambda(m_velocity0, d);

	return Velocity(d * lambda);
}

Velocity Ecosystem::Thing::Attack()
{
	if (m_target.GetTarget() == nullptr)
	{
		m_isMovingFreely = true;
		return m_velocity;
	}

	//assert(this->m_target.GetName() != -1);
	if (m_target.GetName() == -1)
	{
		throw logic_error("Thing::Attack(): trying to Attack target named ""-1""");
	}

	m_isMovingFreely = false;

	Position targetPos = m_target.GetPosition();

	if (m_position == targetPos)
	{
		return m_velocity;
	}

	Velocity d = (Velocity)(targetPos - m_position);

	long double lambda = Velocity::Lambda(m_velocity0, d);

	return Velocity(d * lambda);
}

void Ecosystem::Thing::Kill()
{
	if (m_target.GetTarget() != nullptr && m_target.IsAlive())
	{
		m_target.GetTarget()->OnKillUnsubscribe(this);
	}

	m_isAlive = false;
	m_isMovingFreely = false;

	m_killEvent.Notify(new SpeialTargetEventArgs(TargetThing(this)));
}

void Ecosystem::Thing::OnHandle(SpeialTargetEventArgs* e)
{
	if (e == nullptr)
	{
		return;
	}

	//if (e->GetEventType() != EventType::KILL)
	//{
	//	return;
	//}

	//KillArgs* sender = dynamic_cast<KillArgs*>(e);

	//assert(temp != nullptr);
	/*if (sender == nullptr)
	{
		throw runtime_error("Thing::Update(): dynamic_cast<KillArgs*> returned nullptr");
	}*/

	Thing* sender = e->GetSender().GetTarget();

	/*if (sender == nullptr)
	{
		return;
	}*/

	if (sender != m_target.GetTarget())
	{
		throw logic_error("Thing::Update(): error occurs during handling event. this->m_target != sender->m_target");
	}

	if (sender != nullptr)
	{
		m_target.SetThing(nullptr);
		m_target.SetIsAlive(false);
		m_isMovingFreely = true;
	}
}

void Ecosystem::Thing::OnKillSubscribe(Subscriber* sub)
{
	m_killEvent.Subscribe(sub);
}

void Ecosystem::Thing::OnKillUnsubscribe(Subscriber* sub)
{
	m_killEvent.Unsubscribe(sub);
}

void Ecosystem::Thing::ResetObjCounter()
{
	s_objCounter = 0;
}

int Ecosystem::Thing::GetNameTarget() const
{
	return m_target.GetName();
}

Thing* Ecosystem::Thing::GetTargetTarget() const
{
	return m_target.GetTarget();
}

bool Ecosystem::Thing::IsAliveTarget() const
{
	return m_target.IsAlive();
}

void Ecosystem::Thing::SetTargetTarget(Thing* target)
{
	if (m_target.GetTarget() != nullptr && m_target.IsAlive())
	{
		m_target.GetTarget()->OnKillUnsubscribe(this);
	}

	m_target.SetThing(target);

	if (m_target.GetTarget() == nullptr)
	{
		m_target.SetIsAlive(false);
		return;
	}

	m_target.SetIsAlive(m_target.GetTarget()->IsAlive());

	if (m_target.IsAlive() == false)
	{
		return;
	}

	m_target.GetTarget()->OnKillSubscribe(this);
}

///////////////////////////////////////////////class Dragonfly////////////////////////////////////

int Ecosystem::Dragonfly::CanKill(const Thing& thing2) const
{
	/*if (!thing2)
	{
	throw invalid_argument("Dragonfly::CanKill(thing2). thing2 is null");
	}*/

	ThingType type2 = thing2.GetThingType();

	if (m_type == type2 || type2 == ThingType::HEDGEHOG)
	{
		return 0;
	}

	return -1;
}

Velocity Ecosystem::Dragonfly::Attack()
{
	if (m_target.GetTarget() == nullptr)
	{
		this->SetMovingFreely(true);
		return m_velocity;
	}

	if (m_target.GetName() == -1)
	{
		throw logic_error("Dragonfly::Attack(): trying to Attack target named ""-1""");
	}

	this->SetMovingFreely(false);

	return m_velocity;
}

/////////////////////////////////////////////class Stork/////////////////////////////////////////

int Ecosystem::Stork::CanKill(const Thing& thing2) const
{
	/*if (!thing2)
	{
	throw invalid_argument("Stork::CanKill(thing2). thing2 is null");
	}*/

	ThingType type2 = thing2.GetThingType();

	if (m_type == type2 || type2 == ThingType::HEDGEHOG)
	{
		return 0;
	}

	return 1;
}

////////////////////////////////////////////class Frog//////////////////////////////////////////

int Ecosystem::Frog::CanKill(const Thing& thing2) const
{
	/*if (!thing2)
	{
	throw invalid_argument("Frog::CanKill(thing2). thing2 is null");
	}*/

	ThingType type2 = thing2.GetThingType();

	if (type2 == ThingType::DRAGONFLY)
	{
		return 1;
	}

	if (m_type == type2)
	{
		return 0;
	}

	return -1;
}

Velocity Ecosystem::Frog::Escape()
{
	if (this->m_target.GetTarget() == nullptr)
	{
		this->SetMovingFreely(true);
		return m_velocity;
	}

	if (m_target.GetName() == -1)
	{
		throw logic_error("Frog::Escape(): trying to Escape target named ""-1""");
	}

	this->SetMovingFreely(false);

	if (m_position == m_target.GetPosition())
	{
		return m_velocity;
	}

	Position thisPos = m_position;
	thisPos.m_z = 0;

	Position fromWhoPos = m_target.GetPosition();
	fromWhoPos.m_z = 0;

	Velocity d = (Velocity)(thisPos - fromWhoPos);

	long double lambda = Velocity::Lambda(m_velocity0, d);

	return Velocity(d * lambda);
}

Velocity Ecosystem::Frog::Attack()
{
	if (m_target.GetTarget() == nullptr)
	{
		this->SetMovingFreely(true);
		return m_velocity;
	}

	if (m_target.GetName() == -1)
	{
		throw logic_error("Frog::Attack(): trying to Attack target named ""-1""");
	}

	this->SetMovingFreely(false);

	if (m_position == m_target.GetPosition())
	{
		return m_velocity;
	}

	Position whoPos = m_target.GetPosition();
	whoPos.m_z = 0;

	Position thisPos = m_position;
	thisPos.m_z = 0;

	Velocity d = (Velocity)(whoPos - thisPos);

	long double lambda = Velocity::Lambda(m_velocity0, d);

	return Velocity(d * lambda);
}

///////////////////////////////////////////class Hedgehog////////////////////////////////////////

int Ecosystem::Hedgehog::CanKill(const Thing& thing2) const
{
	//if (!thing2) 
	//{
	//	throw invalid_argument("Hedgehog::CanKill(thing2). thing2 is null");
	//}

	ThingType type2 = thing2.GetThingType();

	if (type2 == ThingType::FROG)
	{
		return 1;
	}

	return 0;
}

Velocity Ecosystem::Hedgehog::Escape()
{
	if (m_target.GetTarget() == nullptr)
	{
		this->SetMovingFreely(true);
		return m_velocity;
	}

	if (m_target.GetName() == -1)
	{
		throw logic_error("Hedgehog::Escape(): trying to Escape target named ""-1""");
	}

	this->SetMovingFreely(false);

	if (m_position == m_target.GetPosition())
	{
		return m_velocity;
	}

	return m_velocity;
}

Velocity Ecosystem::Hedgehog::Attack()
{
	if (m_target.GetTarget() == nullptr)
	{
		this->SetMovingFreely(true);
		return m_velocity;
	}

	if (this->m_target.GetName() == -1)
	{
		throw logic_error("Hedgehog::Attack(): trying to Attack target named ""-1""");
	}

	this->SetMovingFreely(false);

	if (m_position == m_target.GetPosition())
	{
		return m_velocity;
	}

	Position whoPos = m_target.GetPosition();
	whoPos.m_z = 0;

	Position thisPos = m_position;
	thisPos.m_z = 0;

	Velocity d = (Velocity)(whoPos - thisPos);

	long double lambda = Velocity::Lambda(m_velocity0, d);

	return Velocity(d * lambda);
}