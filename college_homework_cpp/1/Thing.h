﻿#pragma once

#include "Ecosystem.h"

namespace EM = Ecosystem::EcosystemMath;

namespace Ecosystem
{
	

#define COORDINATE_LIMIT 1000000

	class Thing;
	class Subscriber;

	enum class ThingType
	{
		DRAGONFLY,
		STORK,
		FROG,
		HEDGEHOG
	};

	enum class EventType
	{
		KILL,
		CHANGE_DIRECTION,
		STOP_UNIVERSE
	};

	struct TargetThing
	{
	public:
		TargetThing();
		TargetThing(Thing* sender);

		Thing* GetTarget() const;
		bool IsAlive() const;
		EM::Position GetPosition() const;
		EM::Velocity GetVelocity() const;
		int GetName() const;

		void SetThing(Thing* thing);
		void SetIsAlive(bool isAlive);

	private:
		Thing* m_target;
		bool m_isAlive;
	};

	class Event
	{
	public:
		Event() {}

		Event(EventType type);

		Event(EventType type,
			std::list<std::unique_ptr<Thing>>::iterator& iter);

		virtual EventType GetEventType() const;

		//void ContinueInit(std::list<std::unique_ptr<Thing>>* collection);
		void ContinueInit(std::list<std::unique_ptr<Thing>>::iterator& iter);

		//std::list<std::unique_ptr<Thing>>* GetCollecton() const;
		std::list<std::unique_ptr<Thing>>::iterator GetIter() const;

	private:
		//std::list<std::unique_ptr<Thing>>* m_collection;
		std::list<std::unique_ptr<Thing>>::iterator m_iter;

		EventType m_type;
	};

	class KillEvent : public Event
	{
	public:
		//KillEvent(const TargetThing& sender) : Event(EventType::KILL) {}
		KillEvent(std::list<std::unique_ptr<Thing>>::iterator iter) : Event(EventType::KILL, iter) {}
	};

	class ChangeDirectionEvent : public Event
	{
	public:
		ChangeDirectionEvent(std::list<std::unique_ptr<Thing>>::iterator who,
			const EM::Velocity& vel);

		EM::Velocity GetVelocity() const;

	private:
		EM::Velocity m_vel;
	};

	class SpeialTargetEventArgs
	{
	public:
		SpeialTargetEventArgs(const TargetThing& sender);

		TargetThing GetSender() const;

	private:
		TargetThing m_sender;
	};

	class SpeialTargetEvent
	{
	public:
		virtual void Subscribe(Subscriber* sub);

		virtual void Unsubscribe(Subscriber* sub);

		virtual void Notify(SpeialTargetEventArgs* e);

		EventType GetType() const;

	protected:
		SpeialTargetEvent() {}

		EventType m_type;
	private:
		std::set<Subscriber*> m_subscribers;
	};

	class TargetDiedEvent : public SpeialTargetEvent {};

	class Subscriber
	{
	public:
		virtual void OnHandle(SpeialTargetEventArgs*) = 0;

	protected:
		Subscriber() {}
	};

	class Thing : public Subscriber
	{
	public:
		Thing();
		Thing(ThingType type, int sensetivity, int eatRadius, const EM::Position& pos, const EM::Velocity& vel);
		Thing(const Thing& thing2);

		//расстояние между this и  thing2
		double Distance(const Thing& thing2) const;

		long DistanceFast(const Thing& thing2) const;

		double DistanceToTarget() const;

		long DistanceToTargetFast() const;

		virtual int CanKill(const Thing& thing2) const;

		bool IsInRange(int begin, int end) const;

		bool IsAlive() const;

		bool IsMovingFreely() const;

		int GetSensetivity() const;
		int GetSensetivitySqr() const;

		int GetEatRadius() const;
		int GetEatRadiusSqr() const;

		int GetName() const;

		ThingType GetThingType() const;

		EM::Position GetPosition() const;

		EM::Velocity GetVelocity() const;

		EM::Velocity GetInitialVelocity() const;

		void SetPosition(const EM::Position& position);

		void SetVelocity(const EM::Velocity& velocity);

		void SetMovingFreely(bool isMovingFreely);

		//throw logic_error
		virtual EM::Velocity Escape();

		//throw logic_error
		virtual EM::Velocity Attack();

		void Kill();

		virtual void OnHandle(SpeialTargetEventArgs* e) override;

		void OnKillSubscribe(Subscriber* sub);

		void OnKillUnsubscribe(Subscriber* sub);

		static void ResetObjCounter();

		//methods for m_target

		//Position GetPositionTarget() const;

		int GetNameTarget() const;

		Thing* GetTargetTarget() const;

		bool IsAliveTarget() const;

		void SetTargetTarget(Thing* target);

	private:
		int m_sensitivity;
		bool m_isAlive;
		bool m_isMovingFreely;
		int m_name;
		int m_eatRadius;


		static int s_objCounter;

	protected:
		EM::Velocity m_velocity0;
		EM::Position m_position;
		EM::Velocity m_velocity;
		ThingType m_type;

		TargetThing m_target;

		TargetDiedEvent m_killEvent;
	};

	class Dragonfly : public Thing
	{
	public:
		Dragonfly() : Thing() {}
		Dragonfly(ThingType type, int sensetivity, int eatRadius, const EM::Position& pos, const EM::Velocity& vel) :
			Thing(type, sensetivity, eatRadius, pos, vel) {}
		Dragonfly(const Dragonfly& thing2) : Thing(thing2) {}

		virtual int CanKill(const Thing& thing2) const override;

		//throw logic_error
		virtual EM::Velocity Attack() override;
	private:

	};

	class Stork : public Thing
	{
	public:
		Stork() : Thing() {}
		Stork(ThingType type, int sensetivity, int eatRadius, const EM::Position& pos, const EM::Velocity& vel) :
			Thing(type, sensetivity, eatRadius, pos, vel) {}
		Stork(const Stork& thing2) : Thing(thing2) {}

		virtual int CanKill(const Thing& thing2) const override;
	private:

	};

	class Frog : public Thing
	{
	public:
		Frog() : Thing() {}
		Frog(ThingType type, int sensetivity, int eatRadius, const EM::Position& pos, const EM::Velocity& vel) :
			Thing(type, sensetivity, eatRadius, pos, vel) {}
		Frog(const Frog& thing2) : Thing(thing2) {}

		//throw std::invalid_argument
		virtual int CanKill(const Thing& thing2) const override;

		virtual EM::Velocity Escape() override;

		virtual EM::Velocity Attack() override;
	private:

	};

	class Hedgehog : public Thing
	{
	public:
		Hedgehog() : Thing() {}
		Hedgehog(ThingType type, int sensetivity, int eatRadius, const EM::Position& pos, const EM::Velocity& vel) :
			Thing(type, sensetivity, eatRadius, pos, vel) {}
		Hedgehog(const Hedgehog& thing2) : Thing(thing2) {}

		//throw std::invalid_argument
		virtual int CanKill(const Thing& thing2) const override;

		virtual EM::Velocity Escape() override;

		virtual EM::Velocity Attack() override;
	private:

	};
}