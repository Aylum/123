#include "Ecosystem.h"

using namespace std;
using namespace Ecosystem;
namespace EM = Ecosystem::EcosystemMath;

///////////////////////////////////////////////////class EcosystemReader/////////////////////////

Ecosystem::EcosystemIO::EcosystemReader::EcosystemReader(const std::string& fileName)
{
	m_fileName = fileName;
}

void Ecosystem::EcosystemIO::EcosystemReader::Read()
{
	ifstream inptFile(m_fileName);
	if (!inptFile)
	{
		m_isSuccsessReading = false;
		throw ios_base::failure("void EcosystemReader::Read(). Can't read file");
	}

	int dragonflyNumber = 0, dragonflySensitivity = 0;
	inptFile >> dragonflyNumber >> dragonflySensitivity;
	this->AddToThings(ThingType::DRAGONFLY, dragonflyNumber, dragonflySensitivity);

	int storkNumber = 0, storkSensitivity = 0;
	inptFile >> storkNumber >> storkSensitivity;
	this->AddToThings(ThingType::STORK, storkNumber, storkSensitivity);

	int frogNumber = 0, frogSensitivity = 0;
	inptFile >> frogNumber >> frogSensitivity;
	this->AddToThings(ThingType::FROG, frogNumber, frogSensitivity);

	int hedgehogNumber = 0, hedgehogSensitivity = 0;
	inptFile >> hedgehogNumber >> hedgehogSensitivity;
	this->AddToThings(ThingType::HEDGEHOG, hedgehogNumber, hedgehogSensitivity);

	int iterationsNumber = 0;
	inptFile >> iterationsNumber;
	m_iterationsNumber = iterationsNumber;

	m_initParams.dr = dragonflySensitivity;
	m_initParams.st = storkSensitivity;
	m_initParams.fr = frogSensitivity;
	m_initParams.hd = hedgehogSensitivity;
	m_initParams.iterationsNumber = iterationsNumber;

	for (auto& i : m_things)
	{
		if (i.m_type1 == ThingType::DRAGONFLY || i.m_type1 == ThingType::STORK)
		{
			inptFile >> i.m_pos.m_x >> i.m_pos.m_y >> i.m_pos.m_z >> i.m_vel.m_x >> i.m_vel.m_y >> i.m_vel.m_z;
		}
		else
		{
			inptFile >> i.m_pos.m_x >> i.m_pos.m_y >> i.m_vel.m_x >> i.m_vel.m_y;
			i.m_pos.m_z = 0;
			i.m_vel.m_z = 0;
		}
	}

	m_isSuccsessReading = !inptFile.fail();

	inptFile.close();
}

void Ecosystem::EcosystemIO::EcosystemReader::SetEatRadius(int drRad, int stRad, int frRad, int hdRad)
{
	m_initParams.drEatRadius = drRad;
	m_initParams.stEatRadius = stRad;
	m_initParams.frEatRadius = frRad;
	m_initParams.hdEatRadius = hdRad;
}

bool Ecosystem::EcosystemIO::EcosystemReader::IsSuccsessReading() const
{
	return m_isSuccsessReading;
}

std::list<ThingInfo> Ecosystem::EcosystemIO::EcosystemReader::GetThings() const
{
	return m_things;
}

int Ecosystem::EcosystemIO::EcosystemReader::GetIterationsNumber() const
{
	return m_iterationsNumber;
}

void Ecosystem::EcosystemIO::EcosystemReader::AddToThings(ThingType type1, int number, int senstivity)
{
	for (int i = 0; i < number; i++)
	{
		m_things.emplace_back
		(
			ThingInfo
			(
				type1,
				senstivity
			)
		);
	}
}

InitParams Ecosystem::EcosystemIO::EcosystemReader::GetInitParams() const
{
	return m_initParams;
}

///////////////////////////////////////////////class EcosystemWriter/////////////////////////////

Ecosystem::EcosystemIO::EcosystemWriter::EcosystemWriter
(
	const std::list<ThingInfo>& things,
	int iterationsNumber,
	InitParams initParams,
	const std::string& fileName
)
{
	m_things = things;
	m_initParams = initParams;
	m_iterationsNumber = iterationsNumber;
	m_fileName = fileName;
}

void Ecosystem::EcosystemIO::EcosystemWriter::Print()
{
	int dragonflyNumber = 0;
	int dragonflySensitivity = m_initParams.dr;

	int storkNumber = 0;
	int storkSensitivity = m_initParams.st;

	int frogNumber = 0;
	int frogSensitivity = m_initParams.fr;

	int hedgehogNumber = 0;
	int hedgehogSensitivity = m_initParams.hd;

	list<EM::Position> dragonflyCoords;
	list<EM::Position> storkCoords;
	list<EM::Position> frogCoords;
	list<EM::Position> hedgehogCoords;

	for (auto& i : m_things)
	{
		switch (i.m_type1)
		{
		case ThingType::DRAGONFLY:
		{
			dragonflyNumber++;
			//dragonflySensitivity = i.sensetivity;
			//dragonflySensitivity = this->allSense.dr;
			dragonflyCoords.push_back(i.m_pos);
			dragonflyCoords.push_back(EM::Position(i.m_vel.m_x, i.m_vel.m_y, i.m_vel.m_z));
			break;
		}
		case ThingType::STORK:
		{
			storkNumber++;
			//storkSensitivity = i.sensetivity;
			//storkSensitivity = this->allSense.st;
			storkCoords.push_back(i.m_pos);
			storkCoords.push_back(EM::Position(i.m_vel.m_x, i.m_vel.m_y, i.m_vel.m_z));
			break;
		}
		case ThingType::FROG:
		{
			frogNumber++;
			//frogSensitivity = i.sensetivity;
			//frogSensitivity = this->allSense.fr;
			frogCoords.push_back(i.m_pos);
			frogCoords.push_back(EM::Position(i.m_vel.m_x, i.m_vel.m_y, i.m_vel.m_z));
			break;
		}
		case ThingType::HEDGEHOG:
		{
			hedgehogNumber++;
			//hedgehogSensitivity = i.sensetivity;
			//hedgehogSensitivity = this->allSense.hd;
			hedgehogCoords.push_back(i.m_pos);
			hedgehogCoords.push_back(EM::Position(i.m_vel.m_x, i.m_vel.m_y, i.m_vel.m_z));
			break;
		}
		default:
			break;
		}
	}

	ofstream outFile(m_fileName);

	if (!outFile)
	{
		m_isSuccsessWriting = false;
		throw ios_base::failure("void EcosystemWriter::Print() const. can't open file");
	}

	outFile << dragonflyNumber << " " << dragonflySensitivity << " "
		<< storkNumber << " " << storkSensitivity << " "
		<< frogNumber << " " << frogSensitivity << " "
		<< hedgehogNumber << " " << hedgehogSensitivity << " "
		<< m_iterationsNumber << endl;

	auto lambda = [&outFile](list<EM::Position>& collection, ThingType type)
	{
		if (collection.empty())
		{
			return;
		}

		auto iter = collection.begin();
		auto endIter = collection.end();
		auto preEndIter = collection.end();
		preEndIter--;
		while (iter != preEndIter && iter != endIter)
		{
			if (type == ThingType::DRAGONFLY || type == ThingType::STORK)
			{
				outFile << iter->m_x << " " << iter->m_y << " " << iter->m_z << " ";
				iter++;
				outFile << iter->m_x << " " << iter->m_y << " " << iter->m_z << endl;
				iter++;
			}
			else
			{
				outFile << iter->m_x << " " << iter->m_y << " ";
				iter++;
				outFile << iter->m_x << " " << iter->m_y << endl;
				iter++;
			}
		}
	};

	lambda(dragonflyCoords, ThingType::DRAGONFLY);
	lambda(storkCoords, ThingType::STORK);
	lambda(frogCoords, ThingType::FROG);
	lambda(hedgehogCoords, ThingType::HEDGEHOG);

	m_isSuccsessWriting = !outFile.fail();

	outFile.close();
}