#pragma once

#include <exception>
#include <stdexcept>
#include <cmath>
#include <list>
#include <set>
#include <memory>
#include <fstream>
#include <iostream>
#include <string>

#include "EcosystemMath.h"
#include "Thing.h"
#include "Universe.h"
#include "EcosystemIO.h"