#pragma once

class Tree
{
private:
	class Element{
	private:
		friend class Tree;
		Client client;
		Element *left;
		Element *right;
		Element *parent;
		int deep;

		//��������� ������� � ������
		void addRoot(Client &client, Element *el, Element *&res){
			if (client.getCountBanks() < el->client.getCountBanks()){
				if (el->left){
					addRoot(client, el->left, res);
				}
				else{
					el->left = new Element(client);
					el->left->parent = el;
					el->left->deep = el->deep + 1;
					res = el->left;
					return;
				}
			}
			else if (client.getCountBanks() > el->client.getCountBanks()){
				if (el->right){
					addRoot(client, el->right, res);
				}
				else{
					el->right = new Element(client);
					el->right->parent = el;
					el->right->deep = el->deep + 1;
					res = el->right;
					return;
				}
			}
		}
		//��������� el � ������
		void addElement(Element *recur_step, Element *el){
			if (el->client.getCountBanks() < recur_step->client.getCountBanks()){
				if (recur_step->left){
					addElement(recur_step->left, el);
				}
				else{
					recur_step->left = el;
					recur_step->left->deep = recur_step->deep + 1;
					correctDeep(recur_step->left);
					return;
				}
			}
			else{
				if (recur_step->right){
					addElement(recur_step->right, el);
				}
				else{
					recur_step->right = el;
					recur_step->right->deep = recur_step->deep + 1;
					correctDeep(recur_step->right);
					return;
				}
			}
		}
		//���������� ������� ������
		void correctDeep(Element *el){
			if (el->left){
				el->left->deep = el->deep + 1;
				correctDeep(el->left);
			}
			if (el->right){
				el->right->deep = el->deep + 1;
				correctDeep(el->right);
			}
		}
		//���� ������� � �������� ���������
		void methodForSearch(Client &client, Element *el, Element *&res);

		//������� ������� � �������� ���������
		void del(Client &client, Element *el);

		//���������� ���-�� �������
		void methodForGetLeafsNumber(Element *el, int &counter);

		//���������� ���-�� ������
		void methodForGetRootsNumber(Element *el, int &counter);

		//���������� ������������ ������� ������
		void methodForMaxTreeDeep(Element *el, int &max_deep);

		//������� ������
		void methodForClear(Element *el);

		void treeToVector(Element *el, std::vector<Client> &vec);
	public:
		Element(){
			client = Client::Client();
			left = nullptr;
			right = nullptr;
			parent = nullptr;
			deep = 0;
		}
		Element(Client client){
			this->client = client;
			left = nullptr;
			right = nullptr;
			parent = nullptr;
			deep = 0;
		}
		/*~Element(){
			if(left){
				delete left;
			}
			if(right){
				delete right;
			}
			delete this;
		}*/
	};

	Element *el;
public:
	Tree(){
		el = new Element;
	}
	Tree(Client client){
		el = new Element(client);
	}
	~Tree(){
		this->el->methodForClear(this->el);
	}
	void add(Client &client){
		Element *buf;
		this->el->addRoot(client, this->el, buf);
	}
	Tree search(Client client){
		Tree tr;
		this->el->methodForSearch(client, this->el, tr.el);
		
		if (tr.el->client != client){
			tr.el = nullptr;
		}
		return tr;
	}
	Tree searchWithInsert(Client client){
		Tree tr;
		this->el->methodForSearch(client, this->el, tr.el);

		if (tr.el->client == client){
			return tr;
		}
		
		this->el->addRoot(client, this->el, tr.el);

		return tr;
	}
	void remove(Client client){
		this->el->del(client, this->el);
	}
	int getLeafsNumber(){
		int res = 0;
		this->el->methodForGetLeafsNumber(this->el, res);
		return res;
	}
	int getRootsNumber(){
		int res = 0;
		this->el->methodForGetRootsNumber(this->el, res);
		return res;
	}
	int getMaxTreeDeep(){
		int res = 0;
		el->methodForMaxTreeDeep(el, res);
		return res;
	}
	bool equals(Tree &tr2){
		std::vector<Client> vec1;
		std::vector<Client> vec2;

		this->el->treeToVector(this->el, vec1);
		tr2.el->treeToVector(tr2.el, vec2);
		int n = vec1.size();
		if (n != vec2.size())
			return false;
		
		std::vector<Client>::iterator i1 = vec1.begin();
		std::vector<Client>::iterator i2 = vec2.begin();
		bool f = true;
		int i = 0; 
		while (i < n && f){
			f = *i1 == *i2;
			i++;
		}

		return f;
	}
};