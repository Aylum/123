#include "stdafx.h"

void Tree::Element::methodForSearch(Client &client, Element *el, Element *&res)
{
	if (el->client == client){
		res = el;
		return;
	}
	if (el->left){
		methodForSearch(client, el->left, res);
	}
	if (el->right){
		methodForSearch(client, el->right, res);
	}
}
void Tree::Element::del(Client &client, Element *el)
{
	if (client == el->client){

		if (el->left && el->right){
			if (el->parent->left == el){
				el->parent->left = el->left;
			}
			else el->parent->right = el->left;

			el->left->deep = el->deep;
			el->left->parent = el->parent;

			Element *temp_tree = el->right;
			Element *new_el = el->left;

			delete el;

			addElement(new_el, temp_tree);
			return;
			
			//Element *temp = el->right;
			//while (temp->left){
			//	temp = temp->left;
			//}

			////temp->parent->left = nullptr;

			//temp->left = el->left;
			////temp->right = el->right;
			//temp->parent = el->parent;

			//if (el->parent->left == el){
			//	el->parent->left = temp;
			//}
			//else el->parent->right = temp;
		}
		else{

			if (!el->left && !el->right){
				if (el->parent->left == el){
					el->parent->left = nullptr;
				}
				else{
					el->parent->right = nullptr;
				}
				delete el;
				return;
			}

			Element *temp_tree = nullptr;
			if (el->left){
				temp_tree = el->left;
			}
			else if (el->right){
				temp_tree = el->right;
			}

			temp_tree->deep = el->deep;
			temp_tree->parent = el->parent;
			correctDeep(temp_tree);

			if (el->parent->left == el){
				el->parent->left = temp_tree;
			}
			else{
				el->parent->right = temp_tree;
			}

			delete el;
			return;


			/*if (el->parent->left == el){
				if (el->left){
					el->parent->left = el->left;
					el->left->deep = el->deep;
					el->left->parent = el->parent;
				} else{
					el->parent->left = el->right;
					el->right->deep = el->deep;
					el->right->parent = el->parent;
				}
			}
			else{
				if (el->left){
					el->parent->right = el->left;
					el->left->deep = el->deep;
					el->left->parent = el->parent;
				}
				else{
					el->parent->right = el->right;
					el->right->deep = el->deep;
					el->right->parent = el->parent;
				}
			}
			delete el;
			return;*/
		}
	}

	if (client.getCountBanks() < el->client.getCountBanks()){
		if (el->left){
			del(client, el->left);
		}
	}
	else if (client.getCountBanks() > el->client.getCountBanks()){
		if (el->right){
			del(client, el->right);
		}
	}
}
void Tree::Element::methodForGetLeafsNumber(Element *el, int &counter)
{
	if (!el->left && !el->right){
		counter++;
		return;
	}
	
	if (el->left){
		methodForGetLeafsNumber(el->left, counter);
	}
	if (el->right){
		methodForGetLeafsNumber(el->right, counter);
	}
}
void Tree::Element::methodForGetRootsNumber(Element *el, int &counter){
	if (el->left){
		methodForGetRootsNumber(el->left, counter);
	}
	if (el->right){
		methodForGetRootsNumber(el->right, counter);
	}
	counter++;
}
void Tree::Element::methodForMaxTreeDeep(Element *el, int &max_deep)
{
	if (el->left){
		methodForMaxTreeDeep(el->left, max_deep);
	}
	if (el->right){
		methodForMaxTreeDeep(el->right, max_deep);
	}
	if (el->deep > max_deep){
		max_deep = el->deep;
	}
}
void Tree::Element::methodForClear(Element *el)
{
	if (el->left){
		methodForClear(el->left);
	}
	if (el->right){
		methodForClear(el->right);
	}
	el->left = nullptr;
	el->right = nullptr;
	el->parent = nullptr;
	delete el;
}
void Tree::Element::treeToVector(Element *el, std::vector<Client> &vec)
{
	vec.push_back(el->client);
	if (el->left){
		treeToVector(el->left, vec);
	}
	if (el->right){
		treeToVector(el->right, vec);
	}
}