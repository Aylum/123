#pragma once

template<typename KeyType, typename ValType>
struct KeyVal
{
	KeyType m_key;
	ValType m_val;
};

template<typename T>
class Node
{
public:
	Node();
	Node(unsigned int key, const T& val);
	Node(const Node<T>& Node2);

	~Node();

private:
	KeyVal<unsigned int, T> m_data;
	Node* m_next;
};

template<typename T>
class SparseVec
{
public:
	SparseVec();
	SparseVec(unsigned int key, const T& val);
	SparseVec(const SparseVec<T>& Node2);

	~SparseVec();

	void SetDefaultVal(const T& val); //TODO: move to wrapper for Node<T>
	T GetDefaultVal(); //TODO: move to wrapper for Node<T>

	T& operator[](unsigned int key);

	void Emplace(unsigned int key, const T& val);
	T Search(unsigned int key) const;
	void Remove(unsigned int key);

private:
	Node<T>* m_head;

	T m_defaultVal;
};

template<typename T>
Node<T>::Node()
{
	this->m_data = nullptr;
	this->m_next = nullptr;
}

template<typename T>
Node<T>::Node(unsigned int key, const T& val)
{
	this->m_data.m_key = key;
	this->m_data.m_val = val;

	this->m_next = nullptr;
	
}

template<typename T>
Node<T>::Node(const Node& Node2)
{
	this->m_data = Node2.m_data;
	this->m_next = Node2.m_next;
}

template<typename T>
Node<T>::~Node()
{
	if (this->m_next != nullptr)
	{
		this->m_next->~Node(); // delete this->m_next;
	}
}

template<typename T>
static void Node<T>::SetDefaultVal(const T& val)
{
	s_defaultVal = val;
}

template<typename T>
static T Node<T>::GetDefaultVal()
{
	return s_defaultVal;
}

template<typename T>
T& Node<T>::operator[](unsigned int key)
{
	Node<T>* p = this->m_head;
	while (p != nullptr && p->m_data->m_key != key)
	{
		p = p->m_next;
	}

	if (p == nullptr)
	{
		return s_defaultVal;
	}

	return p->m_data->m_val;
}

template<typename T>
void Node<T>::Emplace(unsigned int key, const T& val)
{
	if (this->m_head == nullptr)
	{
		this->m_head = this;
		
		this->m_data = new KeyVal<unsigned int, T>;
		this->m_data->m_key = key;
		this->m_data->m_val = val;

		this->m_next = nullptr;

		return;
	}


	Node<T>* p = this->m_head;
	while (p->m_next != nullptr && key < p->m_next->m_data->m_key)
	{
		p = p->m_next;
	}

	if (p->m_next != nullptr && p->m_next->m_data->m_key == key)
	{
		p->m_next->m_data->m_val = val;

		return;
	}

	Node<T>* temp = new Node<T>(key, val);
	//initialize temp
}