#pragma once

template<typename KeyType, typename ValType>
struct KeyVal
{
	KeyType m_key;
	ValType m_val;
};

template<typename T>
class Node
{
public:
	Node();
	Node(unsigned int key, const T& val);
	Node(const Node<T>& Node2);

	~Node();

	void Emplace(unsigned int key, const T& val);
	void Remove(unsigned int key);
	Node<T>* Search(unsigned int key);

private:
	KeyVal<unsigned int, T> m_data;
	Node<T>* m_next;
};

template<typename T>
class SparseVec
{
public:
	SparseVec();
	SparseVec(unsigned int key, const T& val);
	SparseVec(unsigned int key, const T& val, const T& defaultVal);
	SparseVec(const SparseVec<T>& node2);

	~SparseVec();

	void SetDefaultVal(const T& val);
	T const* GetDefaultVal() const;

	T* operator[](unsigned int key);

	void Emplace(unsigned int key, const T& val);
	T* Search(unsigned int key);
	void Remove(unsigned int key);
	void Clear();

	bool IsEmpty() const;

private:
	Node<T>* m_head;

	T* m_defaultVal;
};

///////////////////////////////////class Node<T>///////////////////////////////////////

template<typename T>
Node<T>::Node()
{
	this->m_next = nullptr;
}

template<typename T>
Node<T>::Node(unsigned int key, const T& val)
{
	this->m_data.m_key = key;
	this->m_data.m_val = val;

	this->m_next = nullptr;
}

template<typename T>
Node<T>::Node(const Node& Node2)
{
	this->m_data = Node2.m_data;
	this->m_next = Node2.m_next;
}

template<typename T>
Node<T>::~Node()
{
	if (this->m_next != nullptr)
	{
		this->m_next->~Node(); // delete this->m_next;
	}
}

template<typename T>
void Node<T>::Emplace(unsigned int key, const T& val)
{
	if (key < this->m_data.m_key)
	{
		Node<T>* temp = new Node<T>(*this);

		temp->m_next = this->m_next;
		this->m_next = temp;

		this->m_data.m_key = key;
		this->m_data.m_val = val;

		return;
	}

	Node<T>* p = this;
	while (p->m_next != nullptr)
	{
		if (p->m_next->m_data.m_key == key)
		{
			p->m_next->m_data.m_val = val;
			return;
		}

		if (p->m_next->m_data.m_key > key)
		{
			Node<T>* temp = new Node<T>(key, val);
			
			temp->m_next = p->m_next;
			p->m_next = temp;

			return;
		}

		p = p->m_next;
	}
}

template<typename T>
Node<T>* Node<T>::Search(unsigned int key)
{
	Node<T>* p = this;
	while (p != nullptr && p->m_data.m_key != key)
	{
		p = p->m_next;
	}

	if (p == nullptr)
	{
		return nullptr;
	}

	return p;
}

////////////////////////////////////class SparseVec/////////////////////////////////////////

template<typename T>
SparseVec<T>::SparseVec()
{
	this->m_head = nullptr;
	this->m_defaultVal = nullptr;
}

template<typename T>
SparseVec<T>::SparseVec(unsigned int key, const T& val)
{
	this->m_head = new Node<T>(key, val);
	this->m_defaultVal = nullptr;
}

template<typename T>
SparseVec<T>::SparseVec(unsigned int key, const T& val, const T& defaultVal)
{
	this->m_head = new Node<T>(key, val);
	this->m_defaultVal = new T;
	*this->m_defaultVal = defaultVal;
}

template<typename T>
SparseVec<T>::SparseVec(const SparseVec<T>& node2)
{
	this->m_defaultVal = node2.m_defaultVal;
	this->m_head = node2.m_head;
}

template<typename T>
SparseVec<T>::~SparseVec()
{
	if (this->m_head != nullptr)
	{
		delete this->m_head;
	}
	
	if (this->m_defaultVal != nullptr)
	{
		delete this->m_defaultVal;
	}
}

template<typename T>
void SparseVec<T>::SetDefaultVal(const T& val)
{
	if (this->m_defaultVal == nullptr)
	{
		this->m_defaultVal = new T;
	}

	*this->m_defaultVal = val;
}

template<typename T>
T const* SparseVec<T>::GetDefaultVal() const
{
	return this->m_defaultVal;
}

template<typename T>
T* SparseVec<T>::operator[](unsigned int key)
{
	return this->Search(key);
}

template<typename T>
void SparseVec<T>::Emplace(unsigned int key, const T& val)
{
	if (this->m_head == nullptr)
	{
		this->m_head = new Node<T>(key, val);
		return;
	}

	this->m_head->Emplace(key, val);
}

template<typename T>
T* SparseVec<T>::Search(unsigned int key)
{
	if (this->m_head == nullptr)
	{
		return this->m_defaultVal;
	}

	Node<T>* res = this->m_head->Search(key);

	if (res == nullptr)
	{
		return this->m_defaultVal;
	}

	return &res->m_data.m_val;
}

template<typename T>
void SparseVec<T>::Clear()
{
	if (this->m_head != nullptr)
	{
		delete this->m_head;
	}
}

template<typename T>
bool SparseVec<T>::IsEmpty() const
{
	if (this->m_head == nullptr)
	{
		return true;
	}

	return false;
}