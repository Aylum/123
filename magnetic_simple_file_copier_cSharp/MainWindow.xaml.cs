﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace file_copier
{
    public class Constants
    {
        //public static readonly string mainDirPath = "";
        public static readonly string logFileName = "file_copier.log";
    }

    public partial class MainWindow : Window
    {
        private Dictionary<string, string> variables = new Dictionary<string, string>(){ { "ep", "" }, { "sc", "" } };

        private string logFilePath = "";

        private SortedList<string, string> _files = new SortedList<string, string>();

        private void Init()
        {
            try
            {
                string rootDir = Directory.GetCurrentDirectory();

                //create log file
                System.IO.File.WriteAllText(rootDir + "\\" + Constants.logFileName, "");

                //warn if log file creation is failed
                if(!System.IO.File.Exists(rootDir + "\\" + Constants.logFileName))
                {
                    MessageBox.Show("couldn't create .log file, "
                        + "probably you shoud run the program with Administrator rights");
                }
                else
                {
                    logFilePath = rootDir + "\\" + Constants.logFileName;
                }

                //check if config dir exists
                if (!Directory.Exists(rootDir + "\\config"))
                {
                    throw new System.ArgumentException("Folder 'config' doesnt exists. "
                        + "Folder 'config' should be located in the same folder"
                        + " where the program is.");
                }

                //list all .proc files
                string[] filesPaths = Directory.GetFiles(rootDir + "\\config", "*.proc");

                if(filesPaths.Length < 1)
                {
                    throw new System.ArgumentException("Folder 'config' contains 0 *.proc files");
                }

                //get .proc file names without extension
                for (int i = 0; i < filesPaths.Length; i++)
                {
                    _files.Add(System.IO.Path.GetFileNameWithoutExtension(filesPaths[i]), filesPaths[i]);
                }

                if(btn1 != null)
                {
                    btn1.Click += OnCopyButtonClick;
                }

                //draw buttons with those names
                DrawButtons();
            }
            catch (Exception ex)
            {
                PrintToOutWindow("Exception in Init(): " + ex.GetType().Name + ":"
                    + "\n" + ex.Message);
                MessageBox.Show("Exception in Init(): " + ex.GetType().Name + ":"
                    + "\n" + ex.Message);
                PrintLog("\nException: Init(): " + ex.GetType().Name + ":\n" + ex.Message
                    + "\n-------------------------------------------");
            }
        }

        private void PrintToOutWindow(string message)
        {
            this.outWindow1.Text = message;
        }

        private void PrintLog( string message)
        {
            if(logFilePath == "")
            {
                return;
            }

            System.IO.File.AppendAllText(logFilePath, message);
        }

        private void DrawButtons()
        {
            try
            {
                //-672,-240,0,0
                int startTopMargin = -240;
                foreach (var el in _files)
                {
                    Button button = new Button()
                    {
                        Content = el.Key,
                        Tag = el.Key,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        VerticalAlignment = VerticalAlignment.Top,
                        Width = 292,
                        Height = 25,
                        Margin = new Thickness(-672, startTopMargin, 0, 0)
                    };

                    button.Click += new RoutedEventHandler(OnButtonClick);
                    this.bla.Children.Add(button);

                    startTopMargin += 35;
                }
            }
            catch(Exception ex)
            {
                PrintToOutWindow("Exception in DrawButtons(): " + ex.GetType().Name + ":"
                    + "\n" + ex.Message);
                MessageBox.Show("Exception in DrawButtons(): " + ex.GetType().Name + ":"
                    + "\n" + ex.Message);
                PrintLog("\nException: DrawButtons(): " + ex.GetType().Name + ":\n" + ex.Message
                    + "\n-------------------------------------------");
            }
        }

        //path == segment[0], position == 1
        private void WalkDirsWithPattern(string path, string[] segment, int position, ref List<string> collection)
        {
            if(position >= segment.Length)
            {
                if (Directory.Exists(path))
                {
                    collection.Add(path);
                }
                
                return;
            }

            string last = segment[position];

            if(last.Contains('*'))
            {
                var dirs = Directory.GetDirectories(path, last);
                if(dirs.Length > 0)
                {
                    foreach (var dir in dirs)
                    {
                        WalkDirsWithPattern(path + "\\" + System.IO.Path.GetFileName(dir), segment, position + 1, ref collection);
                    }
                }
                else if (Directory.Exists(path))
                {
                    collection.Add(path);
                    return;
                }
            }
            else
            {
                WalkDirsWithPattern(path + "\\" + last, segment, position + 1, ref collection);
            }
        }

        private void WalkDirs2(string path, int skip, ref List<string> collection)
        {
            string[] dirs = Directory.GetDirectories(path);

            if(dirs.Length < 1)
            {
                string[] files = Directory.GetFiles(path);
                if(files.Length > 0)
                {
                    foreach(string file in files)
                    {
                        string[] splittedFile = file.Split('\\');
                        if(skip < splittedFile.Length)
                        {
                            splittedFile = splittedFile.Skip(skip).ToArray();
                            collection.Add(string.Join("\\", splittedFile));
                        }
                    }
                }
                return;
            }
            else
            {
                string[] files2 = Directory.GetFiles(path);
                if (files2.Length > 0)
                {
                    foreach (string file in files2)
                    {
                        string[] splittedFile = file.Split('\\');
                        if (skip < splittedFile.Length)
                        {
                            splittedFile = splittedFile.Skip(skip).ToArray();
                            collection.Add(string.Join("\\", splittedFile));
                        }
                    }
                }
            }

            foreach(string dir in dirs)
            {
                WalkDirs2(dir, skip, ref collection);
            }
        }

        private string Normpath(string path, bool trimEndSlash = false)
        {
            string res = path.Replace("/", "\\");

            bool isSlashAtTheEnd = res[res.Length - 1] == '\\' ? true : false;

            string temp = "";
            string[] splitted1 = res.Split('\\');

            if(splitted1.Length > 1)
            {
                temp += splitted1[0];
            }

            for(int i = 1; i < splitted1.Length; i++)
            {
                if(splitted1[i] != "" && splitted1[i].IndexOf("\\") == -1)
                {
                    temp += "\\" + splitted1[i];
                }
            }

            res = trimEndSlash ? temp : isSlashAtTheEnd ? temp + "\\" : temp;

            string[] splitted = res.Split(':');

            if(splitted.Length != 2)
            {
                return res;
            }

            return splitted[0].ToUpper() + ":" + splitted[1];
        }

        private static void CopyDir(string sourceDirName, string destDirName, bool copySubDirs = true)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName
                );
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = System.IO.Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = System.IO.Path.Combine(destDirName, subdir.Name);
                    CopyDir(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            string buttonName = "";
            try
            {
                //get text inside button
                Button button = (Button)e.Source;

                if(e != null && button.Tag is string)
                {
                    buttonName = (string)button.Tag;
                }
                else
                {
                    buttonName = (string)button.Content;
                }

                //if something wrong
                if(buttonName == null || buttonName == "")
                {
                    throw new System.ArgumentException("buttonName == null || buttonName == \"\"");
                }

                //get text from ep/sc text fields
                foreach(var child in ((Grid)button.Parent).Children)
                {
                    if(child is TextBox
                        && (((TextBox)child).Name == "ep" || ((TextBox)child).Name == "sc"))
                    {
                        string tempStr = ((TextBox)child).Text;
                        if(tempStr != null && tempStr != "")
                        {
                            variables[((TextBox)child).Name] = tempStr;
                        }
                        else
                        {
                            PrintLog("\nWarning: OnButonClick(): " + buttonName + ": "
                                + "name, retrieved from episode/scene name text field, is empty string or null\n");
                        }
                    }
                }

                //read whole file into lines collection
                string filePath = _files[buttonName];
                string[] lines = System.IO.File.ReadAllLines(filePath);

                //iterate lines collection, find line with word #variable, copy "path" lines along the way
                List<string> pathLines = new List<string>(lines.Length);
                int variableLineIdx = 0;
                for(; variableLineIdx < lines.Length; variableLineIdx++)
                {
                    if(lines[variableLineIdx] == ""
                        || !lines[variableLineIdx].Contains("\\") && !lines[variableLineIdx].Contains("/")
                        && !lines[variableLineIdx].ToLower().Contains("#variable"))
                    {
                        continue;
                    }

                    if (lines[variableLineIdx].ToLower().Contains("#variable"))
                    {
                        break;
                    }
                    else
                    {
                        int commentIdx = lines[variableLineIdx].IndexOf("::");
                        if(commentIdx != -1)
                        {
                            pathLines.Add(lines[variableLineIdx].Substring(0, commentIdx));
                        }
                        else
                        {
                            pathLines.Add(lines[variableLineIdx]);
                        }
                    }
                }

                //manage variables inside lines collection
                for(int i = variableLineIdx + 1; i < lines.Length; i++)
                {
                    if(lines[i] == "" || !lines[i].Contains("="))
                    {
                        continue;
                    }

                    string[] splitted = lines[i].Split(' ');

                    foreach(var spl in splitted)
                    {
                        var varVal = spl.Split('=');
                        
                        if(varVal.Length != 2)
                        {
                            PrintLog("\nWarning: OnButonClick(): " + buttonName + ": " + "Couldn't read variable: " + spl + "\n");
                            continue;
                        }

                        if(!variables.ContainsKey(varVal[0]))
                        {
                            variables.Add(varVal[0], varVal[1]);
                        }
                        else
                        {
                            variables[varVal[0]] = varVal[1];
                        }
                    }
                }

                //iterate pathLines collection
                int lineInd = 0;
                foreach(var line in pathLines)
                {
                    lineInd++;

                    string _line = line;

                    while (_line.IndexOf('|') != -1)
                    {
                        foreach (var variable in variables)
                        {
                            _line = _line.Replace("|" + variable.Key, variable.Value);
                        }
                    }

                    int grSymbolIdx = _line.IndexOf(">");

                    if (grSymbolIdx == -1)
                    {
                        PrintLog("\nError: OnButtonClick(): " + buttonName + ": "
                                + "couldn't split line by '>' symbol, line: " + _line + "\n");
                        continue;
                    }

                    string grSymbols = "";
                    for(int i = grSymbolIdx; i < _line.Length && _line[i] == '>'; i++)
                    {
                        grSymbols += _line[i];
                    }

                    var sourceDest = _line.Split(new string[] { grSymbols }, StringSplitOptions.RemoveEmptyEntries);

                    sourceDest[0] = Normpath(sourceDest[0], true);
                    sourceDest[1] = Normpath(sourceDest[1], true);

                    string[] splittedSD1 = sourceDest[0].Split('\\');
                    string[] splittedSD2 = sourceDest[1].Split('\\');

                    if(splittedSD1.Length < 2)
                    {
                        PrintLog("\nError: OnButtonClick(): " + buttonName + ": "
                            + "after splitting source path by '\\' splitted length < 2, line: " + _line + "\n");
                        continue;
                    }

                    if(splittedSD1.Contains("*") || !Directory.Exists(splittedSD1[0]))
                    {
                        PrintLog("\nError: OnButtonClick(): " + buttonName + ": "
                            + "Wrong path, directory " + splittedSD1[0] + " doesn't exists, line number: " + lineInd + "\n");
                        continue;
                    }

                    if (splittedSD2.Length < 1 || splittedSD2.Contains("*") || !Directory.Exists(splittedSD2[0]))
                    {
                        PrintLog("\nError: OnButtonClick(): " + buttonName + ": "
                            + "Wrong path, directory " + splittedSD2[0] + " doesn't exists, line: " + _line + "\n");
                        continue;
                    }

                    string fileAtTheEnd = null;
                    if(splittedSD1[splittedSD1.Length - 1].Contains('.'))
                    {
                        fileAtTheEnd = splittedSD1[splittedSD1.Length - 1];
                        string[] tempStringArr = new string[splittedSD1.Length - 1];
                        for(int i = 0; i < tempStringArr.Length; i++)
                        {
                            tempStringArr[i] = splittedSD1[i];
                        }
                        splittedSD1 = tempStringArr;
                    }

                    if (splittedSD1.Length < 2)
                    {
                        PrintLog("\nError: OnButtonClick(): " + buttonName + ": "
                            + "after splitting source path by '\\' splitted length < 2, line: " + _line + "\n");
                        continue;
                    }

                    List<string> paths = new List<string>();
                    WalkDirsWithPattern(splittedSD1[0], splittedSD1, 1, ref paths);

                    if(paths.Count < 1)
                    {
                        PrintLog("\nError: OnButtonClick(): " + buttonName + ": "
                            + "after walking dirs, couldn't define source paths for copying, line: " + _line + "\n");
                        continue;
                    }

                    SortedList<string, string> copiedFiles = new SortedList<string, string>();

                    if(fileAtTheEnd == null)
                    {
                        foreach(string path in paths)
                        {
                            try
                            {
                                CopyDir(path, sourceDest[1]);
                            }
                            catch(Exception ex)
                            {
                                PrintLog("\nWarning: OnButonClick(): " + buttonName + ": " + "Couldn't copy from: " + path + " to: " + sourceDest[1]
                                    + "\nexception: " + ex.GetType().Name + "\nmessage: " + ex.Message + "\n");
                            }
                            
                            string normpath = Normpath(path, true);
                            List<string> sourceFiles = new List<string>();
                            WalkDirs2(normpath, normpath.Split('\\').Length, ref sourceFiles);
                            
                            foreach(string sourceFile in sourceFiles)
                            {
                                if(!System.IO.File.Exists(sourceDest[1] + "\\" + sourceFile))
                                {
                                    PrintLog("\nWarning: OnButonClick(): " + buttonName + ": " + "Failed to copy file from: " + path + "\\" + sourceFile + " to: "
                                            + sourceDest[1] + "\\" + sourceFile + "\n");
                                }
                                else
                                {
                                    copiedFiles.Add(normpath + "\\" + sourceFile, sourceDest[1] + "\\" + sourceFile);
                                }
                            }

                        }
                    }
                    else
                    {
                        bool isContainsAsterisk = fileAtTheEnd.Contains("*");
                        foreach(string path in paths)
                        {
                            if(isContainsAsterisk)
                            {
                                string[] filesToCopy = Directory.GetFiles(path, fileAtTheEnd);
                                foreach(string file in filesToCopy)
                                {
                                    string destPath = splittedSD2[0];
                                    for(int i = 1; i < splittedSD2.Length; i++)
                                    {
                                        destPath += "\\" + splittedSD2[i];
                                        if (!Directory.Exists(destPath))
                                        {
                                            Directory.CreateDirectory(destPath);
                                        }
                                    }
                                    try
                                    {
                                        string fileName = System.IO.Path.GetFileName(file);
                                        System.IO.File.Copy(file, sourceDest[1] + "\\" + fileName, true);
                                        if(!System.IO.File.Exists(sourceDest[1] + "\\" + fileName))
                                        {
                                            PrintLog("\nWarning: OnButonClick(): " + buttonName + ": " + "Failed to copy file from: " + file + " to: " + sourceDest[1] + "\\" + fileName + "\n");
                                        }
                                        else
                                        {
                                            copiedFiles.Add(file, sourceDest[1] + "\\" + fileName);
                                        }
                                    }
                                    catch(Exception ex)
                                    {
                                        PrintLog("\nWarning: OnButonClick(): " + buttonName + ": " + "Failed to copy file from: " + file + " to: "
                                            + sourceDest[1] + "\\" + System.IO.Path.GetFileName(file)
                                            + "\nException: " + ex.GetType().Name + "Message: " + ex.Message + "\n");
                                    }
                                }
                            }
                            else
                            {
                                string destPath = splittedSD2[0];
                                for (int i = 1; i < splittedSD2.Length; i++)
                                {
                                    destPath += "\\" + splittedSD2[i];
                                    if (!Directory.Exists(destPath))
                                    {
                                        Directory.CreateDirectory(destPath);
                                    }
                                }
                                try
                                {
                                    System.IO.File.Copy(path + "\\" + fileAtTheEnd, sourceDest[1] + "\\" + fileAtTheEnd, true);
                                    if (!System.IO.File.Exists(sourceDest[1] + "\\" + fileAtTheEnd))
                                    {
                                        PrintLog("\nWarning: OnButonClick(): " + buttonName + ": " + "Failed to copy file from: " + path + "\\" + fileAtTheEnd + " to: " + sourceDest[1] + "\\" + fileAtTheEnd + "\n");
                                    }
                                    else
                                    {
                                        copiedFiles.Add(path + "\\" + fileAtTheEnd, sourceDest[1] + "\\" + fileAtTheEnd);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    PrintLog("\nWarning: OnButonClick(): " + buttonName + ": " + "Failed to copy file from: " + path + "\\" + fileAtTheEnd + " to: "
                                        + sourceDest[1] + "\\" + fileAtTheEnd
                                        + "\nException: " + ex.GetType().Name + "Message: " + ex.Message + "\n");
                                }
                            }
                        }
                    }

                    PrintLog("\n\n----------------------------------------------------------\nCopied files: ");

                    foreach (var el in copiedFiles)
                    {
                        PrintLog("\n\n" + el.Key + "    >>    " + el.Value);
                    }
                }

                MessageBox.Show("done.");
            }
            catch (Exception ex)
            {
                string tempButtonName = "";
                if(buttonName != null)
                {
                    tempButtonName = buttonName;
                }

                PrintToOutWindow("Exception in OnClick(): " + tempButtonName + ": " + ex.GetType().Name + ":"
                    + "\n" + ex.Message);
                MessageBox.Show("Exception in OnClick(): " + tempButtonName + ": " + ex.GetType().Name + ":"
                    + "\n" + ex.Message);
                PrintLog("Exception in OnClick(): " + tempButtonName + ": " + ex.GetType().Name + ":"
                    + "\n" + ex.Message
                    + "\n-------------------------------------------");
            }
        }

        private void OnCopyButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = e.Source as Button;
                if (button == null)
                {
                    throw new System.ArgumentException("Error: OnCopyButtonClick(): Button button = e.Source as Button; button==null");
                }

                string procFileName = null;
                foreach (var child in ((Grid)button.Parent).Children)
                {
                    if (child is TextBox && ((TextBox)child).Name == "procFileTextField")
                    {
                        string tempStr = ((TextBox)child).Text;
                        if (tempStr != null && tempStr != "")
                        {
                            if (tempStr.Contains("."))
                            {
                                tempStr = tempStr.Split('.')[0];
                            }

                            foreach (var file in _files)
                            {
                                if (file.Key == tempStr)
                                {
                                    procFileName = file.Key;
                                    break;
                                }
                            }

                            if (procFileName == null)
                            {
                                throw new System.ArgumentException("Couldn't find proc file with name: " + tempStr);
                            }
                        }
                        else
                        {
                            throw new System.ArgumentException("\nWarning: OnCopyButtonClick(): "
                                + "name, retrieved from text field is empty string or null\n");
                        }
                        break;
                    }
                }

                if (procFileName == null)
                {
                    throw new System.ArgumentException("\nWarning: OnCopyButtonClick(): "
                        + "couldn't get .proc file name");
                }

                button.Tag = procFileName;
                e.Source = button;
                OnButtonClick("copyButton", e);
            }
            catch (Exception ex)
            {
                PrintToOutWindow("Exception in OnCopyButtonClick(): " + ex.GetType().Name + ":"
                    + "\n" + ex.Message);
                MessageBox.Show("Exception in OnCopyButtonClick(): " + ex.GetType().Name + ":"
                    + "\n" + ex.Message);
                PrintLog("Exception in OnCopyButtonClick(): " + ex.GetType().Name + ":"
                    + "\n" + ex.Message
                    + "\n-------------------------------------------");

                PrintLog("Error: OnCopyButtonClick(): Button button = e.Source as Button; button==null");
                PrintToOutWindow("Error: OnCopyButtonClick(): Button button = e.Source as Button; button==null");
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            Init();
        }
    }
}
