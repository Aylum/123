#include "windows.h"

#include "COMPortReader.h"

#include <string>

#include <maya/MGlobal.h>

using namespace std;

int COMPortReader::portNum = 3;
thread COMPortReader::readerThread;
bool COMPortReader::isRunning = false;
vector<int> COMPortReader::vec(20, 0);
int COMPortReader::state1 = 1;
int COMPortReader::val = 0;

void COMPortReader::Run()
{
	if (COMPortReader::isRunning)
	{
		//COMPortReader::Stop();
		return;
	}

	COMPortReader::isRunning = true;

	COMPortReader::readerThread = thread(COMPortReader::ThreadMethod);
	readerThread.detach();
}

void COMPortReader::Stop()
{
	if (!COMPortReader::isRunning)
	{
		return;
	}

	try
	{
		int a = 5;

		COMPortReader::isRunning = false;

		//COMPortReader::readerThread.join();

		int b = 5;
	}
	catch (const exception& e)
	{
		string str1(e.what());
		int a = 5;
	}
}

void COMPortReader::ThreadMethod()
{
	// Open serial port
	HANDLE serialHandle;

	serialHandle = CreateFile(string("\\\\.\\COM" + to_string(COMPortReader::portNum)).c_str(), GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

	// Do some basic settings
	DCB serialParams = { 0 };
	serialParams.DCBlength = sizeof(serialParams);

	GetCommState(serialHandle, &serialParams);
	serialParams.BaudRate = 9600;
	//serialParams.ByteSize = BYTE_D;
	//serialParams.StopBits = stopBits;
	//serialParams.Parity = parity;
	SetCommState(serialHandle, &serialParams);

	// Set timeouts
	COMMTIMEOUTS timeout = { 0 };
	timeout.ReadIntervalTimeout = 50;
	timeout.ReadTotalTimeoutConstant = 50;
	timeout.ReadTotalTimeoutMultiplier = 50;
	timeout.WriteTotalTimeoutConstant = 50;
	timeout.WriteTotalTimeoutMultiplier = 10;
	
	SetCommTimeouts(serialHandle, &timeout);

	string numbers("");
	char symbol = 'z';

	while (isRunning)
	{
		/*if (COMPortReader::vals.size() >= COMPortReader::MAX_QUEUE_SIZE)
			COMPortReader::vals = queue<int>();*/

		DWORD nRead;
		ReadFile(serialHandle, (void*)&symbol, 1, &nRead, NULL);

		if (string("0123456789").find(symbol) == string::npos)
		{
			int finger = atoi(numbers.c_str());

			numbers.clear();

			if (symbol == 'a')
			{
				COMPortReader::vec[0] = finger;
			}
			else if (symbol == 'b')
			{
				COMPortReader::vec[1] = finger;
			}
			else if (symbol == 'c')
			{
				COMPortReader::vec[2] = finger;
			}
			else if (symbol == 'd')
			{
				COMPortReader::vec[3] = finger;
			}
		}
		else
		{
			numbers += symbol;
		}

		COMPortReader::val = atoi(&symbol);

		//if (string("0123456789").find(buff) == string::npos)
		//{
		//	//COMPortReader::val = atoi(numbers.c_str);
		//	COMPortReader::vals.push(7);
		//	numbers = "";
		//	COMPortReader::val = 9;
		//}
		//else
		//	COMPortReader::val = 500;
		//	//COMPortReader::vals.push(atoi(&buff));

		//COMPortReader::vals.push(state1);
	}

	CloseHandle(serialHandle);
}