#pragma once

#include <thread>
#include <future>
#include <vector>

class COMPortReader
{
private:

	static int portNum;

	static std::thread readerThread;

	static bool isRunning;

	static const int MAX_QUEUE_SIZE = 1000;
	
	static int val;
	static int state1;

	static std::vector<int> vec;

public:

	static void Run();
	static void Stop();
	static bool IsRunning()
	{
		return COMPortReader::isRunning;
	}

	static void SetPortNum(int num)
	{
		COMPortReader::portNum = num;
	}

	static int GetPortNum()
	{
		return COMPortReader::portNum;
	}

	static int GetVal()
	{
		if (state1 >= 4)
			state1 = 1;
		else
			state1++;

		//return state1;

		//int res = vals.front();
		//vals.pop();
		return val;
	}

	static std::vector<int> GetVec()
	{
		return vec;
	}

private:

	static void ThreadMethod();
};