#include "PortReaderCommand.h"
#include "COMPortReader.h"

#include <string>
#include <vector>

#include <maya/MArgList.h>
#include <maya/MAnimControl.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MCommandResult.h>
#include <maya/MString.h>
#include <maya/MTimerMessage.h>
#include <maya/MMessage.h>
#include <maya/MCommandResult.h>
#include <maya/MTimeArray.h>
#include <maya/MDoubleArray.h>

std::string PortReaderCommand::nodeName("COM_reader1");
MCallbackId PortReaderCommand::callbackId = 1;
bool PortReaderCommand::isCallbackSet = false;
MTime PortReaderCommand::prevTime;

std::vector<int> PortReaderCommand::f1Vec;
std::vector<int> PortReaderCommand::f2Vec;
std::vector<int> PortReaderCommand::f3Vec;
std::vector<int> PortReaderCommand::f4Vec;

using namespace std;

void PortReaderCommand::Unload()
{
	//RemoveTimeChangedCallback();
	COMPortReader::Stop();
}

MStatus	PortReaderCommand::doIt(const MArgList& args)
{
	MStatus stat;

	try
	{
		string str1 = (string("objExists ") + PortReaderCommand::nodeName);
		MCommandResult resCmd;
		stat = MGlobal::executeCommand(str1.c_str(), resCmd);

		bool f = stat;
		int ff = resCmd.stringResult().asInt();

		if (!ff)
		{
			MGlobal::executeCommand(string("createNode \"network\" -n \"" + PortReaderCommand::nodeName + "\"").c_str());
			MGlobal::executeCommand(string("select \"" + PortReaderCommand::nodeName + "\"").c_str());
			MGlobal::executeCommand("addAttr -longName val1 -attributeType long -dv 0");
			MGlobal::executeCommand("addAttr -longName val2 -attributeType long -dv 0");
			MGlobal::executeCommand("addAttr -longName val3 -attributeType long -dv 0");
			MGlobal::executeCommand("addAttr -longName val4 -attributeType long -dv 0");

			MGlobal::executeCommand(string("createNode \"network\" -n \"" + string("trueFalse") + "\"").c_str());
			MGlobal::executeCommand(string("select \"" + string("trueFalse") + "\"").c_str());
			MGlobal::executeCommand("addAttr -longName val1 -attributeType bool -dv 1");
		}

		if (args.length() == 1)
		{
			int comPortNum = 3;

			int res = args.asInt(0, &stat);
			if (MS::kSuccess == stat) {
				comPortNum = res;
			}

			COMPortReader::SetPortNum(comPortNum);
		}

		if (!COMPortReader::IsRunning())
		{
			//PortReaderCommand::AddTimeChangedCallback();
			callbackId = MTimerMessage::addTimerCallback(0.02, PortReaderCommand::ScTimeChangedCallback, nullptr, &stat);
			COMPortReader::Run();
			MGlobal::displayInfo("COMPortReader::Run()");
		}
		else
		{
			//PortReaderCommand::RemoveTimeChangedCallback();
			COMPortReader::Stop();
			MGlobal::displayInfo("COMPortReader::Stop()");
		}

	}
	catch (std::exception e) {}

	return stat;
}

void PortReaderCommand::ScTimeChangedCallback(float elapsedTime, float lastTime, void*)
{
	//MTime currentTime = MAnimControl::currentTime();

	//if (PortReaderCommand::prevTime == currentTime)
	//	return;

	//PortReaderCommand::prevTime = currentTime;

	//MGlobal::displayInfo((string("elapsed: ") + to_string(elapsedTime) + "    lastTime: " + to_string(lastTime)).c_str());

	vector<int> fingers = COMPortReader::GetVec();

	MGlobal::executeCommand(string("setAttr " + PortReaderCommand::nodeName + ".val1 " + to_string(fingers[0])).c_str());
	MGlobal::executeCommand(string("setAttr " + PortReaderCommand::nodeName + ".val2 " + to_string(fingers[1])).c_str());
	MGlobal::executeCommand(string("setAttr " + PortReaderCommand::nodeName + ".val3 " + to_string(fingers[2])).c_str());
	MGlobal::executeCommand(string("setAttr " + PortReaderCommand::nodeName + ".val4 " + to_string(fingers[3])).c_str());

	//MGlobal::displayInfo((string("elapsed: ") + to_string(elapsedTime) + "    lastTime: " + to_string(lastTime)).c_str());

	MCommandResult res;
	MGlobal::executeCommand(string("getAttr trueFalse.val1").c_str(), res);
	MString str1 = res.stringResult();
	bool f = (int)str1.asInt();

	if (f)
	{
		PortReaderCommand::f1Vec.push_back(fingers[0]);
		PortReaderCommand::f2Vec.push_back(fingers[1]);
		PortReaderCommand::f3Vec.push_back(fingers[2]);
		PortReaderCommand::f4Vec.push_back(fingers[3]);
	}
	else
	{
		MFnAnimCurve f1Curve = MFnAnimCurve().create(MFnAnimCurve::kAnimCurveTU, nullptr);
		MFnAnimCurve f2Curve = MFnAnimCurve().create(MFnAnimCurve::kAnimCurveTU, nullptr);
		MFnAnimCurve f3Curve = MFnAnimCurve().create(MFnAnimCurve::kAnimCurveTU, nullptr);
		MFnAnimCurve f4Curve = MFnAnimCurve().create(MFnAnimCurve::kAnimCurveTU, nullptr);

		MTimeArray timeArr1 = MTimeArray((int)f1Vec.size(), MTime());
		MTimeArray timeArr2 = MTimeArray((int)f2Vec.size(), MTime());
		MTimeArray timeArr3 = MTimeArray((int)f3Vec.size(), MTime());
		MTimeArray timeArr4 = MTimeArray((int)f4Vec.size(), MTime());

		MDoubleArray peaksArr1 = MDoubleArray(timeArr1.length());
		MDoubleArray peaksArr2 = MDoubleArray(timeArr2.length());
		MDoubleArray peaksArr3 = MDoubleArray(timeArr3.length());
		MDoubleArray peaksArr4 = MDoubleArray(timeArr4.length());

		int peaksInd1 = 0;

		MTime time1;
		time1.setValue(0);

		for (auto& el : f1Vec)
		{
			timeArr1[peaksInd1] = time1;
			peaksArr1[peaksInd1] = el;

			peaksInd1++;
			time1++;
		}

		int peaksInd2 = 0;
		time1.setValue(0);

		for (auto& el : f2Vec)
		{
			timeArr2[peaksInd2] = time1;
			peaksArr2[peaksInd2] = el;

			peaksInd2++;
			time1++;
		}

		int peaksInd3 = 0;
		time1.setValue(0);

		for (auto& el : f3Vec)
		{
			timeArr3[peaksInd3] = time1;
			peaksArr3[peaksInd3] = el;

			peaksInd3++;
			time1++;
		}

		int peaksInd4 = 0;
		time1.setValue(0);

		for (auto& el : f4Vec)
		{
			timeArr4[peaksInd4] = time1;
			peaksArr4[peaksInd4] = el;

			peaksInd4++;
			time1++;
		}
		

		f1Curve.addKeys(&timeArr1, &peaksArr1);
		f2Curve.addKeys(&timeArr2, &peaksArr2);
		f3Curve.addKeys(&timeArr3, &peaksArr3);
		f4Curve.addKeys(&timeArr4, &peaksArr4);

		MTimerMessage::removeCallback(callbackId);
	}
}

void PortReaderCommand::TimeChangedCallback(void*)
{
	MTime currentTime = MAnimControl::currentTime();

	if (PortReaderCommand::prevTime == currentTime)
		return;

	PortReaderCommand::prevTime = currentTime;

	vector<int> fingers = COMPortReader::GetVec();

	MGlobal::executeCommand(string("setAttr " + PortReaderCommand::nodeName + ".val1 " + to_string(fingers[0])).c_str());
	MGlobal::executeCommand(string("setAttr " + PortReaderCommand::nodeName + ".val2 " + to_string(fingers[1])).c_str());
	MGlobal::executeCommand(string("setAttr " + PortReaderCommand::nodeName + ".val3 " + to_string(fingers[2])).c_str());
	MGlobal::executeCommand(string("setAttr " + PortReaderCommand::nodeName + ".val4 " + to_string(fingers[3])).c_str());
}

MStatus PortReaderCommand::AddTimeChangedCallback()
{
	MStatus stat = RemoveTimeChangedCallback();

	if (!stat)
		return stat;

	callbackId = MEventMessage::addEventCallback(MString("timeChanged"), TimeChangedCallback, nullptr, &stat);

	if (stat)
		isCallbackSet = true;
	else
	{
		isCallbackSet = false;
		stat.perror("addEventCallback");
	}

	return stat;
}

MStatus PortReaderCommand::RemoveTimeChangedCallback()
{
	if (!isCallbackSet)
		return MStatus::kSuccess;

	MStatus stat = MEventMessage::removeCallback(PortReaderCommand::callbackId);

	if (stat)
		isCallbackSet = false;
	else
	{
		isCallbackSet = true;
		stat.perror("removeCallback");
	}

	return stat;
}