#pragma once

#include <vector>

#include <maya/MGlobal.h>
#include <maya/MPxCommand.h>
#include <maya/MObject.h>
#include <maya/MEventMessage.h>
#include <maya/MTime.h>

class PortReaderCommand : public MPxCommand
{
private:

	static MCallbackId callbackId;
	static bool isCallbackSet;
	static MTime prevTime;

	static std::string nodeName;

	static std::vector<int> f1Vec;
	static std::vector<int> f2Vec;
	static std::vector<int> f3Vec;
	static std::vector<int> f4Vec;

public:

	static void* creator()
	{
		return new PortReaderCommand;
	}

	static void Unload();

	MStatus	doIt(const MArgList& args) override;

private:
	static void TimeChangedCallback(void*);
	static MStatus AddTimeChangedCallback();
	static MStatus RemoveTimeChangedCallback();

	static void ScTimeChangedCallback(float elapsedTime, float lastTime, void*);
};