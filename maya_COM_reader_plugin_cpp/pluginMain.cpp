#include <maya/MFnPlugin.h>

#include "PortReaderCommand.h"

MStatus initializePlugin( MObject obj )
{ 
	MStatus status;
	MFnPlugin plugin(obj, "ILYA", "2019", "Any");

	status = plugin.registerCommand("readCOMPort", PortReaderCommand::creator);
	if (!status)
	{
		status.perror("registerCommand");
		return status;
	}

	return status;
}

MStatus uninitializePlugin( MObject obj)
{
	PortReaderCommand::Unload();

	MStatus status;
	MFnPlugin plugin( obj );

	status = plugin.deregisterCommand("readCOMPort");
	if (!status)
	{
		status.perror("registerCommand");
		return status;
	}

	return status;
}