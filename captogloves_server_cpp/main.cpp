#include <vector>

#include "captos.h"
#include "server.h"

using namespace std;

int main()
{
	server serv;
	serv.run();

	Capto::CaptosManager captos;
	captos.start();

	if (captos.isFingers1Updated())
	{
		vector<float> fingersVec1 = captos.getFingersVec1();
		vector<float> vec1;
		vec1.reserve(12);
		vec1.push_back(-1.f);
		vec1.insert(++vec1.begin(), fingersVec1.begin(), fingersVec1.end());
		vec1.push_back(-2.f);

		serv.send_message(vec1);

		cout << "sending glove 1 data" << endl;

		cout << "vec1......." << endl;
		for (auto el : vec1)
			cout << el << "    ";
		cout << endl;
	}

	if (captos.isFingers2Updated())
	{
		vector<float> fingersVec2 = captos.getFingersVec2();
		vector<float> vec2;
		vec2.reserve(12);
		vec2.push_back(-3.f);
		vec2.insert(++vec2.begin(), fingersVec2.begin(), fingersVec2.end());
		vec2.push_back(-6.f);

		serv.send_message(vec2);

		cout << "sending glove 2 data" << endl;

		cout << "vec2......." << endl;
		for (auto el : vec2)
			cout << el << "    ";
		cout << endl;
	}

	while (true)
	{
		if (captos.isFingers1Updated())
		{
			vector<float> fingersVec1 = captos.getFingersVec1();
			vector<float> vec1;
			vec1.reserve(12);
			vec1.push_back(-1.f);
			vec1.insert(++vec1.begin(), fingersVec1.begin(), fingersVec1.end());
			vec1.push_back(-2.f);

			serv.send_message(vec1);

			cout << "vec1......." << endl;
			for (auto el : vec1)
				cout << el << "    ";
			cout << endl;
		}

		if (captos.isFingers2Updated())
		{
			vector<float> fingersVec2 = captos.getFingersVec2();
			vector<float> vec2;
			vec2.reserve(12);
			vec2.push_back(-3.f);
			vec2.insert(++vec2.begin(), fingersVec2.begin(), fingersVec2.end());
			vec2.push_back(-6.f);

			serv.send_message(vec2);

			cout << "vec2......." << endl;
			for (auto el : vec2)
				cout << el << "    ";
			cout << endl;
		}
	}

	return 0;
}