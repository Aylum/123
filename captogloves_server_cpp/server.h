#pragma once

#include <iostream>
#include <queue>
#include <vector>
#include <list>
#include <thread>
#include <mutex>
#include <utility>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

using boost::asio::ip::tcp;

class session
	: public std::enable_shared_from_this<session>
{
private:
	tcp::socket m_socket;

	std::queue<std::vector<float>> m_message_queue;
	std::recursive_mutex m_message_queue_mutex;
	std::vector<float> m_placeholder_message_vec;

	bool m_is_session_active;

	static const int M_READ_DATA_MAX_LENGTH = 1024;
	char m_read_data[M_READ_DATA_MAX_LENGTH];
	enum { read_dada_max_length = M_READ_DATA_MAX_LENGTH };

public:
	session(tcp::socket socket)
		: m_socket(std::move(socket)),
		m_is_session_active(true)
	{
	}

	~session()
	{
		m_socket.close();
		//std::cout << "session: Destructor!" << std::endl;
	}

	bool is_session_active()
	{
		return m_is_session_active;
	}

	void write()
	{
		bool is_mutex_locked = false;
		
		try
		{
			int queue_size = 0;
			do
			{
				m_message_queue_mutex.lock();
				is_mutex_locked = true;

				queue_size = m_message_queue.size();

				m_message_queue_mutex.unlock();
				is_mutex_locked = false;
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
			} while (queue_size < 1);

			m_message_queue_mutex.lock();
			is_mutex_locked = true;

			auto tempVecPrt = std::make_shared<std::vector<float>>(std::vector<float>(m_message_queue.front()));
			m_message_queue.pop();

			m_message_queue_mutex.unlock();
			is_mutex_locked = false;


			auto self(shared_from_this());
			boost::asio::async_write(m_socket, boost::asio::buffer(*tempVecPrt),
				[this, self, tempVecPrt](boost::system::error_code ec, std::size_t /*length*/)
			{
				if (ec.value() == 0)
					write();
				else
					m_is_session_active = false;
			});
		}
		catch (const std::exception& e)
		{
			if (is_mutex_locked)
			{
				m_message_queue_mutex.unlock();
				is_mutex_locked = false;
			}

			std::cout << std::endl << std::endl << "EXCEPTION SERVER::WRITE()" << e.what() << std::endl << std::endl;
		}
	}

	void read()
	{
		try
		{
			auto self(shared_from_this());
			m_socket.async_read_some(boost::asio::buffer(m_read_data, M_READ_DATA_MAX_LENGTH),
				[this, self](boost::system::error_code ec, std::size_t length)
			{
				if (ec.value() == 0)
				{
					if ((std::string)m_read_data != "")
						//std::cout << "Received: " << (std::string)m_read_data << std::endl;

						read();
				}
			});
		}
		catch (const std::exception&) {}
	}

	bool send_message(const std::vector<float> message)
	{
		bool res = false;
		bool is_mutex_locked = false;
		try
		{
			if (m_message_queue_mutex.try_lock())
			{
				is_mutex_locked = true;

				if (m_message_queue.size() < 1000)
				{
					m_message_queue.push(message);
					res = true;
				}

				m_message_queue_mutex.unlock();
				is_mutex_locked = false;
			}

		}
		catch (const std::exception& e)
		{
			if (is_mutex_locked)
			{
				m_message_queue_mutex.unlock();
				is_mutex_locked = false;
			}
			std::cout << std::endl << std::endl << "EXCEPTION SEND_MESSAGE: " << e.what() << std::endl << std::endl;
		}

		return res;
	}

};

class server
{
private:
	boost::asio::io_context m_io_context;

	tcp::acceptor m_acceptor_write;
	tcp::acceptor m_acceptor_read;
	static const int m_port_write = 4374;
	static const int m_port_read = 4367;

	std::list<std::shared_ptr<session>> m_sessions;
	std::queue<std::vector<float>> m_message_queue;

public:

	server()
		: m_io_context(),
		m_acceptor_write(m_io_context, tcp::endpoint(tcp::v4(), m_port_write)),
		m_acceptor_read(m_io_context, tcp::endpoint(tcp::v4(), m_port_read))
	{
		do_accept_write();
		do_accept_read();
	}

	//server(boost::asio::io_context& io_context)
	//	: m_acceptor_write(io_context, tcp::endpoint(tcp::v4(), m_port_write)),
	//	m_acceptor_read(io_context, tcp::endpoint(tcp::v4(), m_port_read))
	//{
	//	do_accept_write();
	//	do_accept_read();
	//}

	void run()
	{
		try
		{
			std::shared_ptr<boost::asio::io_service::work> work(new boost::asio::io_service::work(m_io_context));
			std::thread(boost::bind(&boost::asio::io_context::run, &m_io_context)).detach();
		}
		catch (std::exception& e)
		{
			std::cerr << "server: Exception: " << e.what() << "\n";
		}
	}

	void send_message(const std::vector<float>& message)
	{
		try
		{
			if (m_message_queue.size() < 1000)
				m_message_queue.push(message);

			auto iter = m_sessions.begin();
			while (iter != m_sessions.end())
			{
				if (!(*iter)->is_session_active())
				{
					iter = m_sessions.erase(iter);
				}
				else
				{
					//(*iter)->send_message(message);
					iter++;
				}
			}

			bool is_message_sent = false;

			do
			{
				if (m_message_queue.size() < 1)
					break;

				std::vector<float> message_to_send = m_message_queue.front();

				for (auto sess : m_sessions)
				{
					if (sess->send_message(message_to_send))
					{
						is_message_sent = true;
					}

				}

				if (is_message_sent)
					m_message_queue.pop();

			} while (is_message_sent);

		}
		catch (const std::exception& e){ std::cout << std::endl << std::endl << "EXCEPTION SERVER_SEND_MESSAGE: " << e.what() << std::endl << std::endl; }
	}

private:

	void do_accept_write()
	{
		m_acceptor_write.async_accept(
			[this](boost::system::error_code ec, tcp::socket socket)
		{
			if (ec.value() == 0)
			{
				auto temp = std::make_shared<session>(std::move(socket));
				m_sessions.push_back(temp);
				temp->write();
				//std::thread(boost::bind(&session::write, temp)).detach();

				std::cout << "server: writing..." << std::endl;
			}

			do_accept_write();
		});
	}

	void do_accept_read()
	{
		m_acceptor_read.async_accept(
			[this](boost::system::error_code ec, tcp::socket socket)
		{
			if (ec.value() == 0)
			{
				std::thread(boost::bind(&session::read, std::make_shared<session>(std::move(socket)))).detach();
				std::cout << "server: reading..." << std::endl;
			}

			do_accept_read();
		});
	}

};