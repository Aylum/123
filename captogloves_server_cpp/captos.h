#pragma once

#include <iostream>
#include <thread>
#include <iostream>
#include <functional>
#include <mutex>
#include <vector>

#include "GSdkCore.h"

namespace Capto
{
	using namespace GSdkBase;
	using namespace GSdkBoard;
	using namespace GSdkBoardTools;
	using namespace GSdkPeripheral;

	class CaptosManager
	{
	private:
		bool isPeriphChangedEventOccured = false;
		PeripheralCentralPtr m_boardCentral;
		std::vector<PeripheralBasePtr> m_periphs;
		StreamTimeslots m_streamTimeslots;
		int m_streamId1 = -1;
		int m_streamId2 = -1;

		//std::thread m_periph1Thread;
		//std::thread m_periph2Thread;
		std::mutex m_streamMutex;

		static const int FINGERS_ARR_LENGTH = 10;
		std::vector<float> m_fingersVec1 = std::vector<float>(FINGERS_ARR_LENGTH, 0);
		std::vector<float> m_fingersVec2 = std::vector<float>(FINGERS_ARR_LENGTH, 0);

		bool m_isFingers1Updated = false;
		bool m_isFingers2Updated = false;

		std::mutex m_vecMutex1;
		std::mutex m_vecMutex2;

	public:

		/*CaptosManager()
		{
		}*/

		~CaptosManager()
		{
			this->stop();
		}

		int getA() { return 5; }

		std::vector<float> getFingersVec1()
		{
			m_vecMutex1.lock();
			std::vector<float> temp = std::vector<float>(m_fingersVec1.begin(), m_fingersVec1.end());
			m_isFingers1Updated = false;
			m_vecMutex1.unlock();

			return temp;
		}

		std::vector<float> getFingersVec2()
		{
			m_vecMutex2.lock();
			std::vector<float> temp = std::vector<float>(m_fingersVec2.begin(), m_fingersVec2.end());
			m_isFingers2Updated = false;
			m_vecMutex2.unlock();

			return temp;
		}

		bool isFingers1Updated()
		{
			return m_isFingers1Updated;
		}

		bool isFingers2Updated()
		{
			return m_isFingers2Updated;
		}

		void start()
		{
			using namespace std;

			auto factory = BoardFactory();
			m_boardCentral = factory.makeBoardCentral();
			m_boardCentral->scanChanged().connect([](const ScanEventArgs& args){ cout << "Captos: Central status changed: " + std::to_string((int)(args.scan())) << endl << endl; });
			m_boardCentral->peripheralsChanged().connect([this](const PeripheralsEventArgs& args){ onPeripheralsChanged(args); });

			m_boardCentral->startScan(GSdkScanOptionsMake(5));

			auto discoveredPeriphs = m_boardCentral->peripherals();

			while (!isPeriphChangedEventOccured && discoveredPeriphs.size() < 1)
				discoveredPeriphs = m_boardCentral->peripherals();

			isPeriphChangedEventOccured = false;

			for (auto periph : discoveredPeriphs)
				if (periph)
					m_periphs.push_back(periph);

			m_streamTimeslots = StreamTimeslots();
			m_streamTimeslots.sensorsState = 6;

			if (m_periphs.size() > 0)
			{
				//m_periph1Thread = move(thread([this](){ periphMethod(1); }));
				thread([this](){ periphMethod(1); }).detach();
			}


			if (m_periphs.size() > 1)
			{
				//m_periph2Thread = move(thread([this](){ periphMethod(2); }));
				thread([this](){ periphMethod(2); }).detach();
			}

			/*m_periph1Thread.join();
			m_periph2Thread.join();*/
		}

		void stop()
		{
			m_boardCentral->stopScan();
			unsubscribe();

			for (auto el : m_periphs)
				el->stop();
		}

	private:
		inline void periphMethod(int whichPeriph)
		{
			using namespace std;

			std::string threadName;
			string periphName;
			PeripheralBasePtr periph;
			int* streamId;
			function<void(const BoardStreamEventArgs&)> onPeripheralStreamReceived;

			if (whichPeriph == 1)
			{
				threadName = "Thread1";
				periphName = "periph1";
				periph = m_periphs[0];
				streamId = &m_streamId1;
				onPeripheralStreamReceived = [this](const BoardStreamEventArgs& args){ onPeripheral1StreamReceived(args); };
			}
			else if (whichPeriph == 2)
			{
				threadName = "Thread2";
				periphName = "periph2";
				periph = m_periphs[1];
				streamId = &m_streamId2;
				onPeripheralStreamReceived = [this](const BoardStreamEventArgs& args){ onPeripheral2StreamReceived(args); };
			}
			else
				return;

			auto boardPeriph = std::dynamic_pointer_cast<BoardPeripheral>(periph);
			m_streamMutex.lock();
			cout << "Captos: " << threadName << ": Trying to connect " << periphName << endl;
			cout << "Captos: " << threadName << ": ID: " << boardPeriph->id() << endl;
			cout << "Captos: " << threadName << ": Name: " << boardPeriph->name() << endl;
			m_streamMutex.unlock();

			bool didSubscribedToEvent = false;

			if (boardPeriph->start())
			{
				m_streamMutex.lock();
				cout << "Captos: " << threadName << ": Status: " << std::to_string(boardPeriph->status()) << endl;
				m_streamMutex.unlock();
			}
			else
			{
				m_streamMutex.lock();
				cout << "Captos: " << threadName << ": Status: " << std::to_string(boardPeriph->status()) << endl;
				m_streamMutex.unlock();
			}

			if (boardPeriph->streamTimeslotsWrite(m_streamTimeslots))
			{
				*streamId = boardPeriph->streamReceived().connect(onPeripheralStreamReceived);
				didSubscribedToEvent = true;
			}
			else
			{
				m_streamMutex.lock();
				cout << "Captos: " << threadName << ": Unable to set stream timeslots for " << periphName << endl;
				m_streamMutex.unlock();
				if (didSubscribedToEvent)
				{
					didSubscribedToEvent = false;
					boardPeriph->streamReceived().disconnect(*streamId);
				}

				boardPeriph->rebootBoard();
				while (!boardPeriph->status());
			}

			while (true)
			{
				if (!boardPeriph->streamTimeslotsRead() || !boardPeriph->status())
				{
					m_streamMutex.lock();
					cout << "Captos: " << threadName << ": peripheral disconnected, trying to reconnect" << endl;
					m_streamMutex.unlock();
					//boardPeriph->stop();
					if (didSubscribedToEvent)
					{
						didSubscribedToEvent = false;
						boardPeriph->streamReceived().disconnect(*streamId);
					}

					boardPeriph->rebootBoard();
					while (!boardPeriph->start());

					if (boardPeriph->status())
					{
						m_streamMutex.lock();
						cout << "Captos: " << threadName << ": peripheral connected" << endl;
						m_streamMutex.unlock();

						if (boardPeriph->streamTimeslotsWrite(m_streamTimeslots))
						{
							*streamId = boardPeriph->streamReceived().connect(onPeripheralStreamReceived);
							didSubscribedToEvent = true;
						}
						else
						{
							m_streamMutex.lock();
							cout << "Captos: " << threadName << ": Unable to set stream timeslots for " << periphName << endl;
							m_streamMutex.unlock();
						}
					}
					else
					{
						m_streamMutex.lock();
						cout << "Captos: " << threadName << ": connection attempt failed" << endl;
						m_streamMutex.unlock();
					}
				}
			}
		}

		inline void onPeripheralsChanged(const PeripheralsEventArgs& args)
		{
			std::cout << "Captos: peripheralsChangedEvent" << std::endl;
			isPeriphChangedEventOccured = true;
		}

		inline void onPeripheral1StreamReceived(const BoardStreamEventArgs& args)
		{
			if (args.streamType == BoardStreamTypeSensorsState)
			{
				auto sensorsArgs = static_cast<const BoardFloatSequenceEventArgs &>(args);

				auto temp = sensorsArgs.value();
				m_vecMutex1.lock();
				m_isFingers1Updated = true;
				m_fingersVec1.assign(temp.begin(), temp.end());
				m_vecMutex1.unlock();
				//std::cout << "Received sensors from " << m_periphs[0]->name() << ": " << to_string(m_fingersVec1);
				//auto value = sensorsArgs.value();
				//m_streamMutex.lock();
				//std::cout << "Received sensors from " << m_periphs[0]->name() << ": " << to_string(value);
				//m_streamMutex.unlock();
			}
		}

		inline void onPeripheral2StreamReceived(const BoardStreamEventArgs& args)
		{
			if (args.streamType == BoardStreamTypeSensorsState)
			{
				auto sensorsArgs = static_cast<const BoardFloatSequenceEventArgs &>(args);

				auto temp = sensorsArgs.value();
				m_vecMutex2.lock();
				m_isFingers2Updated = true;
				m_fingersVec2.assign(temp.begin(), temp.end());
				m_vecMutex2.unlock();

				//m_fingersVec2 = sensorsArgs.value();
				//std::cout << "Received sensors from " << m_periphs[1]->name() << ": " << to_string(m_fingersVec2);

				//auto value = sensorsArgs.value();
				//m_streamMutex.lock();
				//std::cout << "Received sensors from " << m_periphs[1]->name() << ": " << to_string(value);
				//m_streamMutex.unlock();
			}
		}

		void unsubscribe()
		{
			if (m_periphs.size() > 0 && m_periphs[0])
				std::dynamic_pointer_cast<BoardPeripheral>(m_periphs[0])->streamReceived().disconnect(m_streamId1);

			if (m_periphs.size() > 1 && m_periphs[1])
				std::dynamic_pointer_cast<BoardPeripheral>(m_periphs[1])->streamReceived().disconnect(m_streamId2);
		}

		static std::string to_string(const std::vector<float> &value)
		{
			using namespace std;

			stringstream ss;
			for (auto item : value) {
				ss << item;
				ss << ",    ";
			}
			ss << endl;
			return ss.str();
		}
	};
}